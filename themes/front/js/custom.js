$(document).ready(function(){
	$(".stars :radio").change(function(){
		var me = $(this);
		$.ajax({
			url: me.parent().attr("data-href"),
			type: "GET",
			data: {rating:me.val()},
			beforeSend: function() {
				
			},
			success: function(response) {
				
			},
			error: function(a,n,err){
				
			},
			complete: function(response) {
				console.log(response);
			}
		});
	});

	$(".demo1").bootstrapNews({
		newsPerPage: 1,
		autoplay: true,
		pauseOnHover: true,
		direction: 'up',
		newsTickerInterval: 4000,
		onToDo: function () {
			//console.log(this);
		}
	});


	$('.button-checkbox').each(function () {

		// Settings
		var $widget = $(this),
				$button = $widget.find('button'),
				$checkbox = $widget.find('input:checkbox'),
				color = $button.data('color'),
				settings = {
					on: {
						icon: 'glyphicon glyphicon-check'
					},
					off: {
						icon: 'glyphicon glyphicon-unchecked'
					}
				};

		// Event Handlers
		$button.on('click', function () {
			$checkbox.prop('checked', !$checkbox.is(':checked'));
			$checkbox.triggerHandler('change');
			updateDisplay();
		});
		$checkbox.on('change', function () {
			updateDisplay();
		});

		// Actions
		function updateDisplay() {
			var isChecked = $checkbox.is(':checked');

			// Set the button's state
			$button.data('state', (isChecked) ? "on" : "off");

			// Set the button's icon
			$button.find('.state-icon')
					.removeClass()
					.addClass('state-icon ' + settings[$button.data('state')].icon);

			// Update the button's color
			if (isChecked) {
				$button
						.removeClass('btn-default')
						.addClass('btn-' + color + ' active');
			} else {
				$button
						.removeClass('btn-' + color + ' active')
						.addClass('btn-default');
			}
		}

		// Initialization
		function init() {

			updateDisplay();

			// Inject the icon if applicable
			if ($button.find('.state-icon').length == 0) {
				$button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
			}
		}
		init();
	});

	$('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-nav'
	});
	$('.slider-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		arrows: false,
	});

	$(".advanced-search").click(function(e){
		e.preventDefault();
		$("#filterDropDown").toggle();
		if (typeof initSearchMap != "undefined") initSearchMap();
	});
	$("#btn-close-filter").click(function(e){
		e.preventDefault();
		$("#filterDropDown").hide();
	});
	
	$("a.void_url").click(function(e){
		e.preventDefault();
	});
});
