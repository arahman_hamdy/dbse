(function($) { "use strict";
	
	// --------------------------------------------------
    // owlCarousel
    // --------------------------------------------------

    /* latest-projects-items */
    $("#latest-projects-items").owlCarousel({
        items: 3,
        itemsCustom : false,
        itemsDesktop : [1199, 3],
        itemsDesktopSmall : [979, 2],
        itemsTablet : [768, 2],
        itemsTabletSmall : false,
        itemsMobile : [479, 1],
        navigation: true,
        pagination: false,
        navigationText: [
          "<i class='fa fa-angle-left'></i>",
          "<i class='fa fa-angle-right'></i>"
        ],
    });

    /* latest-news-items */
    $(".latest-news-items").owlCarousel({
        items : 1,
        singleItem:true,    
        navigation : false,
        pagination : false,
        autoPlay : false,
        navigationText: [
          "<i class='fa fa-angle-left'></i>",
          "<i class='fa fa-angle-right'></i>"
        ],
        slideSpeed : 400,
    });    

    // Custom Navigation owlCarousel - ".latest-news-items"
    $(".latest-next").on("click", function() {
        $(this).parent().parent().find('.latest-news-items').trigger('owl.next');
    });
    $(".latest-prev").on("click", function() {
        $(this).parent().parent().find('.latest-news-items').trigger('owl.prev');
    });  

    /*testimonials-slider */
    $(".testimonials-slider").owlCarousel({
        items : 1,
        singleItem:true,    
        navigation : false,
        pagination : false,
        autoPlay : false,
        slideSpeed : 400,
    }); 

    // Custom Navigation owlCarousel - ".testimonials-slider"
    $(".testi-next").on("click", function() {
        $(this).parent().parent().find('.testimonials-slider').trigger('owl.next');
    });
    $(".testi-prev").on("click", function() {
        $(this).parent().parent().find('.testimonials-slider').trigger('owl.prev');
    });

	$('#filterDropDown').on({
	    "click":function(e){
	      e.stopPropagation();
	    }
	});
    var isRequestingRegions = false;
    jQuery("#filterDropDown #PlaceFilter_city, #filterDropDown #SchoolFilter_city").change(function(){
        if (isRequestingRegions) return;
        jQuery("#PlaceFilter_city, #SchoolFilter_city").attr("disabled","disabled");
        jQuery("#PlaceFilter_region_id, #SchoolFilter_region_id").attr("disabled","disabled");

        jQuery.ajax({
                url: baseUrl + "/site/getCityRegions/id/" + jQuery("#PlaceFilter_city, #SchoolFilter_city").val(),
                type: "get",
                dataType: "json",
                success: function (response) {
                    jQuery("#PlaceFilter_region_id option.generated, #SchoolFilter_region_id option.generated").remove();
                    for(var i in response){
                        var option = jQuery("<option>").addClass("generated").val(i).html(response[i]);
                        jQuery("#PlaceFilter_region_id, #SchoolFilter_region_id").append(option);
                    }
                },
                complete: function(jqXHR, textStatus, errorThrown) {
                    jQuery("#PlaceFilter_city, #SchoolFilter_city").removeAttr("disabled");
                    jQuery("#PlaceFilter_region_id, #SchoolFilter_region_id").removeAttr("disabled");
                }


            });
    });
    jQuery("#PlaceFilter_city, #SchoolFilter_city").trigger("change");

    $(".search-btn").click(function(){
        $("#filterDropDown input, #filterDropDown select").attr('disabled','disabled');
    });
})(jQuery); 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 





	