<?php
include'header.php';
?>


        <!-- content begin -->
        <div id="content" class="no-padding"> 

            <!-- section begin -->
            <section id="section-contact">                
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="text-left">
                                <h4>Nice to Hear From You</h4>
                                <p>Suspendisse ut interdum lectus. Integer ac neque faucibus, venenatis nisl quis, pellentesque lacus. Integer et ipsum sit amet dui ultrices hendrerit. Pellentesque leo massa, suscipit a felis non, posuere convallis velit. Sed tincidunt lacinia velit et aliquam.</p>
                            </div>
                            <form action="" class="wpcf7-form">
                                <div class="col-one-third">
                                    <input type="text" placeholder="Your Name">
                                </div>
                                <div class="col-one-third margin-one-third">
                                    <input type="email" placeholder="Your Email">
                                </div>
                                <div class="col-one-third">
                                    <input type="text" placeholder="Your Phone">
                                </div>
                                <div class="col-full"><textarea placeholder="Your Message"></textarea></div>
                                <div class="clearfix"></div>
                                <div class="text-center">
                                    <div class="divider-single"></div>
                                    <button class="btn btn-primary btn-big">Send Email</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <h4>GENERAL INFO</h4>
                            <ul class="contact-list">
                                <li><i class="fa fa-location-arrow"></i> 121 King Street, Melbourne Victoria 3000 Australia</li>
                                <li><i class="fa fa-phone"></i> +61 3 8376 6284</li>
                                <li><i class="fa fa-envelope"></i> <a href="contact@compact.com">contact@compact.com</a></li>
                                <li><i class="fa fa-globe"></i> <a href="www.compact.com">www.compact.com</a></li>
                            </ul>
                            <h4>WORKING HOURS</h4>
                            <ul class="contact-list">
                                <p>Our support Hotline is available 24 Hours a day: +61 3 8376 6284</p>
                                <li><i class="fa fa-clock-o"></i> Monday–Friday: 8am to 6pm</li>
                                <li><i class="fa fa-clock-o"></i> Saturday: 10am to 2pm</li>
                                <li><i class="fa fa-times-circle"></i> Sunday: Closed</li>
                            </ul>
                        </div>
                    </div>
                </div>      
            </section>
            <!-- section close -->            

            <!-- section gmap begin -->
            <section id="section-gmap" class="no-padding">                
                <div id="map-canvas" class="map-canvas"></div>       
            </section>
            <!-- section gmap close -->

        </div>
        <!-- content close -->





<?php
include'footer.php';

?>