<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

        <!-- content begin -->
        <div id="content" class="no-padding"> 

            <!-- section begin -->
            <section id="section-404" class="no-padding">                
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-1 col-sm-7">
                            <div class="content-404 intro-text margin-top-170">
                                <h2>Error <?php echo $code; ?></h2>
                                <p><?php echo CHtml::encode($message); ?></p>
                                <div class="divider-single"></div>
                                <a href="<?php echo $this->createUrl("//");?>" class="btn btn-primary">Home page</a>
                            </div>                            
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <img alt="" src="<?php echo $this->themeURL;?>/images/404.png" class="img-responsive margin-top-30">
                        </div>
                    </div>
                </div>      
            </section>
            <!-- section close -->   

        </div>
        <!-- content close -->