<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<?php Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>

		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

		<!-- Respomsive slider -->
		<link href="<?php echo $this->themeURL;?>/css/responsive-calendar.css" rel="stylesheet">

		<!--slick-->
		<link rel="stylesheet" type="text/css" href="<?php echo $this->themeURL;?>/slick/slick.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->themeURL;?>/slick/slick-theme.css"/>


		<!-- news Bar Css -->
		<link href="<?php echo $this->themeURL;?>/css/newsbar.css" rel="stylesheet">


		<!-- Favicons
		================================================== -->
		<link rel="icon" type="image/png" href="<?php echo $this->themeURL;?>/images/favicons/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="<?php echo $this->themeURL;?>/images/favicons/favicon-16x16.png" sizes="16x16" />		

		<!-- LOAD CSS FILES -->
		<link href="<?php echo $this->themeURL;?>/css/jquery.timepicker.css" rel="stylesheet">
		<link href="<?php echo $this->themeURL;?>/style.css?v=56<?php //echo time();?>" rel="stylesheet" type="text/css">  			

		<!-- color scheme -->
		<link rel="stylesheet" href="<?php echo $this->themeURL;?>/switcher/demo.css" type="text/css">
		<link rel="stylesheet" href="<?php echo $this->themeURL;?>/switcher/colors/lime.css" type="text/css" id="colors">  

		<link href='https://fonts.googleapis.com/earlyaccess/droidarabicnaskh.css' rel='stylesheet' type='text/css'/>

		<?php if(Yii::app()->language=='ar'):?>
			<script>var is_rtl=true;</script>
			<link rel="stylesheet" href="//cdn.rawgit.com/morteza/bootstrap-rtl/master/dist/css/bootstrap-rtl.min.css">
			<link rel="stylesheet" href="<?php echo $this->themeURL;?>/rtl.css?v=8" type="text/css"> 
			<link rel="stylesheet" href="<?php echo $this->themeURL;?>/css/override.css?v=4" type="text/css"> 
		<?php else: ?>
			<script>var is_rtl=false;</script>			
		<?php endif;?>

		<script>var baseUrl='<?php echo Yii::app()->baseUrl;?>';</script>			

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NRV7WQQ');</script>
		<!-- End Google Tag Manager -->

		<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyDE6-eQZtDYz7GjXtrczOf_pG48y0a-BmU'></script>

		<meta property="fb:app_id" content="1060011157368075" />

		<meta name="google-site-verification" content="brjLLdSlhQRUmGP4I_oNSbD_79M03zopGn7-1RO5_Jk" />

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '408056332862167'); // Insert your pixel ID here.
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=408056332862167&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->

		<!-- Start Alexa Certify Javascript -->
		<script type="text/javascript">
		_atrk_opts = { atrk_acct:"22nHo1IWx810Io", domain:"dbse.co",dynamic: true};
		(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
		</script>
		<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=22nHo1IWx810Io" style="display:none" height="1" width="1" alt="" /></noscript>
		<!-- End Alexa Certify Javascript -->   

	</head>

	<body>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-89670051-1', 'auto');
		  ga('send', 'pageview');

		</script>

		<div id="fb-root"></div>
		<script>
		  window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '1060011157368075',
		      xfbml      : true,
		      version    : 'v2.7'
		    });
		  };

		  (function(d, s, id){
		     var js, fjs = d.getElementsByTagName(s)[0];
		     if (d.getElementById(id)) {return;}
		     js = d.createElement(s); js.id = id;
		     js.src = "//connect.facebook.net/<?php echo (Yii::app()->language=='ar')?"ar_AR":"en_US";?>/sdk.js";
		     fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
		</script>

		<!-- Preload images start //-->
		<!--   <div class="images-preloader" id="images-preloader">
			   <div class="spinner">
				   <div class="bounce1"></div>
				   <div class="bounce2"></div>
				   <div class="bounce3"></div>
			   </div>
		   </div>
	   
		-->
		<!-- Preload images end //-->

		<div id="wrapper">

			<!-- header begin -->
			<header class="site-header-1 site-header">
				<!-- Main bar start -->
				<div id="sticked-menu" class="main-bar">
					<div class="container">                
						<div class="row">                    
							<div class="col-md-12">

								<!-- logo begin -->
								<div id="logo" class="pull-left">                            
									<a href="<?php echo $this->createUrl('/' . $this->getMainController());?>">
										<img src="<?php echo $this->themeURL;?>/images/logo.png" alt="" class="logo">
									</a>
								</div>
								<!-- logo close -->

								<!-- btn-mobile menu begin -->
								<a id="show-mobile-menu" class="btn-mobile-menu hidden-lg hidden-md"><i class="fa fa-bars"></i></a>
								<!-- btn-mobile menu close -->  

								<!-- mobile menu begin -->
								<nav id="mobile-menu" class="site-mobile-menu hidden-lg hidden-md">
									<ul></ul>
								</nav>  
								<!-- mobile menu close -->                        

								<!-- desktop menu begin -->
								<nav id="desktop-menu" class="site-desktop-menu hidden-xs hidden-sm">

									<?php $this->widget('zii.widgets.CMenu',array(
										'htmlOptions'=>array('class'=>'clearfix'),
										'items'=>array_merge(array(
											array('label'=>Yii::t('app','Mother page'), 'url'=>array('//site/landing')),
											array('label'=>Yii::t('app','Home'), 'url'=>array('//' . $this->getMainController() . '')),
											//array('label'=>Yii::t('app','About'), 'url'=>array('/' . $this->getMainController() . '/about')),
											),$this->menus,array(
											array('label'=>Yii::t('app','Latest Places'), 'url'=>array('/places/latest'),'visible'=>$this->getMainController()=='coworking'),
											array('label'=>Yii::t('app','Blog'), 'url'=>array('/blog')),
											array('label'=>Yii::t('app','Events'), 'url'=>array('/events'),'visible'=>$this->getMainController()=='coworking'),
											array('label'=>Yii::t('app','Offers'), 'url'=>array('/offers'),'visible'=>$this->getMainController()=='coworking'),
											array('label'=>Yii::t('app','Contact'), 'url'=>array('/' . $this->getMainController() . '/contact')),
											array('label'=>Yii::t('app','My Account'), 'url'=>array('/user/account'), 'visible'=>false && !Yii::app()->user->isGuest, 'items'=>array(
												array('label'=>Yii::t('app','My Profile'), 'url'=>array('/user/profile')),
												array('label'=>Yii::t('app','My Favorites'), 'url'=>array('/user/favorites')),

												)),
										)),
									)); ?>
								</nav>
								<!-- desktop menu close -->

								<!-- Header Group Button Right begin -->
								<div class="header-buttons pull-right hidden-xs hidden-sm">
								<div style="" class="social_icon">
									<a target="_blank" href="https://www.facebook.com/DatabaseSE.CO/" class="facebook"></a>
									<a target="_blank" href="https://twitter.com/DbSE_platform" class="twitter"></a>
									<a href="#" class="googleplus"></a>
									<a target="_blank" href="https://www.linkedin.com/company-beta/17976745/" class="linkedin"></a>
								</div>
									<div class="header-contact">

										<?php  $this->widget('zii.widgets.CMenu',array(
											'htmlOptions'=>array('class'=>'clearfix'),
											'items'=>array(
												array('label'=>'Login', 'url'=>array('/user/login'),'linkOptions'=>array(),'visible'=>Yii::app()->user->isGuest),
												array('label'=>Yii::t('app','My Profile'), 'url'=>array('/user/profile'), 'visible'=>!Yii::app()->user->isGuest),
												array('label'=>Yii::t('app','Membership'), 'url'=>array('/membership'), 'visible'=>!Yii::app()->user->isGuest),
												array('label'=>'Logout', 'url'=>array('/' . $this->getMainController() . '/logout'), 'visible'=>!Yii::app()->user->isGuest),
												array('label'=>'Register', 'url'=>array('/user/registration'), 'htmlOptions'=>array("data-toggle"=>"modal","data-target"=>"#login-modal"),'visible'=>Yii::app()->user->isGuest),
												//array('label'=>'|','htmlOptions'=>array("class"=>"border-line"),'visible'=>Yii::app()->user->isGuest),
												array('label'=>'English', 'url'=>"?lang=en",'visible'=>Yii::app()->language=='ar'),
												array('label'=>'العربية', 'url'=>"?lang=ar",'visible'=>Yii::app()->language!='ar','itemOptions'=>array('class'=>"arabic"))
											),
										));  ?>
										<div class="beta-version" style="display:none">بث تجريبي</div>
									</div>

									<!-- Button Modal popup searchbox -->
									<div class="search-button">
										<!-- Trigger the modal with a button -->
										<a href="" class="advanced-search" data-toggle="modal" data-target="#myModal_disabled"><i class="fa fa-search"></i></a>                                                                        
									</div>

	   

									<!-- Button Menu OffCanvas right -->
									<div class="navright-button">
										<a href="" id="btn-offcanvas-menu"><i class="fa fa-bars"></i></a>                                    
									</div> 

								</div>
								<!-- Header Group Button Right close -->

							</div>
						</div>

						<!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis-float addthis_inline_share_toolbox"></div>

					</div>

				</div>

			</header>
			<!-- header close -->
			<div class="gray-line"></div>

			<!-- Modal Search begin -->
			<div id="myModal" class="modal fade" role="dialog">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="modal-dialog myModal-search">
					<!-- Modal content-->
					<div class="modal-content">                                        
						<div class="modal-body">
							<form role="search" method="get" class="search-form" action="<?php echo $this->createUrl($this->getMainController() . "/search");?>">
								<input type="search" class="search-field" placeholder="<?php echo Yii::t('app','Search here...');?>" name="q" value="" title="" />
								<button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal Search close -->

			<!-- Menu OffCanvas right begin -->
			<div class="navright-button hidden-sm">
				<div class="compact-menu-canvas" id="offcanvas-menu">
					<h3><?php echo Yii::t('app','Menu');?></h3><a id="btn-close-canvasmenu"><i class="fa fa-close"></i></a>
					<nav>

						<?php $this->widget('zii.widgets.CMenu',array(
						'htmlOptions'=>array('class'=>'clearfix'),
						'items'=>array_merge(array(
							array('label'=>Yii::t('app','Mother page'), 'url'=>array('//site/landing')),
							array('label'=>Yii::t('app','Home'), 'url'=>array('//' . $this->getMainController() . '')),
							//array('label'=>Yii::t('app','About'), 'url'=>array('/' . $this->getMainController() . '/about')),
							),$this->menus,array(
							array('label'=>Yii::t('app','Latest Places'), 'url'=>array('/places/latest'),'visible'=>$this->getMainController()=='coworking'),
							array('label'=>Yii::t('app','Events'), 'url'=>array('/events'),'visible'=>$this->getMainController()=='coworking'),
							array('label'=>Yii::t('app','Offers'), 'url'=>array('/offers'),'visible'=>$this->getMainController()=='coworking'),
							array('label'=>Yii::t('app','Blog'), 'url'=>array('/blog')),
							array('label'=>Yii::t('app','Contact'), 'url'=>array('/' . $this->getMainController() . '/contact')),
							array('label'=>'Login', 'url'=>array('/user/login'),'linkOptions'=>array(),'visible'=>Yii::app()->user->isGuest),
							array('label'=>Yii::t('app','My Profile'), 'url'=>array('/user/profile'), 'visible'=>!Yii::app()->user->isGuest),
							array('label'=>Yii::t('app','My Bookings'), 'url'=>array('/myBookings'), 'visible'=>!Yii::app()->user->isGuest),
							array('label'=>Yii::t('app','Membership'), 'url'=>array('/membership'), 'visible'=>!Yii::app()->user->isGuest),
							array('label'=>'Logout', 'url'=>array('/' . $this->getMainController() . '/logout'), 'visible'=>!Yii::app()->user->isGuest),
							array('label'=>'Register', 'url'=>array('/user/registration'), 'htmlOptions'=>array("data-toggle"=>"modal","data-target"=>"#login-modal"),'visible'=>Yii::app()->user->isGuest),
						)),
						)); ?>                    
					</nav>
				</div>
			</div> 
			<!-- Menu OffCanvas right close -->

		<?php if(isset($this->breadcrumbs) && !empty($this->breadcrumbs)):?>

        <section id="sub-header-2" class="subheader hidden-lg-block visible-lg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?php echo str_replace(Yii::app()->name . " - ","",$this->pageTitle); ?></h1>
                        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
							'links'=>$this->breadcrumbs,
						)); ?>           
                    </div>
                </div>
            </div>
        </section>

		<!-- breadcrumbs -->
	<?php endif?>

			<?php echo $content; ?>

	<?php 
	switch($this->getMainController()){
		case 'coworking':
			$form = $this->beginWidget('GxActiveForm', array(
			'id' => 'place-search-form-advanced',
			'action' => $this->createUrl("places/search"),
			'method' => 'GET',
			'enableAjaxValidation' => false,
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
				'afterValidate' => 'js:function(form, data, hasError) { 
					if(hasError) {
						for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
						return false;
					}
					else {
						form.children().removeClass("validate-has-error");
						return true;
					}
				}',
				'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
					if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
					else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
				}'
			),
			'htmlOptions'=>array(
				'class'=>'form-horizontal',
			)
		));
		$model = new PlaceFilter();
		if (!empty($_GET['PlaceFilter'])) $model->setAttributes($_GET['PlaceFilter']);
		?>
		<div class="dropdown dropdown-lg">
			<div class="dropdown-menu dropdown-menu-right" id="filterDropDown">
				<a id="btn-close-filter"><i class="fa fa-close"></i></a>

				<label for="advanced-name">Name</label>
				<input id="advanced-name" name="q" type="text" class="form-control" value="<?php echo (!empty($_GET['q']))?$_GET['q']:"";?>"/>

				<div class="form-group">
					<?php echo $form->label($model,'city'); ?>
						<?php echo $form->dropDownList($model, 'city', array('' => '-') + GxHtml::listDataEx(City::model()->findAllAttributes(null, true))); ?>
						<?php echo $form->error($model,'city'); ?>
				</div>

				<div class="form-group">
					<?php echo $form->label($model,'region_id'); ?>
						<?php echo $form->dropDownList($model, 'region_id',array('' => '-')); ?>
						<?php echo $form->error($model,'region_id'); ?>
				</div>
				<div class="form-group">
					<?php echo $form->label($model,'price'); ?>
					<div class="clearfix"></div>
					<div class='col-xs-5'>
					<?php echo $form->numberField($model, 'lowest_price'); ?>
					</div>
					<div class='col-xs-2'>
					-
					</div>
					<div class='col-xs-5'>
					<?php echo $form->numberField($model, 'heigest_price'); ?>
					</div>
					<div class="clearfix"></div>

					<div class='col-xs-5'>
					<?php echo $form->error($model,'lowest_price'); ?>
					</div>
					<div class='col-xs-2'>
					</div>
					<div class='col-xs-5'>
					<?php echo $form->error($model,'heigest_price'); ?>
					</div>

				</div>

				<div class="form-group">
					<?php echo $form->label($model,'rooms'); ?>
					<div class="clearfix"></div>
					<div class='col-xs-5'>
					<?php echo $form->numberField($model, 'rooms_from'); ?>
					</div>
					<div class='col-xs-2'>
					-
					</div>
					<div class='col-xs-5'>
					<?php echo $form->numberField($model, 'rooms_to'); ?>
					</div>
					<div class="clearfix"></div>

					<div class='col-xs-5'>
					<?php echo $form->error($model,'rooms_from'); ?>
					</div>
					<div class='col-xs-2'>
					</div>
					<div class='col-xs-5'>
					<?php echo $form->error($model,'rooms_to'); ?>
					</div>

				</div>

				<div class="form-group">
					<?php echo $form->label($model,'capacity'); ?>
					<div class="clearfix"></div>
					<div class='col-xs-5'>
					<?php echo $form->numberField($model, 'capacity_from'); ?>
					</div>
					<div class='col-xs-2'>
					-
					</div>
					<div class='col-xs-5'>
					<?php echo $form->numberField($model, 'capacity_to'); ?>
					</div>
					<div class="clearfix"></div>

					<div class='col-xs-5'>
					<?php echo $form->error($model,'capacity_from'); ?>
					</div>
					<div class='col-xs-2'>
					</div>
					<div class='col-xs-5'>
					<?php echo $form->error($model,'capacity_to'); ?>
					</div>

				</div>

				<?php
				if (!empty($_GET['PlaceFilter']['properties'])){
					$model->properties = $_GET['PlaceFilter']['properties'];
				}
				?>
				<div class="form-group">
					<?php echo $form->label($model,'properties'); ?>
					<div class="clearfix"></div>
					<?php echo $form->checkBoxList($model, 'properties', GxHtml::encodeEx(GxHtml::listDataEx(Property::model()->findAllAttributes(null, true)), false, true)); ?>
					<?php echo $form->error($model,'properties'); ?>
				</div>

				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Location'),null); ?>
					<div class="clearfix"></div>

					<div id="searchmap-container">
					    <div id="searchmap"></div>
					</div>

					<div class="clearfix"></div>
					<div id="searchmap-floating-panel">
				      <input class="pull-left" onclick="currentLocation();" type=button value="<?php echo Yii::t('app','My location');?>">
				      <input class="pull-right" onclick="deleteMarkers();" type=button value="<?php echo Yii::t('app','Remove');?>">
				    </div>
				    <input type="hidden" id="searchmap_location" name="searchmap_location" value="" />

	            </div>



				<button type="submit" class="btn btn-primary btn-block advanced-search-btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
			</div>
		</div>
		<?php $this->endWidget(); ?><!-- form -->
	<?php
		break;
		case 'schools':
			$form = $this->beginWidget('GxActiveForm', array(
			'id' => 'school-search-form-advanced',
			'action' => $this->createUrl("schools/search"),
			'method' => 'GET',
			'enableAjaxValidation' => false,
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
				'afterValidate' => 'js:function(form, data, hasError) { 
					if(hasError) {
						for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
						return false;
					}
					else {
						form.children().removeClass("validate-has-error");
						return true;
					}
				}',
				'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
					if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
					else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
				}'
			),
			'htmlOptions'=>array(
				'class'=>'form-horizontal',
			)
		));
		$model = new SchoolFilter();
		if (!empty($_GET['SchoolFilter'])) $model->setAttributes($_GET['SchoolFilter']);
		?>
		<div class="dropdown dropdown-lg">
			<div class="dropdown-menu dropdown-menu-right" id="filterDropDown">
				<a id="btn-close-filter"><i class="fa fa-close"></i></a>

				<label for="advanced-name">Name</label>
				<input id="advanced-name" name="q" type="text" class="form-control" value="<?php echo (!empty($_GET['q']))?$_GET['q']:"";?>"/>

				<div class="form-group">
					<?php echo $form->label($model,'city'); ?>
						<?php echo $form->dropDownList($model, 'city', array('' => '-') + GxHtml::listDataEx(City::model()->findAllAttributes(null, true))); ?>
						<?php echo $form->error($model,'city'); ?>
				</div>

				<div class="form-group">
					<?php echo $form->label($model,'region_id'); ?>
						<?php echo $form->dropDownList($model, 'region_id',array('' => '-')); ?>
						<?php echo $form->error($model,'region_id'); ?>
				</div>

				<div class="form-group">
					<?php echo CHtml::label(Yii::t('app','Classification'),null); ?>
					<div class="clearfix"></div>
					<?php 
					foreach(array('arabic','languages','azhar','islamic','sisters','hotel','international','british','american','french','germany','ib','turkish','sudanese','libyan') as $classification){
						echo $form->checkBox($model, $classification) . ' ';
						echo $form->label($model,$classification);
						?><br /><?php
					}
					?>
					<?php echo $form->error($model,'classification'); ?>
				</div>

				<div class="form-group">
					<?php echo CHtml::label(Yii::t('app','Certificates'),null); ?>
					<div class="clearfix"></div>
					<?php 
					foreach(array('ncacs','cita','msa','sis','naas','madonna','manhattan','advanced','cambridge','iso','naqaae') as $certificate){
						echo $form->checkBox($model, $certificate) . ' ';
						echo $form->label($model,$certificate);
						?><br /><?php
					}
					?>
				</div>

				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Location'),null); ?>
					<div class="clearfix"></div>

					<div id="searchmap-container">
					    <div id="searchmap"></div>
					</div>

					<div class="clearfix"></div>
					<div id="searchmap-floating-panel">
				      <input class="pull-left" onclick="currentLocation();" type=button value="<?php echo Yii::t('app','My location');?>">
				      <input class="pull-right" onclick="deleteMarkers();" type=button value="<?php echo Yii::t('app','Remove');?>">
				    </div>
				    <input type="hidden" id="searchmap_location" name="searchmap_location" value="" />

	            </div>



				<button type="submit" class="btn btn-primary btn-block advanced-search-btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
			</div>
		</div>
		<?php $this->endWidget(); ?><!-- form -->
	<?php
		break;
		case 'saudiUniversities':
		case 'universities':
			$form = $this->beginWidget('GxActiveForm', array(
			'id' => 'universitites-search-form-advanced',
			'action' => $this->createUrl($this->getMainController() . "/search"),
			'method' => 'GET',
			'enableAjaxValidation' => false,
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
				'afterValidate' => 'js:function(form, data, hasError) { 
					if(hasError) {
						for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
						return false;
					}
					else {
						form.children().removeClass("validate-has-error");
						return true;
					}
				}',
				'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
					if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
					else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
				}'
			),
			'htmlOptions'=>array(
				'class'=>'form-horizontal',
			)
		));
		$model = new UniversityFilter("search");
		if (!empty($_GET['UniversityFilter'])) $model->setAttributes($_GET['UniversityFilter']);
		?>
		<div class="dropdown dropdown-lg">
			<div class="dropdown-menu dropdown-menu-right" id="filterDropDown">
				<a id="btn-close-filter"><i class="fa fa-close"></i></a>

				<label for="advanced-name">Name</label>
				<input id="advanced-name" name="q" type="text" class="form-control" value="<?php echo (!empty($_GET['q']))?$_GET['q']:"";?>"/>

				<?php /* ?>
				<div class="form-group">
					<?php echo $form->label($model,'city'); ?>
						<?php echo $form->dropDownList($model, 'city', array('' => '-') + GxHtml::listDataEx(City::model()->findAllAttributes(null, true))); ?>
						<?php echo $form->error($model,'city'); ?>
				</div>

				<div class="form-group">
					<?php echo $form->label($model,'region_id'); ?>
						<?php echo $form->dropDownList($model, 'region_id',array('' => '-')); ?>
						<?php echo $form->error($model,'region_id'); ?>
				</div>
				<?php */ ?>

				<div class="form-group">
					<?php echo $form->label($model,'type'); ?>
					<div class="clearfix"></div>
					<?php 
					$criteria = new CDbCriteria;
					$criteria->compare("show_in_menu",1);
					$criteria->order = "sortid";

						echo $form->dropDownList($model, 'type', GxHtml::encodeEx(array(''=>'-') + GxHtml::listDataEx(EntityType::model()->active()->findAll($criteria)), false, true)); ?>
					<?php echo $form->error($model,'type'); ?>
				</div>

				<?php
				if (!empty($_GET['UniversityFilter']['entityAttributes'])){
					$model->entityAttributes = $_GET['UniversityFilter']['entityAttributes'];
				}
				?>
				<div id="entity_attributes">
				</div>


				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Fees'),null); ?>
					<div class="clearfix"></div>

					<div class="col-md-5 nopadding">
						<?php echo CHtml::label(Yii::t('app','From'),"fees_0");
							echo CHtml::numberField("fees[0]", (!empty($_GET["fees"][0]))?$_GET["fees"][0]:"", array('id'=>"fees_0", 'class'=>'form-control'));
	            		?>
	            	</div>
	            	<div class="col-md-2 nopadding"></div>
	            	<div class="col-md-5 nopadding">
						<?php echo CHtml::label(Yii::t('app','To'),"fees_1");
							echo CHtml::numberField("fees[1]", (!empty($_GET["fees"][1]))?$_GET["fees"][1]:"", array('id'=>"fees_1", 'class'=>'form-control'));
	            		?>
	            	</div>
	            </div>

	            <?php /* ?>
				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Admission Certificates'),null); ?>
					<div class="clearfix"></div>

					<select class="form-control selectboxit" name="certificate" id="certificate">
						<option value="">-</option>
						<option value="thnwy_adby">ثانوية عامة (أدبي)</option>
						<option value="thnwy_elmy_math">ثانوية عامة (علمي رياضة)</option>
						<option value="thnwy_elmy_science">ثانوية عامة (علمي علوم)</option>
						<option value="thnwy_sena3y">ثانوية صناعية</option>
						<option value="thnwy_tegary">ثانوية تجارية</option>
						<option value="thnwy_zera3y">ثانوية زراعية</option>
						<option value="deb_seyaha">دبلوم سياحة وفنادق</option>
						<option value="thnwy_azhar">ثانوية أزهرية</option>
						<option value="deb_american">الدبلومة الأمريكية</option>
						<option value="deb_british">الدبلومة البريطانية</option>
						<option value="mo3adala">الشهادات المعادلة</option>
					</select>	
	            </div>
	            <?php */ ?>

				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Years of study'),null); ?>
					<div class="clearfix"></div>

					<div class="col-md-5 nopadding">
						<?php echo CHtml::label(Yii::t('app','From'),"years_0");
							echo CHtml::numberField("years[0]", (!empty($_GET["years"][0]))?$_GET["years"][0]:"", array('id'=>"years_0", 'class'=>'form-control'));
	            		?>
	            	</div>
	            	<div class="col-md-2 nopadding"></div>
	            	<div class="col-md-5 nopadding">
						<?php echo CHtml::label(Yii::t('app','To'),"years_1");
							echo CHtml::numberField("years[1]", (!empty($_GET["years"][1]))?$_GET["years"][1]:"", array('id'=>"years_1", 'class'=>'form-control'));
	            		?>
	            	</div>
	            </div>

				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Degrees Awarded'),null); ?>
					<div class="clearfix"></div>

					<select class="form-control selectboxit" name="degree" id="degree">
						<option value="">-</option>
						<option value="البكالوريوس">البكالوريوس</option>
						<option value="الليسانس">الليسانس</option>
						<option value="الدبلوم">الدبلوم</option>
						<option value="الماجستير">الماجستير</option>
						<option value="الدكتوراة">الدكتوراة</option>
					</select>	
	            </div>

				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Ranking among Egyptian Universities (4ICU)'),null); ?>
					<div class="clearfix"></div>

					<div class="col-md-5 nopadding">
						<?php echo CHtml::label(Yii::t('app','From'),"rank_4icu_0");
							echo CHtml::numberField("rank_4icu[0]", (!empty($_GET["rank_4icu"][0]))?$_GET["rank_4icu"][0]:"", array('id'=>"rank_4icu_0", 'class'=>'form-control'));
	            		?>
	            	</div>
	            	<div class="col-md-2 nopadding"></div>
	            	<div class="col-md-5 nopadding">
						<?php echo CHtml::label(Yii::t('app','To'),"rank_4icu_1");
							echo CHtml::numberField("rank_4icu[1]", (!empty($_GET["rank_4icu"][1]))?$_GET["rank_4icu"][1]:"", array('id'=>"rank_4icu_1", 'class'=>'form-control'));
	            		?>
	            	</div>
	            </div>

				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Rankings according to Shanghai University'),null); ?>
					<div class="clearfix"></div>

					<div class="col-md-5 nopadding">
						<?php echo CHtml::label(Yii::t('app','From'),"rank_shanghai_0");
							echo CHtml::numberField("rank_shanghai[0]", (!empty($_GET["rank_shanghai"][0]))?$_GET["rank_shanghai"][0]:"", array('id'=>"rank_shanghai_0", 'class'=>'form-control'));
	            		?>
	            	</div>
	            	<div class="col-md-2 nopadding"></div>
	            	<div class="col-md-5 nopadding">
						<?php echo CHtml::label(Yii::t('app','To'),"rank_shanghai_1");
							echo CHtml::numberField("ank_shanghai[1]", (!empty($_GET["rank_shanghai"][1]))?$_GET["rank_shanghai"][1]:"", array('id'=>"rank_shanghai_1", 'class'=>'form-control'));
	            		?>
	            	</div>
	            </div>

				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Rankings according to Webmetrics'),null); ?>
					<div class="clearfix"></div>

					<div class="col-md-5 nopadding">
						<?php echo CHtml::label(Yii::t('app','From'),"rank_webmetrics_0");
							echo CHtml::numberField("rank_webmetrics[0]", (!empty($_GET["rank_webmetrics"][0]))?$_GET["rank_webmetrics"][0]:"", array('id'=>"rank_webmetrics_0", 'class'=>'form-control'));
	            		?>
	            	</div>
	            	<div class="col-md-2 nopadding"></div>
	            	<div class="col-md-5 nopadding">
						<?php echo CHtml::label(Yii::t('app','To'),"rank_webmetrics_1");
							echo CHtml::numberField("rank_webmetrics[1]", (!empty($_GET["rank_webmetrics"][1]))?$_GET["rank_webmetrics"][1]:"", array('id'=>"rank_webmetrics_1", 'class'=>'form-control'));
	            		?>
	            	</div>
	            </div>

				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Result Type'),null); ?>
					<div class="clearfix"></div>

					<select class="form-control selectboxit" name="result_type" id="result_type">
						<option value="">All</option>
						<option value="university">University</option>
						<option value="college">College</option>
						<option value="institute">Institute</option>
					</select>	
	            </div>


				<div class="form-group row">
					<?php echo CHtml::label(Yii::t('app','Location'),null); ?>
					<div class="clearfix"></div>

					<div id="searchmap-container">
					    <div id="searchmap"></div>
					</div>

					<div class="clearfix"></div>
					<div id="searchmap-floating-panel">
				      <input class="pull-left" onclick="currentLocation();" type=button value="<?php echo Yii::t('app','My location');?>">
				      <input class="pull-right" onclick="deleteMarkers();" type=button value="<?php echo Yii::t('app','Remove');?>">
				    </div>
				    <input type="hidden" id="searchmap_location" name="searchmap_location" value="" />

	            </div>

				<button type="submit" class="btn btn-primary btn-block advanced-search-btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
			</div>
		</div>
		<script type="text/javascript">
			function loadAttributes(){
				if (!jQuery('#UniversityFilter_type').val()) return;
				var type_id = jQuery('#UniversityFilter_type').val();
				var university_id = jQuery('#UniversityFilter_university').val();
				if (typeof university_id == "undefined") university_id = "";

				jQuery("#entity_attributes").html("");

				jQuery("#entity_attributes").load('<?php echo $this->createUrl($this->getMainController() . "/entityAttributes",array('id'=>'TYPE_ID_PLACEHOLER','university'=>'UNIVERSITY_ID_PLACEHOLDER'));?>'
					.replace('TYPE_ID_PLACEHOLER',type_id)
					.replace('UNIVERSITY_ID_PLACEHOLDER',university_id));
			}
			jQuery(document).on('change','#UniversityFilter_type, #UniversityFilter_university',function(){
				loadAttributes();
			});
			jQuery(document).ready(function(){
				loadAttributes();
			})
			
		</script>


		<?php $this->endWidget(); ?><!-- form -->
	<?php
		break;
	}
	?>

<style>
  /* Always set the map height explicitly to define the size of the div
   * element that contains the map. */
  #searchmap {
    height: 100%;
  }
  #searchmap-container {
  	position: relative;
  	height: 150px;
  }
}
</style>


<script>

  // In the following example, markers appear when the user clicks on the map.
  // The markers are stored in an array.
  // The user can then click an option to hide, show or delete the markers.
  var map;
  var markers = [];
  var infoWindow = null;
  var haightAshbury = {lat: 30.044132, lng: 31.235772};
  
  function initSearchMap() {

    map = new google.maps.Map(document.getElementById('searchmap'), {
      zoom: 12,
      center: haightAshbury,
      mapTypeId: 'terrain'
    });

    // This event listener will call addMarker() when the map is clicked.
    map.addListener('click', function(event) {
      addMarker(event.latLng);
    });

    // Adds a marker at the center of the map.
    // addMarker(haightAshbury);
    
  }

  // Adds a marker to the map and push to the array.
  function addMarker(location) {
  	clearMarkers();
    var marker = new google.maps.Marker({
      position: location,
      map: map
    });
    markers.push(marker);
    var loc = (typeof location.lat=="function")?location.lat():location.lat;
    loc += ",";
    loc += (typeof location.lng=="function")?location.lng():location.lng;
    $("#searchmap_location").val(loc);
  }

  // Sets the map on all markers in the array.
  function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkers() {
    setMapOnAll(null);
  }

  // Shows any markers currently in the array.
  function showMarkers() {
    setMapOnAll(map);
  }

  // Deletes all markers in the array by removing references to them.
  function deleteMarkers() {
    clearMarkers();
    $("#searchmap_location").val("");
    //map.setCenter(haightAshbury);
    markers = [];
  }

  function currentLocation(){
    // Try HTML5 geolocation.

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        map.setCenter(pos);
        addMarker(pos);
      }, function(error) {
        handleLocationError(true, map.getCenter());
      },{timeout: 30000, enableHighAccuracy: true, maximumAge: 75000});
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, map.getCenter());
    }

  }
  function handleLocationError(browserHasGeolocation, pos) {

	$.ajax({
	  dataType: "json",
	  url: "https://ipinfo.io",
	  success: function(ipinfo){
		    var latLong = ipinfo.loc.split(",");
            var pos = {
              lat: latLong[0]*1,
              lng: latLong[1]*1
            };
            map.setCenter(pos);
        	addMarker(pos);    
		},
	  error: function(err){
      	if (infoWindow) infoWindow.close();
      	infoWindow = new google.maps.InfoWindow({map: map});		

        //infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
	  }
	});

  }			      
</script>


<?php /* ?>
<!-- footer begin -->
<footer class="footer-1 bg-color-1">

	<!-- main footer begin -->
	<div class="main-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="compact-widget">                                                
						<div class="widget-inner">
							<img class="logo-footer" src="<?php echo $this->themeURL;?>/images/logo-footer.png" alt="compact company">
							<p>Compact is a clean PSD theme suitable for corporate, You can customize it very easy to fit your needs, semper suscipit metus accumsan at. Vestibulum et lacus urna. Nam luctus ac tortor eu</p>
							<div class="social-icons clearfix">
								<a href="#" class="facebook tipped" data-title="facebook"  data-tipper-options='{"direction":"top","follow":"true"}'><i class="fa fa-facebook"></i></a>
								<a href="#" class="twitter tipped" data-title="twitter"  data-tipper-options='{"direction":"top","follow":"true"}'><i class="fa fa-twitter"></i></a>
								<a href="#" class="google tipped" data-title="google +"  data-tipper-options='{"direction":"top","follow":"true"}'><i class="fa fa-google-plus"></i></a>
								<a href="#" class="youtube tipped" data-title="youtube"  data-tipper-options='{"direction":"top","follow":"true"}'><i class="fa fa-youtube-play"></i></a>
								<a href="#" class="linkedin tipped" data-title="linkedin"  data-tipper-options='{"direction":"top","follow":"true"}'><i class="fa fa-linkedin"></i></a>
							</div>
						</div>    
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="compact-widget">
						<h3 class="widget-title"><?php echo Yii::t('app','Features');?></h3> 
						<div class="widget-inner">
							<ul>
								<li><a href="#">About Us</a></li>
								<li><a href="#">Our Story</a></li>
								<li><a href="#">Term &amp; Conditions</a></li>
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Sites Map</a></li>
							</ul>
						</div>                                                   
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="compact-widget">
						<h3 class="widget-title"><?php echo Yii::t('app','Contact Us');?></h3>
						<div class="widget-inner">
							<p>Address: 379 5th Ave  New York, NYC <br> 10018, United States</p>
							<p>Phone: +(112) 345 6879</p>
							<p>Fax: +(112) 345 8796</p>
							<p>Email: contact@compact.com</p>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="compact-widget">
						<h3 class="widget-title"><?php echo Yii::t('app','Newsletter');?></h3>
						<div class="widget-inner">
							<div class="newsletter newsletter-widget">
								<p>Stay informed about our news and events</p>
								<form action="" method="post">
									<p><input class="newsletter-email" type="email" name="email" placeholder="<?php echo Yii::t('app','Your email');?>"><i class="fa fa-envelope-o"></i></p>
									<p><input class="newsletter-submit" type="submit" value="<?php echo Yii::t('app','Subscribe');?>"></p>
								</form>
							</div>                                
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>            
	<!-- main footer close -->

	<!-- sub footer begin -->
	<div class="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<?php echo Yii::t('app','Copyright &copy; 2016 Designed by AuThemes. All rights reserved.');?>
				</div>
			</div>
		</div>
	</div>
	<!-- sub footer close -->

</footer>
<!-- footer close -->        

<?php */ ?>

</div>

<a id="to-the-top" ><i class="fa fa-angle-up"></i></a>








<!-- Start login modal-->


<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="loginmodal-container">
			<h1>Login to Your Account</h1><br>
			<form>
				<input type="text" name="user" placeholder="Username">
				<input type="password" name="pass" placeholder="Password">
				<input type="submit" name="login" class="login loginmodal-submit" value="Login">
			</form>

			<div class="login-help">
				<a href="#">Register</a> - <a href="#">Forgot Password</a>
			</div>
		</div>
	</div>
</div>


<!-- end login modal-->

<!-- LOAD JS FILES -->
<script src="<?php echo $this->themeURL;?>/js/bootstrap.min.js"></script>
<script src="<?php echo $this->themeURL;?>/js/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo $this->themeURL;?>/js/easing.js"></script>
<script src="<?php echo $this->themeURL;?>/js/owl.carousel.js"></script>
<script src="<?php echo $this->themeURL;?>/js/jquery.fitvids.js"></script>    
<script src="<?php echo $this->themeURL;?>/js/wow.min.js"></script>
<script src="<?php echo $this->themeURL;?>/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $this->themeURL;?>/js/tabs.js"></script>



<!-- Waypoints-->
<script src="<?php echo $this->themeURL;?>/js/jquery.waypoints.min.js"></script>
<script src="<?php echo $this->themeURL;?>/js/sticky.min.js"></script>
<script src="<?php echo $this->themeURL;?>/js/tipper.js"></script>
<script src="<?php echo $this->themeURL;?>/js/compact.js"></script>
<script src="<?php echo $this->themeURL;?>/js/custom-index1.js?v=2"></script>

<!-- SLIDER REVOLUTION SCRIPTS  -->
<script type="text/javascript" src="<?php echo $this->themeURL;?>/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo $this->themeURL;?>/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo $this->themeURL;?>/js/revslider-custom.js"></script> 


<script src="<?php echo $this->themeURL;?>/js/responsive-calendar.js"></script>
<script src="<?php echo $this->themeURL;?>/js/jquery.timepicker.min.js"></script>







<!-- News bar -->

<script src="<?php echo $this->themeURL;?>/js/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>   


<!-- start Register js-->

<script>


	$(function () {

	});


</script>
<!-- End Register js-->

<!--slick-->
<script type="text/javascript" src="<?php echo $this->themeURL;?>/slick/slick.min.js"></script>

<script type="text/javascript" src="<?php echo $this->themeURL;?>/js/custom.js?v=3"></script>
<script type="text/javascript" src="<?php echo $this->themeURL;?>/js/bootstap-select.js?v=3"></script>
</body>
</html>





