<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="container">
    <div class="row"> 

<?php if (!empty($this->menues)): ?>
	<div class="col-md-12">
	<!-- desktop menu begin -->
	<nav id="blog-menu" class="site-desktop-menu site-desktop-menu-2">
		<ul class="clearfix">
			<?php foreach ($this->menues as $menu): ?>
			<li>
				<?php echo $menu->createMenuLink();?>
				<?php if (!empty($menu->childrenSorted)): ?>
				<ul>
					<?php foreach ($menu->childrenSorted as $submenu): ?>
					<li><?php echo $submenu->createMenuLink();?></li>
					<?php endforeach; ?>
				</ul>
				<?php endif;?>
			</li>
			<?php endforeach; ?>
		</ul>
	</nav>
	</div>
	<!-- desktop menu close -->
	<div class="clearfix"></div>
<?php endif; ?>

<div class="col-md-9">
<?php echo $content; ?>
</div>
<div class="col-md-3">
	<div class="main-sidebar">
		<?php if ($this->categories):?>
		<aside class="widget widget_categories">
			<h3 class="widget-title"><?php echo Yii::t('app','Categories');?></h3> 
			<div class="tiny-border"></div>   
				<?php foreach($this->categories as $cat):?>
					- <a href="<?php echo $cat->url;?>"><?php echo $cat;?></a><br />
				<?php endforeach; ?>
		</aside>
		<?php endif;?>

		<?php if (!empty($this->related)): ?>
		<aside class="widget widget_related">
			<h3 class="widget-title"><?php echo Yii::t('app','Related');?></h3> 
			<div class="tiny-border"></div>   
				<?php foreach($this->related as $url=>$title):?>
					- <a href="<?php echo $url;?>"><?php echo $title;?></a><br />
				<?php endforeach; ?>
		</aside>
		<?php endif; ?>

		<?php if (!empty($this->blocks)): ?>
		<?php foreach($this->blocks as $block):?>
		<aside class="widget widget_block">
			<h3 class="widget-title"><?php echo $block;?></h3> 
			<div class="tiny-border"></div>
			<?php switch ($block->template_id) {
				case 1:
					$category = BlogCategory::model()->cache(60)->findByPK($block->article_cat_id);
					if (!$category || $category->is_deleted) continue;
					?>
						<?php 

						$criteria = new CDbCriteria;
						$criteria->compare("category_id",$block->article_cat_id);
						$criteria->compare("t.is_deleted",0);
						$criteria->compare("t.status",1);
						$criteria->order = "t.id desc";
						$criteria->limit = "10";

						$articles = BlogArticle::model()->cache(0)->findAll($criteria);
						foreach($articles as $article):?>
							<?php if ($article->image_file): ?>
							<img class="related_img pull-left" src="<?php echo $article->image_file;?>"  alt="">
							<?php endif; ?>
							<a class="pull-left" href="<?php echo $article->url;?>"><?php echo $article;?></a>
							<div class="clearfix"></div>
						<?php endforeach; ?>
					<?php
					break;

				case 11:
					echo $block->html;
					break;
				default:
					continue;
			} ?>
		</aside>
		<?php endforeach; ?>
		<?php endif; ?>

	</div>         
</div>

	</div>
</div>
<?php $this->endContent(); ?>
<script type="text/javascript">
	$(document).ready(function($){
		$(".project-slider").owlCarousel({
            singleItem: true,
            lazyLoad: true,
            navigation: true,
            autoPlay : true,
            navigationText: [
              "<i class='fa fa-chevron-left'></i>",
              "<i class='fa fa-chevron-right'></i>"
            ],
            slideSpeed : 400,
        });		
    });
  
</script>