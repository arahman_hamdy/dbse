<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Database for Students &amp; Entrepreneurs</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Respomsive slider -->
        <link href="css/responsive-calendar.css" rel="stylesheet">

        <!--slick-->
        <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>


        <!-- news Bar Css -->
        <link href="css/newsbar.css" rel="stylesheet">


        <!-- Favicons
        ================================================== -->
        <link rel="icon" href="images/favicon.png" type="image/x-icon">    

        <!-- LOAD CSS FILES -->    
        <link href="style.css" rel="stylesheet" type="text/css">  

        <!-- color scheme -->
        <link rel="stylesheet" href="switcher/demo.css" type="text/css">
        <link rel="stylesheet" href="switcher/colors/lime.css" type="text/css" id="colors">  



        <style>

            .slider-nav img{
                padding-right: 10px;
            }

            .slider-for img{
                margin: 0 auto;
                width: 100%;
            }

            /*slick*/
            .slick-prev:before, .slick-next:before {

                color:gray;}
            /*---------End slick css---------*/    







            .colorgraph {
                height: 5px;
                border-top: 0;
                background: #c4e17f;
                border-radius: 5px;
                background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            }




        </style> 
        <style>

            .dropdown.dropdown-lg .dropdown-menu {
                margin-top: -1px;
                padding: 6px 20px;
            }
            .input-group-btn .btn-group {
                display: flex !important;
            }
            .btn-group .btn {
                border-radius: 0;
                margin-left: -1px;
            }
            .btn-group .btn:last-child {
                border-top-right-radius: 4px;
                border-bottom-right-radius: 4px;
            }
            .btn-group .form-horizontal .btn[type="submit"] {
                border-top-left-radius: 4px;
                border-bottom-left-radius: 4px;
            }
            .form-horizontal .form-group {
                margin-left: 0;
                margin-right: 0;
            }
            .form-group .form-control:last-child {
                border-top-left-radius: 4px;
                border-bottom-left-radius: 4px;
            }


            .form-control{height: 42px;}

            @media screen and (min-width: 768px) {
                #adv-search {
                    width: 100%;
                    margin: 0 auto;
                }
                .dropdown.dropdown-lg {
                    position: static !important;
                }
                .dropdown.dropdown-lg .dropdown-menu {
                    min-width:200%;
                }
            }  

            /***  End news Bar Css *****/





            @import url(https://fonts.googleapis.com/css?family=Roboto);

            /****** LOGIN MODAL ******/
            .loginmodal-container {
                padding: 30px;
                max-width: 350px;
                width: 100% !important;
                background-color: #F7F7F7;
                margin: 0 auto;
                border-radius: 2px;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                overflow: hidden;
                font-family: roboto;
                margin-top: 100px;
            }

            .loginmodal-container h1 {
                text-align: center;
                font-size: 1.8em;
                font-family: roboto;
            }

            .loginmodal-container input[type=submit] {
                width: 100%;
                display: block;
                margin-bottom: 10px;
                position: relative;
            }

            .loginmodal-container input[type=text], input[type=password] {
                height: 44px;
                font-size: 16px;
                width: 100%;
                margin-bottom: 10px;
                -webkit-appearance: none;
                background: #fff;
                border: 1px solid #d9d9d9;
                border-top: 1px solid #c0c0c0;
                /* border-radius: 2px; */
                padding: 0 8px;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }

            .loginmodal-container input[type=text]:hover, input[type=password]:hover {
                border: 1px solid #b9b9b9;
                border-top: 1px solid #a0a0a0;
                -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
                -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
                box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
            }

            .loginmodal {
                text-align: center;
                font-size: 14px;
                font-family: 'Arial', sans-serif;
                font-weight: 700;
                height: 36px;
                padding: 0 8px;
                /* border-radius: 3px; */
                /* -webkit-user-select: none;
                  user-select: none; */
            }

            .loginmodal-submit {
                /* border: 1px solid #3079ed; */
                border: 0px;
                color: #fff;
                text-shadow: 0 1px rgba(0,0,0,0.1); 
                background-color: #4d90fe;
                padding: 17px 0px;
                font-family: roboto;
                font-size: 14px;
                /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
            }

            .loginmodal-submit:hover {
                /* border: 1px solid #2f5bb7; */
                border: 0px;
                text-shadow: 0 1px rgba(0,0,0,0.3);
                background-color: #357ae8;
                /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
            }

            .loginmodal-container a {
                text-decoration: none;
                color: #666;
                font-weight: 400;
                text-align: center;
                display: inline-block;
                opacity: 0.6;
                transition: opacity ease 0.5s;
            } 

            .login-help{
                font-size: 12px;
            }





        </style>
    </head>

    <body>
        <!-- Preload images start //-->
        <!--   <div class="images-preloader" id="images-preloader">
               <div class="spinner">
                   <div class="bounce1"></div>
                   <div class="bounce2"></div>
                   <div class="bounce3"></div>
               </div>
           </div>
       
        -->
        <!-- Preload images end //-->

        <div id="wrapper">

            <!-- header begin -->
            <header class="site-header-1 site-header">
                <!-- Main bar start -->
                <div id="sticked-menu" class="main-bar">
                    <div class="container">                
                        <div class="row">                    
                            <div class="col-md-12">

                                <!-- logo begin -->
                                <div id="logo" class="pull-left">                            
                                    <a href="index.php">
                                        <img src="images/logo.png" alt="" class="logo">
                                    </a>
                                </div>
                                <!-- logo close -->

                                <!-- btn-mobile menu begin -->
                                <a id="show-mobile-menu" class="btn-mobile-menu hidden-lg hidden-md"><i class="fa fa-bars"></i></a>
                                <!-- btn-mobile menu close -->  

                                <!-- mobile menu begin -->
                                <nav id="mobile-menu" class="site-mobile-menu hidden-lg hidden-md">
                                    <ul></ul>
                                </nav>  
                                <!-- mobile menu close -->                        

                                <!-- desktop menu begin -->
                                <nav id="desktop-menu" class="site-desktop-menu hidden-xs hidden-sm">
                                    <ul class="clearfix">
                                        <li class="active"><a href="index.php">Home</a></li>
                                        <li><a href="aboutus.php">About US</a></li>
                                        <li><a href="places.php">latest Places</a></li>
                                        <li><a href="contact.php">Contact Us</a></li>
                                        <li><a href="my_account.php">My Account</a>
                                            <ul>
                                                <li><a href="my_profile.php">my profile</a></li>
                                                <li><a href="my_favorites.php">my favorites</a></li>


                                            </ul>
                                        </li>

                                    </ul>
                                </nav>
                                <!-- desktop menu close -->

                                <!-- Header Group Button Right begin -->
                                <div class="header-buttons pull-right hidden-xs hidden-sm">

                                    <div class="header-contact">
                                        <ul class="clearfix">
                                            <li ><a href="#" data-toggle="modal" data-target="#login-modal"><i class="fa fa-user"></i>Login</a></li>
                                            <li class="border-line">|</li>
                                            <li ><a href="register.php"><i class="fa fa-star"></i>Register</a></li>
                                            <li class="border-line">|</li>
                                            <li ><a href="#"><i class="fa fa-language"></i>العربية</a></li>
                                        </ul>
                                    </div>

                                    <!-- Button Modal popup searchbox -->
                                    <div class="search-button">
                                        <!-- Trigger the modal with a button -->
                                        <a href="" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i></a>                                                                        
                                    </div>

       

                                    <!-- Button Menu OffCanvas right -->
                                    <div class="navright-button">
                                        <a href="" id="btn-offcanvas-menu"><i class="fa fa-bars"></i></a>                                    
                                    </div> 

                                </div>
                                <!-- Header Group Button Right close -->

                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- header close -->
            <div class="gray-line"></div>

            <!-- Modal Search begin -->
            <div id="myModal" class="modal fade" role="dialog">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-dialog myModal-search">
                    <!-- Modal content-->
                    <div class="modal-content">                                        
                        <div class="modal-body">
                            <form role="search" method="get" class="search-form" action="">
                                <input type="search" class="search-field" placeholder="Search here..." value="" title="" />
                                <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal Search close -->

            <!-- Menu OffCanvas right begin -->
            <div class="navright-button hidden-sm">
                <div class="compact-menu-canvas" id="offcanvas-menu">
                    <h3>menu</h3><a id="btn-close-canvasmenu"><i class="fa fa-close"></i></a>
                    <nav>
                        <ul class="clearfix">
                            <li><a href="index.php">Home</a>
                            <li><a href="aboutus.php">About US</a>
                            <li><a href="places.php">latest Places</a>
                            <li><a href="contact.php">Contact Us</a>
                            <li><a href="login.php" >Login</a>
                            <li><a href="register.php">Register</a>

                        </ul>                
                    </nav>
                </div>
            </div> 
            <!-- Menu OffCanvas right close -->

