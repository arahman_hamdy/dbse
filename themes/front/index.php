<?php
include'header.php';
?>
<section  style="background-color: graytext;" id="section-cta">
        <div class="sep-background-mask"></div>

<div class="container">
    <div class="row">
        <div id="search_for_places" class="col-md-10 col-md-offset-1">
            
             <!--  Start Search -->
        <h1 class="text-center">Search For Places</h1>
        <div class="input-group" id="adv-search">
            <form class="form-horizontal" role="form" action="<?php echo $this->createUrl("places/search"); ?>">

            <input required name="q" type="text" class="form-control" placeholder="Search for Places" />
            <div class="input-group-btn">
                <div class="btn-group" role="group">
                    <div class="dropdown dropdown-lg">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                                <div class="form-group">
                                    <label for="filter">Filter by</label>
                                    <select class="form-control">
                                        <option value="0" selected>All Places</option>
                                        <option value="1">Featured</option>
                                        <option value="2">Most popular</option>
                                        <option value="3">Top rated</option>
                                        <option value="4">Most commented</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="contain">Author</label>
                                    <input class="form-control" type="text" />
                                </div>
                                <div class="form-group">
                                    <label for="contain">Contains the words</label>
                                    <input class="form-control" type="text" />
                                </div>
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                </div>
            </div>
            </form>

        </div>
        <!-- End Search-->
            
        </div>
    </div>
    
</div>
</section>
<div class="container">
    <div class="col-md-3">

        <!--  Start Most visited and new-->

        <div class="tabs tabs-2" data-active="1">
            <header>
                <h4 class="active">Most visited</h4>
                <h4 class="">New </h4>

            </header>

            <div class="tabs-main">
                <div style="display: block;" class="tabs-item">
                    <p>Fusce ornare mi vel risus porttitor dignissim. Nunc eget risus at ipsum blandit ornare vel sed velit. Proin gravida arcu nisl, a dignissim mauris placerat id. Vivamus interdum urna at sapien varius elementum. Suspendisse ut mi felis et interdum libero lacinia vel. Aenean elementum odio ut lorem cursus, eu auctor magna pellentesque.  Cras facilisis quam sed rhoncus dapibus. Quisque lorem enim</p>
                </div>
                <div style="display: none;" class="tabs-item">
                    <p>Fusce ornare mi vel risus porttitor dignissim. Nunc eget risus at ipsum blandit ornare vel sed velit. Proin gravida arcu nisl, a dignissim mauris placerat id. Vivamus interdum urna at sapien varius elementum. Suspendisse ut mi felis et interdum libero lacinia vel. Aenean elementum odio ut lorem cursus, eu auctor magna pellentesque.  Cras facilisis quam sed rhoncus dapibus. Quisque lorem enim</p>
                </div>

            </div>

        </div>
        <!--  End Most visited and new-->

        <!--  Start Advertisement-->

        <div class="tabs tabs-2" data-active="1">
            <header>
                <h4 class="active">Advertisement</h4>

            </header>

            <div class="tabs-main">
                <div style="display: block;" class="tabs-item">
                    <img src="images/news/thumb/thumb-1.jpg" alt="image name">
                    <p>Fusce ornare mi vel risus porttitor dignissim. Nunc eget risus at ipsum blandit ornare vel sed velit. Proin gravida arcu nisl, a dignissim mauris placerat id. Vivamus interdum urna at sapien varius elementum. Suspendisse ut mi felis et interdum libero lacinia vel. Aenean elementum odio ut lorem cursus, eu auctor magna pellentesque.  Cras facilisis quam sed rhoncus dapibus. Quisque lorem enim</p>
                </div>

            </div>

        </div>
        <!--  End Advertisement-->


        <!--  Start Advertisement-->

        <div class="tabs tabs-2" data-active="1">
            <header>
                <h4 class="active">Advertisement</h4>

            </header>

            <div class="tabs-main">
                <div style="display: block;" class="tabs-item">
                    <p>Fusce ornare mi vel risus porttitor dignissim. Nunc eget risus at ipsum blandit ornare vel sed velit. Proin gravida arcu nisl, a dignissim mauris placerat id. Vivamus interdum urna at sapien varius elementum. Suspendisse ut mi felis et interdum libero lacinia vel. Aenean elementum odio ut lorem cursus, eu auctor magna pellentesque.  Cras facilisis quam sed rhoncus dapibus. Quisque lorem enim</p>
                    <img src="images/news/thumb/thumb-1.jpg" alt="image name">
                </div>

            </div>

        </div>
        <!--  End Advertisement-->

    </div>






    <div class="col-md-9">

        <!-- Start Governats-->
        <div class="tabs tabs-2" data-active="1">
            <header>
                <h4>governorates</h4>

            </header>

            <div class="tabs-main">
                <div class="tabs-item">
                    <p>Also you can search by governorates names by clicking in its name :) </p>

                    <a class="btn btn-primary btn-radius width grey mleft" href="#"> Alexandria</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Aswan</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Asyut</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Beheira</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Beni Suef</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Cairo</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Dakahlia 	</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Damietta</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Faiyum</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Gharbia</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Giza</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#"> 	Ismailia</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Kafr el-Sheikh</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Luxor</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Matruh</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Minya</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Monufia</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">New Valley 	</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">North Sinai 	</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Port Said</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Qalyubia</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Qena</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Red Sea</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Sharqia</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Sohag</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">South Sinai 	</a>
                    <a class="btn btn-primary btn-radius width grey mleft" href="#">Suez</a>
                </div>

            </div>

        </div>
        <!--End governats-->

        <div class="clearfix"></div>      

        <!--Start calender --->

        <div class="tabs tabs-2" data-active="1">
            <header>
                <h4>calendar</h4>

            </header>

            <div class="tabs-main">
                <div class="tabs-item">     






                    <!-- Responsive calendar - START -->
                    <div class="responsive-calendar">
                        <div class="controls">
                            <a class="pull-left" data-go="prev"><div class="btn btn-primary">Prev</div></a>
                            <h4><span data-head-year></span> <span data-head-month></span></h4>
                            <a class="pull-right" data-go="next"><div class="btn btn-primary">Next</div></a>
                        </div><hr/>
                        <div class="day-headers">
                            <div class="day header">Mon</div>
                            <div class="day header">Tue</div>
                            <div class="day header">Wed</div>
                            <div class="day header">Thu</div>
                            <div class="day header">Fri</div>
                            <div class="day header">Sat</div>
                            <div class="day header">Sun</div>
                        </div>
                        <div class="days" data-group="days">

                        </div>
                    </div>
                    <!-- Responsive calendar - END -->


                </div>
            </div>
        </div>
        <!--End calender-->


        <div class="clearfix"></div>      


        
        <!-- Start News Bar-->

        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-list-alt"></span><b>News from shafaff.com </b></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="demo1">
                            <li class="news-item">
                                <table cellpadding="4">
                                    <tr>
                                        <td><img src="images/1.png" width="60" class="img-circle" /></td>
                                        <td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
                                    </tr>
                                </table>
                            </li>
                            <li class="news-item">
                                <table cellpadding="4">
                                    <tr>
                                        <td><img src="images/2.png" width="60" class="img-circle" /></td>
                                        <td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
                                    </tr>
                                </table>
                            </li>
                            <li class="news-item">
                                <table cellpadding="4">
                                    <tr>
                                        <td><img src="images/3.png" width="60" class="img-circle" /></td>
                                        <td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
                                    </tr>
                                </table>
                            </li>
                            <li class="news-item">
                                <table cellpadding="4">
                                    <tr>
                                        <td><img src="images/4.png" width="60" class="img-circle" /></td>
                                        <td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
                                    </tr>
                                </table>
                            </li>
                            <li class="news-item">
                                <table cellpadding="4">
                                    <tr>
                                        <td><img src="images/5.png" width="60" class="img-circle" /></td>
                                        <td>LLorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,orem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
                                    </tr>
                                </table>
                            </li>
                            <li class="news-item">
                                <table cellpadding="4">
                                    <tr>
                                        <td><img src="images/6.png" width="60" class="img-circle" /></td>
                                        <td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
                                    </tr>
                                </table>
                            </li>
                            <li class="news-item">
                                <table cellpadding="4">
                                    <tr>
                                        <td><img src="images/7.png" width="60" class="img-circle" /></td>
                                        <td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
                                    </tr>
                                </table>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel-footer">

            </div>
        </div>
        <!-- End news Bar-->
    </div>
</div>



<!-- section begin -->
<section id="section-cta">
    <div class="sep-background-mask"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cal-to-action text-center">
                    <span>We’ve Completed More Than <b>100+</b> place for our amazing clients, If you interested?</span>
                    <a href="#" class="btn btn-border-light">Contact Us</a>
                </div>                            
            </div>
        </div>
    </div>        
</section>
<!-- section close -->






<?php
include'footer.php';
?>