CKEDITOR.plugins.add( 'libraryimage', {
    icons: 'libraryimage',
    init: function( editor ) {
        //Plugin logic goes here.
    	editor.addCommand( 'insertLibraryImage', {
    	    exec: function( editor ) {
    	        var now = new Date();
    	        editor.insertHtml( 'The current date and time is: <em>' + now.toString() + '</em>' );
    	    }
    	});
    	
    	editor.ui.addButton( 'LibraryImage', {
    	    label: 'Insert Library Image',
    	    command: 'insertLibraryImage',
    	    toolbar: 'insert,100'
    	});

    	
    }
});
