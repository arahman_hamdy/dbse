<?php
$baseUrl = Yii::app()->baseUrl;
$themeUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($themeUrl.'/assets/js/selectboxit/jquery.selectBoxIt.css');
$cs->registerCssFile($themeUrl.'/assets/js/select2/select2.css');
$cs->registerCssFile($themeUrl.'/assets/js/selectboxit/jquery.selectBoxIt.css');
$cs->registerCssFile($themeUrl.'/assets/js/icheck/skins/minimal/_all.css');

$cs->registerScriptFile($themeUrl.'/assets/js/select2/select2.min.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/bootstrap-datepicker.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/bootstrap-timepicker.min.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/bootstrap-tagsinput.min.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/icheck/icheck.min.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/selectboxit/jquery.selectBoxIt.min.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/jquery-ui-autocomplete.min.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/typeahead.min.js', CClientScript::POS_END);

// CKeditor WYSIWYG


$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/highstock.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/standalone-framework.js', CClientScript::POS_END);

$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/highcharts-more.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/highcharts-3d.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/data.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/exporting.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/funnel.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/solid-gauge.js', CClientScript::POS_END);


$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/highcharts-editor.js', CClientScript::POS_END);
$cs->registerCssFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/highcharts-editor.min.css');

$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/ckeditor.js?t=2', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/adapters/jquery.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/ckeditor/plugins/highcharts/highcharts-editor.ckeditor.min.js', CClientScript::POS_END);


?>

