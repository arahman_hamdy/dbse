<?php /* @var $this Controller */
	$baseUrl = Yii::app()->baseUrl;
	$themeUrl = Yii::app()->theme->baseUrl;
?>
<?php $this->beginContent('//layouts/base'); ?>
	<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

		<div class="main-content">
					
			<?php if(isset($this->breadcrumbs)):?>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
					'htmlOptions'=>array('class'=>'breadcrumb bc-3'),
					'separator'=>'',
					'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
					'inactiveLinkTemplate'=>'<li class="active"><strong>{label}</strong></li>',
					'homeLink'=>'<li><a href="'.Yii::app()->homeUrl.'">' . Yii::t("app","Home") . '</a></li>',
					'tagName'=>'ol'
				)); ?><!-- breadcrumbs -->
			<?php endif?>
			
			<?php echo $content; ?>
			
			<!-- Footer -->
		</div>
	
	</div>
	


<?php $this->endContent(); ?>

