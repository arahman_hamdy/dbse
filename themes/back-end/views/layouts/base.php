<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$baseUrl = Yii::app()->baseUrl;
		$absoluteUrl = Yii::app()->createAbsoluteUrl('/');
		$themeUrl = Yii::app()->theme->baseUrl;
	?>
	<script src="<?php echo $themeUrl; ?>/assets/js/jquery-1.11.0.min.js"></script>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/css/font-icons/entypo/css/entypo.css">
	<?php /*
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	 * 
	 */ ?>
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/css/neon-core.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/css/neon-theme.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/css/neon-forms.css">
	<?php if (Yii::app()->language=="ar") { ?>
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/css/neon-rtl.css">
	<?php } ?>
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/css/custom.css?v=3">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/css/skins/green.css">

	<?php Yii::app()->clientScript->registerCoreScript('jquery');
		Yii::app()->clientScript->scriptMap=array(
			'jquery-ui.js' => false,
			'jquery-ui.min.js' => false,
		);
	
	?>

	<script>
		jQuery.uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ /]([w.]+)/.exec( ua ) ||
                /(webkit)[ /]([w.]+)/.exec( ua ) ||
                /(opera)(?:.*version|)[ /]([w.]+)/.exec( ua ) ||
                /(msie) ([w.]+)/.exec( ua ) ||
                ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([w.]+)|)/.exec( ua ) ||
                [];

        return {
                browser: match[ 1 ] || "",
                version: match[ 2 ] || "0"
        };
};

// Don't clobber any existing jQuery.browser in case it's different
if ( !jQuery.browser ) {
        matched = jQuery.uaMatch( navigator.userAgent );
        browser = {};

        if ( matched.browser ) {
                browser[ matched.browser ] = true;
                browser.version = matched.version;
        }

        // Chrome is Webkit, but Webkit is also Safari.
        if ( browser.chrome ) {
                browser.webkit = true;
        } else if ( browser.webkit ) {
                browser.safari = true;
        }

        jQuery.browser = browser;
}

	</script>
	
	<!--[if lt IE 9]><script src="<?php echo $themeUrl; ?>/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php 
		Yii::app()->clientScript->registerScript("init","var \$baseUrl='$baseUrl';var \$themeUrl='$themeUrl';var \$absoluteUrl='$absoluteUrl';",CClientScript::POS_HEAD);
	?>
</head>
<body class="page-body <?php if (isset($this->body_class))echo $this->body_class;?> skin-green">


		<?php echo $content; ?>
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>/assets/js/rickshaw/rickshaw.min.css">
	
	<!-- Bottom scripts (common) -->
	<script src="<?php echo $themeUrl; ?>/assets/js/gsap/main-gsap.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/bootstrap.min.js"></script>
	<?php /* * ?>
	<script src="<?php echo $themeUrl; ?>/assets/js/bootstrap-modal.js"></script>
	<?php /* */ ?>
	
	<script src="<?php echo $themeUrl; ?>/assets/js/joinable.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/resizeable.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/neon-api.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<?php if(Yii::app()->urlManager->parseUrl(Yii::app()->request) == "admin/index" || Yii::app()->urlManager->parseUrl(Yii::app()->request) == "admin"){ ?>
	<script src="<?php echo $themeUrl; ?>/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/jquery.sparkline.min.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/rickshaw/vendor/d3.v3.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/rickshaw/rickshaw.min.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/raphael-min.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/morris.min.js"></script>
	<script src="<?php echo $themeUrl; ?>/assets/js/toastr.js"></script>

	<script src="<?php echo $themeUrl; ?>/assets/js/neon-chat.js"></script>
	<?php } ?>

	<!-- JavaScripts initializations and stuff -->
	<?php 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($themeUrl.'/assets/js/neon-custom.js?v=3', CClientScript::POS_END);
	?>

	
	<script type="text/javascript">
		var lib_ret_functions = [];
		var file_browser_url = $baseUrl + "/admin/libCat/editorBrowse";
		
		var libraryFilePopupFunction = function(funcNum, id, thumb_url, img_url){
			return lib_ret_functions[funcNum](id, thumb_url, img_url);
		}
		
		jQuery("div.lib_image button").click(function(e){
			var divObject = $(this).closest("div.lib_image");
			var retfunc = function(id,thumb_url,img_url){
				if (!id){
					return false;
				}
				
				divObject.find(".image-thumb img").attr("src",thumb_url);
				divObject.find(".image-thumb .image").attr("href",img_url);
				divObject.find("input._lib_image_thumb").val(img_url);
				
				divObject.find("input._lib_image_id").val(id);
				divObject.find(".image-thumb").show();
				return true; 
			};
			lib_ret_functions.push(retfunc);
			
			var func_num = lib_ret_functions.length - 1;
		
			var url = file_browser_url + "?libraryFilePopup=1&types[]=i&func_num=" + func_num + "&langCode=<?php echo rawurlencode(Yii::app()->language); ?>";
			//window.open(url);
		    jQuery("#modalExternal .modal-body").html('<iframe width="100%" height="400px" frameborder="0" scrolling="yes" allowtransparency="true" src="'+url+'"></iframe>');
		    jQuery('#modalExternal').modal('show'); 
		    
			
		});
		
		jQuery("div.lib_image .image-thumb .image").click(function(e){
			//e.preventDefault();
		});
		
		jQuery("div.lib_image .image-options .delete").click(function(e){
			e.preventDefault();
			jQuery(this).closest('.image-thumb').hide();
			jQuery(this).closest('.image-thumb').find("img").attr("src","");
			jQuery(this).closest('div.lib_image').find("input").val("");
		});
		
		jQuery("div.lib_video button").click(function(e){
			var divObject = $(this).closest("div.lib_video");
			var retfunc = function(id,thumb_url,img_url){
				if (!id){
					return false;
				}
				
				divObject.find(".image-thumb img").attr("src",thumb_url);
				divObject.find(".image-thumb .image").attr("href",img_url);
				divObject.find("input._lib_video_thumb").val(img_url);
				
				divObject.find("input._lib_video_id").val(id);
				divObject.find(".image-thumb").show();
				return true; 
			};
			lib_ret_functions.push(retfunc);
			
			var func_num = lib_ret_functions.length - 1;
		
			var url = file_browser_url + "?libraryFilePopup=1&types[]=v&types[]=y&func_num=" + func_num + "&langCode=<?php echo rawurlencode(Yii::app()->language); ?>";
			//window.open(url);
		    jQuery("#modalExternal .modal-body").html('<iframe width="100%" height="400px" frameborder="0" scrolling="yes" allowtransparency="true" src="'+url+'"></iframe>');
		    jQuery('#modalExternal').modal('show'); 
		    
			
		});
		
		jQuery("div.lib_video .image-thumb .image").click(function(e){
			//e.preventDefault();
		});
		
		jQuery("div.lib_video .image-options .delete").click(function(e){
			e.preventDefault();
			jQuery(this).closest('.image-thumb').hide();
			jQuery(this).closest('.image-thumb').find("img").attr("src","");
			jQuery(this).closest('div.lib_video').find("input").val("");
		});		

	</script>
</body>
	
</html>