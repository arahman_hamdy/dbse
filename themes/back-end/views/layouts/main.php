<?php /* @var $this Controller */
	$baseUrl = Yii::app()->baseUrl;
	$themeUrl = Yii::app()->theme->baseUrl;
?>
<?php $this->beginContent('//layouts/base'); ?>
	<div class="page-container">

	<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a target="_blank" href="<?php echo $this->createUrl("//") . "/"; ?>">
						<h1><?php echo Yii::t("app","DBSE"); ?></h1>
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
			
			<ul id="main-menu" class="main-menu">
				<li>
					<a href="<?php echo Yii::app()->createAbsoluteUrl("admin") ?>">
						<i class="entypo-gauge"></i>
						<span class="title"><?php echo Yii::t("app","Dashboard"); ?></span>
					</a>
				</li>
				
				<?php /*
				 if (
					Yii::app()->user->can(UserPermissions::Category_Create) ||
					Yii::app()->user->can(UserPermissions::Category_Edit)
				):
				?>
					<li <?php if (in_array($this->id, array('category'))) echo ' class="opened" ';?>>
						<a href="" target="_blank">
							<i class="entypo-list"></i>
							<span class="title"><?php echo Yii::t("app","Categories"); ?></span>
						</a>
						<ul>
							<li <?php if ("{$this->id}/{$this->action->id}"=='category/index') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/category") ?>"><?php echo Yii::t('app','Categories list') ?></a></li>
							<?php if (Yii::app()->user->can(UserPermissions::Category_Create)): ?>
								<li <?php if ("{$this->id}/{$this->action->id}"=='category/create') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/category/create") ?>"><?php echo Yii::t('app','New Category') ?></a></li>
							<?php endif;?>
						</ul>
					</li>
				<?php endif ?>
				
				<?php if (Yii::app()->user->RoleId == 1): ?>										
					<li <?php if (in_array($this->id, array('block'))) echo ' class="opened" ';?>>
						<a href="" target="_blank">
							<i class="entypo-layout"></i>
							<span class="title"><?php echo Yii::t("app","Blocks"); ?></span>
						</a>
						<ul>
							<li <?php if ("{$this->id}/{$this->action->id}"=='block/index') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/block") ?>"><?php echo Yii::t('app','Blocks list') ?></a></li>
							<li <?php if ("{$this->id}/{$this->action->id}"=='block/create') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/block/create") ?>"><?php echo Yii::t('app','New Block') ?></a></li>
						</ul>
					</li>				

				<?php endif; */ ?>

				<?php if (
					Yii::app()->user->can(UserPermissions::Translation_All)
				):
				?>
					<li <?php if (in_array($this->id, array('translation'))) echo ' class="opened" ';?>>
						<a href="" target="_blank">
							<i class="entypo-language"></i>
							<span class="title"><?php echo Yii::t("app","Translations"); ?></span>
						</a>
						<ul>
							<li <?php if ("{$this->id}/{$this->action->id}"=='translation/index') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/translation/index") ?>"><?php echo Yii::t('app','Manage translations') ?></a></li>
						</ul>
					</li>				
				<?php endif; ?>

				<?php if (Yii::app()->user->RoleId == 1): ?>										
					<li <?php if (in_array($this->id, array('advertisement'))) echo ' class="opened" ';?>>
						<a href="" target="_blank">
							<i class="entypo-layout"></i>
							<span class="title"><?php echo Yii::t("app","Advertisements"); ?></span>
						</a>
						<ul>
							<li <?php if ("{$this->id}/{$this->action->id}"=='advertisement/index') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/advertisement/index") ?>"><?php echo Yii::t('app','Manage advertisements') ?></a></li>
						</ul>
					</li>				

					<li <?php if (in_array($this->id, array('slider','sliderImage'))) echo ' class="opened" ';?>>
						<a href="" target="_blank">
							<i class="entypo-picture"></i>
							<span class="title"><?php echo Yii::t("app","Sliders"); ?></span>
						</a>
						<ul>
							<li <?php if ("{$this->id}/{$this->action->id}"=='sliderImage/index') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/sliderImage/index",array("sliderId"=>1)) ?>"><?php echo Yii::t('app','Universities slider') ?></a></li>
						</ul>
					</li>				
				<?php endif; ?>

				<?php if (Yii::app()->user->RoleId == 1): ?>										
				<li <?php if (in_array($this->id, array('regions','cities'))) echo ' class="opened" ';?>>
					<a href="" target="_blank">
						<i class="entypo-globe"></i>
						<span class="title"><?php echo Yii::t("app","Cities & Regions"); ?></span>
					</a>
					<ul>
						<li <?php if ($this->id=='cities') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/cities") ?>"><?php echo Yii::t('app','Manage Cities') ?></a></li>

						<li <?php if ($this->id=='regions') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/regions") ?>"><?php echo Yii::t('app','Manage Regions') ?></a></li>
					</ul>
				</li>
				<?php endif; ?>

				<?php if (
					Yii::app()->user->can(UserPermissions::Place_Create) ||
					Yii::app()->user->can(UserPermissions::Place_Publish) ||
					Yii::app()->user->can(UserPermissions::Place_Edit) ||
					Yii::app()->user->can(UserPermissions::Offer_Create) ||
					Yii::app()->user->can(UserPermissions::Offer_Publish) ||
					Yii::app()->user->can(UserPermissions::Offer_Edit) ||
					Yii::app()->user->can(UserPermissions::Offer_Delete) ||
					Yii::app()->user->can(UserPermissions::Booking_View) ||
					Yii::app()->user->can(UserPermissions::Booking_Delete)
				):
				?>

				<li <?php if (in_array($this->id, array('places','targets','properties','booking','room','service','premium','offer'))) echo ' class="opened" ';?>>
					<a href="" target="_blank">
						<i class="entypo-briefcase"></i>
						<span class="title"><?php echo Yii::t("app","Places"); ?></span>
					</a>
					<ul>

						<li <?php if ($this->id=='places' && $this->action->id!='pending') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/places") ?>"><?php echo Yii::t('app','Manage Places') ?></a></li>

						<li <?php if ($this->id=='places' && $this->action->id=='pending') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/places/pending") ?>"><?php echo Yii::t('app','Pending Places') ?></a></li>

					<?php if (Yii::app()->user->RoleId == 1): ?>										

						<li <?php if ($this->id=='properties') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/properties") ?>"><?php echo Yii::t('app','Manage Properties') ?></a></li>						

						<li <?php if ($this->id=='targets') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/targets") ?>"><?php echo Yii::t('app','Manage Targets') ?></a></li>
					<?php endif; ?>

						<li <?php if ($this->id=='offer' && $this->action->id!='pending') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/offer") ?>"><?php echo Yii::t('app','Manage Offers') ?></a></li>

						<li <?php if ($this->id=='offer' && $this->action->id=='pending') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/offer/pending") ?>"><?php echo Yii::t('app','Pending Offers') ?></a></li>

						<li <?php if ($this->id=='booking') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/booking") ?>"><?php echo Yii::t('app','List Bookings') ?></a></li>

					<?php if (Yii::app()->user->RoleId == 1): ?>										
						<li <?php if ($this->id=='premium') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/premium") ?>"><?php echo Yii::t('app','List Premiums') ?></a></li>
					<?php endif; ?>

					</ul>
				</li>
				<?php endif; ?>

				<?php if (
					Yii::app()->user->can(UserPermissions::Entity_Create_Egypt) ||
					Yii::app()->user->can(UserPermissions::Entity_Publish_Egypt) ||
					Yii::app()->user->can(UserPermissions::Entity_Edit_Egypt) ||
					Yii::app()->user->can(UserPermissions::Entity_Delete_Egypt) ||
					Yii::app()->user->can(UserPermissions::Entity_Create_Saudi) ||
					Yii::app()->user->can(UserPermissions::Entity_Publish_Saudi) ||
					Yii::app()->user->can(UserPermissions::Entity_Edit_Saudi) ||
					Yii::app()->user->can(UserPermissions::Entity_Delete_Saudi)
				):
				?>
				<li <?php if (in_array($this->id, array('entity','entityType','entityTypeAttribute'))) echo ' class="opened" ';?>>
					<a href="" target="_blank">
						<i class="entypo-graduation-cap"></i>
						<span class="title"><?php echo Yii::t("app","Entities"); ?></span>
					</a>
					<ul>

						<li <?php if ($this->id=='entity') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/entity") ?>"><?php echo Yii::t('app','Entities') ?></a></li>

					<?php if (Yii::app()->user->RoleId == 1): ?>										

						<li <?php if ($this->id=='entityType') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/entityType") ?>"><?php echo Yii::t('app','Entity types') ?></a></li>

						<li <?php if ($this->id=='entityCategory') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/entityCategory") ?>"><?php echo Yii::t('app','Entity categories') ?></a></li>
					<?php endif; ?>

					</ul>
				</li>
				<?php endif; ?>

				<?php if (
					Yii::app()->user->can(UserPermissions::School_Create) ||
					Yii::app()->user->can(UserPermissions::School_Publish) ||
					Yii::app()->user->can(UserPermissions::School_Edit) ||
					Yii::app()->user->can(UserPermissions::School_Delete)
				):
				?>
				<li <?php if (in_array($this->id, array('school'))) echo ' class="opened" ';?>>
					<a href="" target="_blank">
						<i class="entypo-book"></i>
						<span class="title"><?php echo Yii::t("app","Schools"); ?></span>
					</a>
					<ul>

						<li <?php if ($this->id=='school') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/school") ?>"><?php echo Yii::t('app','Manage Schools') ?></a></li>

					</ul>
				</li>
				<?php endif; ?>

				<?php if (
					Yii::app()->user->can(UserPermissions::BlogArticle_Create) ||
					Yii::app()->user->can(UserPermissions::BlogArticle_Publish) ||
					Yii::app()->user->can(UserPermissions::BlogArticle_Edit) ||
					Yii::app()->user->can(UserPermissions::BlogArticle_Delete)
				):
				?>
				<li <?php if (in_array($this->id, array('blogArticle','blogCategory','blogArticleRelations','blogMenu','block'))) echo ' class="opened" ';?>>
					<a href="" target="_blank">
						<i class="entypo-newspaper"></i>
						<span class="title"><?php echo Yii::t("app","Blog"); ?></span>
					</a>
					<ul>

						<li <?php if ($this->id=='blogArticle' && $this->action->id!='pending') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/blogArticle") ?>"><?php echo Yii::t('app','Articles') ?></a></li>

						<li <?php if ($this->id=='blogArticle' && $this->action->id=='pending') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/blogArticle/pending") ?>"><?php echo Yii::t('app','Pending Articles') ?></a></li>

					<?php if (Yii::app()->user->RoleId == 1): ?>										

						<li <?php if ($this->id=='blogCategory') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/blogCategory") ?>"><?php echo Yii::t('app','Categories') ?></a></li>

						<li <?php if ($this->id=='blogMenu') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/blogMenu") ?>"><?php echo Yii::t('app','Menus') ?></a></li>						

						<li <?php if ($this->id=='block') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/block") ?>"><?php echo Yii::t('app','Blocks') ?></a></li>						
					<?php endif; ?>

					</ul>
				</li>
				<?php endif; ?>

				<?php if (
					Yii::app()->user->can(UserPermissions::Event_Create) ||
					Yii::app()->user->can(UserPermissions::Event_Publish) ||
					Yii::app()->user->can(UserPermissions::Event_Edit) ||
					Yii::app()->user->can(UserPermissions::Event_Delete)
				):
				?>

				<li <?php if (in_array($this->id, array('event','eventRegistration'))) echo ' class="opened" ';?>>
					<a href="" target="_blank">
						<i class="entypo-bell"></i>
						<span class="title"><?php echo Yii::t("app","Events"); ?></span>
					</a>
					<ul>

						<li <?php if ($this->id=='event' && $this->action->id!='pending') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/event") ?>"><?php echo Yii::t('app','Manage Events') ?></a></li>

						<li <?php if ($this->id=='event' && $this->action->id=='pending') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/event/pending") ?>"><?php echo Yii::t('app','Pending Events') ?></a></li>

						<li <?php if ($this->id=='eventRegistration') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/eventRegistration") ?>"><?php echo Yii::t('app','List Events Registrations') ?></a></li>

					</ul>
				</li>
				<?php endif; ?>

				<?php if (
					Yii::app()->user->can(UserPermissions::Transaction_View)
				):
				?>
				<li <?php if (in_array($this->id, array('transaction'))) echo ' class="opened" ';?>>
					<a href="" target="_blank">
						<i class="entypo-credit-card"></i>
						<span class="title"><?php echo Yii::t("app","Transactions"); ?></span>
					</a>
					<ul>

						<li <?php if ($this->id=='transaction') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/transaction") ?>"><?php echo Yii::t('app','View Transactions') ?></a></li>

					</ul>
				</li>
				<?php endif; ?>

				<?php if (
					Yii::app()->user->can(UserPermissions::User_Create) ||
					Yii::app()->user->can(UserPermissions::User_Edit) ||
					Yii::app()->user->can(UserPermissions::User_Delete)
				):
				?>
					<li <?php if (in_array($this->id, array('roles','users','profileField'))) echo ' class="opened" ';?>>
						<a href="" target="_blank">
							<i class="entypo-users"></i>
							<span class="title"><?php echo Yii::t("app","Users"); ?></span>
						</a>
						<ul>
							<?php  if (Yii::app()->user->RoleId == 1): ?>
								<li <?php if ($this->id=='roles') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/roles") ?>"><?php echo Yii::t('app','Manage Roles') ?></a></li>
							<?php endif; ?>
							
							<li <?php if ($this->id=='users') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/users") ?>"><?php echo Yii::t('app','Manage Users') ?></a></li>

							<li <?php if ($this->id=='profileField') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("user/profileField") ?>"><?php echo Yii::t('app','Manage Profile Fields') ?></a></li>
						</ul>
					</li>
				<?php endif; ?>					

				<?php if (Yii::app()->user->RoleId == 1): ?>

					<li <?php if (in_array($this->id, array('searchHistory'))) echo ' class="opened" ';?>>
						<a href="" target="_blank">
							<i class="entypo-doc-text"></i>
							<span class="title"><?php echo Yii::t("app","Contact Form"); ?></span>
						</a>
						<ul>
							<li <?php if ("{$this->id}/{$this->action->id}"=='contactFormElements/index') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/contactFormElements/index") ?>"><?php echo Yii::t('app','Contact Form Elements') ?></a></li>
						</ul>
					</li>				
					
					<li <?php if (in_array($this->id, array('searchHistory'))) echo ' class="opened" ';?>>
						<a href="" target="_blank">
							<i class="entypo-doc-text"></i>
							<span class="title"><?php echo Yii::t("app","Logs"); ?></span>
						</a>
						<ul>
							<li <?php if ("{$this->id}/{$this->action->id}"=='searchHistory/index') echo ' class="active" ';?>><a href="<?php echo Yii::app()->createAbsoluteUrl("admin/searchHistory/index") ?>"><?php echo Yii::t('app','Search History') ?></a></li>
						</ul>
					</li>				


				<?php endif; ?>

			</ul>
			
		</div>

	</div>

	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
		
				<ul class="user-info pull-left pull-none-xsm">
		
					<!-- Profile Info -->
					<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?php echo $themeUrl; ?>/assets/images/user.png" alt="" class="img-circle" width="44" />
							<strong><?php echo Yii::app()->user->name; ?></strong>
						</a>
							
						<!-- 
						<ul class="dropdown-menu">
		
							<!-- Reverse Caret -- >
							<li class="caret"></li>
		
							<!-- Profile sub-links -- >
							<li>
								<a href="extra-timeline.html">
									<i class="entypo-user"></i>
									Edit Profile
								</a>
							</li>
						</ul>
						 -->
					</li>
		
				</ul>
				
						
			</div>
		
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">
		
					<!-- Language Selector -->
					<li class="dropdown language-selector">
						<?php /*
						<?php echo Yii::t("app","Language");?>: &nbsp;
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
							<?php if (Yii::app()->language == "ar"):?>
								<img src="<?php echo $themeUrl; ?>/assets/images/flag-eg.png" />
							<?php else: ?>
								<img src="<?php echo $themeUrl; ?>/assets/images/flag-uk.png" />
							<?php endif;?>
						</a>
		
						<?php
						parse_str($_SERVER['QUERY_STRING'],$queryArray);
						unset($queryArray['language']);
						$queryArray['language'] = "__lang__";
						$req_array = explode("?",$_SERVER["REQUEST_URI"]);
						$languageUrl = $req_array[0] . "?" . http_build_query($queryArray);
						 
						?>
						<ul class="dropdown-menu pull-right">
							<li <?php if (Yii::app()->language=="en")echo ' class="active" '; ?>>
								<a href="<?php echo str_replace("__lang__",'en',$languageUrl); ?>">
									<img src="<?php echo $themeUrl; ?>/assets/images/flag-uk.png" />
									<span>English</span>
								</a>
							</li>
							<li <?php if (Yii::app()->language=="ar")echo ' class="active" '; ?>>
								<a href="<?php echo str_replace("__lang__",'ar',$languageUrl); ?>">
									<img src="<?php echo $themeUrl; ?>/assets/images/flag-eg.png" />
									<span>العربية</span>
								</a>
							</li>
						</ul>
						<?php */ ?>
		
					</li>
		
					<li class="sep"></li>
		
					<li>
						<a href="<?php echo $this->createUrl("logout/"); ?>"> <?php echo Yii::t("app","Log Out");?> <i class="entypo-logout right"></i></a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
		
		<?php if(isset($this->breadcrumbs)):?>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array(
				'links'=>$this->breadcrumbs,
				'htmlOptions'=>array('class'=>'breadcrumb bc-3'),
				'separator'=>'',
				'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
				'inactiveLinkTemplate'=>'<li class="active"><strong>{label}</strong></li>',
				'homeLink'=>'<li><a href="'.((is_array(Yii::app()->homeUrl))?$this->createUrl(Yii::app()->homeUrl[0]): Yii::app()->homeUrl).'">' . Yii::t("app","Home") . '</a></li>',
				'tagName'=>'ol'
			)); ?><!-- breadcrumbs -->
		<?php endif?>
		
		<?php echo $content; ?>
		
		<!-- Footer -->
	</div>

		
	<div id="chat" class="fixed" data-current-user="Art Ramadani" data-order-by-status="1" data-max-chat-history="25">
	
		<div class="chat-inner">
	
	
			<h2 class="chat-header">
				<a href="#" class="chat-close"><i class="entypo-cancel"></i></a>
	
				<i class="entypo-users"></i>
				Chat
				<span class="badge badge-success is-hidden">0</span>
			</h2>
	
	
			<div class="chat-group" id="group-1">
				<strong>Favorites</strong>
	
				<a href="#" id="sample-user-123" data-conversation-history="#sample_history"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
			</div>
	
	
			<div class="chat-group" id="group-2">
				<strong>Work</strong>
	
				<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
				<a href="#" data-conversation-history="#sample_history_2"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
			</div>
	
	
			<div class="chat-group" id="group-3">
				<strong>Social</strong>
	
				<a href="#"><span class="user-status is-busy"></span> <em>Velma G. Pearson</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Margaret R. Dedmon</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Kathleen M. Canales</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Tracy J. Rodriguez</em></a>
			</div>
	
		</div>
	
		<!-- conversation template -->
		<div class="chat-conversation">
	
			<div class="conversation-header">
				<a href="#" class="conversation-close"><i class="entypo-cancel"></i></a>
	
				<span class="user-status"></span>
				<span class="display-name"></span>
				<small></small>
			</div>
	
			<ul class="conversation-body">
			</ul>
	
			<div class="chat-textarea">
				<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
			</div>
	
		</div>
	
	</div>
	
	
	<!-- Chat Histories -->
	<ul class="chat-history" id="sample_history">
		<li>
			<span class="user">Art Ramadani</span>
			<p>Are you here?</p>
			<span class="time">09:00</span>
		</li>
	
		<li class="opponent">
			<span class="user">Catherine J. Watkins</span>
			<p>This message is pre-queued.</p>
			<span class="time">09:25</span>
		</li>
	
		<li class="opponent">
			<span class="user">Catherine J. Watkins</span>
			<p>Whohoo!</p>
			<span class="time">09:26</span>
		</li>
	
		<li class="opponent unread">
			<span class="user">Catherine J. Watkins</span>
			<p>Do you like it?</p>
			<span class="time">09:27</span>
		</li>
	</ul>
	
	
	
	
	<!-- Chat Histories -->
	<ul class="chat-history" id="sample_history_2">
		<li class="opponent unread">
			<span class="user">Daniel A. Pena</span>
			<p>I am going out.</p>
			<span class="time">08:21</span>
		</li>
	
		<li class="opponent unread">
			<span class="user">Daniel A. Pena</span>
			<p>Call me when you see this message.</p>
			<span class="time">08:27</span>
		</li>
	</ul>

	
	<!-- Sample Modal (Default skin) -->
	<div class="modal fade" id="sample-modal-dialog-1">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Widget Options - Default Modal</h4>
				</div>
				
				<div class="modal-body">
					<p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Sample Modal (Skin inverted) -->
	<div class="modal invert fade" id="sample-modal-dialog-2">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Widget Options - Inverted Skin Modal</h4>
				</div>
				
				<div class="modal-body">
					<p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Sample Modal (Skin gray) -->
	<div class="modal gray fade" id="sample-modal-dialog-3">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Widget Options - Gray Skin Modal</h4>
				</div>
				
				<div class="modal-body">
					<p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modalExternal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:90%;height:90%">
			<div class="modal-content">
				
				<div class="modal-header">
					<button id="modalExternal_close" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Library</h4>
				</div>
				
				<div class="modal-body">
					
				</div>
							</div>
		</div>
	</div><!-- /.modal -->
	</div>
<?php $this->endContent(); ?>

