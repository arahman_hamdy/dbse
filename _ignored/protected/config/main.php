<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Database for Students and Entrepreneurs',
	'theme'=>'front',
	'language'=>'ar',
	'sourceLanguage' => 'en',
	// preloading 'log' component
	'preload'=>array('log','tstranslation'),
	'homeUrl' => array('/site/index'),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',
		'ext.giix-components.*', // giix components
	),
		
	'controllerMap' => array(
			'tstranslation' => 'tstranslation.controllers.TsTranslationController'
	),
	
	'modules'=>array(
		'admin',			
		'user'=>array(
				# encrypting method (php hash function)
				'hash' => 'md5',
		
				# send activation email
				'sendActivationMail' => false,
		
				# allow access for non-activated users
				'loginNotActiv' => true,
		
				# activate user on registration (only sendActivationMail = false)
				'activeAfterRegister' => true,
		
				# automatically login from registration
				'autoLogin' => true,
		
				# registration path
				'registrationUrl' => array('/user/registration'),
		
				# recovery password path
				'recoveryUrl' => array('/user/recovery'),
		
				# login form path
				'loginUrl' => array('/user/login'),
		
				# page after login
				'returnUrl' => array('/user/profile'),
		
				# page after logout
				'returnLogoutUrl' => array('/user/login'),
		),
		#...
				
			
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'1234',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			
			'generatorPaths' => array(
					'ext.giix-core', // giix generators
			),			
		),
		
	),

	// application components
	'components'=>array(
		'cache'=>array(
				'class'=>'system.caching.CDummyCache',
		),
		'tstranslation'=>array(
				/**
				 * Set `tstranslation` class
		*/
				'class' => 'ext.tstranslation.components.TsTranslation',
		
				/**
				 * Set `accessRules` parameter (NOT REQUIRED),
		* parameter effects to dynamic content translation and language managment
		*
		* AVAILABLE VALUES:
		* - '*' means all users
		* - '@' means all registered users
		* - `username`. Example: 'admin' means Yii::app()->user->name === 'admin'
		* - `array of usernames`. Example: array('admin', 'manager') means in_array(array('admin', 'manager'), Yii::app()->user->name)
		* - your custom expression. Example: array('expression' => 'Yii::app()->user->role === "admin"')
		* DEFAULT VALUE: '@'
		*/
				'accessRules' => '@',
		
				/**
				 * Set `languageChangeFunction` (NOT REQUIRED),
		* function processing language change
		*
		* AVAILABLE VALUES:
		* - `true` means uses extension internal function (RECOMENDED)
		* - `array()` means user defined function. Example: array('TestClass', 'testMethod'), 'TestClass' and 'testMethod' must be exist and imported to project
		* DEFAULT VALUE: `true`
		*/
				'languageChangeFunction' => true,
		),
		
		/**
		 * Add `messages` component
		 */
		'messages' => array(
				/**
				 * Set `messages` class
		*/
				'class' => 'TsDbMessageSource',
		
				/**
				 * Set `Missing Messages` translation action
		*/
				'onMissingTranslation' => array('TsTranslation', 'addTranslation'),
		
				/**
				 * Set `notTranslatedMessage` parameter (NOT REQUIRED),
		*
		* AVAILABLE VALUES:
		* - `false / null` means nothing shows if message translation is empty
		* - `text` means shows defined text if message translation is empty.
		*      Example: 'Not translated data!'
		* DEFAULT VALUE: `null`
		*/
				'notTranslatedMessage' => 'Not translated data!',
		
				/**
				 * Set `ifNotTranslatedShowDefault` parameter (NOT REQUIRED),
		*
		* AVAILABLE VALUES:
		* - `false` means shows `$this->notTranslatedMessage` if message translation is empty
		* - `true` means shows default language translation if message translation is empty.
		* DEFAULT VALUE: `true`
		*/
				'ifNotTranslatedShowDefault' => false,
		
		),
		
		
		
		'upload'=>array(
				'class' => 'application.components.CUpload',
		),
		'thumb'=>array(
				'class' => 'application.components.CThumb',
		),
		'simpleImage'=>array(
				'class' => 'application.extensions.simpleImage.CSimpleImage',
		),
		
		'user'=>array(
			'class' => 'WebUser',
		
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			/**
			 * Set `urlManager` class
			 */
			'class' => 'TsUrlManager',
			
			/**
			 * Set `showLangInUrl` parameter (NOT REQUIRED),
			 *
			 * AVAILABLE VALUES:
			 * - `true` means language code shows in url. Example: .../mysite/en/article/create
			 * - `false` means language code not shows in url. Example: .../mysite/article/create
			 * DEFAULT VALUE: `true`
			 */
			'showLangInUrl' => false,
			
			/**
			 * Set `prependLangRules` parameter (NOT REQUIRED),
			 * this parameter takes effect only if `showLangInUrl` parameter is `true`.
			 * It strongly recomended to add language rule to `rules` parameter handly
			 *
			 * AVAILABLES VALUES:
			 * - `true` means automaticly prepends `_lang` parameter before all rules.
			 *      Example: '<_lang:\w+><controller:\w+>/<id:\d+>' => '<controller>/view',
			 * - `false` means `_lang` parameter you must add handly
			 * DEFAULT VALUE: `true`
			 */
			'prependLangRules' => false,
			
			/******************************/
			
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				'/news/<slug:.+>'=>'/article/viewBySlug',
				'/page/<slug:.+>'=>'/page/viewBySlug',
				'author/<id:\d+>'=>'auther/view',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=dbse',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '123456',
			'charset' => 'utf8',
			'tablePrefix' => '',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
					'filter'=>array(
                        'class'=>'LogFilter',
                        'ignoreCategories'=>array(
                               'exception.CHttpException.*'
                                /*,'some.category.*'*/
                        )
                    ),
				),
				// uncomment the following to show log messages on web pages
				/* *
				array(
					'class'=>'CWebLogRoute',
				),
				/* */
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);