<?php

Yii::import('application.models._base.BaseBlockTemplate');

class BlockTemplate extends BaseBlockTemplate
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public static function label($n = 1) {
		return Yii::t('app', 'Block template|Block templates', $n);
	}
	
}