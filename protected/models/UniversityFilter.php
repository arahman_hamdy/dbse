<?php

class UniversityFilter extends Entity
{

	public function rules() {
		return array_merge(array(
			array('city,region_id', 'numerical', 'integerOnly'=>true),
		),
		array(
			array('type, parent_id, has_contents, region_id, sortid, is_active, is_deleted', 'numerical', 'integerOnly'=>true),
			array('lat, lng', 'numerical'),
			array('name_ar, name_en, slug', 'length', 'max'=>50),
			array('main_image_id', 'length', 'max'=>20),
			array('keywords, _location, _album', 'length', 'max'=>100),
			array('description', 'length', 'max'=>200),
			array('contents_ar, contents_en, dateline', 'safe'),
			array('parent_id, has_contents, slug, main_image_id, contents_ar, contents_en, keywords, description, region_id, lat, lng, sortid, is_active, is_deleted, dateline, _location, _album', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, type, name_ar, name_en, parent_id, has_contents, slug, main_image_id, contents_ar, contents_en, keywords, description, region_id, lat, lng, sortid, is_active, is_deleted, dateline, _location, _album', 'safe', 'on'=>'search'),
		));
	}


	public function attributeLabels() {
		return array(
		) + parent::attributeLabels();
	}

}