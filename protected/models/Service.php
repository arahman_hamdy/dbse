<?php

Yii::import('application.models._base.BaseService');

class Service extends BaseService
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}

	public function search() {
		$dp = parent::search();
		$dp->criteria->compare("t.is_deleted", 0);
		return $dp;
	}
}