<?php

Yii::import('application.models._base.BaseEntityDivision');

class EntityDivision extends BaseEntityDivision
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'title_' . Yii::app()->language;
	}
}