<?php

Yii::import('application.models._base.BaseEntityCategory');

class EntityCategory extends BaseEntityCategory
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'title_' . Yii::app()->language;
	}
}