<?php

Yii::import('application.models._base.BasePage');

class Page extends BasePage
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public function rules() {
		return array_merge(parent::rules(),array(
				array('slug', 'match', 'pattern' => '/^[a-zA-Z0-9\-\_]{3,50}$/'),
		));
	}
	
}