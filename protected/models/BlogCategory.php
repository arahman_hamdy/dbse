<?php

Yii::import('application.models._base.BaseBlogCategory');

class BlogCategory extends BaseBlogCategory
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}

	public function attributeLabels(){
		return array(
				'parent_id' =>  Yii::t('app', 'Parent category'),
				'parent' => Yii::t('app', 'Parent category'),
		) + parent::attributeLabels();
	}
	
	public function relations(){
		return array(
			'childrenSorted' => array(self::HAS_MANY, 'BlogCategory', 'parent_id','scopes'=>array(
                        'scopes'=>'sorted'
                ),),
		) + parent::relations();
	}
	
	public function scopes()
	{
		return array(
				'sorted'=>array(
						'order'=>'sortid',
				),
		);
	}
	
	private static $catTree = array();
	public static function getCategoryTree() {
		if (empty(self::$catTree)) {
			$rows = self::model()->sorted()->findAll('parent_id IS NULL');
			foreach ($rows as $item) {
				self::$catTree[] = self::getCategoryItems($item);
			}
		}
		return self::$catTree;
	}

	private static function getCategoryItems($modelRow) {
	
		if (!$modelRow)
			return;
	
		if (isset($modelRow->childrenSorted)) {
			$chump = self::getCategoryItems($modelRow->childrenSorted);
			if ($chump != null)
				$res = array('id'=>$modelRow->id, 'label' => (string)$modelRow, 'items' => $chump, 'url' => Yii::app()->createUrl('category/view', array('id' => $modelRow->id)));
			else
				$res = array('id'=>$modelRow->id,'label' => (string)$modelRow, 'url' => Yii::app()->createUrl('category/view', array('id' => $modelRow->id)));
			return $res;
		} else {
			if (is_array($modelRow)) {
				$arr = array();
				foreach ($modelRow as $leaves) {
					$arr[] = self::getCategoryItems($leaves);
				}
				return $arr;
			} else {
				return array('id'=>$modelRow->id,'label' => (string)$modelRow, 'url' => Yii::app()->createUrl('category/view', array('id' => $modelRow->id)));
			}
		}
	}
	
	
	public static function getListTreeView() {
		if (empty(self::$catTree)) {
			self::getCategoryTree();
		}
		return self::visualTree(self::$catTree, 0);
	}
	
	private static function visualTree($catTree, $level) {
		$res = array();
		foreach ($catTree as $item) {
			$res[$item['id']] = '' . str_pad('', $level * 2, '-') . ' ' . $item['label'];
			if (isset($item['items'])) {
				$res_iter = self::visualTree($item['items'], $level + 1);
				foreach ($res_iter as $key => $val) {
					$res[$key] = $val;
				}
			}
		}
		return $res;
	}

	public function getUrl()
	{
		return Yii::app()->createUrl('blog/category', array('id' => $this->id, 'title' => (string)$this));
	}

	public function getShortUrl()
	{
		return Yii::app()->createUrl('blog/category', array('id' => $this->id));
	}
}