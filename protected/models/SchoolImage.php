<?php

Yii::import('application.models._base.BaseSchoolImage');

class SchoolImage extends BaseSchoolImage
{
	public function relations() {
		return array(
			'image' => array(self::BELONGS_TO, 'LibFile', 'image_id'),
		) + parent::relations();
	}

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}