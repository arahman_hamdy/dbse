<?php

Yii::import('application.models._base.BaseBooking');

class Booking extends BaseBooking
{
	public $date;
	public $time_from;
	public $time_to;
	public $owner_id;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array_merge(parent::rules(), array(
			array('name', 'length', 'min'=>7),
			array('phone', 'length', 'min'=>9),
			array('email', 'email'),
			array('persons', 'capacityValidator','room_id'),
			array('persons', 'numerical', 'integerOnly'=>true, 'min'=>1, 'max'=>99),
			array('date, time_from, time_to', 'required', 'on'=>'insert'),
			array('start_date', 'availableValidator','end_date')
		));
	}

	public function availableValidator($attribute,$params)
	{
		$start = $this->$attribute;
		$end = $this->{$params[0]};

		$criteria = new CDbCriteria;
		$criteria->compare('status' , 1);
		$criteria->compare('start_date','<' . $end);
		$criteria->compare('end_date','>' . $start);

		if (Booking::model()->exists($criteria)){
	    	$this->addError($attribute, Yii::t('app', 'This time slot already booked!'));
	    }
	}

	public function capacityValidator($attribute,$params)
	{
		$persons = $this->$attribute;
		$room_id = $this->{$params[0]};
		$room = Room::model()->cache(2)->findByPK($room_id);
		
		if (!$room) return true;

		if ($persons>$room->capacity){
	    	$this->addError($attribute, Yii::t('app', 'This room capacity is only {num}!', array('{num}'=>$room->capacity)));
	    }
	}

	public function attributeLabels() {
		return array(
			'date' => Yii::t('app', 'Date'),
			'time_from' => Yii::t('app', 'Time from'),
			'time_to' => Yii::t('app', 'Time to'),
			'room_id' => Yii::t('app', 'Room'),
		) + parent::attributeLabels();
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('t.id', $this->id);
		$criteria->compare('t.place_id', $this->place_id);
		$criteria->compare('t.offer_id', $this->offer_id);
		$criteria->compare('t.room_id', $this->room_id);
		$criteria->compare('t.user_id', $this->user_id);
		$criteria->compare('t.transaction_id', $this->transaction_id);
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.phone', $this->phone, true);
		$criteria->compare('t.email', $this->email, true);
		$criteria->compare('t.price', $this->price);
		$criteria->compare('t.total', $this->total);
		$criteria->compare('t.start_date', $this->start_date, true);
		$criteria->compare('t.end_date', $this->end_date, true);
		$criteria->compare('t.persons', $this->persons);
		$criteria->compare('t.notes', $this->notes, true);
		$criteria->compare('t.is_paid', $this->is_paid);
		$criteria->compare('t.status', $this->status);
		$criteria->compare('t.dateline', $this->dateline, true);

		if ($this->owner_id){
			$criteria->with[] = 'place';
			$criteria->together = true;
			$criteria->compare('place.user_id', $this->owner_id);
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort'=>array(
				'defaultOrder' => 't.id desc',
			),
		));
	}

}