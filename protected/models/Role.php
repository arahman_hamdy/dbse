<?php

Yii::import('application.models._base.BaseRole');

class Role extends BaseRole
{
	public $permissions = array();
	
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public function rules() {
		return array_merge(parent::rules(),array(
				array('permissions', 'safe'),
		));
	}
	
	public function attributeLabels(){
		return array(
				'permissions' =>  Yii::t('UserModule', 'Permissions'),
		) + parent::attributeLabels();
	}
}