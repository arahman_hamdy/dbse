<?php

Yii::import('application.models._base.BaseSearchHistory');

class SearchHistory extends BaseSearchHistory
{
	public static function label($n = 1) {
		return Yii::t('app', 'Search History');
	}

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('department', $this->department, true);
		$criteria->compare('query', $this->query, true);
		$criteria->compare('advanced', $this->advanced, true);
		$criteria->compare('dateline', $this->dateline, true);
		$criteria->order = "id desc";

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

}