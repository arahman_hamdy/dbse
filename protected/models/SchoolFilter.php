<?php

class SchoolFilter extends School
{
	public $classification;
	public $certificate;

	public function rules() {
		return array_merge(array(
			array('city,region_id', 'numerical', 'integerOnly'=>true),
			array('founded', 'length', 'max'=>4),
		),parent::rules());
	}


	public function attributeLabels() {
		return array(
		) + parent::attributeLabels();
	}

}