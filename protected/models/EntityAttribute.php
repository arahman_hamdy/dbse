<?php

Yii::import('application.models._base.BaseEntityAttribute');

class EntityAttribute extends BaseEntityAttribute
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'value_' . Yii::app()->language;
	}

}