<?php

Yii::import('application.models._base.BaseLibCat');

class LibCat extends BaseLibCat
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public static function label($n = 1) {
		return Yii::t('app', 'Library album|Library albums', $n);
	}
	
	public function search() {
		$criteria = new CDbCriteria;
	
		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('thumb_file', $this->thumb_file, true);
		
		if ($this->parent_cat_id == "NULL")
			$criteria->addCondition('parent_cat_id is null');
		else
			$criteria->compare('parent_cat_id', $this->parent_cat_id);
		$criteria->compare('sortid', $this->sortid);
		$criteria->compare('is_deleted', 0);
		
		$criteria->order = 'sortid,title';
		
		return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			    'pagination'=>array(
			      'pageSize'=>120,
			    ),
		));
	}
	
	public function attributeLabels(){
		return array(
				'parent_cat_id' =>  Yii::t('app', 'Parent album'),
				'parentCat' => Yii::t('app', 'Parent album'),
		) + parent::attributeLabels();
	}
	
	
	public function relations(){
		return array(
				'childrenSorted' => array(self::HAS_MANY, 'LibCat', 'parent_cat_id','scopes'=>array(
						'scopes'=>'sorted'
				),),
		) + parent::relations();
	}
	
	public function scopes()
	{
		return array(
				'sorted'=>array(
						'order'=>'sortid',
				),
		);
	}
	
	private static $catTree = array();
	public static function getAlbumTree() {
		if (empty(self::$catTree)) {
			$rows = self::model()->sorted()->findAll('parent_cat_id IS NULL');
			foreach ($rows as $item) {
				self::$catTree[] = self::getAlbumItems($item);
			}
		}
		return self::$catTree;
	}
	
	private static function getAlbumItems($modelRow) {
	
		if (!$modelRow)
			return;
	
		if (isset($modelRow->childrenSorted)) {
			$chump = self::getAlbumItems($modelRow->childrenSorted);
			if ($chump != null)
				$res = array('id'=>$modelRow->id, 'label' => $modelRow->title, 'items' => $chump, 'url' => Yii::app()->createUrl('Album/view', array('id' => $modelRow->id)));
			else
				$res = array('id'=>$modelRow->id,'label' => $modelRow->title, 'url' => Yii::app()->createUrl('Album/view', array('id' => $modelRow->id)));
			return $res;
		} else {
			if (is_array($modelRow)) {
				$arr = array();
				foreach ($modelRow as $leaves) {
					$arr[] = self::getAlbumItems($leaves);
				}
				return $arr;
			} else {
				return array('id'=>$modelRow->id,'label' => ($modelRow->title), 'url' => Yii::app()->createUrl('Album/view', array('id' => $modelRow->id)));
			}
		}
	}
	
	
	public static function getListTreeView() {
		if (empty(self::$catTree)) {
			self::getAlbumTree();
		}
		return self::visualTree(self::$catTree, 0);
	}
	
	private static function visualTree($catTree, $level) {
		$res = array();
		foreach ($catTree as $item) {
			$res[$item['id']] = '' . str_pad('', $level * 2, '-') . ' ' . $item['label'];
			if (isset($item['items'])) {
				$res_iter = self::visualTree($item['items'], $level + 1);
				foreach ($res_iter as $key => $val) {
					$res[$key] = $val;
				}
			}
		}
		return $res;
	}
	
	
	
}