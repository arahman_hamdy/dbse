<?php

Yii::import('application.models._base.BaseEntity');

class Entity extends BaseEntity
{
	public $city;
	public $distance;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}

	public function attributeLabels() {
		return array(
			'parent_id' => Yii::t('app', 'Parent entity'),
			'entityImages' => Yii::t('app', 'Entity Images'),
		) + parent::attributeLabels();
	}

	public function relations() {
		return array(
			'entityImages' => array(self::HAS_MANY, 'EntityImages', 'entity_id'),
		) + parent::relations();
	}

	public function scopes()
	{
		return array(
			'visible'=>array(
				'condition'=>'is_active=1 and is_deleted=0',
			),
		);
	}

	public function getUrl(){
		$url = "";
		$controller = "universities";
		if ($this->country_id == 2){
			$controller = "saudiUniversities";
		}
		if ($this->has_contents){
		    if ($this->slug){
		        $url = Yii::app()->controller->createUrl("/$controller/page",array('p'=>$this->slug));
		    }else{
		        $url = Yii::app()->controller->createUrl("/$controller/view",array('id'=>$this->id));
		    }
		}
		return $url;
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('type', $this->type);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('name_ar', $this->name_ar, true);
		$criteria->compare('name_en', $this->name_en, true);
		if ($this->parent_id){
			$criteria->compare('parent_id', $this->parent_id);
		}else if (empty($_GET['Entity'])){
			$criteria->addCondition('parent_id is null');
		}
		$criteria->compare('has_contents', $this->has_contents);
		$criteria->compare('slug', $this->slug, true);
		$criteria->compare('main_image_id', $this->main_image_id);
		$criteria->compare('contents_ar', $this->contents_ar, true);
		$criteria->compare('contents_en', $this->contents_en, true);
		$criteria->compare('keywords', $this->keywords, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('region_id', $this->region_id);
		$criteria->compare('lat', $this->lat);
		$criteria->compare('lng', $this->lng);
		$criteria->compare('sortid', $this->sortid);
		$criteria->compare('is_active', $this->is_active);
		$criteria->compare('is_deleted', $this->is_deleted);
		$criteria->compare('dateline', $this->dateline, true);
		$criteria->compare('_location', $this->_location, true);
		$criteria->compare('_album', $this->_album, true);
		$criteria->compare('d1', $this->d1, true);
		$criteria->compare('d2', $this->d2, true);
		$criteria->compare('d3', $this->d3, true);
		$criteria->compare('d4', $this->d4, true);
		$criteria->compare('d5', $this->d5, true);
		$criteria->compare('d6', $this->d6, true);
		$criteria->compare('d7', $this->d7, true);
		$criteria->compare('d8', $this->d8, true);
		$criteria->compare('d9', $this->d9, true);
		$criteria->compare('d10', $this->d10, true);
		$criteria->compare('d11', $this->d11, true);
		$criteria->compare('d12', $this->d12, true);
		$criteria->compare('d13', $this->d13, true);
		$criteria->compare('d14', $this->d14, true);
		$criteria->compare('d15', $this->d15, true);
		$criteria->compare('d16', $this->d16, true);
		$criteria->compare('d17', $this->d17, true);
		$criteria->compare('d18', $this->d18, true);
		$criteria->compare('d19', $this->d19, true);
		$criteria->compare('d20', $this->d20, true);
		$criteria->compare('d21', $this->d21, true);
		$criteria->compare('d22', $this->d22, true);
		$criteria->compare('d23', $this->d23, true);
		$criteria->compare('d24', $this->d24, true);
		$criteria->compare('d25', $this->d25, true);
		$criteria->compare('d26', $this->d26, true);
		$criteria->compare('d27', $this->d27, true);
		$criteria->compare('d28', $this->d28, true);
		$criteria->compare('d29', $this->d29, true);
		$criteria->compare('d30', $this->d30, true);
		$criteria->compare('d31', $this->d31, true);
		$criteria->compare('d32', $this->d32, true);
		$criteria->compare('d33', $this->d33, true);
		$criteria->compare('d34', $this->d34, true);
		$criteria->compare('d35', $this->d35, true);
		$criteria->compare('d36', $this->d36, true);
		$criteria->compare('d37', $this->d37, true);
		$criteria->compare('d38', $this->d38, true);
		$criteria->compare('d39', $this->d39, true);
		$criteria->compare('d40', $this->d40, true);
		$criteria->compare('c1', $this->c1);
		$criteria->compare('c2', $this->c2);
		$criteria->compare('c3', $this->c3);
		$criteria->compare('thnwy_adby', $this->thnwy_adby);
		$criteria->compare('thnwy_elmy_math', $this->thnwy_elmy_math);
		$criteria->compare('thnwy_elmy_science', $this->thnwy_elmy_science);
		$criteria->compare('thnwy_sena3y', $this->thnwy_sena3y);
		$criteria->compare('thnwy_tegary', $this->thnwy_tegary);
		$criteria->compare('thnwy_zera3y', $this->thnwy_zera3y);
		$criteria->compare('deb_seyaha', $this->deb_seyaha);
		$criteria->compare('thnwy_azhar', $this->thnwy_azhar);
		$criteria->compare('deb_american', $this->deb_american);
		$criteria->compare('deb_british', $this->deb_british);
		$criteria->compare('mo3adala', $this->mo3adala);
		$criteria->compare('depend_on_dep', $this->depend_on_dep);
		$criteria->compare('credit_hours', $this->credit_hours);
		$criteria->compare('country_id', $this->country_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	protected function afterDelete() {
		parent::afterDelete ();
		EntityChildren::model ()->deleteAll ( "child_id=:id", array (
				":id" => $this->id 
		) );
	}

	public function afterSave() {
		parent::afterSave ();
		if ($this->parent_id && $this->isNewRecord) {
			$newChild = new EntityChildren ();
			$newChild->child_id = $this->id;
			$newChild->parent_id = $this->parent_id;
			$newChild->save ();
			
			Yii::app ()->db->createCommand ( "insert into entity_children (child_id,parent_id)
     										select " . $newChild->child_id . ",parent_id from entity_children c where c.child_id=" . $this->parent_id )->execute ();
		}
	}
}