<?php

Yii::import('application.models._base.BasePlace');

class Place extends BasePlace
{
	public $city;
	public $distance;
	public $i_premium;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array_merge(parent::rules(),array(
				array('main_image_id','required'),
				array('user_id, name_ar, name_en, description_ar, description_en, notes_ar, notes_en, region_id, address_ar, address_en, prices_plans_ar, prices_plans_en, lowest_price, heigest_price, catering_ar, catering_en, featured_ar, featured_en, rooms, capacity, lat, lng, founded, email, mobile, phone, open_hour, close_hour, website, facebook_page, twitter_page, is_premium, status, draft_for, ratings_total, ratings_count, ratings_avg, visits, properties_cache, main_image_id, gallery_cat_id, is_deleted, loc, special_hours, targets_text_ar, targets_text_en, works_from_here_ar, works_from_here_en, main_branch, country_id, dateline','safe'),
		));
	}

	public function scopes()
	{
		return array(
			'visible'=>array(
				'condition'=>'t.status=1 and t.is_draft=0 AND t.is_deleted=0',
			),
		);
	}

	public function relations() {
		return array(
			'placeProperties' => array(self::HAS_MANY, 'PlaceProperty', 'place_id'),
			'placeImages' => array(self::HAS_MANY, 'PlaceImage', 'place_id'),
		) + parent::relations();
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}

	public function attributeLabels() {
		return array(
			'user_id' => Yii::t('app', 'Owner'),
			'draft_for' => Yii::t('app', 'Draft for'),
			'main_image_id' => Yii::t('app', 'Main image'),
			'is_deleted' => Yii::t('app', 'Is Deleted'),
			'dateline' => Yii::t('app', 'Dateline'),
			'placeImages' => Yii::t('app', 'Images'),
			'events' => null,
			'user' => null,
			'region' => null,
			'draftFor' => null,
			'places' => null,
			'mainImage' => null,
			'placeProperties' => null,
			'placeTargets' => null,
		) + parent::attributeLabels();
	}

}