<?php

/**
 * This is the model base class for the table "slider".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Slider".
 *
 * Columns in table "slider" available as properties of the model,
 * followed by relations of table "slider" available as properties of the model.
 *
 * @property integer $id
 * @property string $title
 * @property integer $position
 * @property integer $sortid
 *
 * @property SliderImage[] $sliderImages
 */
abstract class BaseSlider extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'slider';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Slider|Sliders', $n);
	}

	public static function representingColumn() {
		return 'title';
	}

	public function rules() {
		return array(
			array('title, position', 'required'),
			array('position, sortid', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>100),
			array('sortid', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, title, position, sortid', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'sliderImages' => array(self::HAS_MANY, 'SliderImage', 'slider_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'position' => Yii::t('app', 'Position'),
			'sortid' => Yii::t('app', 'Sortid'),
			'sliderImages' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('position', $this->position);
		$criteria->compare('sortid', $this->sortid);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}