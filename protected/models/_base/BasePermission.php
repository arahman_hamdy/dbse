<?php

/**
 * This is the model base class for the table "permission".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Permission".
 *
 * Columns in table "permission" available as properties of the model,
 * followed by relations of table "permission" available as properties of the model.
 *
 * @property integer $id
 * @property string $name
 * @property string $model
 * @property string $action
 * @property integer $sortid
 *
 * @property RolePermission[] $rolePermissions
 */
abstract class BasePermission extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'permission';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Permission|Permissions', $n);
	}

	public static function representingColumn() {
		return 'name';
	}

	public function rules() {
		return array(
			array('name, model, action', 'required'),
			array('sortid', 'numerical', 'integerOnly'=>true),
			array('name, model', 'length', 'max'=>100),
			array('action', 'length', 'max'=>20),
			array('sortid', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name, model, action, sortid', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'rolePermissions' => array(self::HAS_MANY, 'RolePermission', 'permission_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'name' => Yii::t('app', 'Name'),
			'model' => Yii::t('app', 'Model'),
			'action' => Yii::t('app', 'Action'),
			'sortid' => Yii::t('app', 'Sortid'),
			'rolePermissions' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('model', $this->model, true);
		$criteria->compare('action', $this->action, true);
		$criteria->compare('sortid', $this->sortid);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}