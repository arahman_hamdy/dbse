<?php

/**
 * This is the model base class for the table "place_property".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PlaceProperty".
 *
 * Columns in table "place_property" available as properties of the model,
 * followed by relations of table "place_property" available as properties of the model.
 *
 * @property integer $id
 * @property integer $place_id
 * @property integer $property_id
 *
 * @property Property $property
 * @property Place $place
 */
abstract class BasePlaceProperty extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'place_property';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'PlaceProperty|PlaceProperties', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

	public function rules() {
		return array(
			array('place_id, property_id', 'required'),
			array('place_id, property_id', 'numerical', 'integerOnly'=>true),
			array('id, place_id, property_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'property' => array(self::BELONGS_TO, 'Property', 'property_id'),
			'place' => array(self::BELONGS_TO, 'Place', 'place_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'place_id' => null,
			'property_id' => null,
			'property' => null,
			'place' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('place_id', $this->place_id);
		$criteria->compare('property_id', $this->property_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}