<?php

/**
 * This is the model base class for the table "entity_type".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "EntityType".
 *
 * Columns in table "entity_type" available as properties of the model,
 * followed by relations of table "entity_type" available as properties of the model.
 *
 * @property integer $id
 * @property string $name_ar
 * @property string $name_en
 * @property integer $is_system
 * @property integer $show_in_menu
 * @property integer $is_active
 * @property integer $sortid
 * @property integer $country_id
 *
 * @property Entity[] $entities
 * @property Countries $country
 * @property EntityTypeAttribute[] $entityTypeAttributes
 */
abstract class BaseEntityType extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'entity_type';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'EntityType|EntityTypes', $n);
	}

	public static function representingColumn() {
		return 'name_ar';
	}

	public function rules() {
		return array(
			array('name_ar, name_en', 'required'),
			array('is_system, show_in_menu, is_active, sortid, country_id', 'numerical', 'integerOnly'=>true),
			array('name_ar, name_en', 'length', 'max'=>50),
			array('is_system, show_in_menu, is_active, sortid, country_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name_ar, name_en, is_system, show_in_menu, is_active, sortid, country_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'entities' => array(self::HAS_MANY, 'Entity', 'type'),
			'country' => array(self::BELONGS_TO, 'Countries', 'country_id'),
			'entityTypeAttributes' => array(self::HAS_MANY, 'EntityTypeAttribute', 'entity_type_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'name_ar' => Yii::t('app', 'Name Ar'),
			'name_en' => Yii::t('app', 'Name En'),
			'is_system' => Yii::t('app', 'Is System'),
			'show_in_menu' => Yii::t('app', 'Show In Menu'),
			'is_active' => Yii::t('app', 'Is Active'),
			'sortid' => Yii::t('app', 'Sortid'),
			'country_id' => null,
			'entities' => null,
			'country' => null,
			'entityTypeAttributes' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name_ar', $this->name_ar, true);
		$criteria->compare('name_en', $this->name_en, true);
		$criteria->compare('is_system', $this->is_system);
		$criteria->compare('show_in_menu', $this->show_in_menu);
		$criteria->compare('is_active', $this->is_active);
		$criteria->compare('sortid', $this->sortid);
		$criteria->compare('country_id', $this->country_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}