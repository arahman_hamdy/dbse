<?php

/**
 * This is the model base class for the table "school".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "School".
 *
 * Columns in table "school" available as properties of the model,
 * followed by relations of table "school" available as properties of the model.
 *
 * @property integer $id
 * @property string $name_ar
 * @property string $name_en
 * @property integer $region_id
 * @property integer $user_id
 * @property string $main_image_id
 * @property string $address_ar
 * @property string $address_en
 * @property double $lat
 * @property double $lng
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property string $website
 * @property string $facebook
 * @property string $email
 * @property integer $founded
 * @property integer $arabic
 * @property integer $languages
 * @property integer $azhar
 * @property integer $islamic
 * @property integer $sisters
 * @property integer $hotel
 * @property integer $international
 * @property integer $british
 * @property integer $american
 * @property integer $french
 * @property integer $germany
 * @property integer $ib
 * @property integer $turkish
 * @property integer $sudanese
 * @property integer $libyan
 * @property string $application_start
 * @property string $application_end
 * @property string $kg_1
 * @property string $kg_2
 * @property string $pri_1
 * @property string $pri_2
 * @property string $pri_3
 * @property string $pri_4
 * @property string $pri_5
 * @property string $pri_6
 * @property string $prep_1
 * @property string $prep_2
 * @property string $prep_3
 * @property string $sec_1
 * @property string $sec_2
 * @property string $sec_3
 * @property string $transportation
 * @property string $notes_ar
 * @property string $notes_en
 * @property integer $ncacs
 * @property integer $cita
 * @property integer $msa
 * @property integer $sis
 * @property integer $naas
 * @property integer $madonna
 * @property integer $manhattan
 * @property integer $advanced
 * @property integer $cambridge
 * @property integer $iso
 * @property integer $naqaae
 * @property string $dateline
 * @property integer $is_deleted
 * @property integer $visits
 * @property integer $country_id
 *
 * @property Region $region
 * @property Users $user
 * @property LibFile $mainImage
 * @property LibFile[] $libFiles
 */
abstract class BaseSchool extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'school';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'School|Schools', $n);
	}

	public static function representingColumn() {
		return 'name_ar';
	}

	public function rules() {
		return array(
			array('name_ar, name_en, user_id, dateline', 'required'),
			array('region_id, user_id, founded, arabic, languages, azhar, islamic, sisters, hotel, international, british, american, french, germany, ib, turkish, sudanese, libyan, ncacs, cita, msa, sis, naas, madonna, manhattan, advanced, cambridge, iso, naqaae, is_deleted, visits, country_id', 'numerical', 'integerOnly'=>true),
			array('lat, lng', 'numerical'),
			array('name_ar, name_en, fax', 'length', 'max'=>200),
			array('main_image_id', 'length', 'max'=>20),
			array('address_ar, address_en, kg_1, kg_2, pri_1, pri_2, pri_3, pri_4, pri_5, pri_6, prep_1, prep_2, prep_3, sec_1, sec_2, sec_3', 'length', 'max'=>300),
			array('phone, mobile, facebook, email', 'length', 'max'=>500),
			array('website', 'length', 'max'=>400),
			array('application_start', 'length', 'max'=>6),
			array('application_end', 'length', 'max'=>17),
			array('transportation', 'length', 'max'=>157),
			array('notes_ar, notes_en', 'safe'),
			array('region_id, main_image_id, address_ar, address_en, lat, lng, phone, mobile, fax, website, facebook, email, founded, arabic, languages, azhar, islamic, sisters, hotel, international, british, american, french, germany, ib, turkish, sudanese, libyan, application_start, application_end, kg_1, kg_2, pri_1, pri_2, pri_3, pri_4, pri_5, pri_6, prep_1, prep_2, prep_3, sec_1, sec_2, sec_3, transportation, notes_ar, notes_en, ncacs, cita, msa, sis, naas, madonna, manhattan, advanced, cambridge, iso, naqaae, is_deleted, visits, country_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name_ar, name_en, region_id, user_id, main_image_id, address_ar, address_en, lat, lng, phone, mobile, fax, website, facebook, email, founded, arabic, languages, azhar, islamic, sisters, hotel, international, british, american, french, germany, ib, turkish, sudanese, libyan, application_start, application_end, kg_1, kg_2, pri_1, pri_2, pri_3, pri_4, pri_5, pri_6, prep_1, prep_2, prep_3, sec_1, sec_2, sec_3, transportation, notes_ar, notes_en, ncacs, cita, msa, sis, naas, madonna, manhattan, advanced, cambridge, iso, naqaae, dateline, is_deleted, visits, country_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'mainImage' => array(self::BELONGS_TO, 'LibFile', 'main_image_id'),
			'libFiles' => array(self::MANY_MANY, 'LibFile', 'school_image(school_id, image_id)'),
		);
	}

	public function pivotModels() {
		return array(
			'libFiles' => 'SchoolImage',
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'name_ar' => Yii::t('app', 'Name Ar'),
			'name_en' => Yii::t('app', 'Name En'),
			'region_id' => null,
			'user_id' => null,
			'main_image_id' => null,
			'address_ar' => Yii::t('app', 'Address Ar'),
			'address_en' => Yii::t('app', 'Address En'),
			'lat' => Yii::t('app', 'Lat'),
			'lng' => Yii::t('app', 'Lng'),
			'phone' => Yii::t('app', 'Phone'),
			'mobile' => Yii::t('app', 'Mobile'),
			'fax' => Yii::t('app', 'Fax'),
			'website' => Yii::t('app', 'Website'),
			'facebook' => Yii::t('app', 'Facebook'),
			'email' => Yii::t('app', 'Email'),
			'founded' => Yii::t('app', 'Founded'),
			'arabic' => Yii::t('app', 'Arabic'),
			'languages' => Yii::t('app', 'Languages'),
			'azhar' => Yii::t('app', 'Azhar'),
			'islamic' => Yii::t('app', 'Islamic'),
			'sisters' => Yii::t('app', 'Sisters'),
			'hotel' => Yii::t('app', 'Hotel'),
			'international' => Yii::t('app', 'International'),
			'british' => Yii::t('app', 'British'),
			'american' => Yii::t('app', 'American'),
			'french' => Yii::t('app', 'French'),
			'germany' => Yii::t('app', 'Germany'),
			'ib' => Yii::t('app', 'Ib'),
			'turkish' => Yii::t('app', 'Turkish'),
			'sudanese' => Yii::t('app', 'Sudanese'),
			'libyan' => Yii::t('app', 'Libyan'),
			'application_start' => Yii::t('app', 'Application Start'),
			'application_end' => Yii::t('app', 'Application End'),
			'kg_1' => Yii::t('app', 'Kg 1'),
			'kg_2' => Yii::t('app', 'Kg 2'),
			'pri_1' => Yii::t('app', 'Pri 1'),
			'pri_2' => Yii::t('app', 'Pri 2'),
			'pri_3' => Yii::t('app', 'Pri 3'),
			'pri_4' => Yii::t('app', 'Pri 4'),
			'pri_5' => Yii::t('app', 'Pri 5'),
			'pri_6' => Yii::t('app', 'Pri 6'),
			'prep_1' => Yii::t('app', 'Prep 1'),
			'prep_2' => Yii::t('app', 'Prep 2'),
			'prep_3' => Yii::t('app', 'Prep 3'),
			'sec_1' => Yii::t('app', 'Sec 1'),
			'sec_2' => Yii::t('app', 'Sec 2'),
			'sec_3' => Yii::t('app', 'Sec 3'),
			'transportation' => Yii::t('app', 'Transportation'),
			'notes_ar' => Yii::t('app', 'Notes Ar'),
			'notes_en' => Yii::t('app', 'Notes En'),
			'ncacs' => Yii::t('app', 'Ncacs'),
			'cita' => Yii::t('app', 'Cita'),
			'msa' => Yii::t('app', 'Msa'),
			'sis' => Yii::t('app', 'Sis'),
			'naas' => Yii::t('app', 'Naas'),
			'madonna' => Yii::t('app', 'Madonna'),
			'manhattan' => Yii::t('app', 'Manhattan'),
			'advanced' => Yii::t('app', 'Advanced'),
			'cambridge' => Yii::t('app', 'Cambridge'),
			'iso' => Yii::t('app', 'Iso'),
			'naqaae' => Yii::t('app', 'Naqaae'),
			'dateline' => Yii::t('app', 'Dateline'),
			'is_deleted' => Yii::t('app', 'Is Deleted'),
			'visits' => Yii::t('app', 'Visits'),
			'country_id' => Yii::t('app', 'Country'),
			'region' => null,
			'user' => null,
			'mainImage' => null,
			'libFiles' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name_ar', $this->name_ar, true);
		$criteria->compare('name_en', $this->name_en, true);
		$criteria->compare('region_id', $this->region_id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('main_image_id', $this->main_image_id);
		$criteria->compare('address_ar', $this->address_ar, true);
		$criteria->compare('address_en', $this->address_en, true);
		$criteria->compare('lat', $this->lat);
		$criteria->compare('lng', $this->lng);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('mobile', $this->mobile, true);
		$criteria->compare('fax', $this->fax, true);
		$criteria->compare('website', $this->website, true);
		$criteria->compare('facebook', $this->facebook, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('founded', $this->founded);
		$criteria->compare('arabic', $this->arabic);
		$criteria->compare('languages', $this->languages);
		$criteria->compare('azhar', $this->azhar);
		$criteria->compare('islamic', $this->islamic);
		$criteria->compare('sisters', $this->sisters);
		$criteria->compare('hotel', $this->hotel);
		$criteria->compare('international', $this->international);
		$criteria->compare('british', $this->british);
		$criteria->compare('american', $this->american);
		$criteria->compare('french', $this->french);
		$criteria->compare('germany', $this->germany);
		$criteria->compare('ib', $this->ib);
		$criteria->compare('turkish', $this->turkish);
		$criteria->compare('sudanese', $this->sudanese);
		$criteria->compare('libyan', $this->libyan);
		$criteria->compare('application_start', $this->application_start, true);
		$criteria->compare('application_end', $this->application_end, true);
		$criteria->compare('kg_1', $this->kg_1, true);
		$criteria->compare('kg_2', $this->kg_2, true);
		$criteria->compare('pri_1', $this->pri_1, true);
		$criteria->compare('pri_2', $this->pri_2, true);
		$criteria->compare('pri_3', $this->pri_3, true);
		$criteria->compare('pri_4', $this->pri_4, true);
		$criteria->compare('pri_5', $this->pri_5, true);
		$criteria->compare('pri_6', $this->pri_6, true);
		$criteria->compare('prep_1', $this->prep_1, true);
		$criteria->compare('prep_2', $this->prep_2, true);
		$criteria->compare('prep_3', $this->prep_3, true);
		$criteria->compare('sec_1', $this->sec_1, true);
		$criteria->compare('sec_2', $this->sec_2, true);
		$criteria->compare('sec_3', $this->sec_3, true);
		$criteria->compare('transportation', $this->transportation, true);
		$criteria->compare('notes_ar', $this->notes_ar, true);
		$criteria->compare('notes_en', $this->notes_en, true);
		$criteria->compare('ncacs', $this->ncacs);
		$criteria->compare('cita', $this->cita);
		$criteria->compare('msa', $this->msa);
		$criteria->compare('sis', $this->sis);
		$criteria->compare('naas', $this->naas);
		$criteria->compare('madonna', $this->madonna);
		$criteria->compare('manhattan', $this->manhattan);
		$criteria->compare('advanced', $this->advanced);
		$criteria->compare('cambridge', $this->cambridge);
		$criteria->compare('iso', $this->iso);
		$criteria->compare('naqaae', $this->naqaae);
		$criteria->compare('dateline', $this->dateline, true);
		$criteria->compare('is_deleted', $this->is_deleted);
		$criteria->compare('visits', $this->visits);
		$criteria->compare('country_id', $this->country_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}