<?php

/**
 * This is the model base class for the table "city".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "City".
 *
 * Columns in table "city" available as properties of the model,
 * followed by relations of table "city" available as properties of the model.
 *
 * @property integer $id
 * @property string $name_ar
 * @property string $name_en
 * @property string $image_id
 * @property integer $country_id
 *
 * @property LibFile $image
 * @property Countries $country
 * @property Region[] $regions
 */
abstract class BaseCity extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'city';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'City|Cities', $n);
	}

	public static function representingColumn() {
		return 'name_ar';
	}

	public function rules() {
		return array(
			array('name_ar, name_en', 'required'),
			array('country_id', 'numerical', 'integerOnly'=>true),
			array('name_ar, name_en', 'length', 'max'=>100),
			array('image_id', 'length', 'max'=>20),
			array('image_id, country_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name_ar, name_en, image_id, country_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'image' => array(self::BELONGS_TO, 'LibFile', 'image_id'),
			'country' => array(self::BELONGS_TO, 'Countries', 'country_id'),
			'regions' => array(self::HAS_MANY, 'Region', 'city_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'name_ar' => Yii::t('app', 'Name Ar'),
			'name_en' => Yii::t('app', 'Name En'),
			'image_id' => null,
			'country_id' => null,
			'image' => null,
			'country' => null,
			'regions' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name_ar', $this->name_ar, true);
		$criteria->compare('name_en', $this->name_en, true);
		$criteria->compare('image_id', $this->image_id);
		$criteria->compare('country_id', $this->country_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}