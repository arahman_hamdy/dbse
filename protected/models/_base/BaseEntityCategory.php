<?php

/**
 * This is the model base class for the table "entity_category".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "EntityCategory".
 *
 * Columns in table "entity_category" available as properties of the model,
 * followed by relations of table "entity_category" available as properties of the model.
 *
 * @property integer $id
 * @property string $title_ar
 * @property string $title_en
 * @property integer $sortid
 * @property integer $country_id
 *
 * @property Entity[] $entities
 * @property Countries $country
 */
abstract class BaseEntityCategory extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'entity_category';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'EntityCategory|EntityCategories', $n);
	}

	public static function representingColumn() {
		return 'title_ar';
	}

	public function rules() {
		return array(
			array('title_ar, title_en', 'required'),
			array('sortid, country_id', 'numerical', 'integerOnly'=>true),
			array('title_ar, title_en', 'length', 'max'=>200),
			array('sortid, country_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, title_ar, title_en, sortid, country_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'entities' => array(self::HAS_MANY, 'Entity', 'category_id'),
			'country' => array(self::BELONGS_TO, 'Countries', 'country_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'title_ar' => Yii::t('app', 'Title Ar'),
			'title_en' => Yii::t('app', 'Title En'),
			'sortid' => Yii::t('app', 'Sortid'),
			'country_id' => null,
			'entities' => null,
			'country' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title_ar', $this->title_ar, true);
		$criteria->compare('title_en', $this->title_en, true);
		$criteria->compare('sortid', $this->sortid);
		$criteria->compare('country_id', $this->country_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}