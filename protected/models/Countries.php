<?php

Yii::import('application.models._base.BaseCountries');

class Countries extends BaseCountries
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Country|Countries', $n);
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}

}