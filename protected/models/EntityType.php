<?php

Yii::import('application.models._base.BaseEntityType');

class EntityType extends BaseEntityType
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function scopes()
	{
		return array(
			'active'=>array(
				'condition'=>'t.is_active=1',
			),
		);
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}

}