<?php
/**
 * RegistrationForm class.
 * RegistrationForm is the data structure for keeping
 * user registration form data. It is used by the 'registration' action of 'UserController'.
 */
class ManageUser extends User {
	public $verifyPassword;
	public $verifyCode;
	
	public function rules() {
		$rules = array(
			array('email', 'required'),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => Yii::t('UserModule', "Incorrect password (minimal length 4 symbols).")),
			array('email', 'email'),
			array('role_id', 'numerical', 'integerOnly'=>true),
			array('role_id', 'required'),
			array('status', 'required'),
			array('email', 'unique', 'message' => Yii::t('UserModule', "This user's email address already exists.")),
			//array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => Yii::t('UserModule', "Retype Password is incorrect.")),
		);
		
		array_push($rules,array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => Yii::t('UserModule', "Retype Password is incorrect.")));
		return $rules;
	}
	
}