<?php

Yii::import('application.models._base.BaseSliderImage');

class SliderImage extends BaseSliderImage
{
	public $image_file = "";
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return array_merge(parent::rules(),array(
				array('image_file', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'), // this will allow empty field when page is update (remember here i create scenario update)
				array('image_file', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>false, 'on'=>'insert'),			
				array('image_file', 'required', 'on'=>'insert'),
		));
	}

	public function afterFind(){
		$this->image_file = Yii::app()->controller->uploadsUrl . $this->image->path;;
	}

}