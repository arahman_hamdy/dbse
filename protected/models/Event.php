<?php

Yii::import('application.models._base.BaseEvent');

class Event extends BaseEvent
{
	public $number;
	public $image_file = "";
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return array_merge(parent::rules(),array(
				array('image_file', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'), // this will allow empty field when page is update (remember here i create scenario update)
				array('image_file', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'insert'),			
		));
	}

	public function scopes()
	{
		return array(
			'visible'=>array(
				'condition'=>'t.is_deleted=0 AND t.is_draft=0 AND t.is_active=1',
			),
		);
	}

	public function afterFind(){
		if ($this->image)
			$this->image_file = Yii::app()->controller->uploadsUrl . $this->image->path;
	}

	public function getImageUrl(){
		if (!$this->image){
			return Yii::app()->controller->createUrl("/images") . '/placeholder.gif';
		}
		return Yii::app()->controller->uploadsUrl . $this->image->path;
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}

	public function search() {
		$dp = parent::search();
		$dp->criteria->compare("t.is_deleted", 0);
		return $dp;
	}

}