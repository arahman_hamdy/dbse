<?php

Yii::import('application.models._base.BaseSchool');

class School extends BaseSchool
{
	public $city;
	public $distance;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array_merge(parent::rules(),array(
				array('city','numerical'),
				array('main_image_id','safe'),
		));
	}

	public function scopes()
	{
		return array(
			'visible'=>array(
				'condition'=>'is_deleted=0',
			),
		);
	}

	public function relations() {
		return array(
			'schoolImages' => array(self::HAS_MANY, 'SchoolImage', 'school_id'),
		) + parent::relations();
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}

	public function attributeLabels() {
		return array(
			'lat' => Yii::t('app', 'Lat'),
			'lng' => Yii::t('app', 'Lng'),
			'ib' => Yii::t('app', 'International bachelor'),
			'kg_1' => Yii::t('app', 'KG 1'),
			'kg_2' => Yii::t('app', 'KG 2'),
			'pri_1' => Yii::t('app', 'Primary 1'),
			'pri_2' => Yii::t('app', 'Primary 2'),
			'pri_3' => Yii::t('app', 'Primary 3'),
			'pri_4' => Yii::t('app', 'Primary 4'),
			'pri_5' => Yii::t('app', 'Primary 5'),
			'pri_6' => Yii::t('app', 'Primary 6'),
			'prep_1' => Yii::t('app', 'Preparatory 1'),
			'prep_2' => Yii::t('app', 'Preparatory 2'),
			'prep_3' => Yii::t('app', 'Preparatory 3'),
			'sec_1' => Yii::t('app', 'Secondary 1'),
			'sec_2' => Yii::t('app', 'Secondary 2'),
			'sec_3' => Yii::t('app', 'Secondary 3'),
			'ncacs' => Yii::t('app', 'North Central Association of Colleges and Schools'),
			'cita' => Yii::t('app', 'Commission on International and Trans-Regional Accreditation'),
			'msa' => Yii::t('app', 'Middle States Association'),
			'sis' => Yii::t('app', 'Snowden International School'),
			'naas' => Yii::t('app', 'Northwest Association of Accredited Schools (NAAS)'),
			'madonna' => Yii::t('app', 'Madonna University'),
			'manhattan' => Yii::t('app', 'Manhattan College'),
			'advanced' => Yii::t('app', 'AdvancED'),
			'cambridge' => Yii::t('app', 'CAMBRIDGE International Examinations'),
			'iso' => Yii::t('app', ' International Organization for Standardization (ISO)'),
			'naqaae' => Yii::t('app', 'National Authority for Quality Assurance and Accreditation of Education'),

			'main_image_id' => Yii::t('app', 'Main image'),
			'is_deleted' => Yii::t('app', 'Is Deleted'),
			'dateline' => Yii::t('app', 'Dateline'),
			'schoolImages' => Yii::t('app', 'Images'),
		) + parent::attributeLabels();
	}

	public function getImageUrl(){
		if (!$this->mainImage){
			return Yii::app()->controller->createUrl("/images") . '/placeholder.gif';
		}
		return Yii::app()->controller->uploadsUrl . $this->mainImage->path;
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('t.id', $this->id);
		$criteria->compare('t.name_ar', $this->name_ar, true);
		$criteria->compare('t.name_en', $this->name_en, true);
		$criteria->compare('region_id', $this->region_id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('main_image_id', $this->main_image_id);
		$criteria->compare('address_ar', $this->address_ar, true);
		$criteria->compare('address_en', $this->address_en, true);
		$criteria->compare('lat', $this->lat);
		$criteria->compare('lng', $this->lng);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('mobile', $this->mobile, true);
		$criteria->compare('fax', $this->fax, true);
		$criteria->compare('website', $this->website, true);
		$criteria->compare('facebook', $this->facebook, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('founded', $this->founded);
		$criteria->compare('arabic', $this->arabic);
		$criteria->compare('languages', $this->languages);
		$criteria->compare('azhar', $this->azhar);
		$criteria->compare('islamic', $this->islamic);
		$criteria->compare('sisters', $this->sisters);
		$criteria->compare('hotel', $this->hotel);
		$criteria->compare('international', $this->international);
		$criteria->compare('british', $this->british);
		$criteria->compare('american', $this->american);
		$criteria->compare('french', $this->french);
		$criteria->compare('germany', $this->germany);
		$criteria->compare('ib', $this->ib);
		$criteria->compare('turkish', $this->turkish);
		$criteria->compare('sudanese', $this->sudanese);
		$criteria->compare('libyan', $this->libyan);
		$criteria->compare('application_start', $this->application_start, true);
		$criteria->compare('application_end', $this->application_end, true);
		$criteria->compare('kg_1', $this->kg_1, true);
		$criteria->compare('kg_2', $this->kg_2, true);
		$criteria->compare('pri_1', $this->pri_1, true);
		$criteria->compare('pri_2', $this->pri_2, true);
		$criteria->compare('pri_3', $this->pri_3, true);
		$criteria->compare('pri_4', $this->pri_4, true);
		$criteria->compare('pri_5', $this->pri_5, true);
		$criteria->compare('pri_6', $this->pri_6, true);
		$criteria->compare('prep_1', $this->prep_1, true);
		$criteria->compare('prep_2', $this->prep_2, true);
		$criteria->compare('prep_3', $this->prep_3, true);
		$criteria->compare('sec_1', $this->sec_1, true);
		$criteria->compare('sec_2', $this->sec_2, true);
		$criteria->compare('sec_3', $this->sec_3, true);
		$criteria->compare('transportation', $this->transportation, true);
		$criteria->compare('notes_ar', $this->notes_ar, true);
		$criteria->compare('notes_en', $this->notes_en, true);
		$criteria->compare('ncacs', $this->ncacs);
		$criteria->compare('cita', $this->cita);
		$criteria->compare('msa', $this->msa);
		$criteria->compare('sis', $this->sis);
		$criteria->compare('naas', $this->naas);
		$criteria->compare('madonna', $this->madonna);
		$criteria->compare('manhattan', $this->manhattan);
		$criteria->compare('advanced', $this->advanced);
		$criteria->compare('cambridge', $this->cambridge);
		$criteria->compare('iso', $this->iso);
		$criteria->compare('naqaae', $this->naqaae);
		$criteria->compare('dateline', $this->dateline, true);
		$criteria->compare('is_deleted', $this->is_deleted);
		$criteria->compare('visits', $this->visits);
		$criteria->compare('country_id', $this->country_id);

		$criteria->compare('region.city_id', $this->city);
		$criteria->with[] = 'region';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}