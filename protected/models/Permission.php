<?php

Yii::import('application.models._base.BasePermission');

class Permission extends BasePermission
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}