<?php

Yii::import('application.models._base.BaseBlogArticle');

class BlogArticle extends BaseBlogArticle
{
	public $image_file = "";
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return array_merge(parent::rules(),array(
				array('image_file', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'), // this will allow empty field when page is update (remember here i create scenario update)
				array('image_file', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>false, 'on'=>'insert'),			
		));
	}

	public function scopes()
	{
		return array(
			'visible'=>array(
				'condition'=>'t.status=1 and t.is_draft=0 AND t.is_deleted=0',
			),
		);
	}

	public function afterFind(){
		if ($this->image)
			$this->image_file = Yii::app()->controller->uploadsUrl . $this->image->path;
	}

	public static function representingColumn() {
		return 'title_' . Yii::app()->language;
	}

	public function getUrl(){
		return Yii::app()->controller->createUrl("/blog/view",array('id'=>$this->id));
	}

	public function createLink(){
		return CHTML::link((string)$this,$this->url);
	}

}