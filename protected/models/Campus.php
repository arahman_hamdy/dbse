<?php

Yii::import('application.models._base.BaseCampus');

class Campus extends BaseCampus
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}	
}