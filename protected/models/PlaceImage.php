<?php

Yii::import('application.models._base.BasePlaceImage');

class PlaceImage extends BasePlaceImage
{

	public function relations() {
		return array(
			'image' => array(self::BELONGS_TO, 'LibFile', 'image_id'),
		) + parent::relations();
	}

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}