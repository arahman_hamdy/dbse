<?php

class PlaceFilter extends Place
{
	public $price;
	public $rooms_from;
	public $rooms_to;
	public $capacity_from;
	public $capacity_to;

	public function rules() {
		return array_merge(array(
			array('city,region_id, rooms, capacity', 'numerical', 'integerOnly'=>true),
			array('lowest_price, heigest_price', 'numerical'),
			array('founded', 'length', 'max'=>4),
			array('rooms_from,rooms_to,capacity_from,capacity_to','numerical'),
		),parent::rules());
	}


	public function attributeLabels() {
		return array(
			'rooms_from' => Yii::t('app', 'Rooms from'),
			'rooms_to' => Yii::t('app', 'Rooms to'),
			'capacity_from' => Yii::t('app', 'Capacity from'),
			'capacity_to' => Yii::t('app', 'Capacity to'),
		) + parent::attributeLabels();
	}

}