<?php

Yii::import('application.models._base.BaseEventRegistration');

class EventRegistration extends BaseEventRegistration
{
	public $owner_id;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array_merge(parent::rules(), array(
			array('name', 'length', 'min'=>7),
			array('phone', 'length', 'min'=>9),
			array('email', 'email'),
			array('persons', 'numerical', 'integerOnly'=>true, 'min'=>1, 'max'=>10),
		));
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('t.id', $this->id);
		$criteria->compare('t.event_id', $this->event_id);
		$criteria->compare('t.user_id', $this->user_id);
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.phone', $this->phone, true);
		$criteria->compare('t.email', $this->email, true);
		$criteria->compare('t.persons', $this->persons);
		$criteria->compare('t.total', $this->total);
		$criteria->compare('t.notes', $this->notes, true);
		$criteria->compare('t.transaction_id', $this->transaction_id);
		$criteria->compare('t.price', $this->price);
		$criteria->compare('t.is_paid', $this->is_paid);
		$criteria->compare('t.status', $this->status);
		$criteria->compare('t.dateline', $this->dateline, true);

		if ($this->owner_id){
			$criteria->with[] = 'event';
			$criteria->together = true;
			$criteria->compare('event.user_id', $this->owner_id);
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

}