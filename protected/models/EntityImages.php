<?php

Yii::import('application.models._base.BaseEntityImages');

class EntityImages extends BaseEntityImages
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function relations() {
		return array(
			'image' => array(self::BELONGS_TO, 'LibFile', 'image_id'),
		) + parent::relations();
	}	
}