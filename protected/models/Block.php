<?php

Yii::import('application.models._base.BaseBlock');

class Block extends BaseBlock
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'title_' . Yii::app()->language;
	}
	
	public function rules(){
		return array_merge(parent::rules(),array(
				array('lib_cat_id', 'validateAlbum'),
		));
		
	}

	public function attributeLabels(){
		return array(
				'article_cat_id' =>  Yii::t('app', 'Blog category'),
		) + parent::attributeLabels();
	}	
	
	public function validateAlbum($attribute,$params){
		if ($this->type == "Image Album" || $this->type == "Video Album"){
			$this->article_cat_id = null;
			
			if (!$this->$attribute || !LibCat::model()->findByAttributes(array('id'=>$this->$attribute))){
				$this->addError($attribute,Yii::t("app","Invalid album!"));
			}
		}
	}
	
	public function validateCategory($attribute,$params){
		if ($this->type == "Article Category"){
			$this->lib_cat_id = null;
			
			if (!$this->$attribute || !Category::model()->findByAttributes(array('is_deleted'=>0, 'id'=>$this->$attribute))){
				$this->addError($attribute,Yii::t("app","Invalid category!"));
			}
		}		
	}
}