<?php

Yii::import('application.models._base.BaseProperty');

class Property extends BaseProperty
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}
	
}