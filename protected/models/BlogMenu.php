<?php

Yii::import('application.models._base.BaseBlogMenu');

class BlogMenu extends BaseBlogMenu
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}

	public function attributeLabels(){
		return array(
				'parent_id' =>  Yii::t('app', 'Parent Menu'),
				'parent' => Yii::t('app', 'Parent Menu'),
		) + parent::attributeLabels();
	}
	
	public function relations(){
		return array(
				'childrenSorted' => array(self::HAS_MANY, 'BlogMenu', 'parent_id','scopes'=>array(
						'scopes'=>'sorted'
				),),
		) + parent::relations();
	}
	
	public function scopes()
	{
		return array(
				'sorted'=>array(
						'condition'=>'is_deleted=0',
						'order'=>'sortid',
				),
		);
	}
	
	private static $mnuTree = array();
	public static function getMenuItemTree() {
		if (empty(self::$mnuTree)) {
			$rows = self::model()->sorted()->findAll('parent_id IS NULL and is_deleted=0');
			foreach ($rows as $item) {
				self::$mnuTree[] = self::getMenuItemItems($item);
			}
		}
		return self::$mnuTree;
	}
	
	private static function getMenuItemItems($modelRow) {
	
		if (!$modelRow)
			return;
	
		if (isset($modelRow->childrenSorted)) {
			$chump = self::getMenuItemItems($modelRow->childrenSorted);
			if ($chump != null)
				$res = array('id'=>$modelRow->id, 'label' => (string)$modelRow, 'items' => $chump, 'url' => Yii::app()->createUrl('MenuItem/view', array('id' => $modelRow->id)));
			else
				$res = array('id'=>$modelRow->id,'label' => (string)$modelRow, 'url' => Yii::app()->createUrl('MenuItem/view', array('id' => $modelRow->id)));
			return $res;
		} else {
			if (is_array($modelRow)) {
				$arr = array();
				foreach ($modelRow as $leaves) {
					$arr[] = self::getMenuItemItems($leaves);
				}
				return $arr;
			} else {
				return array('id'=>$modelRow->id,'label' => (string)$modelRow, 'url' => Yii::app()->createUrl('MenuItem/view', array('id' => $modelRow->id)));
			}
		}
	}
	
	
	public static function getListTreeView() {
		if (empty(self::$mnuTree)) {
			self::getMenuItemTree();
		}
		return self::visualTree(self::$mnuTree, 0);
	}
	
	private static function visualTree($mnuTree, $level) {
		$res = array();
		foreach ($mnuTree as $item) {
			$res[$item['id']] = '' . str_pad('', $level * 2, '-') . ' ' . $item['label'];
			if (isset($item['items'])) {
				$res_iter = self::visualTree($item['items'], $level + 1);
				foreach ($res_iter as $key => $val) {
					$res[$key] = $val;
				}
			}
		}
		return $res;
	}

	
	public function rules(){
		return array_merge(parent::rules(),array(
				array('url', 'validateUrl'),
				array('category_id', 'validateCategory'),
		));
	
	}
	
	public function validateUrl($attribute,$params){
		if ($this->type == "Free"){
			$this->category_id = null;
			
			if (!$this->$attribute || filter_var($this->$attribute, FILTER_VALIDATE_URL)===false){
				$this->addError($attribute,Yii::t("app","Invalid url!"));
			}
		}		
	}
	
	public function validatePage($attribute,$params){
		if ($this->type == "Page"){
			$this->url = null;
			$this->category_id = null;
				
			if (!$this->$attribute || !Page::model()->findByAttributes(array('id'=>$this->$attribute))){
				$this->addError($attribute,Yii::t("app","Invalid page!"));
			}
		}
	}
	
	public function validateCategory($attribute,$params){
		if ($this->type == "Category"){
			$this->url = null;
			
			
			if (!$this->$attribute || !BlogCategory::model()->findByAttributes(array('is_deleted'=>0, 'id'=>$this->$attribute))){
				$this->addError($attribute,Yii::t("app","Invalid category!"));
			}
		}		
	}
	
	
	public function createMenuLink() {
		if ($this->icon){
			$this->icon = "fa fa-" . preg_replace("#^fa-#","",$this->icon);
		}

		$html = "";
		if($this->type == "Category") {
			$html .= "<a href=".$this->category->getUrl().">".(($this->icon)?"<i class='".$this->icon."''></i> ":"").(string)$this."</a>";
		} else if($this->type == "Free") {
			$html .= "<a href=".$this->url.">".(($this->icon)?"<i class='".$this->icon."''></i> ":"").(string)$this."</a>";
		} else {
			$html .= '<a href="#" class="void_url">'.(($this->icon)?"<i class='".$this->icon."''></i> ":"") . (string)$this . '</a>';
		}
		return $html;
	}
	
}