<?php

Yii::import('application.models._base.BaseLibFile');

class LibFile extends BaseLibFile
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public static function label($n = 1) {
		return Yii::t('app', 'Library file|Library files', $n);
	}
	
	public function search() {
		$criteria = new CDbCriteria;
	
		$criteria->compare('id', $this->id, true);
		$criteria->compare('type', $this->type, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('path', $this->path, true);
		$criteria->compare('thumb_path', $this->thumb_path, true);
		$criteria->compare('lib_cat_id', $this->lib_cat_id);
		$criteria->compare('tags', $this->tags, true);
		$criteria->compare('dateline', $this->dateline, true);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('is_archived', $this->is_archived);
	
		return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
				'pagination'=>array(
						'pageSize'=>120,
				),
		));
	}
		
}