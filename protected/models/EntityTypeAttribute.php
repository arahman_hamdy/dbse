<?php

Yii::import('application.models._base.BaseEntityTypeAttribute');

class EntityTypeAttribute extends BaseEntityTypeAttribute
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function representingColumn() {
		return 'name_' . Yii::app()->language;
	}

	public function relations() {
		return array(
			'entityAttribute' => array(self::BELONGS_TO, 'EntityAttribute', array('id'=>'entity_type_attribute_id')),
		) + parent::relations();
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('entity_type_id', $this->entity_type_id);
		$criteria->compare('attribute_type_id', $this->attribute_type_id);
		$criteria->compare('name_ar', $this->name_ar, true);
		$criteria->compare('name_en', $this->name_en, true);
		$criteria->compare('is_searchable', $this->is_searchable);
		$criteria->compare('sortid', $this->sortid);

		$criteria->order = 'sortid';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

}