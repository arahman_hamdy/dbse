<?php

$criteria = new CDbCriteria;
$criteria->order = "sortid";
$contactFormElements = ContactFormElements::model()->findAll($criteria);

$model = "
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	public \$verifyCode;
";
foreach ($contactFormElements as $contactFormElement) {
	$name = "element_{$contactFormElement->id}";
	$model .= "	public \$$name;\n";
}
$model .= "	
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
";
		$required = array();
		$emails = array();
		$smallText = array();
		$largeText = array();

		$labels = array('verifyCode'=>'Verification Code');
		$rules = "array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),\n";

		foreach ($contactFormElements as $contactFormElement) {
			$name = "element_{$contactFormElement->id}";
			if ($contactFormElement->is_required) $required[] = $name;
			switch($contactFormElement->type){
				case 'email':
					$emails[] = $name;
					$smallText[] = $name;
				break;

				case 'text':
					$smallText[] = $name;
				break;

				case 'textarea':
					$largeText[] = $name;
				break;
			}
			$labels[$name]=$contactFormElement->{"label_" . Yii::app()->language};
		}

		if ($required){
			$rules .= "array('" . implode(", ", $required) . "','required'),\n";
		}

		if ($emails){
			$rules .= "array('" . implode(", ", $emails) . "','email'),\n";
		}

		if ($smallText){
			$rules .= "array('" . implode(", ", $smallText) . "','length', 'max'=>100),\n";
		}

		if ($largeText){
			$rules .= "array('" . implode(", ", $largeText) . "','length', 'max'=>500),\n";
		}

$model .= "
		return array(
			$rules
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
";
$model .= "
		return CJSON::decode('" . CJSON::encode($labels) . "');
	}
}";
eval($model);