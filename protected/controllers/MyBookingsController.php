<?php
/**
 * This is a Yii controller to display the bookings in the frontend.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Frontend
 */

class MyBookingsController extends FrontUserController {
	public $country_id = 1;
	public $blocks = array();

	public function beforeAction($action){
		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$criteria = new CDbCriteria;
		$criteria->compare("t.user_id", $this->user->id);
		$criteria->compare("t.status",1);

		$criteria->with["place"] = array();
		$criteria->together = true;


		$bookings = new CActiveDataProvider('Booking', array(
			'criteria' => $criteria,
			'sort'=>array(
				'defaultOrder'=>'t.id desc',
			)
		));

		$criteria = new CDbCriteria;
		$criteria->compare("t.user_id", $this->user->id);
		$criteria->compare("t.status",1);

		$criteria->with["event"] = array();
		$criteria->together = true;


		$events = new CActiveDataProvider('EventRegistration', array(
			'criteria' => $criteria,
			'sort'=>array(
				'defaultOrder'=>'t.id desc',
			)
		));

		// Render view.
		$this->render('index', array(
			'bookings'=>$bookings,
			'events'=>$events,
		));
	}
}