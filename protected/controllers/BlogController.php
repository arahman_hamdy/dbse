<?php
/**
 * This is a Yii controller to display the blogs in the frontend.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Frontend
 */

class BlogController extends FrontMainController {
	public $country_id = 1;
	public $layout = "//layouts/blog";
	public $menues = array();
	public $categories = array();
	public $blocks = array();
	public $related = array();

	public function beforeAction($action){
		$criteria = new CDbCriteria;
		$criteria->compare('t.is_deleted',0);
		$criteria->addCondition('t.parent_id is null');
		$criteria->order = "t.sortid, t.id";
		$this->menues = BlogMenu::model()->with('childrenSorted')->cache(60)->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->compare('t.is_deleted',0);
		$criteria->addCondition('t.parent_id is null');
		$criteria->order = "t.sortid, t.id";
		$this->categories = BlogCategory::model()->cache(60)->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->compare('t.status',1);
		$criteria->compare('t.position',1);
		$criteria->order = "t.sortid, t.id";
		$this->blocks = Block::model()->cache(60)->findAll($criteria);

		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$this->pageTitle = Yii::app()->name . " - " . Yii::t('app', 'Blog');

		$criteria = new CDbCriteria;
		$criteria->order = "t.id desc";
		$this->search($criteria, "", "",  true);
	}

	/**
	 * View article details.
	 *
	 * @param integer ID of the article to be viewed.
	 */
	public function actionView($id) {
		// Load the record of the requested ID.
		$model = BlogArticle::model()->findByPK($id);

		if (!$model || $model->is_deleted || !$model->status || ($model->is_draft && $model->user_id!=Yii::app()->user->id && !Yii::app()->user->can(UserPermissions::BlogArticle_Publish))){
			throw new CHttpException(404, Yii::t("app","Article not found!"));
		}

		$this->related = array();
		foreach($model->blogArticleRelations as $relation){
			switch ($relation->type) {
				case 'Entity':
					$reference = Entity::model()->visible()->findByAttributes(array('id'=>$relation->referenced_id));
					if (!$reference) continue;
					$this->related[$reference->url] = (string)$reference;
					break;

				case 'School':
					//$reference = School::model()->findByAttributes(array('id'=>$relation->referenced_id));
					break;

				case 'Place':
					$reference = Place::model()->visible()->findByAttributes(array('id'=>$relation->referenced_id));
					if (!$reference) continue;
					$url = $this->createUrl('view', array('id' => $reference->id));
					$this->related[$url] = (string)$reference;
					break;
			}
		}

		// Render view.
		$this->render('view', array(
			'model'=>$model,
		));

	}

	public function actionCategory($id) {

		$cat = BlogCategory::model()->findByPK($id);
		if (!$cat){
			throw new CHttpException(404, Yii::t("app","Category not found!"));
		}
		$this->pageTitle = Yii::app()->name . " - " . (string)$cat;

		$criteria = new CDbCriteria;
		$criteria->compare("category_id",$cat->id);
		$criteria->order = "t.id desc";
		$this->search($criteria,$cat);
	}

	public function search($criteria, $title = "", $message = "", $allowEmpty = false) {
		if (!$allowEmpty && !$criteria->condition) $this->redirect("index",true);

		// Instantiate a BlogCategory model to generate a DataProvider for gridview.
		$model = new BlogArticle('search');
		$model->unsetAttributes();
		$model->visible();

		// Set attributes of gridview filters.
		if (isset($_GET['BlogArticle']))
			$model->setAttributes($_GET['BlogArticle']);

		$criteria->compare("t.is_deleted",0);
		$criteria->compare("t.status",1);

		// Inistantiate data provider to display the records
		$dataProvider = $model->search();
		$dataProvider->criteria->mergeWith($criteria);
		$dataProvider->pagination = array(
			'pageSize'=>12,
		);

		// Render index view.
		$this->render('index', array(
			'model' => $model,
			'dataProvider' => $dataProvider,
		));
	}


}