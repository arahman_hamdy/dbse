<?php
/**
 * This is a Yii controller to display the offers in the frontend.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Frontend
 */

class OffersController extends FrontMainController {
	public $country_id = 1;
	public $blocks = array();

	public function beforeAction($action){
		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$this->pageTitle = Yii::app()->name . " - " . Yii::t('app', 'Offers');

		$criteria = new CDbCriteria;
		$criteria->order = "t.id desc";
		$this->search($criteria, "", "",  true);
	}

	/**
	 * View offer details.
	 *
	 * @param integer ID of the offer to be viewed.
	 */
	public function actionView($id) {
		// Load the record of the requested ID.
		$model = Offer::model()->visible()->findByPK($id);

		if (!$model || $model->is_deleted || !$model->is_active){
			throw new CHttpException(404, Yii::t("app","Offer not found!"));
		}


		// Instantiate an Booking model to add new record.
		$formModel = new Booking;

		if (isset($_POST['Booking'])) {

			if (Yii::app()->user->isGuest){
				Yii::app()->user->loginRequired();
				Yii::app()->end();
			}

			$formModel->persons = 1;
			$formModel->setAttributes($_POST['Booking']);
			$formModel->offer_id = $model->id;
			$formModel->place_id = $model->place_id;
			$formModel->user_id = Yii::app()->user->id;
			$formModel->is_paid = 0;
			$formModel->status = 0;
			$formModel->transaction = null;

			$costs = 0;

			$time_from = explode(":", $formModel->time_from);
			$time_from_converted = 0;
			if (isset($time_from[1])){
				$time_from_converted = ($time_from[0]) + ($time_from[1]/60);
				$formModel->start_date = $formModel->date . ' ' . $formModel->time_from . ':00';
			}

			$time_to = explode(":", $formModel->time_to);
			$time_to_converted = 0;
			if (isset($time_to[1])){
				$time_to_converted = ($time_to[0]) + ($time_to[1]/60);
				$formModel->end_date = $formModel->date . ' ' . $formModel->time_to . ':00';
			}
			
			$hours = 0;
			if ($time_to_converted <= $time_from_converted){
				$time_to_converted += 24;

				$formModel->end_date = date("Y-m-d H:i:s", strtotime("+1 days", strtotime($formModel->end_date)));				
			}

			if (isset($time_from[1]) && isset($time_to[1])){
				$hours = $time_to_converted - $time_from_converted;
			}

			$room = $formModel->room;
			if (!$room || $room->is_deleted || !$room->is_active || $room->place_id != $model->place_id){
				$formModel->room_id = null;
				$room = null;
			}

			if ($room){
				$costs += $room->price;
				$formModel->price = $room->price;
			}
			
			$servicesPrices = array();
			$services = @$_POST['services'];
			if (!$services) $services = array();
			foreach($services as $id){
				$service = Service::model()->findByAttributes(array(
					'id' => $id,
					'place_id' => $model->place_id,
					'is_deleted' => 0,
					'is_active' => 1,
				));
				if (!$service) continue;

				$servicesPrices[$id] = $service->price;
				$costs += $service->price;
			}

			$costs *= $hours;

			$formModel->total = $costs;

			$formModel->dateline = date("Y-m-d H:i:s");
			if ($formModel->save()) {
				foreach($servicesPrices as $id=>$price){
					$bookingDetail = new BookingDetail;
					$bookingDetail->booking_id = $formModel->id;
					$bookingDetail->service_id = $id;
					$bookingDetail->price = $price;
					$bookingDetail->save();
				}

				$paymentOptions = array();
				$paymentOptions["type"] = "offer";
				$paymentOptions["amount"] = $formModel->total;
				$paymentOptions["currency"] = $model->place->country->payment_currency;
				$paymentOptions["reference"] = $model->id;
				$paymentOptions["form_reference"] = $formModel->id;
				$paymentOptions["payment_method"] = $formModel->payment_method;

				if ($formModel->payment_method == 1 && $model->place->accepts_online_payment)
					Yii::app()->payment->makePayment($paymentOptions);
				elseif ($formModel->payment_method == 2 && $model->place->accepts_offline_payment)
					Yii::app()->payment->makePayment($paymentOptions);
			}

		}


		$criteria = new CDbCriteria;
		$criteria->compare('place_id', $model->place_id);
		$criteria->compare('t.is_deleted', 0);
		$criteria->compare('t.is_active', 1);
		$rooms = Room::model()->cache(60)->findAll($criteria);

		if (!$rooms){
			throw new CHttpException(404, Yii::t("app","No rooms available!"));
		}

		$criteria = new CDbCriteria;
		$criteria->compare('place_id', $model->id);
		$criteria->compare('t.is_deleted', 0);
		$criteria->compare('t.is_active', 1);
		$services = Service::model()->cache(60)->findAll($criteria);

		// Render view.
		$this->render('view', array(
			'model'=>$model,
			'formModel'=>$formModel,
			'rooms'=>$rooms,
			'services'=>$services,
		));

	}

	public function actionMyoffers() {
		// Render view.
		$this->render('myoffers', array(
		));

	}

	public function search($criteria, $title = "", $message = "", $allowEmpty = false) {
		if (!$allowEmpty && !$criteria->condition) $this->redirect("index",true);

		// Instantiate a BlogCategory model to generate a DataProvider for gridview.
		$model = new Offer('search');
		$model->unsetAttributes();
		$model->visible();

		// Set attributes of gridview filters.
		if (isset($_GET['Offer']))
			$model->setAttributes($_GET['Offer']);

		$criteria->compare("t.is_deleted",0);
		$criteria->compare("t.is_active",1);
		$criteria->with['place'] = array("with"=>"country");

		// Inistantiate data provider to display the records
		$dataProvider = $model->search();
		$dataProvider->criteria->mergeWith($criteria);
		$dataProvider->pagination = array(
			'pageSize'=>12,
		);

		// Render index view.
		$this->render('index', array(
			'model' => $model,
			'dataProvider' => $dataProvider,
		));
	}

	public function actionReport($id)
	{
		if (Yii::app()->user->getState('offer_report_' . $id,0)){
			throw new CHttpException(403);
		}

		// Load the record of the requested ID.
		$model = Offer::model()->findByPK($id);

		if (!$model || $model->is_deleted || !$model->is_active){
			throw new CHttpException(404, Yii::t("app","Offer not found!"));
		}

		$model->saveCounters(array('reports'=>1));

		Yii::app()->user->setState('offer_report_' . $id,1);
		echo $model->reports;
	}

	public function actionLike($id)
	{
		if (Yii::app()->user->getState('offer_like_' . $id,0)){
			throw new CHttpException(403);
		}

		// Load the record of the requested ID.
		$model = Offer::model()->findByPK($id);

		if (!$model || $model->is_deleted || !$model->is_active){
			throw new CHttpException(404, Yii::t("app","Offer not found!"));
		}

		$model->saveCounters(array('likes'=>1));

		Yii::app()->user->setState('offer_like_' . $id,1);

		echo $model->likes;
	}


}