<?php
/**
 * This is a Yii controller to display Place in the frontend.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Frontend
 */

class PlacesController extends FrontMainController {
	public $country_id = 1;
	public $defaultAction = "latest";
	public $related = array();

	public function actionIndex() {
		$this->forward("latest");
	}

	public function actionLatest() {
		$criteria = new CDbCriteria;
		$criteria->order = "t.id desc";
		$criteria->limit = 90;

		$this->search($criteria,"","",true);
	}

	public function actionSearch($q) {

		$q = trim($q);

		$history = new SearchHistory;
		$history->department = "Coworking Spaces";
		$history->query = $q;
		if (!empty($_GET['PlaceFilter']) && is_array($_GET['PlaceFilter'])){
			$history->advanced = CJSON::encode(array_filter($_GET['PlaceFilter']));
		}
		$history->dateline = date("Y-m-d H:i:s");
		$history->save();

		$criteria = new CDbCriteria;
		$criteria->compare("t.name_ar",$q,true,"OR");
		$criteria->compare("t.name_en",$q,true,"OR");
		$criteria->order = "t.is_premium desc, t.id desc";
		$criteria->group = 't.id';
		$criteria->limit = 90;

		$filters = array();

		if (!empty($_GET['PlaceFilter']) && is_array($_GET['PlaceFilter'])) $filters = $_GET['PlaceFilter'];

		if(!empty($filters['city'])){
			$criteria->with[] = "region";
			$criteria->compare("region.city_id",$filters['city']);
		}

		if(!empty($filters['region_id'])){
			$criteria->compare("t.region_id",$filters['region_id']);
		}


		if(!empty($filters['lowest_price']) && empty($filters['heigest_price'])){
			$criteria->compare("t.lowest_price",'>0');
			$criteria->compare("t.lowest_price",'>='.$filters['lowest_price']);
		}

		if(!empty($filters['heigest_price']) && empty($filters['lowest_price'])){
			$criteria->compare("t.lowest_price",'>0');
			$criteria->compare("t.lowest_price",'<='.$filters['heigest_price']);
		}

		if(!empty($filters['lowest_price']) || !empty($filters['heigest_price'])){
			$criteria->compare("t.heigest_price",'>0');
			$criteria->compare("t.heigest_price",'>='.$filters['lowest_price']);

			$criteria->compare("t.lowest_price",'>0');
			$criteria->compare("t.lowest_price",'<='.$filters['heigest_price']);
		}


		if(!empty($filters['rooms_from']) || !empty($filters['rooms_to'])){
			if (empty($filters['rooms_from'])) $filters['rooms_from']=0;
			if (empty($filters['rooms_to'])) $filters['rooms_to']=99999;
			
			$criteria->compare("t.rooms",'>0');
			$criteria->addBetweenCondition("t.rooms",$filters['rooms_from'],$filters['rooms_to']);
		}
		
		if(!empty($filters['capacity_from']) || !empty($filters['capacity_to'])){
			if (empty($filters['capacity_from'])) $filters['capacity_from']=0;
			if (empty($filters['capacity_to'])) $filters['capacity_to']=99999;

			$criteria->compare("t.capacity",'>0');
			$criteria->addBetweenCondition("t.capacity",$filters['capacity_from'],$filters['capacity_to']);
		}

		// By Location
		if(!empty($_GET['searchmap_location'])){
			list($lat, $lng) = explode(",", $_GET['searchmap_location']);
			$lat = (float)$lat;
			$lng = (float)$lng;
			$distance = 5; // km

			$criteria->select = "*, ( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) as distance";
			$criteria->addCondition("( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) <= $distance");
			$criteria->order = "distance";

		}

		if(!empty($filters['properties']) && is_array($filters['properties'])){
			$criteria->with[] = "placeProperties";
			$criteria->together = true;
			$criteria->group .= ",placeProperties.property_id";
			$criteria->addInCondition("placeProperties.property_id",$filters['properties']);
		}
		$this->search($criteria);
	}

	public function actionCity($id) {

		$city = City::model()->findByPK($id);
		if (!$city){
			throw new CHttpException(404, Yii::t("app","City not found!"));
		}
		$this->pageTitle = Yii::app()->name . " - " . $city;

		$criteria = new CDbCriteria;
		$criteria->with = array("region");
		$criteria->compare("city_id",$city->id);
		$criteria->order = "t.is_premium desc, t.id desc";
		$criteria->limit = 90;
		$this->search($criteria);
	}

	public function actionProperty($id) {

		$property = Property::model()->findByPK($id);
		if (!$property){
			throw new CHttpException(404, Yii::t("app","Property is invalid!"));
		}
		$this->pageTitle = Yii::app()->name . " - " . $property;

		$criteria = new CDbCriteria;
		$criteria->with = array("placeProperties");
		$criteria->together = true;
		$criteria->compare("placeProperties.property_id",$property->id);
		$criteria->order = "t.is_premium desc, t.id desc";
		$criteria->limit = 90;
		$this->search($criteria);
	}

	public function search($criteria, $title = "", $message = "", $allowEmpty = false) {
		if (!$allowEmpty && !$criteria->condition) $this->redirect("index",true);

		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new Place('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['Place']))
			$model->setAttributes($_GET['Place']);

		$criteria->compare("t.is_deleted",0);
		$criteria->compare("t.status",1);

		// Inistantiate data provider to display the records
		$dataProvider = $model->visible()->search();
		$dataProvider->criteria->mergeWith($criteria);
		$dataProvider->pagination = array(
			'pageSize'=>12,
		);

		// Render index view.
		$this->render('index', array(
			'model' => $model,
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * View Place details.
	 *
	 * @param integer ID of the Place to be viewed.
	 */
	public function actionView($id) {

		// Load the record of the requested ID.
		$model = Place::model()->visible()->findByPK($id);

		if (!$model){
			throw new CHttpException(404, Yii::t("app","Place not found!"));
		}

		if (isset($_GET['rating'])) {
			$_GET['rating'] = min(5,intval($_GET['rating']));
			$_GET['rating'] = max(0,intval($_GET['rating']));
			$model->ratings_total = $model->ratings_total+$_GET['rating'];
			$model->ratings_count = $model->ratings_count + 1;
			$model->ratings_avg = (($model->ratings_total)/$model->ratings_count);
			if($model->save(false))
				echo "saved";
			else {
				echo "not saved";
			}
			die;
		}


		//if (!Yii::app()->user->getState('place_visitied_' . $model->id,0)){
			$model->visits++;
		//	Yii::app()->user->setState('place_visitied_' . $model->id,1);
		//}
		$model->save();

		if ($model->loc && !$model->lat){ // Get lat, lng if missing
			// Get cURL resource
			$curl = curl_init();
			// Set some options - we are passing in a useragent too here
			curl_setopt_array($curl, array(
				CURLOPT_URL => $model->loc,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_SSL_VERIFYHOST => 0,
				CURLOPT_NOBODY => true,

				CURLOPT_USERAGENT => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.76 Mobile Safari/537.36',
			));
			// Send the request & save response to $resp
			$resp = curl_exec($curl);
			$info = curl_getinfo($curl);
			// Close request to clear up some resources
			curl_close($curl);

			$model->loc = '';

			if (!empty($info['url'])){
				preg_match('/@(\d+\.\d+),(\d+\.\d+)/', $info['url'], $matches);
				if (empty($matches[1])){
					preg_match('/rllag=(\d+),(\d+)/', $info['url'], $matches);
					if (!empty($matches[1])){
						$matches[1] = $matches[1]/1000000;
						$matches[2] = $matches[2]/1000000;
					}
				}

				if (!empty($matches[1])){
					$model->lat = $matches[1];
					$model->lng = $matches[2];
				}
			}

			$model->save(false);
		}
		
		if (!$model->properties_cache && $model->properties){
			$propertiesArray = CHtml::listData($model->properties,'id','id');
			sort($propertiesArray);
			$model->properties_cache = ",".implode($propertiesArray).",";
			$model->save();
		}


		$criteria = new CDbCriteria;
		$criteria->compare("t.id","<>" . $model->id);
		$criteria->compare("t.is_deleted",0);
		$criteria->compare("t.status",1);
		$criteria->with = array('region');
		$criteria->order = "region.city_id={$model->region->city_id} desc, region_id={$model->region_id} desc, t.id desc";
		$criteria->limit = 6;

		$criteria2 = new CDbCriteria;
		$criteria2->addCondition("t.properties_cache != '' and t.properties_cache like '%{$model->properties_cache}%'");		
		$criteria2->addCondition("t.properties_cache != '' and '%{$model->properties_cache}%' like t.properties_cache",'OR');
		$criteria->mergeWith($criteria2);

		// Instantiate data provider to display the new places		
		$similarPlaces = new CActiveDataProvider('Place', array(
				'criteria' => $criteria,
			    'pagination'=>array(
			      'pageSize'=>$criteria->limit,
			    ),
		));


		$criteria = new CDbCriteria;
		$criteria->addCondition("id!=:id1");
		$criteria->addCondition("id=:id2 or main_branch=:main_branch or main_branch=:id3");
		$criteria->compare("is_deleted",0);
		$criteria->compare("status",1);
		$criteria->order = "id";
		$criteria->params[':id1'] = $model->id;
		$criteria->params[':id2'] = $model->main_branch;
		$criteria->params[':id3'] = $model->id;
		$criteria->params[':main_branch'] = $model->main_branch;
		$branches = Place::model()->findAll($criteria);

		$nearbyPlaces = array();
		if ($model->lat && $model->lng){
			$lat = $model->lat;
			$lng = $model->lng;
			$distance = 10; // km

			$criteria = new CDbCriteria;
			$criteria->select = "*, ( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) as distance";
			$criteria->compare("id", "<>" . $model->id);
			$criteria->compare("is_deleted",0);
			$criteria->compare("is_deleted",0);
			$criteria->compare("status",1);
			$criteria->limit = 20;

			$criteria->addCondition("( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) <= $distance");
			$criteria->order = "distance";

			// Instantiate data provider to display the new places		
			$nearbyPlaces = new CActiveDataProvider('Place', array(
					'criteria' => $criteria,
				    'pagination'=>array(
				      'pageSize'=>$criteria->limit,
				    ),
			));
		}

		$criteria = new CDbCriteria;
		$criteria->with = 'blogArticleRelations';
		$criteria->compare('blogArticleRelations.referenced_id', $model->id);
		$criteria->compare('blogArticleRelations.type', 'Place');
		$criteria->compare('t.is_deleted', 0);
		$criteria->compare('t.status', 1);
		$articles = BlogArticle::model()->cache(60)->findAll($criteria);

		foreach($articles as $article){
			$this->related[$article->url] = (string)$article;
		}

		$criteria = new CDbCriteria;
		$criteria->compare('place_id', $model->id);
		$criteria->compare('t.is_deleted', 0);
		$criteria->compare('t.is_active', 1);
		$roomsAvailable = Room::model()->cache(60)->exists($criteria);

		// Render index view.
		$this->render('view', array(
			'model' => $model,
			'similarPlaces' => $similarPlaces,
			'branches' => $branches,
			'nearbyPlaces' => $nearbyPlaces,
			'roomsAvailable' => $roomsAvailable,
		));

	}

	public function actionBook($id) {
		if (Yii::app()->user->isGuest){
			Yii::app()->user->loginRequired();
			Yii::app()->end();
		}
		// Load the record of the requested ID.
		$model = Place::model()->visible()->findByPK($id);

		if (!$model){
			throw new CHttpException(404, Yii::t("app","Place not found!"));
		}

		$criteria = new CDbCriteria;
		$criteria->compare('place_id', $model->id);
		$criteria->compare('t.is_deleted', 0);
		$criteria->compare('t.is_active', 1);
		$rooms = Room::model()->cache(60)->findAll($criteria);

		if (!$rooms){
			throw new CHttpException(404, Yii::t("app","No booking available!"));
		}

		$criteria = new CDbCriteria;
		$criteria->compare('place_id', $model->id);
		$criteria->compare('t.is_deleted', 0);
		$criteria->compare('t.is_active', 1);
		$services = Service::model()->cache(60)->findAll($criteria);

		// Instantiate an Booking model to add new record.
		$formModel = new Booking;

		if (isset($_POST['Booking'])) {
			$formModel->persons = 1;
			$formModel->setAttributes($_POST['Booking']);
			$formModel->place_id = $model->id;
			$formModel->user_id = Yii::app()->user->id;
			$formModel->is_paid = 0;
			$formModel->status = 0;
			$formModel->transaction = null;

			$costs = 0;

			$time_from = explode(":", $formModel->time_from);
			$time_from_converted = 0;
			if (isset($time_from[1])){
				$time_from_converted = ($time_from[0]) + ($time_from[1]/60);
				$formModel->start_date = $formModel->date . ' ' . $formModel->time_from . ':00';
			}

			$time_to = explode(":", $formModel->time_to);
			$time_to_converted = 0;
			if (isset($time_to[1])){
				$time_to_converted = ($time_to[0]) + ($time_to[1]/60);
				$formModel->end_date = $formModel->date . ' ' . $formModel->time_to . ':00';
			}
			
			$hours = 0;
			if ($time_to_converted <= $time_from_converted){
				$time_to_converted += 24;

				$formModel->end_date = date("Y-m-d H:i:s", strtotime("+1 days", strtotime($formModel->end_date)));				
			}

			if (isset($time_from[1]) && isset($time_to[1])){
				$hours = $time_to_converted - $time_from_converted;
			}

			$room = $formModel->room;
			if (!$room || $room->is_deleted || !$room->is_active || $room->place_id != $model->id){
				$formModel->room_id = null;
				$room = null;
			}

			if ($room){
				$costs += $room->price;
				$formModel->price = $room->price;
			}
			
			$servicesPrices = array();
			$services = @$_POST['services'];
			if (!$services) $services = array();
			foreach($services as $id){
				$service = Service::model()->findByAttributes(array(
					'id' => $id,
					'place_id' => $model->id,
					'is_deleted' => 0,
					'is_active' => 1,
				));
				if (!$service) continue;

				$servicesPrices[$id] = $service->price;
				$costs += $service->price;
			}

			$costs *= $hours;

			$formModel->total = $costs;

			$formModel->dateline = date("Y-m-d H:i:s");
			if ($formModel->save()) {
				foreach($servicesPrices as $id=>$price){
					$bookingDetail = new BookingDetail;
					$bookingDetail->booking_id = $formModel->id;
					$bookingDetail->service_id = $id;
					$bookingDetail->price = $price;
					$bookingDetail->save();
				}

				$paymentOptions = array();
				$paymentOptions["type"] = "booking";
				$paymentOptions["amount"] = $formModel->total;
				$paymentOptions["currency"] = $model->country->payment_currency;
				$paymentOptions["reference"] = $model->id;
				$paymentOptions["form_reference"] = $formModel->id;
				$paymentOptions["payment_method"] = $formModel->payment_method;

				if ($formModel->payment_method == 1 && $model->accepts_online_payment)
					Yii::app()->payment->makePayment($paymentOptions);
				elseif ($formModel->payment_method == 2 && $model->accepts_offline_payment)
					Yii::app()->payment->makePayment($paymentOptions);
			}

		}

		// Render view.
		$this->render('book', array(
			'model' => $model,
			'formModel' => $formModel,
			'rooms' => $rooms,
			'services' => $services,
		));

	}
}