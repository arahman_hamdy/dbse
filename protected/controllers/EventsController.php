<?php
/**
 * This is a Yii controller to display the events in the frontend.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Frontend
 */

class EventsController extends FrontMainController {
	public $country_id = 1;
	public $blocks = array();

	public function beforeAction($action){
		return parent::beforeAction($action);
	}

	public function actionIndex($date = "") {
		$this->pageTitle = Yii::app()->name . " - " . Yii::t('app', 'Events');

		$criteria = new CDbCriteria;
		if ($date){
			$criteria->addBetweenCondition("t.start_date", $date, $date . ' 23:59:59');
		}else{
			$criteria->addCondition("t.end_date > now()");
		}
		$criteria->order = "t.id desc";
		$this->search($criteria, "", "",  true);
	}

	public function actionArchived() {
		$this->pageTitle = Yii::app()->name . " - " . Yii::t('app', 'Archived Events');

		$criteria = new CDbCriteria;
		$criteria->addCondition("t.end_date < now()");
		$criteria->order = "t.id desc";
		$this->search($criteria, "", "",  true);
	}

	/**
	 * View event details.
	 *
	 * @param integer ID of the event to be viewed.
	 */
	public function actionView($id) {
		// Load the record of the requested ID.
		$model = Event::model()->visible()->findByPK($id);

		if (!$model || $model->is_deleted || !$model->is_active){
			throw new CHttpException(404, Yii::t("app","Event not found!"));
		}

		// Instantiate a Place model to add new record.
		$formModel = new EventRegistration;

		if (isset($_POST['EventRegistration'])) {
			if (Yii::app()->user->isGuest){
				Yii::app()->user->loginRequired();
				Yii::app()->end();
			}

			$formModel->setAttributes($_POST['EventRegistration']);
			$formModel->event_id = $model->id;
			$formModel->user_id = Yii::app()->user->id;
			$formModel->is_paid = 0;
			$formModel->status = 0;
			$formModel->transaction = null;

			$formModel->price = $model->price;
			$formModel->total = $formModel->price * $formModel->persons;
			$formModel->dateline = date("Y-m-d H:i:s");
			if ($formModel->save()) {

				$paymentOptions = array();
				$paymentOptions["type"] = "event";
				$paymentOptions["amount"] = $formModel->total;
				$paymentOptions["currency"] = $model->country->payment_currency;
				$paymentOptions["reference"] = $model->id;
				$paymentOptions["form_reference"] = $formModel->id;
				$paymentOptions["payment_method"] = $formModel->payment_method;

				if ($formModel->payment_method == 1 && $model->accepts_online_payment)
					Yii::app()->payment->makePayment($paymentOptions);
				elseif ($formModel->payment_method == 2 && $model->accepts_offline_payment)
					Yii::app()->payment->makePayment($paymentOptions);
			}

		}

		// Render view.
		$this->render('view', array(
			'model'=>$model,
			'formModel'=>$formModel,
		));

	}

	public function actionMyevents() {
		// Render view.
		$this->render('myevents', array(
		));

	}

	public function search($criteria, $title = "", $message = "", $allowEmpty = false) {
		if (!$allowEmpty && !$criteria->condition) $this->redirect("index",true);

		// Instantiate a BlogCategory model to generate a DataProvider for gridview.
		$model = new Event('search');
		$model->unsetAttributes();
		$model->visible();

		// Set attributes of gridview filters.
		if (isset($_GET['Event']))
			$model->setAttributes($_GET['Event']);

		$criteria->compare("t.is_deleted",0);
		$criteria->compare("t.is_active",1);
		$criteria->with['place'] = array("with"=>"country");

		// Inistantiate data provider to display the records
		$dataProvider = $model->search();
		$dataProvider->criteria->mergeWith($criteria);
		$dataProvider->pagination = array(
			'pageSize'=>12,
		);

		// Render index view.
		$this->render('index', array(
			'model' => $model,
			'dataProvider' => $dataProvider,
		));
	}

	public function actionReport($id)
	{
		if (Yii::app()->user->getState('event_report_' . $id,0)){
			throw new CHttpException(403);
		}

		// Load the record of the requested ID.
		$model = Event::model()->findByPK($id);

		if (!$model || $model->is_deleted || !$model->is_active){
			throw new CHttpException(404, Yii::t("app","Event not found!"));
		}

		$model->saveCounters(array('reports'=>1));

		Yii::app()->user->setState('event_report_' . $id,1);
		echo $model->reports;
	}

	public function actionLike($id)
	{
		if (Yii::app()->user->getState('event_like_' . $id,0)){
			throw new CHttpException(403);
		}

		// Load the record of the requested ID.
		$model = Event::model()->findByPK($id);

		if (!$model || $model->is_deleted || !$model->is_active){
			throw new CHttpException(404, Yii::t("app","Event not found!"));
		}

		$model->saveCounters(array('likes'=>1));

		Yii::app()->user->setState('event_like_' . $id,1);

		echo $model->likes;
	}

	public function actionAttending($id)
	{
		if (Yii::app()->user->getState('event_attending_' . $id,0)){
			throw new CHttpException(403);
		}

		// Load the record of the requested ID.
		$model = Event::model()->findByPK($id);

		if (!$model || $model->is_deleted || !$model->is_active){
			throw new CHttpException(404, Yii::t("app","Event not found!"));
		}

		$model->saveCounters(array('attending'=>1));

		Yii::app()->user->setState('event_attending_' . $id,1);

		echo $model->attending;
	}


}