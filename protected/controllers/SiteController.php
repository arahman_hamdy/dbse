<?php

class SiteController extends FrontMainController
{
	public $defaultAction = "landing";

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('/site/error', $error);
		}
	}

	/**
	 * This is the action to handle messages.
	 */
	public function actionMessage()
	{
		$message = array();
		$message['type'] = Yii::app()->user->getState("message_type", "info");
		$message['text'] = Yii::app()->user->getState("message_text");
		$this->render('/site/message', array('message'=>$message));
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$criteria = new CDbCriteria;
		$criteria->order = "sortid";
		$contactFormElements = ContactFormElements::model()->findAll($criteria);

		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$toMail = Yii::app()->params['adminEmail'];
				$setting = Settings::model()->findByAttributes(array('key'=>'contact_email'));
				if ($setting && $setting->value){
					$toMail = $setting->value;
				}

				$name='=?UTF-8?B?'.base64_encode($model->element_1).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->element_3).'?=';
				$headers="From: $name <{$model->element_2}>\r\n".
					"Reply-To: {$model->element_2}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				$body = "";
				foreach($contactFormElements as $contactFormElement){
					if ($contactFormElement->is_system) continue;
					$name = "element_{$contactFormElement->id}";
					$text = $model->$name;
					$body .= $contactFormElement->label_ar . "\n--------------------\n$text\n\n";
				}

				mail($toMail, $subject,$body,$headers);
				Yii::app()->user->setFlash('contact',Yii::t('app','Thank you for contacting us. We will respond to you as soon as possible.'));
				$this->refresh();
			}
		}
		$this->render('/site/contact',array('model'=>$model, 'contactFormElements'=>$contactFormElements));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('/site/login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	/**
	 * Show about page.
	 */
	public function actionAbout()
	{

		// display about view
		$this->render('/site/about');
	}

	
	/**
	 * Show about page.
	 */
	public function actionLanding()
	{

		// display about view
		$this->renderPartial('/site/landing');
	}

	/**
	 * List regions to populate the dropdown.
	 *
	 * @param integer ID of the city.
	 */
	public function actionGetCityRegions($id){
		if (!$id) {echo '{}'; die;}

		// Display name depending on the current language
		$displayField = "name_" . Yii::app()->language;

		// Set query criteria
		$criteria = new CDbCriteria;
		$criteria->select = array("id",$displayField);
		$criteria->compare("city_id",$id);
		$criteria->order = $displayField;
		
		// Get regions matches the criteria
		$regions = Region::model()->findAll($criteria);
		
		if (!$regions) die("{}");
		echo CJSON::encode(CHtml::listData($regions,"id",$displayField));
	}

}