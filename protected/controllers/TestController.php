<?php

Yii::import('application.controllers.SiteController');

class TestController extends SiteController
{
	public static $curl;
	public $defaultAction = "index";

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		self::$curl = curl_init();
		$entities=Entity::model()->findAll("name_en=''");
		foreach ($entities as $entity) {
			$entity->name_en = self::translate("ar","en",$entity->name_ar);
			$entity->contents_en = self::translate("ar","en",$entity->contents_ar);
			$entity->save(false);
			//print_r($entity->attributes);die;
		}
		echo "Done";
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex2()
	{
		self::$curl = curl_init();
		
		$c = new CDbCriteria;
		$c->addNotInCondition("id",[27007,26987,26944,26927,26924,26795,26814,26818,26821,26844,26859,26880,26884,26890]);
		$c->addCondition("value_en=''");
		$c->order = "id asc";

		$entity=EntityAttribute::model()->find($c);
		while ($entity) {
			$translated = "";
			$latin = preg_match("#^[a-zA-Z \(\)\-\s\:\/\.\#\?\&\=0-9_\%]*$#",$entity->value_ar);
			if ($latin || strpos($entity->value_ar,"http")===0 ){
				$entity->value_en = $entity->value_ar;
				$entity->save(false);
			}else{
				$translated = self::translate("ar","en",$entity->value_ar,$entity->id);
				EntityAttribute::model()->updateAll(array("value_en"=>$translated),"value_ar='{$entity->value_ar}'");
			}
			$oldentity = $entity->id;
			$entity=EntityAttribute::model()->find($c);
			if ($oldentity==$entity->id){
				$entity->value_en = $translated ;
				$entity->save(false);
				$entity=EntityAttribute::model()->find($c);
			}
			//print_r($entity->attributes);die;
		}
		echo "Done";
	}	

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex4()
	{
		self::$curl = curl_init();
		
		$c = new CDbCriteria;
		$c->addCondition("targets_text!='' and targets_text is not null");
		$c->order = "id asc";

		$entity=Place::model()->find($c);
		while ($entity) {
			$translated = "";
			$latin = preg_match("#^[a-zA-Z \(\)\-\s\:\/\.\#\?\&\=0-9_\%]*$#",$entity->targets_text);
			$translated = self::translate("en","ar",$entity->targets_text,$entity->id);
			Place::model()->updateAll(array("targets_text"=>$translated),"targets_text='{$entity->targets_text}'");
			$oldentity = $entity->id;
			$entity=Place::model()->find($c);
			if ($oldentity==$entity->id){
				$entity->targets_text = $translated ;
				$entity->save(false);
				$entity=Place::model()->find($c);
			}
			//print_r($entity->attributes);die;
		}
		echo "Done";
	}	

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex3()
	{
		self::$curl = curl_init();
		
		$c = new CDbCriteria;
		$c->addNotInCondition("id",[27007,26987,26944,26927,26924]);
		$c->addCondition("value_en=''");
		$c->order = "id desc";

		$entity=EntityAttribute::model()->find($c);
		while ($entity) {
			$translated = "";
			$latin = preg_match("#^[a-zA-Z \(\)\-\s\:\/\.\#\?\&\=0-9_\%]*$#",$entity->value_ar);
			if ($latin || strpos($entity->value_ar,"http")===0 ){
				$entity->value_en = $entity->value_ar;
				$entity->save(false);
			}else{
				$translated = self::translate("ar","en",$entity->value_ar,$entity->id);
				EntityAttribute::model()->updateAll(array("value_en"=>$translated),"value_ar='{$entity->value_ar}'");
			}
			$oldentity = $entity->id;
			$entity=EntityAttribute::model()->find($c);
			if ($oldentity==$entity->id){
				$entity->value_en = $translated ;
				$entity->save(false);
				$entity=EntityAttribute::model()->find($c);
			}
			//print_r($entity->attributes);die;
		}
		echo "Done";
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex5()
	{
		self::$curl = curl_init();
		$entities=Place::model()->findAll("targets_text!='' and targets_text is not null");
		foreach ($entities as $entity) {
			$entity->targets_text = self::translate("en","ar",$entity->targets_text);
			$entity->save(false);
			//print_r($entity->attributes);die;
		}
		echo "Done";
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionClear()
	{
		Yii::app()->cache->flush();
		echo "Done";
	}


	public static function translate($from, $to, $text, $id=0){
        $cUrl = 'https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20161029T154055Z.d94c41e839aafc9d.114e2f238ded609a1a818dc1a9a51bf847e0d9e6&text=' . urlencode(trim($text)) . '&lang=' . $from . '-' . $to;

        curl_setopt(self::$curl, CURLOPT_URL, $cUrl);
        curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(self::$curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt(self::$curl, CURLOPT_SSL_VERIFYPEER, 0);

        $outString = curl_exec(self::$curl);
        $out = CJSON::decode($outString);
        if (isset($out['code']) && $out['code']=200){
            $ok = 1;
            $message = $out['text'][0];
        }else{
        	echo $id;print_r($outString);die;
        }
        //curl_close(self::$curl);
        return $message;
	}

}
