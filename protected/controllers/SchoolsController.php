<?php

Yii::import('application.controllers.SiteController');

class SchoolsController extends SiteController
{
	public $country_id = 1;
	public $related = array();
	public $defaultAction = "index";

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/coworking/index.php'
		// using the default layout 'protected/views/layouts/main.php'

		$criteria = new CDbCriteria;
		$criteria->compare("t.is_deleted",0);
		$criteria->order = "t.id desc";
		$criteria->limit = 6;
		
		// Inistantiate data provider to display the new schools		
		$newSchools = new CActiveDataProvider('School', array(
				'criteria' => $criteria,
			    'pagination'=>array(
			      'pageSize'=>$criteria->limit,
			    ),
		));

		$criteria = new CDbCriteria;
		$criteria->compare("t.is_deleted",0);
		$criteria->order = "t.visits desc, t.id desc";
		$criteria->limit = 6;
		
		// Inistantiate data provider to display the most visited schools		
		$mostVisitedSchools = new CActiveDataProvider('School', array(
				'criteria' => $criteria,
			    'pagination'=>array(
			      'pageSize'=>$criteria->limit,
			    ),
		));

		$displayName = "t.name_" . Yii::app()->language;
		$criteria = new CDbCriteria;;
		$criteria->select = array('t.id','t.name_en','t.name_ar');
		$criteria->with = array('regions'=>array('with'=>array('schools')));
		$criteria->compare('schools.is_deleted',0);
		$criteria->together = true;
		$criteria->order = $displayName;
		$cities = City::model()->cache(3600)->findAll($criteria);

		// Render index view.
		$this->render('index',array('newSchools'=>$newSchools,'mostVisitedSchools'=>$mostVisitedSchools, 'cities'=>$cities));
	}

	public function actionList() {
		$criteria = new CDbCriteria;
		$criteria->order = "t.id desc";
		$criteria->limit = 90;

		$this->search($criteria,"","",true);
	}

	public function actionSearch($q) {

		$q = trim($q);

		$history = new SearchHistory;
		$history->department = "Schools";
		$history->query = $q;
		if (!empty($_GET['SchoolFilter']) && is_array($_GET['SchoolFilter'])){
			$history->advanced = CJSON::encode(array_filter($_GET['SchoolFilter']));
		}
		$history->dateline = date("Y-m-d H:i:s");
		$history->save();

		$model = new School('search');

		$criteria = new CDbCriteria;
		$criteria->compare("t.name_ar",$q,true,"OR");
		$criteria->compare("t.name_en",$q,true,"OR");
		$criteria->order = "t.id desc";
		$criteria->group = 't.id';
		$criteria->limit = 90;

		$filters = array();

		if (!empty($_GET['SchoolFilter']) && is_array($_GET['SchoolFilter'])) $filters = $_GET['SchoolFilter'];

		if (!empty($filters['city'])){
			$criteria->with[] = "region";
			$criteria->compare("region.city_id",$filters['city']);
		}

		$classifications = array('arabic','languages','azhar','islamic','sisters','hotel','international','british','american','french','germany','ib','turkish','sudanese','libyan');

		$classificationsCriteria = new CDbCriteria;
		foreach($classifications as $classification){
			if (empty($filters[$classification])) continue;
			$classificationsCriteria->compare("t.$classification",1,null,'OR');
			unset($filters[$classification]);
		}
		$criteria->mergeWith($classificationsCriteria);

		$certificates = array('ncacs','cita','msa','sis','naas','madonna','manhattan','advanced','cambridge','iso','naqaae');

		$certificatesCriteria = new CDbCriteria;
		foreach($certificates as $certificate){
			if (empty($filters[$certificate])) continue;
			$certificatesCriteria->compare("t.$classification",1,null,'OR');
			unset($filters[$certificate]);
		}
		$criteria->mergeWith($certificatesCriteria);

		foreach($filters as $key=>$filter){
			if (empty($filter)) continue;
			if (!in_array($key,$model->attributes)) continue;
			if ($key == "city"){
				$criteria->compare("region.city_id",$filter);
			}else{
				$criteria->compare("t.$key",$filter);
			}
			
			unset($filters[$key]);
		}


		// By Location
		if(!empty($_GET['searchmap_location'])){
			list($lat, $lng) = explode(",", $_GET['searchmap_location']);
			$lat = (float)$lat;
			$lng = (float)$lng;
			$distance = 5; // km

			$criteria->select = "*, ( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) as distance";
			$criteria->addCondition("( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) <= $distance");
			$criteria->order = "distance";

		}

		$this->search($criteria);
	}

	public function actionCity($id) {

		$city = City::model()->findByPK($id);
		if (!$city){
			throw new CHttpException(404, Yii::t("app","City not found!"));
		}
		$this->pageTitle = Yii::app()->name . " - " . $city;

		$criteria = new CDbCriteria;
		$criteria->with = array("region");
		$criteria->compare("city_id",$city->id);
		$criteria->order = "t.id desc";
		$criteria->limit = 90;
		$this->search($criteria);
	}

	public function search($criteria, $title = "", $message = "", $allowEmpty = false) {
		if (!$allowEmpty && !$criteria->condition) $this->redirect("index",true);

		// Instantiate a School model to generate a DataProvider for gridview.
		$model = new School('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['School']))
			$model->setAttributes($_GET['School']);

		$criteria->compare("t.is_deleted",0);

		// Inistantiate data provider to display the records
		$dataProvider = $model->visible()->search();
		$dataProvider->criteria->mergeWith($criteria);
		$dataProvider->pagination = array(
			'pageSize'=>12,
		);

		// Render list view.
		$this->render('list', array(
			'model' => $model,
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * View School details.
	 *
	 * @param integer ID of the School to be viewed.
	 */
	public function actionView($id) {

		// Load the record of the requested ID.
		$model = School::model()->visible()->findByPK($id);

		if (!$model){
			throw new CHttpException(404, Yii::t("app","School not found!"));
		}

		//if (!Yii::app()->user->getState('school_visitied_' . $model->id,0)){
			$model->visits++;
		//	Yii::app()->user->setState('school_visitied_' . $model->id,1);
		//}
		$model->save();
		
		$nearbySchools = array();
		if ($model->lat && $model->lng){
			$lat = $model->lat;
			$lng = $model->lng;
			$distance = 10; // km

			$criteria = new CDbCriteria;
			$criteria->select = "*, ( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) as distance";
			$criteria->compare("id", "<>" . $model->id);
			$criteria->compare("is_deleted",0);
			$criteria->limit = 20;

			$criteria->addCondition("( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) <= $distance");
			$criteria->order = "distance";

			// Instantiate data provider to display the new schools		
			$nearbySchools = new CActiveDataProvider('School', array(
					'criteria' => $criteria,
				    'pagination'=>array(
				      'pageSize'=>$criteria->limit,
				    ),
			));
		}

		$criteria = new CDbCriteria;
		$criteria->with = 'blogArticleRelations';
		$criteria->compare('blogArticleRelations.referenced_id', $model->id);
		$criteria->compare('blogArticleRelations.type', 'School');
		$criteria->compare('t.is_deleted', 0);
		$articles = BlogArticle::model()->cache(60)->findAll($criteria);

		foreach($articles as $article){
			$this->related[$article->url] = (string)$article;
		}

		// Render index view.
		$this->render('view', array(
			'model' => $model,
			'nearbySchools' => $nearbySchools,
		));

	}

}
