<?php

Yii::import('application.controllers.SiteController');

class CoworkingController extends SiteController
{
	public $defaultAction = "index";

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/coworking/index.php'
		// using the default layout 'protected/views/layouts/main.php'

		$criteria = new CDbCriteria;
		$criteria->compare("t.is_deleted",0);
		$criteria->compare("t.status",1);
		$criteria->order = "t.id desc";
		$criteria->limit = 6;
		
		// Inistantiate data provider to display the new places		
		$newPlaces = new CActiveDataProvider('Place', array(
				'criteria' => $criteria,
			    'pagination'=>array(
			      'pageSize'=>$criteria->limit,
			    ),
		));

		$criteria = new CDbCriteria;
		$criteria->compare("t.is_deleted",0);
		$criteria->compare("t.status",1);
		$criteria->order = "t.visits desc, t.id desc";
		$criteria->limit = 6;
		
		// Inistantiate data provider to display the most visited places		
		$mostVisitedPlaces = new CActiveDataProvider('Place', array(
				'criteria' => $criteria,
			    'pagination'=>array(
			      'pageSize'=>$criteria->limit,
			    ),
		));

		$displayName = "t.name_" . Yii::app()->language;
		$criteria = new CDbCriteria;;
		$criteria->select = array('t.id','t.name_en','t.name_ar');
		$criteria->with = array('regions'=>array('with'=>array('places')));
		$criteria->compare('places.is_deleted',0);
		$criteria->compare('places.status',1);
		$criteria->together = true;
		$criteria->order = $displayName;
		$cities = City::model()->cache(3600)->findAll($criteria);

		$criteria = new CDbCriteria;;
		$criteria->select = array('t.start_date', 'count(*) as number');
		$criteria->compare('is_deleted',0);
		$criteria->compare('is_active',1);
		$criteria->group = 't.start_date';
		$events = Event::model()->cache(3600)->findAll($criteria);


		// Render index view.
		$this->render('index',array(
			'newPlaces'=>$newPlaces,
			'mostVisitedPlaces'=>$mostVisitedPlaces,
			'cities'=>$cities,
			'events'=>$events,
			));
	}

	public function actionSearch($q) {
		$this->redirect(array('places/search','q'=>$q));
	}	
}
