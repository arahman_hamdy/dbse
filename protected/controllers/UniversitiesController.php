<?php

Yii::import('application.controllers.SiteController');

class UniversitiesController extends SiteController
{
	public $defaultAction = "index";
	public $country_id = 1;
	public $related = array();

	public function init(){
		parent::init();

		$criteria = new CDbCriteria;
		$criteria->compare('show_in_menu',1);
		$criteria->addCondition("t.country_id is null or t.country_id = " . $this->country_id);
		$criteria->order = "sortid";
		$criteria->limit = 12;
		
		$types = EntityType::model()->active()->cache(3600)->findAll($criteria);
		foreach($types as $type){
			$label = (string)$type;
			if ($type->id == 6 && $this->country_id == 2){
				$label = "الجامعات اﻷهلية";
			}
			$this->menus[] = array('label'=>$label, 'url'=>array('/' . $this->id . '/type','id'=>$type->id));
		}
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/coworking/index.php'
		// using the default layout 'protected/views/layouts/main.php'

		$criteria = new CDbCriteria;
		$criteria->addCondition('parent_id is null');
		$criteria->compare('is_active',1);
		$criteria->compare('is_deleted',0);
		$criteria->compare("t.country_id", $this->country_id);
		$criteria->order = "sortid";
		//$criteria->limit = 21;
		
		// Instantiate data provider
		$entities = Entity::model()->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->addCondition("t.country_id is null or t.country_id = " . $this->country_id);
		$criteria->order = "sortid";
		//$criteria->limit = 21;
		
		// Instantiate data provider
		$categories = EntityCategory::model()->findAll($criteria);

		/*
		$entities = new CActiveDataProvider('Entity', array(
				'criteria' => $criteria,
			    'pagination'=>array(
			      'pageSize'=>$criteria->limit,
			    ),
		));
		*/

		$criteria = new CDbCriteria;
		$criteria->compare('slider_id',1);
		$criteria->addCondition("t.country_id is null or t.country_id = " . $this->country_id);
		$criteria->order = "sortid";
		$criteria->limit = 12;
		
		$sliderImages = SliderImage::model()->findAll($criteria);

		// Render index view.
		$this->render('/universities/index',array('entities'=>$entities,'categories'=>$categories,'sliderImages'=>$sliderImages));
	}

	public function actionType($id)
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id',$id);
		$criteria->addCondition("t.country_id is null or t.country_id = " . $this->country_id);

		$entityType = EntityType::model()->active()->find($criteria);
		if (!$entityType){
			throw new CHttpException(404, Yii::t("app","Category not found!"));
		}

		$criteria = new CDbCriteria;
		$criteria->compare('type',$id);
		$criteria->compare("t.country_id", $this->country_id);

		$this->search($criteria, (string)$entityType);
	}

	public function actionCat($id)
	{

		$criteria = new CDbCriteria;
		$criteria->compare('id',$id);
		$criteria->addCondition("t.country_id is null or t.country_id = " . $this->country_id);

		$category = EntityCategory::model()->find($criteria);
		if (!$category){
			throw new CHttpException(404, Yii::t("app","Category not found!"));
		}

		$criteria = new CDbCriteria;
		$criteria->compare('category_id',$id);
		$criteria->compare("t.country_id", $this->country_id);

		$this->search($criteria, (string)$category);
	}

	public function actionSearch($q = "") {
		$q = trim($q);
		
		$history = new SearchHistory;
		$history->department = "Universities";
		$history->query = $q;
		$history->dateline = date("Y-m-d H:i:s");
		$history->save();


		$criteria = new CDbCriteria;
		$criteria->compare("t.name_ar",$q,true,"OR");
		$criteria->compare("t.name_en",$q,true,"OR");
		$criteria->group = 't.id';
		$criteria->limit = 90;

		
		$filters = array();

		if (!empty($_GET['UniversityFilter']) && is_array($_GET['UniversityFilter'])) $filters = $_GET['UniversityFilter'];

		if (!empty($_GET['attribute']) && is_array($_GET['attribute'])) $filters['attributes'] = $_GET['attribute'];

		if (!empty($filters['college'])){
			$this->redirect(array('/universities/view','id'=>$filters['college']),true);
		}

		if(empty($filters['college']) && empty($_GET['result_type']) && !empty($filters['type'])){
			$criteria->compare("type",$filters['type']);
		}

		if(!empty($filters['city'])){
			$criteria->with[] = "region";
			$criteria->compare("region.city_id",$filters['city']);
		}

		if(!empty($filters['region_id'])){
			$criteria->compare("t.region_id",$filters['region_id']);
		}


		if (!empty($filters['college'])){
			$criteria->compare('id',$filters['college']);
		}

		if (!empty($filters['university']) && !empty($filters['college'])){
			$criteria->compare('parent_id',$filters['university']);
		}

		if (!empty($filters['university']) && empty($filters['college'])){
			$criteria->compare('id',$filters['university']);
		}

       $certificates = array(
            'thnwy_adby',
            'thnwy_elmy_math',
            'thnwy_elmy_science',
            'thnwy_sena3y',
            'thnwy_tegary',
            'thnwy_zera3y',
            'deb_seyaha',
            'thnwy_azhar',
            'deb_american',
            'deb_british',
            'mo3adala',
        );

		if (!empty($filters['certificate']) && in_array($filters['certificate'], $certificates)){
			$criteria->compare($filters['certificate'],1);
		}

		if (!empty($filters['division'])){
			$divisionCriteria = new CDbCriteria;

			$divisionCriteria->compare('t.c1',$filters['division'],null,'OR');
			$divisionCriteria->compare('t.c2',$filters['division'],null,'OR');
			$divisionCriteria->compare('t.c3',$filters['division'],null,'OR');

			$criteria->mergeWith($divisionCriteria);
		}


		$requires_attribute = !empty($filters['attributes']);

		// Result type
		if(!empty($_GET['result_type'])){
			$types = array();

			switch($_GET['result_type']){
				case 'university':
					$types = array(2,5,6,7,9);
				break;
				case 'college':
					$types = array(3);
				break;

				case 'institute':
					$types = array(4);
				break;
			}

			if ($types)
				$criteria->addInCondition("type",$types);
		}

		// Fees
		if(!empty($_GET['fees'][0]) || !empty($_GET['fees'][1])){

			if (empty($_GET['fees'][0])) $_GET['fees'][0]=0;
			if (empty($_GET['fees'][1])) $_GET['fees'][1]=99999;

			$subCriteria = new CDbCriteria;
			
			$subCriteria->addInCondition("entityAttributes.entity_type_attribute_id",array(96, 97, 113));
			$subCriteria->addBetweenCondition("entityAttributes.value_en",$_GET['fees'][0],$_GET['fees'][1]);

			$criteria->mergeWith($subCriteria);
			$requires_attribute = true;
		}

		/*
		// Admission Certificates
		if(!empty($_GET['certificate']) && in_array($_GET['certificate'], $certificates)){

			$criteria->compare($_GET['certificate'],1);
		}
		*/

		// Degrees Awarded
		if(!empty($_GET['degree'])){

			$subCriteria = new CDbCriteria;
			
			$subCriteria->addInCondition("entityAttributes.entity_type_attribute_id",array(79, 109, 114));
			$subCriteria->compare("entityAttributes.value_ar",$_GET['degree'],true);

			$criteria->mergeWith($subCriteria);
			$requires_attribute = true;
		}

		// Years of study
		if(!empty($_GET['years'][0]) || !empty($_GET['years'][1])){

			if (empty($_GET['years'][0])) $_GET['years'][0]=0;
			if (empty($_GET['years'][1])) $_GET['years'][1]=99999;

			$subCriteria = new CDbCriteria;
			
			$subCriteria->addInCondition("entityAttributes.entity_type_attribute_id",array(78, 91, 108));
			$subCriteria->addBetweenCondition("entityAttributes.value_en",$_GET['years'][0],$_GET['years'][1]);

			$criteria->mergeWith($subCriteria);
			$requires_attribute = true;
		}

		// 4ICU
		if(!empty($_GET['rank_4icu'][0]) || !empty($_GET['rank_4icu'][1])){

			if (empty($_GET['rank_4icu'][0])) $_GET['rank_4icu'][0]=0;
			if (empty($_GET['rank_4icu'][1])) $_GET['rank_4icu'][1]=99999;

			$subCriteria = new CDbCriteria;
			
			$subCriteria->addInCondition("entityAttributes.entity_type_attribute_id",array(35, 55, 70));
			$subCriteria->addBetweenCondition("entityAttributes.value_en",$_GET['rank_4icu'][0],$_GET['rank_4icu'][1]);

			$criteria->mergeWith($subCriteria);
			$requires_attribute = true;
		}

		// Shanghai
		if(!empty($_GET['rank_shanghai'][0]) || !empty($_GET['rank_shanghai'][1])){

			if (empty($_GET['rank_shanghai'][0])) $_GET['rank_shanghai'][0]=0;
			if (empty($_GET['rank_shanghai'][1])) $_GET['rank_shanghai'][1]=99999;

			$subCriteria = new CDbCriteria;
			
			$subCriteria->addInCondition("entityAttributes.entity_type_attribute_id",array(34, 54, 69));
			$subCriteria->addBetweenCondition("entityAttributes.value_en",$_GET['rank_shanghai'][0],$_GET['rank_shanghai'][1]);

			$criteria->mergeWith($subCriteria);
			$requires_attribute = true;
		}

		// Webmetrics
		if(!empty($_GET['rank_webmetrics'][0]) || !empty($_GET['rank_webmetrics'][1])){

			if (empty($_GET['rank_webmetrics'][0])) $_GET['rank_webmetrics'][0]=0;
			if (empty($_GET['rank_webmetrics'][1])) $_GET['rank_webmetrics'][1]=99999;

			$subCriteria = new CDbCriteria;
			
			$subCriteria->addInCondition("entityAttributes.entity_type_attribute_id",array(33, 53, 68));
			$subCriteria->addBetweenCondition("entityAttributes.value_en",$_GET['rank_webmetrics'][0],$_GET['rank_webmetrics'][1]);

			$criteria->mergeWith($subCriteria);
			$requires_attribute = true;
		}

		// By Location
		if(!empty($_GET['searchmap_location'])){
			list($lat, $lng) = explode(",", $_GET['searchmap_location']);
			$lat = (float)$lat;
			$lng = (float)$lng;
			$distance = 5; // km

			$criteria->select = "*, ( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) as distance";
			$criteria->addCondition("( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) <= $distance");
			$criteria->order = "distance";
		}


		if ($requires_attribute){
			$criteria->with[] = "entityAttributes";
			$criteria->together = true;
			$criteria->group .= ",entityAttributes.id";
		}


		$attributesCriteria = new CDbCriteria;

		if (!empty($filters['attributes']['direct'])){
			foreach($filters['attributes']['direct'] as $id=>$value){
				if ($value === "") continue;
				$subCriteria = new CDbCriteria;
				$subCriteria->compare("entityAttributes.entity_type_attribute_id",$id);
				$orCriteria = new CDbCriteria;
				$orCriteria->compare("entityAttributes.value_en",$value,true);
				$orCriteria->compare("entityAttributes.value_ar",$value,true,"OR");
				$subCriteria->mergeWith($orCriteria);

				$attributesCriteria->mergeWith($subCriteria);
			}
		}


		if (!empty($filters['attributes']['range'])){
			foreach($filters['attributes']['range'] as $id=>$values){
				if(empty($values[0]) && empty($values[1])) continue;

				if (empty($values[0])) $values[0]=0;
				if (empty($values[1])) $values[1]=99999;

				$subCriteria = new CDbCriteria;
				
				$subCriteria->compare("entityAttributes.entity_type_attribute_id",$id);
				$subCriteria->addBetweenCondition("entityAttributes.value_en",$values[0],$values[1]);

				$attributesCriteria->mergeWith($subCriteria);
			}
		}

		$criteria->mergeWith($attributesCriteria);

		/*
		if(!empty($filters['heigest_price']) && empty($filters['lowest_price'])){
			$criteria->compare("t.lowest_price",'>0');
			$criteria->compare("t.lowest_price",'<='.$filters['heigest_price']);
		}

		if(!empty($filters['lowest_price']) || !empty($filters['heigest_price'])){
			$criteria->compare("t.heigest_price",'>0');
			$criteria->compare("t.heigest_price",'>='.$filters['lowest_price']);

			$criteria->compare("t.lowest_price",'>0');
			$criteria->compare("t.lowest_price",'<='.$filters['heigest_price']);
		}


		if(!empty($filters['rooms_from']) || !empty($filters['rooms_to'])){
			if (empty($filters['rooms_from'])) $filters['rooms_from']=0;
			if (empty($filters['rooms_to'])) $filters['rooms_to']=99999;
			
			$criteria->compare("t.rooms",'>0');
			$criteria->addBetweenCondition("t.rooms",$filters['rooms_from'],$filters['rooms_to']);
		}
		
		if(!empty($filters['capacity_from']) || !empty($filters['capacity_to'])){
			if (empty($filters['capacity_from'])) $filters['capacity_from']=0;
			if (empty($filters['capacity_to'])) $filters['capacity_to']=99999;

			$criteria->compare("t.capacity",'>0');
			$criteria->addBetweenCondition("t.capacity",$filters['capacity_from'],$filters['capacity_to']);
		}


		if(!empty($filters['properties']) && is_array($filters['properties'])){
			$criteria->with[] = "placeProperties";
			$criteria->together = true;
			$criteria->addInCondition("placeProperties.property_id",$filters['properties']);
		}
		*/
		$this->search($criteria);
	}

	public function actionCity($id) {

		$city = City::model()->findByPK($id);
		if (!$city){
			throw new CHttpException(404, Yii::t("app","City not found!"));
		}
		$this->pageTitle = Yii::app()->name . " - " . $city;

		$criteria = new CDbCriteria;
		$criteria->with = array("region");
		$criteria->compare("city_id",$city->id);
		$criteria->order = "t.id desc";
		$criteria->limit = 90;
		$this->search($criteria);
	}

	public function actionPage($p) {
		$criteria = new CDbCriteria;
		$criteria->compare('slug',$p);
		$criteria->compare('is_active',1);
		$criteria->compare('is_deleted',0);

		$entity = Entity::model()->find($criteria);
		if (!$entity){
			throw new CHttpException(404, Yii::t("app","Page not found!"));
		}

		$this->actionView($entity->id);
	}

	public function search($criteria, $title = "", $message = "", $allowEmpty = false) {
		if (!$allowEmpty && !$criteria->condition) $this->redirect("index",true);

		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new Entity('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['Entity']))
			$model->setAttributes($_GET['Entity']);
		else
			$_GET['Entity'] = array('1'=>'1');

		$model->has_contents = 1;
		$model->country = 1;
		// Inistantiate data provider to display the records
		$dataProvider = $model->visible()->search();
		$dataProvider->criteria->mergeWith($criteria);
		$dataProvider->pagination = array(
			'pageSize'=>30,
		);

		// Render index view.
		$this->render('/universities/search',array('entities'=>$dataProvider, 'title'=>$title, 'message'=>$message));

	}

	/**
	 * View Place details.
	 *
	 * @param integer ID of the Place to be viewed.
	 */
	public function actionView($id) {

		// Load the record of the requested ID.
		$model = Entity::model()->visible()->findByPK($id);

		if (!$model || !$model->has_contents){
			throw new CHttpException(404, Yii::t("app","Page not found!"));
		}

		if ($model->_location && !$model->lat){ // Get lat, lng if missing
			// Get cURL resource
			$curl = curl_init();
			// Set some options - we are passing in a useragent too here
			curl_setopt_array($curl, array(
				CURLOPT_URL => $model->_location,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_SSL_VERIFYHOST => 0,
				CURLOPT_NOBODY => true,

				CURLOPT_USERAGENT => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.76 Mobile Safari/537.36',
			));
			// Send the request & save response to $resp
			$resp = curl_exec($curl);
			$info = curl_getinfo($curl);
			// Close request to clear up some resources
			curl_close($curl);

			$model->_location = '';

			if (!empty($info['url'])){
				preg_match('/@(\d+\.\d+),(\d+\.\d+)/', $info['url'], $matches);
				if (empty($matches[1])){
					preg_match('/rllag=(\d+),(\d+)/', $info['url'], $matches);
					if (!empty($matches[1])){
						$matches[1] = $matches[1]/1000000;
						$matches[2] = $matches[2]/1000000;
					}
				}

				if (!empty($matches[1])){
					$model->lat = $matches[1];
					$model->lng = $matches[2];
				}
			}

			$model->save(false);
		}

		$criteria = new CDbCriteria;
		$criteria->with = array('entities'=>array('order'=>'entities.sortid'));
		$criteria->together = true;
		$criteria->compare('entities.parent_id',$id);
		$criteria->compare('entities.is_active',1);
		$criteria->compare('entities.is_deleted',0);
		$criteria->addCondition("t.country_id is null or t.country_id = " . $this->country_id);
		$criteria->order = "t.sortid";

		// Instantiate data provider		
		$subEntities = EntityType::model()->active()->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->compare('t.entity_id',$id);
		$criteria->order = "t.sortid";

		// Instantiate data provider		
		$campuses = Campus::model()->findAll($criteria);
		foreach($campuses as $campus){
			if ($campus->_location && !$campus->lat){ // Get lat, lng if missing
				// Get cURL resource
				$curl = curl_init();
				// Set some options - we are passing in a useragent too here
				curl_setopt_array($curl, array(
					CURLOPT_URL => $campus->_location,
					CURLOPT_FOLLOWLOCATION => 1,
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_SSL_VERIFYPEER => 0,
					CURLOPT_SSL_VERIFYHOST => 0,
					CURLOPT_NOBODY => true,

					CURLOPT_USERAGENT => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.76 Mobile Safari/537.36',
				));
				// Send the request & save response to $resp
				$resp = curl_exec($curl);
				$info = curl_getinfo($curl);
				// Close request to clear up some resources
				curl_close($curl);

				$campus->_location = '';

				if (!empty($info['url'])){
					preg_match('/@(\d+\.\d+),(\d+\.\d+)/', $info['url'], $matches);
					if (empty($matches[1])){
						preg_match('/rllag=(\d+),(\d+)/', $info['url'], $matches);
						if (!empty($matches[1])){
							$matches[1] = $matches[1]/1000000;
							$matches[2] = $matches[2]/1000000;
						}
					}

					if (!empty($matches[1])){
						$campus->lat = $matches[1];
						$campus->lng = $matches[2];
					}
				}

				
				$campus->save(false);
			}
		}


		$sameUniversityEntities = array();
		if ($model->parent_id){
			$criteria = new CDbCriteria;
			$criteria->compare("t.id",'<>' . $model->id);
			$criteria->compare("t.parent_id",$model->parent_id);
			$criteria->compare('t.is_active',1);
			$criteria->compare('t.is_deleted',0);
			$criteria->compare("t.country_id", $this->country_id);

			$sameUniversityEntities = Entity::model()->findAll($criteria);
		}

		$sameCategoryEntities = array();
		if ($model->category_id){
			$criteria = new CDbCriteria;
			$criteria->compare("t.id",'<>' . $model->id);
			$criteria->compare("t.category_id",$model->category_id);
			$criteria->compare('t.is_active',1);
			$criteria->compare('t.is_deleted',0);
			$criteria->compare("t.country_id", $this->country_id);
			$criteria->addCondition("t.parent_id is not null");

			$sameCategoryEntities = Entity::model()->findAll($criteria);
		}

		$nearbyPlaces = array();
		if ($model->lat && $model->lng){
			$lat = $model->lat;
			$lng = $model->lng;
			$distance = 10; // km

			$criteria = new CDbCriteria;
			$criteria->select = "*, ( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) as distance";
			$criteria->compare("is_deleted",0);
			$criteria->compare("is_deleted",0);
			$criteria->compare("status",1);
			$criteria->limit = 20;

			$criteria->addCondition("( 3959 * 1.60934 * acos( cos( radians($lat) ) * cos( radians( lat ) ) 
* cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) <= $distance");
			$criteria->order = "distance";

			// Instantiate data provider to display the new places		
			$nearbyPlaces = new CActiveDataProvider('Place', array(
					'criteria' => $criteria,
				    'pagination'=>array(
				      'pageSize'=>$criteria->limit,
				    ),
			));
		}


		$criteria = new CDbCriteria;
		$criteria->with = 'blogArticleRelations';
		$criteria->compare('blogArticleRelations.referenced_id', $model->id);
		$criteria->compare('blogArticleRelations.type', 'Entity');
		$criteria->compare('t.is_deleted', 0);
		$criteria->compare('t.status', 1);
		$articles = BlogArticle::model()->cache(60)->findAll($criteria);

		foreach($articles as $article){
			$this->related[$article->url] = (string)$article;
		}

		// Render index view.
		$this->render('/universities/view', array(
			'model' => $model,
			'subEntities' => $subEntities,
			'campuses' => $campuses,
			'sameCategoryEntities' => $sameCategoryEntities,
			'sameUniversityEntities' => $sameUniversityEntities,
			'nearbyPlaces' => $nearbyPlaces,
		));

	}

	public function actionEntityAttributes($id, $type = null, $university = null){
		$criteria = new CDbCriteria;
		$criteria->compare("entity_type_id",$id);
		$criteria->compare("is_searchable",1);
		
		$criteria->order = "sortid";
		$attributes = EntityTypeAttribute::model()->findAll($criteria);
		//if (!$attributes){
		//	Yii::app()->end();
		//}

		$criteria = new CDbCriteria;
		$criteria->compare("t.type",$id);
		$criteria->compare('t.is_active',1);
		$criteria->compare('t.is_deleted',0);
		$criteria->compare("t.country_id", $this->country_id);
		//$criteria->with = 'type0';
		//$criteria->compare('type0.show_in_menu',1);
		//$criteria->order = "t.sortid";
		$universities = Entity::model()->findAll($criteria);

		if ($university){
			$criteria = new CDbCriteria;
			$criteria->addInCondition("type",array(3,4));
			$criteria->compare("parent_id",$university);
			$criteria->compare('is_active',1);
			$criteria->compare('is_deleted',0);
			$criteria->compare("t.country_id", $this->country_id);
			$criteria->order = "sortid";
			$colleges = Entity::model()->findAll($criteria);
		}else{
			$colleges = null;
		}

		if (!empty($_GET['university'])){
			$_GET['UniversityFilter']['university'] = $_GET['university'];
		}

		// Render index view.
		$this->renderPartial('/universities/_entity_attributes', array(
			'attributes' => $attributes,
			'universities' => $universities,
			'colleges' => $colleges,
		));


				
	}
}
