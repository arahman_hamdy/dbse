<?php
/**
 * This is a Yii controller to display the membership in the frontend.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Frontend
 */

class MembershipController extends FrontUserController {
	public $country_id = 1;
	public $blocks = array();

	public function beforeAction($action){
		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$criteria = new CDbCriteria;
		$criteria->select = array("*", "premia.id>0 as i_premium");
		$criteria->compare("t.user_id", $this->user->id);
		$criteria->compare("t.is_deleted",0);
		$criteria->compare("t.status",1);

		$criteria->with["premia"] = array("on"=>"premia.is_active = 1 and premia.user_id = " . $this->user->id);
		$criteria->together = true;

		$criteria->order = "t.id desc";

		$places = new CActiveDataProvider('Place', array(
			'criteria' => $criteria,
		));

		// Render view.
		$this->render('index', array(
			'places'=>$places,
		));

	}

	public function actionSubscribe($id) {
		// Instantiate a Place model to add new record.
		$formModel = new Premium;

		$formModel->user_id = $this->user->id;
		$formModel->place_id = $id;
		$formModel->is_active = 0;
		$formModel->dateline = date("Y-m-d H:i:s");
		$formModel->transaction = null;

		if ($formModel->save()) {

			$paymentOptions = array();
			$paymentOptions["type"] = "premium";
			$paymentOptions["amount"] = 25;
			$paymentOptions["currency"] = 'USD';
			$paymentOptions["reference"] = $id;
			$paymentOptions["form_reference"] = $formModel->id;
			$paymentOptions["payment_method"] = 1;

			Yii::app()->payment->makePayment($paymentOptions);
		}
	}

}