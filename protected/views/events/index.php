<?php
$this->breadcrumbs = array(
    Yii::t('app','Events') => array('index'),
);
?>

<div class="container">
    <div class="row"> 
    	<div class="clearfix" style="margin-top: 20px;"></div>
<div class="col-md-12">

<?php if ($this->action->id == "archived"): ?>
<a href="<?php echo $this->createUrl("index"); ?>" class="last_event"><< <?php echo Yii::t("app","Current events"); ?></a>
<?php else: ?>
<a href="<?php echo $this->createUrl("archived"); ?>" class="last_event"><< <?php echo Yii::t("app","Archived events"); ?></a>
<?php endif; ?>
<div id="offers-grid" class="blog-grid">


    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'_view',
        'itemsCssClass'=>'',
        'pagerCssClass'=>'pagination-ourter text-center',
        'pager'=>array(
            'class'=>'CLinkPager',
            'htmlOptions'=>array(
                'class'=>"pagination"
                ),
            'firstPageLabel' => "<<",
            'lastPageLabel' => ">>",
            'nextPageLabel' => ">",
            'prevPageLabel' => "<",
            'selectedPageCssClass' => 'active',
            'header'=>'',
            'footer'=>'',

        ),
    )); 
    ?>

	</div>
</div>
</div>
</div>
