<?php
$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);
$this->pageTitle = Yii::app()->name . ' - ' . GxHtml::valueEx($model);
?>
<div class="container">
    <div class="row"> 
    	<div class="clearfix" style="margin-top: 20px;"></div>
        <div class="col-md-9">
            <div class="blog-single">
                <!-- post begin -->
                <article>
                    <div class="post-metadata visible-lg-block hidden-lg">
                        <h3><?php echo $model; ?></h3>
                    </div>

                    <div class="post-media">
                        <img src="<?php echo $model->imageUrl; ?>" class="img-responsive">                                    
                    </div>
                    
                    <br />

                    <?php if($model->lat && $model->lng): ?>
                    <a href="#map">
                    <?php endif; ?>
                    <i class="fa fa-fw fa-map-marker"></i><span><?php echo $model->{"address_" . Yii::app()->language};?></span>
                    <?php if($model->lat && $model->lng): ?>
                    </a>
                    <?php endif; ?>
                    <hr />
                    <div class="post-content">
                        <div class="post-entry">
                            <?php echo $model->{"description_" . Yii::app()->language}; ?>
                            <div class="hr"></div>
                           
                            <h3><?php echo Yii::t("app", "Event Information")?></h3>
                            <ul class="list-style-1">
                                <li><?php echo Yii::t("app", "Start Date")?>  <?php echo date("Y-m-d", strtotime($model->start_date)); ?></li>
                                <li><?php echo Yii::t("app", "End Date")?>: <?php echo date("Y-m-d", strtotime($model->end_date)); ?></li>
                                <li><?php echo Yii::t("app", "Cost")?>: <?php echo $model->price . ' ' . $model->country->payment_currency?></li>
                                <?php if ($model->last_payment_date): ?>
                                <li><?php echo Yii::t("app", "Last payment date")?>: <?php echo date("Y-m-d", strtotime($model->last_payment_date)); ?></li>
                                <?php endif; ?>
                                <?php if ($model->{"organization_" . Yii::app()->language}): ?>
                                <li><?php echo Yii::t("app", "Organization")?>: <?php echo $model->{"organization_" . Yii::app()->language}; ?></li>
                                <?php endif; ?>
                            </ul>                                                                  
                        </div>
                        <div class="clearfix"></div>

                        <div id="map">
                        </div>
                        <?php if($model->lat && $model->lng): ?>
                        <hr style="padding: 5px;">
                        <div>
                            <div style='overflow:hidden;height:440px;width:100%;'>
                                <div id='gmap_canvas' style='height:440px;width:100%;'></div>
                                <div><small><a href="http://embedgooglemaps.com">                                   embed google maps                           </a></small></div>
                                <div><small><a href="https://termsofusegenerator.net">terms of use example</a></small></div>
                                <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
                            </div>
                            <script type='text/javascript'>function init_map(){var myOptions = {zoom:15,center:new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>)});infowindow = new google.maps.InfoWindow({content:'<div style="margin-right: 15px;"><strong><?php echo $model;?></strong><br><?php echo $model->{"address_" . Yii::app()->language};?><br></div>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
                            <br />
                        </div>
                        <?php endif; ?>

                        <?php if($model->{"notes_" . Yii::app()->language}): ?>
                        <hr />
                        <?php echo $model->{"notes_" . Yii::app()->language}; ?>
                        <hr />
                        <?php endif; ?>


                    <div id="booking-form">
                        <div class="hr"></div>
                        <div class="clearfix"></div>

                       <div class="white-popup" style="padding: 0; margin: 0;">
                        <h3><?php echo Yii::t("app", "Booking details"); ?></h3>

                <?php if (Yii::app()->user->isGuest):?>
                        <?php echo GxHtml::link(Yii::t('app', 'Login to book'), $this->createUrl("//user/login"),array('class'=>'btn btn-primary btn-big')); ?>
                        <div class="clearfix"></div>
                        <br />
                <?php elseif ((!$model->accepts_online_payment && !$model->accepts_offline_payment) || ($model->last_payment_date && strtotime($model->last_payment_date . ' 23:59:59')<time()) || ($model->end_date && strtotime($model->end_date)<time())):?>
                    <p><?php echo Yii::t("app","Booking to this event already closed!");?></p>
                <?php else: ?>
                    <?php $form = $this->beginWidget('GxActiveForm', array(
                        'id' => 'event-registration-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                            'afterValidate' => 'js:function(form, data, hasError) { 
                                if(hasError) {
                                    for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
                                    return false;
                                }
                                else {
                                    form.children().removeClass("validate-has-error");
                                    return true;
                                }
                            }',
                            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                                if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
                                else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
                            }'
                        ),
                        'htmlOptions'=>array(
                            'class'=>'form-horizontal form-groups-bordered',
                            'enctype' => 'multipart/form-data',
                        )
                    ));
                    ?>
                        <?php if ($formModel->errors): ?> 
                            <div class="alert alert-danger"> 
                                <?php echo $form->errorSummary($formModel); ?> 
                            </div> 
                        <?php endif; ?> 
                            <div class="form-group"> 
                                <?php echo $form->labelEx($formModel,'name'); ?>
                                <div class="col-sm-9"> 
                                    <?php echo $form->textField($formModel, 'name', array('maxlength' => 100)); ?>
                                    <?php echo $form->error($formModel,'name'); ?>
                                </div> 
                            </div> 
                            <div class="form-group"> 
                                <?php echo $form->labelEx($formModel,'phone'); ?>
                                <div class="col-sm-9"> 
                                    <?php echo $form->textField($formModel, 'phone', array('maxlength' => 50)); ?>
                                    <?php echo $form->error($formModel,'phone'); ?>
                                </div> 
                            </div> 
                            <div class="form-group"> 
                                <?php echo $form->labelEx($formModel,'email'); ?>
                                <div class="col-sm-9"> 
                                    <?php echo $form->textField($formModel, 'email', array('maxlength' => 50)); ?>
                                    <?php echo $form->error($formModel,'email'); ?>
                                </div> 
                            </div> 
                            <div class="form-group"> 
                                <?php echo $form->labelEx($formModel,'persons'); ?>
                                <div class="col-sm-9"> 
                                    <?php echo $form->textField($formModel, 'persons'); ?>
                                    <?php echo $form->error($formModel,'persons'); ?>
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <?php echo $form->labelEx($formModel,'payment_method'); ?>
                                <div class="col-sm-9"> 
                                    <?php
                                    $payment_options = array();
                                    if ($model->accepts_online_payment){
                                        $payment_options[1] = Yii::t("app", "Pay online now");
                                    }
                                    if ($model->accepts_offline_payment){
                                        $payment_options[2] = Yii::t("app", "Book now, pay later");
                                    }

                                    ?>
                                    <?php echo $form->dropDownList($formModel, 'payment_method', $payment_options); ?>
                                    <?php echo $form->error($formModel,'payment_method'); ?>
                                </div> 
                            </div> 
                            <div class="form-group"> 
                                <?php echo $form->labelEx($formModel,'notes'); ?>
                                <div class="col-sm-9"> 
                                    <?php echo $form->textField($formModel, 'notes', array('maxlength' => 200)); ?>
                                    <?php echo $form->error($formModel,'notes'); ?>
                                </div> 
                            </div> 


                    <div class="form-group"> 
                        <div class="col-sm-3"> 
                        </div>
                        <div class="col-sm-9"> 
                            <?php echo GxHtml::submitButton(Yii::t('app', 'Book now'), array('class'=>'btn btn-primary btn-big')); ?>
                        </div> 
                    </div> 
                    <?php $this->endWidget(); ?><!-- form -->
                <?php endif; ?>
                     </div>
                     </div>                        
                    </div>
                </article>
                <!-- post close -->
            </div>                       
        </div>
        <div class="col-md-3">
           <div class="like_block col-md-12"> 
               <div class="clearfix"></div>
               <ul> 
                   
                    <li class="pull-right" id="like_tab">
                        <a class="event_like_button" href="#">Like</a>
                        <span class="counter"><span class="likes_counter"><?php echo $model->likes;?></span> <?php echo Yii::t("app", "likes");?></span>
                    </li>
                    <li class="pull-right" id="report_tab">
                        <a class="event_report_button" href="#">Report</a>
                        <span class="counter"><span class="reports_counter"><?php echo $model->reports;?></span> <?php echo Yii::t("app", "reports");?></span>
                    </li>
                </ul> 

                <div class="row">
                <div class="col-md-6">
                    <button class="btn full-width event_attending_button" type="button">
                        <div class="row">
                        <span class="attendings_counter pull-right"><?php echo $model->attending;?></span> <span class="pull-left"><?php echo Yii::t("app", "Attending");?></span>
                        </div>
                    </button>
                </div>   

                <div class="col-md-4" style="display: none">
                    <a class="btn event_remind_button" href="#"><?php echo Yii::t("app", "Remind me");?></a>
                </div>   

                <div class="col-md-6">
                    <a class="btn btn-primary center full-width event_book_button" href="#booking-form"><?php echo Yii::t("app", "Book");?></a>
                </div>   
                </div>
           </div> 
            
        </div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $event_like_button_pressed = false;
        $(document).on('click','.event_like_button ',function(e){
            e.preventDefault();
            if ($event_like_button_pressed) return;
            $.post("<?php echo $this->createUrl("like",array("id"=>$model->id));?>",
                function(data) {
                    $(".likes_counter").html(data)
                }
             );

            $event_like_button_pressed = true;
        });

        $event_report_button_pressed = false;
        $(document).on('click','.event_report_button ',function(e){
            e.preventDefault();
            if ($event_report_button_pressed) return;
            $.post("<?php echo $this->createUrl("report",array("id"=>$model->id));?>",
                function(data) {
                    $(".reports_counter").html(data)
                }
             );

            $event_report_button_pressed = true;
        });

        $event_attending_button_pressed = false;
        $(document).on('click','.event_attending_button ',function(e){
            e.preventDefault();
            if ($event_attending_button_pressed) return;
            $.post("<?php echo $this->createUrl("attending",array("id"=>$model->id));?>",
                function(data) {
                    $(".attendings_counter").html(data)
                }
             );

            $event_attending_button_pressed = true;
        });
    });
</script>