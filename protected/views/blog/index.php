<?php
$this->breadcrumbs = array(
	Yii::t('app','Blog') => array('index'),
);
?>

<!--
<div class="project-slider owl-carousel owl-theme" dir="ltr">
        <div class="item">
        		<a href="www.yahoo.com" target="_blank" >
        	<img class="owl-lazy" src="http://test.uptop1.com/dbse/uploads/slider_1480144800583937a054fc49.97322091.jpg"  alt="">
        		</a>
        </div>
    <div class="item">
        		<a href="https://www.google.com" target="_blank" >
        	<img class="owl-lazy" src="http://test.uptop1.com/dbse/uploads/slider_1480144825583937b98ef720.87365686.jpg"  alt="">
        		</a>
        </div>
    <div class="item">
        	<img class="owl-lazy" src="http://test.uptop1.com/dbse/uploads/slider_1480144836583937c4b6b1d0.42059814.jpg"  alt="">
        </div>
 </div>
 <div class="clearfix"></div>
-->
	<div class="blog-list">
		<?php $this->widget('zii.widgets.CListView', array(
				'dataProvider'=>$dataProvider,
				'itemView'=>'_view',
				'itemsCssClass'=>'items clearfix',
				'template'=>'{items} {pager}',
				'pagerCssClass'=>'linkPager',
				'pager'=>array(
					'class'=>'CLinkPager',
					'htmlOptions'=>array(
						'class'=>"pagination"
						),
					'firstPageLabel' => "<<",
					'lastPageLabel' => ">>",
					'nextPageLabel' => ">",
					'prevPageLabel' => "<",
					'selectedPageCssClass' => 'active',
					'header'=>'',
					'footer'=>'',

				),
			)); 
			?> 
		</div>

