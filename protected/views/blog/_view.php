<!-- post begin -->

<article>
	<div class="post-media">
		<img alt="" src="<?php echo $data->image_file;?>" class="img-responsive">
		<div class="post-date">
			<span class="date-day"><?php echo date('d',strtotime($data->dateline)); ?></span>
			<span class="date-month"><?php echo date('M',strtotime($data->dateline)); ?></span>
		</div>
	</div> 
	<div class="post-content">
		<div class="post-title blog_title">
			<h5><?php echo $data->createLink(); ?></h5>
		</div>
		<div class="post-metadata">
			<span class="byline">
				<i class="fa fa-user"></i>
				<?php echo (string)(($data->user->profile->name)?$data->user->profile->name:$data->user);?>
			</span>
			<span class="cat-links">                                            
				<i class="fa fa-folder-open"></i>
				<a href="<?php echo $data->category->url;?>"><?php echo (string)$data->category;?></a>
			</span>
		</div>
		<div class="post-entry">
			<div class="col-md-12 content_block"><?php echo $data->{"short_description_".Yii::app()->language};?></div>
			<p  class="col-md-12 text-input"><a class="btn btn-border" href="<?php echo $data->url; ?>"><?php echo Yii::t('app','Read more');?></a></p>
		</div>
	</div>
</article>
<!-- post close -->
