<?php
$this->breadcrumbs = array(
	(string)$model->category => $model->category->shortUrl,
	GxHtml::valueEx($model),
);
$this->pageTitle = Yii::app()->name . ' - ' . GxHtml::valueEx($model);
?>
<div class="blog-single">
	<!-- post begin -->
	<article> 
		<div class="post-media">
            <img alt="" src="<?php echo $model->image_file;?>" class="img-responsive">                                    
        </div>
		<div class="post-content">
			<div class="post-title visible-lg-block hidden-lg">
				<h1><?php echo $model;?></h1>
			</div>
			<div class="post-metadata">                                        
				<span class="posted-on">
					<i class="fa fa-clock-o"></i>
					<?php echo date('d M Y',strtotime($model->dateline));?>
				</span>
				<span class="byline">
					<i class="fa fa-user"></i>
				<?php echo (string)(($model->user->profile->name)?$model->user->profile->name:$model->user);?>
				</span>
				<span class="cat-links">                                            
					<i class="fa fa-folder-open"></i>
					<a href="<?php echo $model->category->url; ?>"><?php echo $model->category; ?></a>
				</span>
			</div>
			<div class="hr"></div>
			<div class="post-entry">
				<?php echo $model->{"content_" . Yii::app()->language}; ?>                                                              
			</div>
			<div class="clearfix"></div>
			<br />

			<div class="fb-comments" data-href="<?php echo Yii::app()->createAbsoluteUrl("/blog/view",array("id"=>$model->id)); ?>" width="100%" data-numposts="5"></div>

		</div>
	</article>
	<!-- post close -->
</div>
