<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<section  style="background-color: graytext;" id="section-cta">
		<div class="sep-background-mask"></div>

<div class="container">
	<div class="row">
		<?php $this->generateAds(1); ?>
		<div id="search_for_places" class="col-md-10 col-md-offset-1">
			
					<!--  Start Search -->
		<h1 class="text-center"><?php echo Yii::t('app','Search For Places');?></h1>
		<?php $form = $this->beginWidget('GxActiveForm', array(
			'id' => 'place-search-form',
			'action' => $this->createUrl("places/search"),
			'method' => 'GET',
			'enableAjaxValidation' => false,
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
				'afterValidate' => 'js:function(form, data, hasError) { 
					if(hasError) {
						for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
						return false;
					}
					else {
						form.children().removeClass("validate-has-error");
						return true;
					}
				}',
				'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
					if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
					else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
				}'
			),
			'htmlOptions'=>array(
				'class'=>'form-horizontal',
			)
		));
		?>
		<div class="input-group" id="adv-search">

			<input required name="q" type="text" class="form-control" placeholder="<?php echo Yii::t('app','Search for Places');?>" />
			<div class="input-group-btn">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default advanced-search" aria-expanded="false"><span class="caret"></span></button>
					<button type="submit" class="btn btn-primary search-btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
				</div>
			</div>

		</div>
		<?php $this->endWidget(); ?><!-- form -->

		<!-- End Search-->
			
		</div>
	</div>
	
</div>
</section>
<div class="container">

	<div class="col-md-9 col-md-push-3 main-content-area">
		<?php $this->generateAds(2); ?>
		<!-- Start Governats-->
		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4><?php echo Yii::t('app','Cities');?></h4>

			</header>

			<div class="tabs-main">
				<div class="tabs-item">
					<p><?php echo Yii::t('app',"Also you can search by cities by clicking on it's name.");?></p>
					<div class="main_univ">
						<div class="blog_grid">	
					<?php

					foreach($cities as $city){?>
						<a class="btn btn-primary btn-radius width grey mleft btn-city <?php echo $city->name_en; ?>" href="<?php echo $this->createUrl("places/city",array("id"=>$city->id)) ?>"> <?php echo $city; ?>
						</a>
					<?php
					}
					?>
					</div>
					</div>
					<a class="more_see_univer more" href="#"><?php echo Yii::t('app','See More');?></a>
					<a class="more_see_univer less" href="#"><?php echo Yii::t('app','See Less');?></a>
				</div>

			</div>

		</div>
		<!--End governats-->

		<div class="clearfix"></div>      

		<!--Start calender -->

		<div id="calender" class="tabs tabs-2" data-active="1">
			<header>
				<h4><?php echo Yii::t('app','Calendar');?></h4>

			</header>

			<div class="tabs-main">
				<div class="tabs-item">     


					<!-- Responsive calendar - START -->
					<div class="responsive-calendar">
						<div class="controls">
							<a class="pull-left" data-go="prev"><div class="btn btn-primary">Prev</div></a>
							<h4><span data-head-year></span> <span data-head-month></span></h4>
							<a class="pull-right" data-go="next"><div class="btn btn-primary">Next</div></a>
						</div><hr/>
						<div class="day-headers">
							<div class="day header">Mon</div>
							<div class="day header">Tue</div>
							<div class="day header">Wed</div>
							<div class="day header">Thu</div>
							<div class="day header">Fri</div>
							<div class="day header">Sat</div>
							<div class="day header">Sun</div>
						</div>
						<div class="days" data-group="days">

						</div>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			$(document).ready(function () {
				var now = new Date();
				<?php
				$calendarEvents = array();
				foreach($events as $event){
					$date = date("Y-m-d", strtotime($event['start_date']));
					$calendarEvents[$date] = array(
						"number"=> $event['number'],
						"class"=> "active",
						"url"=> $this->createUrl("/events/index", array('date'=>$date)),
					);
				}
				?>
				$(".responsive-calendar").responsiveCalendar({
					time: now.getFullYear() + '-' + (now.getMonth()+1),
					events: <?php echo CJSON::encode($calendarEvents); ?>
				});
			});
		</script>   

		<!-- End calender-->


		<div class="clearfix"></div>      


		<!-- Start News Bar->

		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="glyphicon glyphicon-list-alt"></span><b><?php echo Yii::t('app','News from shafaff.com');?></b></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<ul class="demo1">
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/1.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/2.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/3.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/4.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/5.png" width="60" class="img-circle" /></td>
										<td>LLorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,orem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/6.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/7.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="panel-footer">

			</div>
		</div>
		<!-- End news Bar-->
		<?php $this->generateAds(5); ?>
	</div>

	<div class="col-md-3 col-md-pull-9 sidebar">

		<!--  Start Most visited and new--> 


		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4 class="active"><?php echo Yii::t('app','Most visited');?></h4>
				<h4 class=""><?php echo Yii::t('app','New');?></h4>

			</header>

			<div class="tabs-main">
				<div style="display: block;" class="tabs-item">
					<?php $this->widget('zii.widgets.CListView', array(
						'dataProvider'=>$mostVisitedPlaces,
						'itemView'=>'/places/_view_mini',
						'itemsCssClass'=>'items clearfix',
						'template'=>'{items}',
						'pager'=>false,
					)); 
					?>                 
				</div>
				<div style="display: none;" class="tabs-item">
					<?php $this->widget('zii.widgets.CListView', array(
						'dataProvider'=>$newPlaces,
						'itemView'=>'/places/_view_mini',
						'itemsCssClass'=>'items clearfix',
						'template'=>'{items}',
						'pager'=>false,
					)); 
					?>                 
				</div>

			</div>

		</div>

		<!--  End Most visited and new-->

		<!--  Start Advertisement-->

		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4 class="active"><?php echo Yii::t('app','Advertisement');?></h4>

			</header>

			<div class="tabs-main">
				<div style="display: block;" class="tabs-item">
					<?php $this->generateAds(3); ?>
				</div>

			</div>

		</div>
		<!--  End Advertisement-->
		<div class="clearfix"></div>      

	</div>


</div>

<section class="adv_bottom">
	<div class="container">
        <div class="col-md-12">
		<?php $this->generateAds(6); ?>
	   </div>
	</div>
    </section>

<?php /* ?>
<!-- section begin -->
<section id="section-cta">
	<div class="sep-background-mask"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cal-to-action text-center">
					<span>We’ve Completed More Than <b>100+</b> place for our amazing clients, If you interested?</span>
					<a href="#" class="btn btn-border-light">Contact Us</a>
				</div>                            
			</div>
		</div>
	</div>        
</section>
<!-- section close -->
<?php */ ?>

<script type="text/javascript">
	$(document).ready(function($){
        $('.main_univ').each(function(){
		  var this_height = $(this).find('.blog_grid').height();
		  if(this_height > 220){
		    $(this).next('a.more_see_univer').css('display','block');
		  }
		});
		$('a.more_see_univer.more').click(function(e){
			e.preventDefault();
			$('a.more_see_univer.less').css('display','block');
			$(this).hide();
			$(this).prev().toggleClass('remove_height');
		});
		$('a.more_see_univer.less').click(function(e){
			e.preventDefault();
			$('a.more_see_univer.more').css('display','block');
			$(this).hide();
			$(this).prev().prev().toggleClass('remove_height');
		});
    });
</script>