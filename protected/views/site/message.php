<?php $this->pageTitle=Yii::app()->name . ' - '.Yii::t('app', "Message");
$this->breadcrumbs=array(
	Yii::t('app', "Message"),
);
?>
<div id="content">
<div class="container">

<div class="alert alert-<?php echo $message['type']; ?>">
<?php echo $message['text']; ?>
</div>

</div>
</div>