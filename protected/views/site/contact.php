<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - ' . Yii::t('app','Contact Us');
$this->breadcrumbs=array(
	Yii::t('app','Contact Us'),	
);
?>
        <!-- content begin -->
        <div id="content" class="no-padding"> 

            <!-- section begin -->
            <section id="section-contact" class="content">                
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">

                    	<?php if(Yii::app()->user->hasFlash('contact')): ?>
                        	<h5 class="title">
								<?php echo Yii::app()->user->getFlash('contact'); ?>
							</h5>
						<?php else: ?>

                            <div class="text-left">
                                <h4><?php echo Yii::t('app','Nice to Hear From You');?></h4>
                            </div>
							<div class="form">
							<?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'contact-form',
								'enableClientValidation'=>true,
								'clientOptions'=>array(
									'validateOnSubmit'=>true,
								),
								'htmlOptions'=>array(
									'class'=>'wpcf7-form',
									)
							)); ?>

							<?php echo $form->errorSummary($model); ?>

                                <?php
                                $smallInputsCounter = 0;
                                 foreach ($contactFormElements as $contactFormElement) {
                                    $name = "element_{$contactFormElement->id}";
                                    
                                    if ($contactFormElement->type=='textarea'){
                                        echo '<div class="col-full">';
                                        echo $form->textArea($model,$name,array("placeholder"=>$contactFormElement->{"label_" . Yii::app()->language},'rows'=>6, 'cols'=>50));
                                    }else{
                                        $smallInputsCounter++;
                                        echo '<div class="col-one-third' . (($smallInputsCounter%3==2)?' margin-one-third':'') . '">';
                                        echo $form->textField($model,$name,array("placeholder"=>$contactFormElement->{"label_" . Yii::app()->language}));
                                    }

                                    echo $form->error($model,$name);
                                    echo "</div>\n";

                                }
                                ?>
								<?php if(CCaptcha::checkRequirements()): ?>
								<div class="col-full">
	                                <div class="clearfix"></div>
									<div>
									<?php $this->widget('CCaptcha'); ?>
									<?php echo $form->textField($model,'verifyCode'); ?>
									</div>
									<div class="hint"><?php echo Yii::t('app','Please enter the letters as they are shown in the image above. Letters are not case-sensitive.');?></div>
									<?php echo $form->error($model,'verifyCode'); ?>
								</div>
								<?php endif; ?>
                                <div class="clearfix"></div>
                                <div class="text-center">
                                    <div class="divider-single"></div>
                                    <?php echo CHtml::submitButton(Yii::t('app','Send Email'),array('class'=>"btn btn-primary btn-big")); ?>
                                </div>
                           <?php $this->endWidget(); ?>
                           </div>
						<?php endif; ?>                            
                        </div>
                        <?php /* ?>
                        <div class="col-md-4">
                            <h4><?php echo Yii::t('app','GENERAL INFO');?></h4>
                            <ul class="contact-list">
                                <li><i class="fa fa-location-arrow"></i> 121 King Street, Melbourne Victoria 3000 Australia</li>
                                <li><i class="fa fa-phone"></i> +61 3 8376 6284</li>
                                <li><i class="fa fa-envelope"></i> <a href="contact@compact.com">contact@compact.com</a></li>
                                <li><i class="fa fa-globe"></i> <a href="www.compact.com">www.compact.com</a></li>
                            </ul>
                            <h4><?php echo Yii::t('app','WORKING HOURS');?></h4>
                            <ul class="contact-list">
                                <p>Our support Hotline is available 24 Hours a day: +61 3 8376 6284</p>
                                <li><i class="fa fa-clock-o"></i> Monday–Friday: 8am to 6pm</li>
                                <li><i class="fa fa-clock-o"></i> Saturday: 10am to 2pm</li>
                                <li><i class="fa fa-times-circle"></i> Sunday: Closed</li>
                            </ul>
                        </div>
                        <?php */ ?>
                    </div>
                </div>      
            </section>
            <!-- section close -->            

            <!-- section gmap begin -->
            <section id="section-gmap" class="no-padding">                
                <div id="map-canvas" class="map-canvas"></div>       
            </section>
            <!-- section gmap close -->

        </div>
        <!-- content close -->