<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Database for Students &amp; Entrepreneurs</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/earlyaccess/droidarabicnaskh.css' rel='stylesheet' type='text/css'/>

		<!-- Favicons
		================================================== -->
		<link rel="icon" type="image/png" href="<?php echo $this->themeURL;?>/images/favicons/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="<?php echo $this->themeURL;?>/images/favicons/favicon-16x16.png" sizes="16x16" />		
         
        <style>
            
            html { 
  background:#FFFFFF ; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
* {
    margin: 0;
    padding: 0;
  	-moz-box-sizing: border-box;
		-o-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
	}

	body {
		width: 100%;
		height: 100%;
		font-family: "helvetica neue", helvetica, arial, sans-serif;
		font-size: 13px;
		text-align: center;
	}
	.arabic {
		direction: rtl;
		font-family: 'Droid Arabic Naskh' , serif !important;
	}

	ul {
		margin: 30px auto;
		text-align: center;
	}

	li {
		list-style: none;
		position: relative;
		display: inline-block;
		width: 150px;
		height: 150px;
                margin-left:20px;
                line-height: 70px;
	}

	@-moz-keyframes rotate {
		0% {transform: rotate(0deg);}
		100% {transform: rotate(-360deg);}
	}

	@-webkit-keyframes rotate {
		0% {transform: rotate(0deg);}
		100% {transform: rotate(-360deg);}
	}

	@-o-keyframes rotate {
		0% {transform: rotate(0deg);}
		100% {transform: rotate(-360deg);}
	}

	@keyframes rotate {
		0% {transform: rotate(0deg);}
		100% {transform: rotate(-360deg);}
	}

	.round {
		display: block;
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		padding-top: 30px;		
		text-decoration: none;		
		text-align: center;
		font-size: 20px;		
		text-shadow: 0 1px 0 rgba(255,255,255,.7);
		letter-spacing: -.065em;
		font-family: "Hammersmith One", sans-serif;		
		-webkit-transition: all .25s ease-in-out;
		-o-transition: all .25s ease-in-out;
		-moz-transition: all .25s ease-in-out;
		transition: all .25s ease-in-out;
		box-shadow: 2px 2px 7px rgba(0,0,0,.2);
		border-radius: 300px;
		z-index: 1;
		border-width: 4px;
		border-style: solid;
	}

	.round:hover {
		width: 130%;
		height: 130%;
		left: -15%;
		top: -15%;
		font-size: 33px;
		padding-top: 38px;
		-webkit-box-shadow: 5px 5px 10px rgba(0,0,0,.3);
		-o-box-shadow: 5px 5px 10px rgba(0,0,0,.3);
		-moz-box-shadow: 5px 5px 10px rgba(0,0,0,.3);
		box-shadow: 5px 5px 10px rgba(0,0,0,.3);
		z-index: 2;
		border-size: 10px;
		-webkit-transform: rotate(-360deg);
		-moz-transform: rotate(-360deg);
		-o-transform: rotate(-360deg);
		transform: rotate(-360deg);
	}

	a.red {
		background-color: rgba(239,57,50,1);
		color: rgba(133,32,28,1);
		color: #FFFFFF;
		border-color: rgba(133,32,28,.2);
		line-height: 40px
	}

	a.red:hover {
		color: rgba(239,57,50,1);
	}

	a.green {
		background-color: rgba(1,151,171,1);
		color: rgba(0,63,71,1);
		color: #FFFFFF;
		border-color: rgba(0,63,71,.2);
		line-height: 44px
	}

	a.green:hover {
		color: rgba(1,151,171,1);
	}

	a.yellow {
		background-color: #3b5998;
    	color: #fff;
    	border-color: rgba(153,38,0,.2);
    	line-height: 44px
	}

	a.yellow:hover {
	    color: #3b5998;
	}
	a.yellow.round span:hover{
		background: #3b5998;
		font-size: 23px;
	}
	.round span.round {
		display: block;
		opacity: 0;
		-webkit-transition: all .5s ease-in-out;
		-moz-transition: all .5s ease-in-out;
		-o-transition: all .5s ease-in-out;
		transition: all .5s ease-in-out;
		font-size: 1px;
		border: none;
		padding: 40% 20% 0 20%;
		color: #fff;
	}

	.round span:hover {
		opacity: .85;
		font-size: 16px;
		-webkit-text-shadow: 0 1px 1px rgba(0,0,0,.5);
		-moz-text-shadow: 0 1px 1px rgba(0,0,0,.5);
		-o-text-shadow: 0 1px 1px rgba(0,0,0,.5);
		text-shadow: 0 1px 1px rgba(0,0,0,.5);	
	}

	.green span {
		background: rgba(0,63,71,.7);		
	}

	.red span {
		background: rgba(133,32,28,.7);		
	}

	.yellow span {
		background: rgba(161,145,0,.7);	

	}
        img{margin-top: 100px;}
        
        
@media only screen and (max-width: 768px) {          
           li {
		
		width: 100px;
		height: 100px;
                margin-left:10px;
                line-height: 30px;
	} 
            
            }
            .social_icon a img {
			    margin-bottom: 60px;
			    margin-top: 25px;
			}
        	.social_icon a {
			    display: inline-block;
			    margin-left: 10px;
			    width: 55px;
			}
        </style>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NRV7WQQ');</script>
		<!-- End Google Tag Manager -->

		<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyDE6-eQZtDYz7GjXtrczOf_pG48y0a-BmU'></script>

		<meta property="fb:app_id" content="1060011157368075" />

		<meta name="google-site-verification" content="brjLLdSlhQRUmGP4I_oNSbD_79M03zopGn7-1RO5_Jk" />

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '408056332862167'); // Insert your pixel ID here.
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=408056332862167&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->

		<!-- Start Alexa Certify Javascript -->
		<script type="text/javascript">
		_atrk_opts = { atrk_acct:"22nHo1IWx810Io", domain:"dbse.co",dynamic: true};
		(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
		</script>
		<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=22nHo1IWx810Io" style="display:none" height="1" width="1" alt="" /></noscript>
		<!-- End Alexa Certify Javascript -->   

	</head>

	<body>
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '408056332862167'); // Insert your pixel ID here.
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=408056332862167&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->


		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-89670051-1', 'auto');
		  ga('send', 'pageview');

		</script>
        <img src="<?php echo $this->themeURL;?>/images/logo.png" alt="logo" />
       <ul>
		  <li>
		  	<a href="<?php echo $this->createUrl("/coworking"); ?>" class="round green">
		  		CoWorking<br />Spaces
		  		<span class="round arabic">مساحات العمل<br />المشتركة</span>
		  	</a>
		  </li>
		  <li>
		  	<a href="<?php echo $this->createUrl("/universities"); ?>" class="round red">
		  		Egyptian<br />Universities
		  		<span class="round arabic">الجامعات<br />المصرية </span>
		  	</a>
		  </li>		  
		  <li>
		  	<a href="<?php echo $this->createUrl("/schools"); ?>" class="round yellow">
		  		Private<br />Schools
		  		<span class="round arabic">المدارس<br />الخاصة</span>
		  	</a>
		  </li>
		  <?php /* ?>
		 <li><a href="<?php echo $this->createUrl("/saudiUniversities"); ?>" class="round red">Saudi<br />Universities<span class="round arabic">الجامعات<br />السعودية</span></a></li>	
		 <?php */ ?>
		</ul> 
		<div class="social_icon" style="">
			<a target="_blank" class="facebook" href="https://www.facebook.com/DatabaseSE.CO/">
				<img src="<?php echo $this->createUrl("/images"); ?>/facebook.svg" />
			</a>
			<a target="_blank" class="twitter" href="https://twitter.com/DbSE_platform">
				<img src="<?php echo $this->createUrl("/images"); ?>/twitter.svg" />
			</a>
			<a class="googleplus" href="#">
				<img src="<?php echo $this->createUrl("/images"); ?>/google-plus.svg" />
			</a>
			<a target="_blank" class="linkedin" href="https://www.linkedin.com/company-beta/17976745/">
				<img src="<?php echo $this->createUrl("/images"); ?>/linkedin.svg" />
			</a>
		</div>
    </body>
</html>
