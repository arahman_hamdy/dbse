<?php
$this->breadcrumbs = array(
    GxHtml::valueEx($model) => array('view', 'id'=>$model->id),
	Yii::t('app','Booking'),
);
$this->pageTitle = Yii::app()->name . ' - ' . GxHtml::valueEx($model) . ' - ' . Yii::t("app", "Booking");
?>
<div class="container">
    <div class="row"> 
		<p></p><p></p>
		<div class="col-md-9">			


                    <div id="booking-form">
                       <div class="white-popup" style="padding: 0; margin: 0;">
                        <h3><?php echo Yii::t("app", "Booking details"); ?></h3>


                    <?php $form = $this->beginWidget('GxActiveForm', array(
                        'id' => 'event-registration-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                            'afterValidate' => 'js:function(form, data, hasError) { 
                                if(hasError) {
                                    for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
                                    return false;
                                }
                                else {
                                    form.children().removeClass("validate-has-error");
                                    return true;
                                }
                            }',
                            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                                if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
                                else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
                            }'
                        ),
                        'htmlOptions'=>array(
                            'class'=>'form-horizontal form-groups-bordered',
                            'enctype' => 'multipart/form-data',
                        )
                    ));
                    ?>
                    <?php if ($formModel->errors): ?> 
                        <div class="alert alert-danger"> 
                            <?php echo $form->errorSummary($formModel); ?> 
                        </div> 
                    <?php endif; ?> 

                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'room_id'); ?>
                            <div class="col-sm-9 selectpicker"> 
                                <?php
                                $items = array();
                                $itemsOptions = array();
                                foreach($rooms as $room){
                                    $items[$room->id] = $room->{"name_" . Yii::app()->language} . "(" . $room->price . $model->country->payment_currency . ")";
                                    //$itemsOptions[$room->id] = array("disabled"=>"disabled");
                                }

                                 echo $form->dropDownList($formModel, 'room_id', $items, array("class"=>"nodefault selectpicker",'options'=>$itemsOptions)); ?>
                                <?php echo $form->error($formModel,'room_id'); ?>
                            </div> 
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'name'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'name', array('maxlength' => 100)); ?>
                                <?php echo $form->error($formModel,'name'); ?>
                            </div> 
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'phone'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'phone', array('maxlength' => 50)); ?>
                                <?php echo $form->error($formModel,'phone'); ?>
                            </div> 
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'email'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'email', array('maxlength' => 50)); ?>
                                <?php echo $form->error($formModel,'email'); ?>
                            </div> 
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'date'); ?>
                            <div class="col-sm-9"> 
                                <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array( 
                            'model' => $formModel, 
                            'attribute' => 'date', 
                            'value' => $formModel->date, 
                            'htmlOptions' => array('class' => 'form-control'), 
                            'options' => array( 
                                'showButtonPanel' => true, 
                                'changeYear' => true, 
                                'dateFormat' => 'yy-mm-dd', 
                                'format' => 'yyyy-mm-dd', 
                                ), 
                            ));
                ; ?>
                                <?php echo $form->error($formModel,'date'); ?>
                            </div> 
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'time_from'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'time_from', array('class' => 'datetimePicker', 'placeholder'=>Yii::t("app","HH:MM"))); ?>
                                <?php echo $form->error($formModel,'time_from'); ?>
                            </div> 	
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'time_to'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'time_to', array('class' => 'datetimePicker', 'placeholder'=>Yii::t("app","HH:MM"))); ?>
                                <?php echo $form->error($formModel,'time_to'); ?>
                            </div> 
                        </div> 

                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'persons'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'persons'); ?>
                                <?php echo $form->error($formModel,'persons'); ?>
                            </div> 
                        </div>

			        <?php if ($services): ?>
			         <div class="form-group">
			        	<label class="col-sm-3 control-label"><?php echo Yii::t("app","Addon services")?></label>
                        <div class="col-sm-9"> 
						<?php foreach($services as $service): ?>
				        	<div class="clearfix"></div>
				        	<label>
				            <input type="checkbox" name="services[]" value="<?php echo $service->id;?>" class="pull-left addon-service" style="margin-left: 10px;"> <?php echo $service;?> (<?php echo $service->price . $model->country->payment_currency;?>)
				            </label>
				        </div>
						<?php endforeach; ?>
			        </div>
			        <?php endif; ?>

                    <div class="form-group"> 
                        <?php echo $form->labelEx($formModel,'payment_method'); ?>
                        <div class="col-sm-9"> 
                            <?php
                            $payment_options = array();
                            if ($model->accepts_online_payment){
                                $payment_options[1] = Yii::t("app", "Pay online now");
                            }
                            if ($model->accepts_offline_payment){
                                $payment_options[2] = Yii::t("app", "Book now, pay later");
                            }

                            ?>
                            <?php echo $form->dropDownList($formModel, 'payment_method', $payment_options); ?>
                            <?php echo $form->error($formModel,'payment_method'); ?>
                        </div> 
                    </div> 


                    <div class="form-group"> 
                        <?php echo $form->labelEx($formModel,'notes'); ?>
                        <div class="col-sm-9"> 
                            <?php echo $form->textField($formModel, 'notes', array('maxlength' => 200)); ?>
                            <?php echo $form->error($formModel,'notes'); ?>
                        </div> 
                    </div> 

                    <div class="form-group"> 
                        <div class="col-sm-3"> 
                        </div>
                        <div class="col-sm-4"> 
                            <?php echo GxHtml::submitButton(Yii::t('app', 'Book now'), array('class'=>'btn btn-primary btn-big')); ?>
                        </div> 
                        <div class="col-sm-5"> 
                            <span class="total_cost_label pull-right">
                            	Total cost
                            </span>
                            <span class="total_cost_value pull-right">
                            	0
                            </span>
                            <span class="total_cost_currency pull-right">
                            	<?php echo $model->country->payment_currency;?>
                            </span>
                        </div>                         
                    </div> 
                    <?php $this->endWidget(); ?><!-- form -->
                    
                     </div>
                     </div> 

	    </div>
		<div class="col-md-3">
			<div class="main-sidebar">

				<?php if (!empty($this->blocks)): ?>
				<?php foreach($this->blocks as $block):?>
				<aside class="widget widget_block">
					<h3 class="widget-title"><?php echo $block;?></h3> 
					<div class="tiny-border"></div>
					<?php switch ($block->template_id) {
						case 1:
							$category = BlogCategory::model()->cache(60)->findByPK($block->article_cat_id);
							if (!$category || $category->is_deleted) continue;
							?>
								<?php 

								$criteria = new CDbCriteria;
								$criteria->compare("category_id",$block->article_cat_id);
								$criteria->compare("t.is_deleted",0);
								$criteria->compare("t.status",1);
								$criteria->order = "t.id desc";
								$criteria->limit = "10";

								$articles = BlogArticle::model()->cache(0)->findAll($criteria);
								foreach($articles as $article):?>
									<?php if ($article->image_file): ?>
									<img class="related_img pull-left" src="<?php echo $article->image_file;?>"  alt="">
									<?php endif; ?>
									<a class="pull-left" href="<?php echo $article->url;?>"><?php echo $article;?></a>
									<div class="clearfix"></div>
								<?php endforeach; ?>
							<?php
							break;

						case 11:
							echo $block->html;
							break;
						default:
							continue;
					} ?>
				</aside>
				<?php endforeach; ?>
				<?php endif; ?>

			</div>         
		</div>

	</div>
</div>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <style type="text/css">

 </style>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
	$(document).ready(function($){
		$('.selectpicker select').selectpicker();
		$('.datetimePicker').timepicker({ 'scrollDefault': 'now', 'timeFormat': 'H:i', 'forceRoundTime': true });

		$('.datetimePicker').on('changeTime', function() {
		    updateCost();
		});

		$('.addon-service').on('change', function() {
		    updateCost();
		});

		updateCost();
    });

    var roomsCosts = <?php echo CJSON::encode(CHtml::listData($rooms, 'id', 'price')); ?>;
    var servicesCosts = <?php echo CJSON::encode(CHtml::listData($services, 'id', 'price')); ?>;

	var costs = 0;

	function updateCost(){
		costs = 0;

		time_from = $("#Booking_time_from").val().split(":");
		time_from_converted = 0;
		if (typeof time_from[1] != "undefined"){
			time_from_converted = parseInt(time_from[0]) + parseFloat(time_from[1]/60);
		}

		time_to = $("#Booking_time_to").val().split(":");
		time_to_converted = 0;
		if (typeof time_to[1] != "undefined"){
			time_to_converted = parseInt(time_to[0]) + parseFloat(time_to[1]/60);
		}

		hours = 0;
		if (time_to_converted <= time_from_converted){
			time_to_converted += 24;
		}

		if (typeof time_from[1] != "undefined" && typeof time_to[1] != "undefined"){
			hours = time_to_converted - time_from_converted;
		}

		room_id = $("#Booking_room_id").val();
		if (typeof roomsCosts[room_id] != "undefined")
			costs += parseFloat(roomsCosts[room_id]);

		services = $(".addon-service:checked").map(function() {
		    return this.value;
		}).get();
		for(i=0; i<services.length; i++){
			service_id = services[i];
			if (typeof servicesCosts[service_id] != "undefined")
				costs += parseFloat(servicesCosts[service_id]);
		}

		costs *= hours;

		$(".total_cost_value").html(costs);
	}
  
</script>