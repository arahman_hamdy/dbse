<?php
$this->breadcrumbs = array(
	Place::label(2)=>array('latest'),
);


$this->pageTitle = Yii::app()->name . ' - ' . Place::label(2);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Place::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Place::label(2), 'url' => array('admin')),
);
?>

<div class="container">

		<div class="row">
		
		<br />

		<?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
		'itemsCssClass'=>'items clearfix',
		'pagerCssClass'=>'linkPager',
		'pager'=>array(
			'class'=>'CLinkPager',
			'htmlOptions'=>array(
				'class'=>"pagination"
				),
			'firstPageLabel' => "<<",
			'lastPageLabel' => ">>",
			'nextPageLabel' => ">",
			'prevPageLabel' => "<",
			'selectedPageCssClass' => 'active',
			'header'=>'',
			'footer'=>'',

		),
	)); 
	?>           
</div>
</div>
<br />