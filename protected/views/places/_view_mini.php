<div class="news-img">

	<a href="<?php echo $this->createUrl('places/view', array('id' => $data->id)); ?>">
	<div class="news-img-wrapper">
<div class="ribbon-box">
	<?php if ($data->is_premium):?>
		<div class="ribbon ribbon-mini"><span><?php echo Yii::t("app", "Premium");?></span></div>
	<?php endif; ?>
		<div class="col-xs-6 place-thumbnail-mini">
			<img   src="<?php echo $this->uploadsUrl . $data->mainImage->path; ?>" style= "width:100%;" />
		</div>
		<div class="col-xs-6 " style="padding:0px;">
			<h6><span class="news-title"><?php echo $data;?></span><br /><span class='place_region'><?php echo $data->region;?></span>
			<div class="clearfix"></div>
			<div class="stars star-rating-2 star-rating-2-mini pull-left">
				<?php
				$vots= intval($data->ratings_avg);
				for ($i=1; $i<=$vots; $i++) {
					echo '<span class="fa fa-star"></span>';
				}
				for ($i=1; $i <= 5-$vots; $i++) { 
					echo '<span class="fa fa-star-o"></span>';
				}
				?>
			</div>
			<div class="visits visits-mini pull-right">
				<i class="fa fa-eye" aria-hidden="right"></i>
				<span><?php echo $data->visits; ?></span>
			</div>
			<?php if($data->distance): ?>
				<div class="clearfix"></div>
				<div class="distance">
				 <?php echo Yii::t("app", "Distance:"); ?> <span><?php echo number_format($data->distance,1); ?></span> <?php echo Yii::t("app", "KM"); ?>
				</div>
			<?php endif; ?>
			</h6>
		 </div>
</div>
	 </div>
	</a>
</div>

