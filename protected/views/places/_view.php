<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
<div class="ribbon-box">
	<?php if ($data->is_premium):?>
		<div class="ribbon"><span><?php echo Yii::t("app", "Premium");?></span></div>
	<?php endif; ?>
	<a href="<?php echo $this->createUrl('view', array('id' => $data->id)); ?>" class="thumbnail place-thumbnail">
	  <img class="img-responsive" src="<?php echo $this->uploadsUrl . $data->mainImage->path; ?>" alt="1">
		  <div class="caption">
		  	<div>
				<h3 class="pull-left"><?php echo $data;?> <span class="pull-right fav-count">
				   <?php /*
				   <i class="fa fa-fw fa-heart-o"></i>
				   */ ?>
				   </span></h3>
				<?php
				$vots= intval($data->ratings_avg);
				?>
				<div class="clearfix"></div>

				<div class="visits visits-large pull-left">
					<i class="fa fa-eye" aria-hidden="right"></i>
					<span><?php echo $data->visits; ?></span>
				</div>

				<div class="stars star-rating-2 pull-right">
					<?php
					
					for ($i=1; $i<=$vots; $i++) {
						echo '<span class="fa fa-star"></span>';
					}
					for ($i=1; $i <= 5-$vots; $i++) { 
						echo '<span class="fa fa-star-o"></span>';
					}
					?>
				</div>

			</div>
			<div class="clearfix"></div>

			<p><?php echo $data->{"address_" . Yii::app()->language};?>&nbsp;</p>
			<?php if($data->distance): ?>
				<div class="clearfix"></div>
				<div class="distance pull-right">
				 <?php echo Yii::t("app", "Distance:"); ?> <span><?php echo number_format($data->distance,1); ?></span> <?php echo Yii::t("app", "KM"); ?>
				</div>
			<?php endif; ?>

		  </div>
	</a>
</div>
</div>

