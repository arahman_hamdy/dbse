<?php

$this->breadcrumbs = array(
	$model->label(2) => array('latest'),
	(string)$model->region->city => array('places/city','id'=>$model->region->city_id),
	GxHtml::valueEx($model),
);
$this->pageTitle = Yii::app()->name . ' - ' . GxHtml::valueEx($model);
$this->layout='//layouts/column1';

$baseUrl = Yii::app()->baseUrl; 
$baseAbsUrl = rtrim(Yii::app()->createAbsoluteUrl('/'),'/');

$place_image = $this->uploadsUrl . $model->mainImage->path;
$summary = (mb_strlen(strip_tags($model->description_ar)) > 300) ? mb_substr(strip_tags($model->description_ar),0,301).'...': strip_tags($model->description_ar);

//<!-- for Facebook -->
Yii::app()->clientScript->registerMetaTag($model->name_ar,null,null,array('property'=>'og:title'));
Yii::app()->clientScript->registerMetaTag($place_image,null,null,array('property'=>'og:image'));
Yii::app()->clientScript->registerMetaTag(Yii::app()->createAbsoluteUrl("/places/view",array("id"=>$model->id)),null,null,array('property'=>'og:url'));
Yii::app()->clientScript->registerMetaTag($summary,null,null,array('property'=>'og:description'));
Yii::app()->clientScript->registerMetaTag('place',null,null,array('property'=>'og:type'));
if ($model->lat){
	Yii::app()->clientScript->registerMetaTag($model->lat,null,null,array('property'=>'place:location:latitude'));
	Yii::app()->clientScript->registerMetaTag($model->lng,null,null,array('property'=>'place:location:longitude'));
}
?>

<!-- BACKEND 
<div id="status">
	<span class="open">متاح حاليا</span>
	<span class="closed">مغلق</span>
</div>
-->

<div class="ribbon-box">
	<?php if ($model->is_premium):?>
		<div class="ribbon ribbon-big"><span><?php echo Yii::t("app", "Premium");?></span></div>
	<?php endif; ?>


<div class="container">

	<div class="col-md-9 col-md-push-3 main-content-area">
		<div class="cart-items">
			<div class="place_head">
				<h2 class="pull-left visible-lg-block hidden-lg"><?php echo $model;?></h2>&nbsp;
				<div class="stars article_stars pull-right visible-sm-block hidden-sm" data-href="<?php echo Yii::app()->createUrl("/places/view",array("id"=>$model->id)); ?>" style="padding: 5px 0;">
					<input type="radio" name="rating" value="1" <?php if (intval($model->ratings_avg)==1) echo ' checked="checked" ';?>><i></i>
					<input type="radio" name="rating" value="2" <?php if (intval($model->ratings_avg)==2) echo ' checked="checked" ';?>><i></i>
					<input type="radio" name="rating" value="3" <?php if (intval($model->ratings_avg)==3) echo ' checked="checked" ';?>><i></i>
					<input type="radio" name="rating" value="4" <?php if (intval($model->ratings_avg)==4) echo ' checked="checked" ';?>><i></i>
					<input type="radio" name="rating" value="5" <?php if (intval($model->ratings_avg)==5) echo ' checked="checked" ';?>><i></i>
				</div>

				<div class="visits visits-large pull-left">
					<i class="fa fa-eye" aria-hidden="right"></i>
					<span><?php echo $model->visits; ?></span>
				</div>

			</div>
			<div class="clearfix"></div>
			<div class="cart-header">
				<div class="store-sec">
					<!--  start slider-->
					<div class="project-slider owl-carousel owl-theme " style="background:#BBD2DA;">
						<?php if (count($model->libFiles)==0): ?>
							<div><img class="img-responsive owl-lazy" src="<?php echo $place_image;?>" alt="alt"></div>
						<?php endif;?>

						<?php
						foreach ($model->libFiles as $image) {
							if ($image->id == $model->main_image_id) continue;
							?>
								<div><img class="img-responsive owl-lazy" src="<?php echo $this->uploadsUrl . $image->path;?>" alt="alt"></div>			
							<?php
						}
						?>						
					</div>
					
					<?php /*
					if (count($model->libFiles)>1){
						?>
						<div class="slider slider-nav">
							<?php if (count($model->libFiles)==0): ?>
								<div><img class="img-responsive" src="<?php echo $place_image;?>" alt="alt"></div>
							<?php endif;?>
							<?php
							foreach ($model->libFiles as $image) {
								if ($image->id == $model->main_image_id) continue;
								?>
									<div><img class="img-responsive" src="<?php echo $this->uploadsUrl . $image->path;?>" alt="alt"></div>			
								<?php
							}
							?>						
						</div>
						<?php
					}
					*/?>
					
					<!--   End start slider-->
					<div class="clearfix"></div>

				</div>
				<div class="details">
					<?php if($model->lat && $model->lng): ?>
					<a href="#map">
					<?php endif; ?>
					<i class="fa fa-fw fa-map-marker"></i><span><?php echo $model->{"address_" . Yii::app()->language};?></span>
					<?php if($model->lat && $model->lng): ?>
					</a>
					<?php endif; ?>
					<?php /*
						<span class="fav">
						<a class="add-fav" data-remote="true" rel="nofollow" data-method="post" href="/favorite_spaces?space_id=31"><i class="fa fa-fw fa-heart-o"></i><span> Add to Favourites</span></a>
						
						</span>
						<?php */ ?>
				</div>

			</div>
		</div>
		<div class="tab-content">
			<div id="features" class="tab-pane active">
				<?php if ($model->{"description_" . Yii::app()->language}): ?>
				<hr>
				<div>
					<h3 class="feature-title"><?php echo Yii::t('app','Description');?></h3>
					<p><?php echo nl2br($model->{"description_" . Yii::app()->language}); ?></p>
				</div>
				<?php endif; ?>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589c28379a2f9dc1"></script> 
				<?php if ($model->{"featured_" . Yii::app()->language}): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Featured');?></h3>
				<p><?php echo nl2br($model->{"featured_" . Yii::app()->language}); ?></p>
				<?php endif; ?>	

				<?php if ($model->properties): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Properties');?></h3>
				<div class="row features">
					<?php foreach ($model->properties as $property): ?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<a href="<?php echo $this->createUrl('/places/property',array('id'=>$property->id));?>">
						<i class="fa fa-check-circle" aria-hidden="true"></i> <?php echo $property;?>
						</a>
					</div>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
				<hr style="padding: 5px;">
				<?php if ($model->targets): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Targets');?></h3>
				<div class="row features">
					<?php foreach ($model->targets as $target): ?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<!-- <a href="<?php echo $this->createUrl('/places/target',array('id'=>$target->id));?>"> -->
						<i class="fa fa-check-circle" aria-hidden="true"></i> <?php echo $target;?>
						<!-- </a> -->
					</div>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Contact Information');?></h3>
				<div class="row features" >
					<?php if ($model->phone): ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item" data-toggle="tooltip" data-placement="top" title="Phone">
							<i class="fa fa-phone-square" aria-hidden="true"></i> <div class="ltr inline"><?php echo $model->phone;?></div>
						</div>
					<?php endif; ?>

					<?php if ($model->mobile): ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item" data-toggle="tooltip" data-placement="top" title="Mobile">
							<i class="fa fa-mobile" aria-hidden="true"></i> <div class="ltr inline"><?php echo $model->mobile;?></div>
						</div>
					<?php endif; ?>

					<?php if ($model->email): ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item" data-toggle="tooltip" data-placement="top" title="Email">
						<i class="fa fa-envelope" aria-hidden="true"></i> <div class="ltr inline"><?php echo $model->email;?></div>
						</div>
					<?php endif; ?>

					<?php if ($model->facebook_page): if (strpos($model->facebook_page,"://")===false)$model->facebook_page="http://".$model->facebook_page; ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item">
						<a href="<?php echo $model->facebook_page;?>" target="_blank">
						<i class="fa fa-facebook-official" aria-hidden="true" style="color: #3b5998;"></i> <div class="ltr inline"><?php echo basename($model->facebook_page);?></div>
						</a>
						</div>
					<?php endif; ?>

					<?php if ($model->twitter_page): if (strpos($model->twitter_page,"://")===false)$model->twitter_page="http://".$model->twitter_page; ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item">
							<a href="<?php echo $model->twitter_page;?>" target="_blank">
							<i class="fa fa-twitter" aria-hidden="true" style="color: #55acee;"></i> <div class="ltr inline">@<?php echo basename($model->twitter_page);?></div>
							</a>
						</div>
					<?php endif; ?>

					<?php if ($model->website): if (strpos($model->website,"://")===false)$model->website="http://".$model->website; ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item">
						<a href="<?php echo $model->website;?>" target="_blank">
						<i class="fa fa-globe" aria-hidden="true" style="color: #3b5998;"></i> <div class="ltr inline"><?php echo parse_url($model->website,PHP_URL_HOST);?></div>
						</a>
						</div>
					<?php endif; ?>

				</div>

				<?php if ($model->{"works_from_here_" . Yii::app()->language}): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Works from here');?></h3>
				<p><?php echo nl2br($model->{"works_from_here_" . Yii::app()->language}); ?></p>	
				<?php endif; ?>	

				<!-- <?php if ($model->{"targets_text_" . Yii::app()->language}): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Targets');?></h3>
				<p><?php echo nl2br($model->{"targets_text_" . Yii::app()->language}); ?></p>	
				<?php endif; ?>	-->

				<?php if ($model->{"prices_plans_" . Yii::app()->language}): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Prices Plans');?></h3>
				<p><?php echo nl2br($model->{"prices_plans_" . Yii::app()->language}); ?></p>	
				<?php endif; ?>	

				<?php if ($model->{"catering_" . Yii::app()->language}): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Catering');?></h3>
				<p><?php echo nl2br($model->{"catering_" . Yii::app()->language}); ?></p>
				<?php endif; ?>	

				<?php
				if ($model->founded == '0000')$model->founded=null;
				
				if ($model->founded || $model->rooms || $model->capacity || $model->open_hour || $model->close_hour): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Details');?></h3>
				<div class="row features">
					<?php if ($model->founded): ?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo Yii::t('app','Founded');?>: <?php echo $model->founded; ?>
					</div>
					<?php endif; ?>
					<?php if ($model->rooms): ?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo Yii::t('app','Rooms');?>: <?php echo $model->rooms; ?>
					</div>
					<?php endif; ?>
					<?php if ($model->capacity): ?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo Yii::t('app','Capacity');?>: <?php echo $model->capacity; ?>
					</div>
					<?php endif; ?>
					<?php if ($model->open_hour || $model->close_hour):
						if (!$model->open_hour) $model->open_hour = "00:00:00";
						$model->open_hour=date("h:i A",strtotime($model->open_hour));
						if ($model->open_hour == "00:00 AM") $model->open_hour = "12:00 AM";
						 ?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo Yii::t('app','Open hour');?>: <?php echo $model->open_hour; ?>
					</div>
					<?php endif; ?>
					<?php 
					if ($model->open_hour || $model->close_hour): 

						if (!$model->close_hour) $model->close_hour = "00:00:00";
						$model->close_hour=date("h:i A",strtotime($model->close_hour));
						if ($model->close_hour == "00:00 AM") $model->close_hour = "12:00 AM";
						?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo Yii::t('app','Close hour');?>: <?php echo $model->close_hour; ?>
					</div>
					<?php endif; ?>


				</div>

				<?php if ($model->{"notes_" . Yii::app()->language}): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Notes');?></h3>
				<p><?php echo nl2br($model->{"notes_" . Yii::app()->language}); ?></p>	
				<?php endif; ?>	
				
				<?php endif; ?>
				<div id="map">
				</div>
				<?php if($model->lat && $model->lng): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"> <?php echo Yii::t('app','Map');?></h3>
				<div>
					<div style='overflow:hidden;height:440px;width:100%;'>
						<div id='gmap_canvas' style='height:440px;width:100%;'></div>
						<div><small><a href="http://embedgooglemaps.com">									embed google maps							</a></small></div>
						<div><small><a href="https://termsofusegenerator.net">terms of use example</a></small></div>
						<style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
					</div>
					<script type='text/javascript'>function init_map(){var myOptions = {zoom:15,center:new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>)});infowindow = new google.maps.InfoWindow({content:'<div style="margin-right: 15px;"><strong><?php echo $model;?></strong><br><?php echo $model->{"address_" . Yii::app()->language};?><br></div>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
					<br />
				</div>
				<?php endif; ?>

				<?php if ($branches): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Branches');?></h3>
				<div class="row features">
				<?php foreach ($branches as $branch): ?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<a href="<?php echo $this->createUrl('/places/view',array('id'=>$branch->id));?>">
						<i class="fa fa-map-o" aria-hidden="true"></i> <?php echo $branch;?>
						</a>
					</div>
				<?php endforeach; ?>
				</div>
				<?php endif; ?>	

                <?php if (Yii::app()->user->isGuest):?>
                	<br />
		            <div class="form-row form-row-wide">
                        <?php echo GxHtml::link(Yii::t('app', 'Login to book'), $this->createUrl("book",array("id"=>$model->id)),array('class'=>'btn btn-primary btn-big')); ?>
                        <div class="clearfix"></div>
                        <br />
                    </div>
                <?php elseif ($roomsAvailable): ?>
					<hr>
		            <div class="form-row form-row-wide">
		                <a id="book_now" class="btn btn-primary btn-big" href="<?php echo $this->createUrl("book",array("id"=>$model->id));?>"><?php echo Yii::t("app","Book now")?></a>           
		            </div>
				<?php endif; ?>

			    <section class="adv_bottom">
					<?php $this->generateAds(7); ?>
			    </section>

				

				<div class="fb-comments" data-href="<?php echo Yii::app()->createAbsoluteUrl("/places/view",array("id"=>$model->id)); ?>" width="100%" data-numposts="5"></div>
				
			</div>
		</div>
	</div>

	<div class="col-md-3 col-md-pull-9 sidebar"> 

		<!--  Start Most visited and new-->


		<?php if ($similarPlaces && $similarPlaces->itemCount) :?>
		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4 class="active"><?php echo Yii::t('app','Similar places');?></h4>
			</header>

			<div class="tabs-main">
				<div style="display: block;" class="tabs-item">
					<?php $this->widget('zii.widgets.CListView', array(
						'dataProvider'=>$similarPlaces,
						'itemView'=>'/places/_view_mini',
						'itemsCssClass'=>'items clearfix',
						'template'=>'{items}',
						'pager'=>false,
					)); 
					?>                 
				</div>
			</div>

		</div>
		<?php endif; ?>


		<?php if ($nearbyPlaces && $nearbyPlaces->itemCount) :?>
		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4 class="active"><?php echo Yii::t('app','Nearby places');?></h4>
			</header>

			<div class="tabs-main">
				<div style="display: block;" class="tabs-item">
					<?php $this->widget('zii.widgets.CListView', array(
						'dataProvider'=>$nearbyPlaces,
						'itemView'=>'/places/_view_mini',
						'itemsCssClass'=>'items clearfix',
						'template'=>'{items}',
						'pager'=>false,
					)); 
					?>                 
				</div>
			</div>

		</div>
		<?php endif; ?>

		<!--  End Most visited and new-->

		<?php if (!empty($this->related)): ?>
		<aside class="widget widget_related_articles">
			<h3 class="widget-title"><?php echo Yii::t('app','Related Articles');?></h3> 
			<div class="tiny-border"></div>   
				<?php foreach($this->related as $url=>$title):?>
					- <a href="<?php echo $url;?>"><?php echo $title;?></a><br />
				<?php endforeach; ?>
		</aside>
		<?php endif; ?>

		<!--  Start Advertisement-->

		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4 class="active"><?php echo Yii::t('app','Advertisement');?></h4>

			</header>

			<div class="tabs-main">
				<div style="display: block;" class="tabs-item">
					<?php $this->generateAds(4); ?>
				</div>

			</div>

		</div>
		<!--  End Advertisement-->
		<div class="clearfix"></div>      

	</div>

</div>
</div>

<div class="clearfix"></div>
<script type="text/javascript">
	$(document).ready(function($){
		$(".project-slider").owlCarousel({
	        singleItem: true,
	        lazyLoad: true,
	        navigation: true,
	        autoPlay : true,
	        navigationText: [
	          "<i class='fa fa-chevron-left'></i>",
	          "<i class='fa fa-chevron-right'></i>"
	        ],
	        slideSpeed : 400,
	    });
	});
</script>
