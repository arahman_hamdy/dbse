<?php
$this->breadcrumbs = array(
    Yii::t('app','Membership') => array('index'),
);
?>

<div class="container">
    <div class="row"> 
    	<p></p><p></p>
<div class="col-md-12">
	 <div class="blog-single">
                <!-- post begin -->
                <article>
                    <div class="post-content">
                            
                        <?php /* ?>
                        <div class="post-metadata">
                            <h3><?php echo Yii::t("app", "Premium Membership"); ?> (<div class="plan-price">
                                    <sup>$</sup>25                                   
                                </div>)</h3>                                     
                        </div>
                        <div class="hr"></div>
                        <h3><?php echo Yii::t("app", "Premium Membership"); ?></h3>
                        <div class="post-entry">
                            <p><?php echo Yii::t("app", "Premium Membership for coworking spaces. Price is $25. Join now for more control over your places"); ?></p>
                           
                            <h3><?php echo Yii::t("app", "Premium Features"); ?></h3>
                            <ul class="list-style-1">
                                <li><?php echo Yii::t("app", "2 free events monthly."); ?></li>
                                <li><?php echo Yii::t("app", "2 free offers monthly."); ?></li>
                                <li><?php echo Yii::t("app", "Add upto 50 images."); ?></li>
                                <li><?php echo Yii::t("app", "Instant data updates."); ?></li>
                                <li><?php echo Yii::t("app", "High priority for related searches."); ?></li>
                            </ul>                                                                  
                        </div>

                        <p></p>
                        <?php */ ?>
                        
                         <div class="hr"></div>
                         <h3><?php echo Yii::t("app", "My co-working spaces"); ?></h3>

                        <div class="dataTables_wrapper form-inline">
                        <?php $this->widget('CGridViewCustom', array(
                            'id' => 'place-grid',
                            'dataProvider' => $places,
                            'filter' => null,
                            'show_create_button' => false,
                            'itemsCssClass' => "shop_table shop_table_responsive cart",
                            'columns' => array(
                                'id',
                                array(
                                        'name'=>'name_' . Yii::app()->language,
                                        'header'=>Yii::t("app", "Name"),
                                    ),
                                array(
                                        'name'=>'status',
                                        'value'=>function($data){
                                            $values=array(0=>"Inactive", 1=>"Active", 2=>"Pending Review");
                                            if (!isset($values[$data->status])) return "";
                                            return $values[$data->status];
                                        },
                                        'filter'=>array(0=>"Inactive", 1=>"Active", 2=>"Pending Review"),
                                        ),
                                array(
                                        'header'=>Yii::t("app", "Is premium"),
                                        'value'=>function($data){
                                            if (!$data->i_premium) $data->i_premium = 0;
                                            $values=array(0=>Yii::t("app","No"), 1=>Yii::t("app","Yes"));
                                            if (!isset($values[$data->i_premium])) return "";
                                            return $values[$data->i_premium];
                                        },
                                    ),
                                'dateline',

                                array(
                                    'class' => 'CButtonColumn',
                                    'header'=>Yii::t("app", "Action"),
                                    'template'=>'{subscribe}',
                                    'buttons'=>array(
                                        'subscribe'=>array(
                                            'visible'=>'!$data->i_premium',
                                            'options'=>array(
                                                'title'=>Yii::t("app", "Subscribe"),
                                                'class'=>'btn btn-primary',
                                            ),
                                            'label'=>Yii::t("app", "Subscribe"),
                                            'url'=>'Yii::app()->controller->createUrl("subscribe", array("id"=>$data->id))',
                                        ),
                                    )
                                ),
                            ),
                        )); ?>
                        </div>
                       
                        <div class="clearfix"></div>
                    </div>
                </article>
                <!-- post close -->
            </div>
</div>

	</div>
</div>
<script type="text/javascript">
	$(document).ready(function($){
		$(".project-slider").owlCarousel({
            singleItem: true,
            lazyLoad: true,
            navigation: true,
            autoPlay : true,
            navigationText: [
              "<i class='fa fa-chevron-left'></i>",
              "<i class='fa fa-chevron-right'></i>"
            ],
            slideSpeed : 400,
        });		
    });
  
</script>