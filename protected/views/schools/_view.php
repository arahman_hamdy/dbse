<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
	<a href="<?php echo $this->createUrl('view', array('id' => $data->id)); ?>" class="thumbnail place-thumbnail">
	  <img class="img-responsive" src="<?php echo $data->imageUrl; ?>" alt="1">
		  <div class="caption">
		  	<div>
				<h3 class="pull-left"><?php echo $data;?> <span class="pull-right fav-count">
				   <?php /*
				   <i class="fa fa-fw fa-heart-o"></i>
				   */ ?>
				   </span></h3>
				<div class="clearfix"></div>

				<div class="visits visits-large pull-left">
					<i class="fa fa-eye" aria-hidden="right"></i>
					<span><?php echo $data->visits; ?></span>
				</div>

			</div>
			<div class="clearfix"></div>

			<p><?php echo $data->{"address_" . Yii::app()->language};?>&nbsp;</p>
			<?php if($data->distance): ?>
				<div class="clearfix"></div>
				<div class="distance pull-right">
				 <?php echo Yii::t("app", "Distance:"); ?> <span><?php echo number_format($data->distance,1); ?></span> <?php echo Yii::t("app", "KM"); ?>
				</div>
			<?php endif; ?>

		  </div>
	</a>
</div>

