<?php

$this->breadcrumbs = array(
	$model->label(2) => array('latest'),
	(string)$model->region->city => array('schools/city','id'=>$model->region->city_id),
	GxHtml::valueEx($model),
);
$this->pageTitle = Yii::app()->name . ' - ' . GxHtml::valueEx($model);
$this->layout='//layouts/column1';

$baseUrl = Yii::app()->baseUrl; 
$baseAbsUrl = rtrim(Yii::app()->createAbsoluteUrl('/'),'/');

$school_image = $model->imageUrl;

//<!-- for Facebook -->
Yii::app()->clientScript->registerMetaTag($model->name_ar,null,null,array('property'=>'og:title'));
Yii::app()->clientScript->registerMetaTag($school_image,null,null,array('property'=>'og:image'));
Yii::app()->clientScript->registerMetaTag(Yii::app()->createAbsoluteUrl("/schools/view",array("id"=>$model->id)),null,null,array('property'=>'og:url'));
Yii::app()->clientScript->registerMetaTag('school',null,null,array('property'=>'og:type'));
if ($model->lat){
	Yii::app()->clientScript->registerMetaTag($model->lat,null,null,array('property'=>'school:location:latitude'));
	Yii::app()->clientScript->registerMetaTag($model->lng,null,null,array('property'=>'school:location:longitude'));
}
?>	
<div class="container">

	<div class="col-md-9 col-md-push-3 main-content-area">
		<div class="cart-items">
			<div class="place_head">
				<h2 class="pull-left visible-lg-block hidden-lg"><?php echo $model;?></h2>&nbsp;

				<div class="visits visits-large pull-left">
					<i class="fa fa-eye" aria-hidden="right"></i>
					<span><?php echo $model->visits; ?></span>
				</div>

			</div>
			<div class="clearfix"></div>
			<div class="cart-header">
				<div class="store-sec">
					<!--  start slider-->
					<div class="project-slider owl-carousel owl-theme " style="background:#BBD2DA;">
						<?php if (count($model->libFiles)==0): ?>
							<div><img class="img-responsive owl-lazy" src="<?php echo $school_image;?>" alt="alt"></div>
						<?php endif;?>

						<?php
						foreach ($model->libFiles as $image) {
							if ($image->id == $model->main_image_id) continue;
							?>
								<div><img class="img-responsive owl-lazy" src="<?php echo $this->uploadsUrl . $image->path;?>" alt="alt"></div>			
							<?php
						}
						?>						
					</div>
					
					<?php /*
					if (count($model->libFiles)>1){
						?>
						<div class="slider slider-nav">
							<?php if (count($model->libFiles)==0): ?>
								<div><img class="img-responsive" src="<?php echo $school_image;?>" alt="alt"></div>
							<?php endif;?>
							<?php
							foreach ($model->libFiles as $image) {
								if ($image->id == $model->main_image_id) continue;
								?>
									<div><img class="img-responsive" src="<?php echo $this->uploadsUrl . $image->path;?>" alt="alt"></div>			
								<?php
							}
							?>						
						</div>
						<?php
					}
					*/?>
					
					<!--   End start slider-->
					<div class="clearfix"></div>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589c28379a2f9dc1"></script> 

				</div>
				<div class="details">
					<?php if($model->lat && $model->lng): ?>
					<a href="#map">
					<?php endif; ?>
					<i class="fa fa-fw fa-map-marker"></i><span><?php echo $model->{"address_" . Yii::app()->language};?></span>
					<?php if($model->lat && $model->lng): ?>
					</a>
					<?php endif; ?>
					<?php /*
						<span class="fav">
						<a class="add-fav" data-remote="true" rel="nofollow" data-method="post" href="/favorite_spaces?space_id=31"><i class="fa fa-fw fa-heart-o"></i><span> Add to Favourites</span></a>
						
						</span>
						<?php */ ?>
				</div>

			</div>
		</div>
		<div class="tab-content">
			<div id="features" class="tab-pane active">
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Contact Information');?></h3>
				<div class="row features" >
					<?php if ($model->phone): ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item" data-toggle="tooltip" data-placement="top" title="Phone">
							<i class="fa fa-phone-square" aria-hidden="true"></i> <div class="ltr inline"><?php echo $model->phone;?></div>
						</div>
					<?php endif; ?>

					<?php if ($model->mobile): ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item" data-toggle="tooltip" data-placement="top" title="Mobile">
							<i class="fa fa-mobile" aria-hidden="true"></i> <div class="ltr inline"><?php echo $model->mobile;?></div>
						</div>
					<?php endif; ?>

					<?php if ($model->fax): ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item" data-toggle="tooltip" data-placement="top" title="fax">
							<i class="fa fa-fax" aria-hidden="true"></i> <div class="ltr inline"><?php echo $model->fax;?></div>
						</div>
					<?php endif; ?>


					<?php if ($model->email): ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item" data-toggle="tooltip" data-placement="top" title="Email">
						<i class="fa fa-envelope" aria-hidden="true"></i> <div class="ltr inline"><?php echo $model->email;?></div>
						</div>
					<?php endif; ?>

					<?php if ($model->facebook): if (strpos($model->facebook,"://")===false)$model->facebook="http://".$model->facebook; ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item">
						<a href="<?php echo $model->facebook;?>" target="_blank">
						<i class="fa fa-facebook-official" aria-hidden="true" style="color: #3b5998;"></i> <div class="ltr inline"><?php echo basename($model->facebook);?></div>
						</a>
						</div>
					<?php endif; ?>

					<?php if ($model->website): if (strpos($model->website,"://")===false)$model->website="http://".$model->website; ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 feature-item">
						<a href="<?php echo $model->website;?>" target="_blank">
						<i class="fa fa-globe" aria-hidden="true" style="color: #3b5998;"></i> <div class="ltr inline"><?php echo parse_url($model->website,PHP_URL_HOST);?></div>
						</a>
						</div>
					<?php endif; ?>

				</div>

				<?php
				if ($model->founded == '0000')$model->founded=null;
				
				if ($model->founded): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Details');?></h3>
				<div class="row features">
					<?php if ($model->founded): ?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo Yii::t('app','Founded');?>: <?php echo $model->founded; ?>
					</div>
					<?php endif; ?>

				</div>
				<?php endif; ?>

				<?php if ($model->arabic || $model->languages || $model->azhar || $model->islamic || $model->sisters || $model->hotel || $model->international || $model->british || $model->american || $model->french || $model->germany || $model->ib || $model->turkish || $model->sudanese || $model->libyan): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Classification');?></h3>
				<div class="row features">
					<?php foreach (array('arabic','languages','azhar','islamic','sisters','hotel','international','british','american','french','germany','ib','turkish','sudanese','libyan') as $classification):
					if (!$model->$classification) continue;
					?>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo $model->getAttributeLabel($classification);?>
					</div>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>

				<?php if ($model->application_start || $model->application_end): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Application');?></h3>
				<div class="row features">
					<?php if ($model->application_start): ?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo Yii::t('app','Application start');?>: <?php echo $model->application_start; ?>
					</div>
					<?php endif; ?>

					<?php if ($model->application_end): ?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo Yii::t('app','Application end');?>: <?php echo $model->application_end; ?>
					</div>
					<?php endif; ?>
				</div>
				<?php endif; ?>

				<?php if ($model->kg_1 || $model->kg_2 || $model->pri_1 || $model->pri_2 || $model->pri_3 || $model->pri_4 || $model->pri_5 || $model->pri_6 || $model->prep_1 || $model->prep_2 || $model->prep_3 || $model->sec_1 || $model->sec_2 || $model->sec_3 || $model->transportation): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Expenses');?></h3>
				<div class="row features">
					<?php foreach (array('kg_1','kg_2','pri_1','pri_2','pri_3','pri_4','pri_5','pri_6','prep_1','prep_2','prep_3','sec_1','sec_2','sec_3','transportation') as $expense):
					if (!$model->$expense) continue;
					?>
					<div class="col-md-12 feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo $model->getAttributeLabel($expense);?>: <?php echo $model->$expense;?>
					</div>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>

				<?php if ($model->{"notes_" . Yii::app()->language}): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Notes');?></h3>
				<p><?php echo nl2br($model->{"notes_" . Yii::app()->language}); ?></p>	
				<?php endif; ?>	

				<?php if ($model->ncacs || $model->cita || $model->msa || $model->sis || $model->naas || $model->madonna || $model->manhattan || $model->advanced || $model->cambridge || $model->iso || $model->naqaae): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"><?php echo Yii::t('app','Certificates');?></h3>
				<div class="row features">
					<?php foreach (array('ncacs','cita','msa','sis','naas','madonna','manhattan','advanced','cambridge','iso','naqaae') as $certificate):
					if (!$model->$certificate) continue;
					?>
					<div class="col-md-12  feature-item">
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo $model->getAttributeLabel($certificate);?>
					</div>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
				
				<div id="map">
				</div>
				<?php if($model->lat && $model->lng): ?>
				<hr style="padding: 5px;">
				<h3 class="feature-title"> <?php echo Yii::t('app','Map');?></h3>
				<div>
					<div style='overflow:hidden;height:440px;width:100%;'>
						<div id='gmap_canvas' style='height:440px;width:100%;'></div>
						<div><small><a href="http://embedgooglemaps.com">									embed google maps							</a></small></div>
						<div><small><a href="https://termsofusegenerator.net">terms of use example</a></small></div>
						<style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
					</div>
					<script type='text/javascript'>function init_map(){var myOptions = {zoom:15,center:new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>)});infowindow = new google.maps.InfoWindow({content:'<div style="margin-right: 15px;"><strong><?php echo $model;?></strong><br><?php echo $model->{"address_" . Yii::app()->language};?><br></div>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
					<br />
				</div>
				<?php endif; ?>

			    <section class="adv_bottom">
					<?php $this->generateAds(20); ?>
			    </section>

				<div class="fb-comments" data-href="<?php echo Yii::app()->createAbsoluteUrl("/schools/view",array("id"=>$model->id)); ?>" width="100%" data-numposts="5"></div>
				
			</div>
		</div>
	</div>

	<div class="col-md-3 col-md-pull-9 sidebar"> 

		<!--  Start Most visited and new-->


		<?php if ($nearbySchools && $nearbySchools->itemCount) :?>
		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4 class="active"><?php echo Yii::t('app','Nearby schools');?></h4>
			</header>

			<div class="tabs-main">
				<div style="display: block;" class="tabs-item">
					<?php $this->widget('zii.widgets.CListView', array(
						'dataProvider'=>$nearbySchools,
						'itemView'=>'/schools/_view_mini',
						'itemsCssClass'=>'items clearfix',
						'template'=>'{items}',
						'pager'=>false,
					)); 
					?>                 
				</div>
			</div>

		</div>
		<?php endif; ?>

		<!--  End Most visited and new-->

		<?php if (!empty($this->related)): ?>
		<aside class="widget widget_related_articles">
			<h3 class="widget-title"><?php echo Yii::t('app','Related Articles');?></h3> 
			<div class="tiny-border"></div>   
				<?php foreach($this->related as $url=>$title):?>
					- <a href="<?php echo $url;?>"><?php echo $title;?></a><br />
				<?php endforeach; ?>
		</aside>
		<?php endif; ?>

		<!--  Start Advertisement-->

		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4 class="active"><?php echo Yii::t('app','Advertisement');?></h4>

			</header>

			<div class="tabs-main">
				<div style="display: block;" class="tabs-item">
					<?php $this->generateAds(18); ?>
				</div>

			</div>

		</div>
		<!--  End Advertisement-->
		<div class="clearfix"></div>      

	</div>

</div>
<div class="clearfix"></div>
<script type="text/javascript">
	$(document).ready(function($){
		$(".project-slider").owlCarousel({
	        singleItem: true,
	        lazyLoad: true,
	        navigation: true,
	        autoPlay : true,
	        navigationText: [
	          "<i class='fa fa-chevron-left'></i>",
	          "<i class='fa fa-chevron-right'></i>"
	        ],
	        slideSpeed : 400,
	    });
	});
</script>
