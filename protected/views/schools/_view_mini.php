<div class="news-img">
	<a href="<?php echo $this->createUrl('schools/view', array('id' => $data->id)); ?>">
	<div class="news-img-wrapper">
		<div class="col-xs-6 place-thumbnail-mini">
			<img   src="<?php echo $data->imageUrl; ?>" style= "width:100%;" />
		</div>
		<div class="col-xs-6 " style="padding:0px;">
			<h6><span class="news-title"><?php echo $data;?></span><br /><span class='place_region'><?php echo $data->region;?></span>
			<div class="clearfix"></div>
			<div class="visits visits-mini pull-right">
				<i class="fa fa-eye" aria-hidden="right"></i>
				<span><?php echo $data->visits; ?></span>
			</div>
			<?php if($data->distance): ?>
				<div class="clearfix"></div>
				<div class="distance">
				 <?php echo Yii::t("app", "Distance:"); ?> <span><?php echo number_format($data->distance,1); ?></span> <?php echo Yii::t("app", "KM"); ?>
				</div>
			<?php endif; ?>
			</h6>
		 </div>
	 </div>
	</a>
</div>

