<?php
if (!$title) $title = Entity::label(2);

$this->breadcrumbs = array(
	$title=>array('index'),
);


$this->pageTitle = Yii::app()->name . ' - ' . $title;

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Entity::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Entity::label(2), 'url' => array('admin')),
);
?>

<div class="container">

		<div class="row">
		<br />
        <div id="blog-grid" class="blog-grid">

		<?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$entities,
        'template'=>"{items}",
		'itemView'=>'/universities/_entity',
		'itemsCssClass'=>'items clearfix',
		'pagerCssClass'=>'linkPager',
		'pager'=>array(
			'class'=>'CLinkPager',
			'htmlOptions'=>array(
				'class'=>"pagination"
				),
			'firstPageLabel' => "<<",
			'lastPageLabel' => ">>",
			'nextPageLabel' => ">",
			'prevPageLabel' => "<",
			'selectedPageCssClass' => 'active',
			'header'=>'',
			'footer'=>'',

		),
	)); 
	?>
	</div> 
</div>
</div>
<br />