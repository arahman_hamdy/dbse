<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<?php if($sliderImages):?>
<section  style="background-color: graytext;" id="section-cta">
		<div class="sep-background-mask"></div>

<div class="container">
	<div class="row">
		<?php $this->generateAds(8); ?>
		 <div class="project-slider owl-carousel owl-theme" dir="ltr">
		 	<?php foreach ($sliderImages as $sliderImage):?>
            <div class="item">
            	<?php if ($sliderImage->url):?>
            		<a href="<?php echo $sliderImage->url; ?>" target="_blank" >
            	<?php endif;?>
            	<img class="owl-lazy" src="<?php echo $sliderImage->image_file; ?>"  alt="">
            	<?php if ($sliderImage->url):?>
            		</a>
            	<?php endif;?>
            </div>
            <?php endforeach; ?>
         </div>
	</div>
	
</div>
</section>
<?php endif;?>
<div class="container">

	<div class="col-md-9 col-md-push-3 main-content-area">
		<?php $this->generateAds(9); ?>
		<!--<header>
			<h4 style="font-size: 14px;"><?php echo Yii::t('app','Universities');?></h4>
		</header>-->
		<!-- Start Governats -->
		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4><?php echo Yii::t('app','Universities');?></h4>

			</header>

			<div class="tabs-main">
				<div class="tabs-item">
					<p></p>
					<div class="main_univ">
						<div class="blog_grid">							
							<?php
		
							foreach($entities as $entity){
								$url = "";
								if ($entity->has_contents){
								    if ($entity->slug){
								        $url = $this->createUrl('page',array('p'=>$entity->slug));
								    }else{
								        $url = $this->createUrl('view',array('id'=>$entity->id));
								    }
								}
								?>
								<a class="btn btn-primary btn-radius width grey mleft" href="<?php echo $url; ?>"> <?php echo $entity; ?></a>
							<?php
							}
							?>
						</div>
					</div>
					<a class="more_see_univer more" href="#"><?php echo Yii::t('app','See More');?></a>
					<a class="more_see_univer less" href="#"><?php echo Yii::t('app','See Less');?></a>
				</div>
			</div>

		</div>
		<!--End governats-->





		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4><?php echo Yii::t('app','الكليات');?></h4>

			</header>

			<div class="tabs-main">
				<div class="tabs-item">
					<p></p>
					<div class="main_univ">
						<div class="blog_grid">
							<?php foreach($categories as $category):
								$url = $this->createUrl('cat',array('id'=>$category->id));
								?>
								<a class="btn btn-primary btn-radius width grey mleft" href="<?php echo $url;?>"><?php echo $category;?></a>
							<?php endforeach; ?>	
						</div>
					</div>
					<a class="more_see_univer more" href="#"><?php echo Yii::t('app','المزيد');?></a>
					<a class="more_see_univer less" href="#"><?php echo Yii::t('app','اقل');?></a>
				</div>
			</div>

		</div>

















		<div class="clearfix"></div>      

		<?php
			//$this->renderPartial('/universities/_entity_group',array('entities'=>$entities),false,true);
		?>
		<!-- <br />


		<div class="clearfix"></div>
		 --> 


		<!-- Start News Bar->

		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="glyphicon glyphicon-list-alt"></span><b><?php echo Yii::t('app','News from shafaff.com');?></b></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<ul class="demo1">
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/1.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/2.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/3.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/4.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/5.png" width="60" class="img-circle" /></td>
										<td>LLorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,orem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/6.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
							<li class="news-item">
								<table cellpadding="4">
									<tr>
										<td><img src="<?php echo $this->themeURL;?>/images/7.png" width="60" class="img-circle" /></td>
										<td>Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
									</tr>
								</table>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="panel-footer">

			</div>
		</div>
		<!-- End news Bar-->

		<?php $this->generateAds(12); ?>

	</div>

	<div class="col-md-3 col-md-pull-9 sidebar">

		<!--  Start Advertisement-->

		<div class="tabs tabs-2" data-active="1">
			<header>
				<h4 class="active"><?php echo Yii::t('app','Advertisement');?></h4>

			</header>

			<div class="tabs-main">
				<div style="display: block;" class="tabs-item">
					<?php $this->generateAds(10); ?>
				</div>

			</div>

		</div>
		<!--  End Advertisement-->
		<div class="clearfix"></div>      

	</div>


</div>

<section class="adv_bottom">
	<div class="container">
            <div class="col-md-12">
		<?php $this->generateAds(13); ?>
	   </div>
	</div>
    </section>

<?php /* ?>
<!-- section begin -->
<section id="section-cta">
	<div class="sep-background-mask"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cal-to-action text-center">
					<span>We’ve Completed More Than <b>100+</b> place for our amazing clients, If you interested?</span>
					<a href="#" class="btn btn-border-light">Contact Us</a>
				</div>                            
			</div>
		</div>
	</div>        
</section>
<!-- section close -->
<?php */ ?>


<script type="text/javascript">
	$(document).ready(function($){
		$(".project-slider").owlCarousel({
            singleItem: true,
            lazyLoad: true,
            navigation: true,
            autoPlay : true,
            navigationText: [
              "<i class='fa fa-chevron-left'></i>",
              "<i class='fa fa-chevron-right'></i>"
            ],
            slideSpeed : 400,
        });
        $('.main_univ').each(function(){
		  var this_height = $(this).find('.blog_grid').height();
		  if(this_height > 220){
		    $(this).next('a.more_see_univer').css('display','block');
		  }
		});
		$('a.more_see_univer.more').click(function(e){
			e.preventDefault();
			$(this).parent().find('a.more_see_univer.less').css('display','block');
			$(this).hide();
			$(this).prev().toggleClass('remove_height');
		});
		$('a.more_see_univer.less').click(function(e){
			e.preventDefault();
			$(this).parent().find('a.more_see_univer.more').css('display','block');
			$(this).hide();
			$(this).prev().prev().toggleClass('remove_height');
		});
		
    });
  
</script>