<style>
	.post-content .post-title a{
		 padding: 15px 5px;
		 display: block;
	}
	.blog-grid .items .item.col-md-4 .post-content{
		padding: 0;
	}
</style>
<?php
$url = "";
if ($data->has_contents){
    if ($data->slug){
        $url = $this->createUrl('page',array('p'=>$data->slug));
    }else{
        $url = $this->createUrl('view',array('id'=>$data->id));
    }
}
?>
    <!-- post begin -->
    <article class="item col-md-12">
        <div class="post-content">
            <div class="post-title entity-thumb-title">
                <h5>
                <?php if ($url): ?>
                <a href="<?php echo $url; ?>">
                <?php endif;?>
                    <?php echo (($data->mainImage)?"":" ") . '- ' . $data; ?>
                <?php if ($url): ?>
                </a>
                <?php endif;?>
                </h5>
            </div>
        </div>
    </article>
    <!-- post close -->
