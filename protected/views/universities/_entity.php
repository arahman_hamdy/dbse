<style>
	.post-content .post-title a{
		 padding: 15px 5px;
		 display: block;
	}
	.blog-grid .items .item.col-md-4 .post-content{
		padding: 0;
	}
</style>
<?php
$url = $data->url;
?>
    <!-- post begin -->
    <article class="item col-md-4 col-sm-6">
        <?php if ($data->mainImage): ?>
        <div class="post-media place-thumbnail">
            <?php if ($url): ?>
            <a href="<?php echo $url; ?>">
            <?php endif; ?>

            <img  alt="" src="<?php echo $this->uploadsUrl . $data->mainImage->path; ?>" class="img-responsive" />
            <?php if ($url): ?>
            </a>
            <?php endif;?>
            <?php /* if($data->distance): ?>
                <div class="clearfix"></div>
                <div class="distance">
                 <?php echo Yii::t("app", "Distance:"); ?> <span><?php echo number_format($data->distance,1); ?></span> <?php echo Yii::t("app", "KM"); ?>
                </div>
            <?php endif; */ ?>
        </div>
        <?php endif; ?>
        <div class="post-content">
            <div class="post-title entity-thumb-title">
                <h5>
                <?php if ($url): ?>
                <a href="<?php echo $url; ?>">
                <?php endif;?>
                    <?php echo (($data->mainImage)?"":" ") . (($data->parent_id)? $data->parent . " - ":"") . $data; ?>
                <?php if ($url): ?>
                </a>
                <?php endif;?>
                </h5>
            </div>
        </div>
    </article>
    <!-- post close -->
