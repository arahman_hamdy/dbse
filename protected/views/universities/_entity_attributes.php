

<?php if (!empty($universities)): ?>
<div class="form-group">
    <?php echo CHtml::label(Yii::t('App','University'),0); ?>
    <div class="clearfix"></div>
    <?php 
        echo CHtml::dropDownList('UniversityFilter[university]', (!empty($_GET['UniversityFilter']['university']))?$_GET['UniversityFilter']['university']:0, GxHtml::encodeEx(array(''=>'-') + GxHtml::listDataEx($universities), false, true),array('class'=>'form-control selectboxit')); ?>
</div>
<?php endif; ?>

<?php if (!empty($colleges)): ?>
<div class="form-group">
    <?php echo CHtml::label(Yii::t('App','College'),0); ?>
    <div class="clearfix"></div>
    <?php 
        echo CHtml::dropDownList('UniversityFilter[college]', (!empty($_GET['UniversityFilter']['college']))?$_GET['UniversityFilter']['college']:0, GxHtml::encodeEx(array(''=>'-') + GxHtml::listDataEx($colleges), false, true),array('class'=>'form-control selectboxit')); ?>
</div>
<?php endif; ?>

<div class="form-group">
    <?php echo CHtml::label(Yii::t('App','Accepted certificate'),0); ?>
    <div class="clearfix"></div>
    <?php 
        $certificates = array(
            'thnwy_adby'=>Yii::t('app','High school (literary)'),
            'thnwy_elmy_math'=>Yii::t('app','High school (sports science)'),
            'thnwy_elmy_science'=>Yii::t('app','High school (Science Science)'),
            'thnwy_azhar'=>Yii::t('app','Secondary Azhar'),
            'thnwy_sena3y'=>Yii::t('app','Secondary industrial'),
            'thnwy_zera3y'=>Yii::t('app','Agricultural High School'),
            'thnwy_tegary'=>Yii::t('app','commercial high School'),
            'deb_seyaha'=>Yii::t('app','Diploma in Tourism and Hotels'),
            'deb_american'=>Yii::t('app','American Diploma'),
            'deb_british'=>Yii::t('app','British diploma'),
            'mo3adala'=>Yii::t('app','Certifications equation'),
        );

        echo CHtml::dropDownList('UniversityFilter[certificate]', (!empty($_GET['UniversityFilter']['college']))?$_GET['UniversityFilter']['college']:0, GxHtml::encodeEx(array(''=>'-') + $certificates, false, true),array('class'=>'form-control selectboxit')); ?>
</div>

<div class="form-group">
    <?php echo CHtml::label(Yii::t('App','Division'),0); ?>
    <div class="clearfix"></div>
    <?php 
        echo CHtml::dropDownList('UniversityFilter[divisions]', (!empty($_GET['UniversityFilter']['divisions']))?$_GET['UniversityFilter']['divisions']:0, GxHtml::encodeEx(array(''=>'-') + GxHtml::listDataEx(EntityDivision::model()->findAll()), false, true),array('class'=>'form-control selectboxit')); ?>
</div>


<?php if (!empty($attributes)): ?>
<div class="form-group">
	<?php echo CHtml::label(Yii::t('App','Attributes'),0); ?>
	<div class="clearfix"></div>
	<?php 

		foreach($attributes as $attribute):?>
			<div class="form-group row">
				<?php echo CHtml::label($attribute,null);
					echo '<div class="clearfix"></div>';
                    switch($attribute->attribute_type_id){
                        case 1: // integer
                        	?><div class="col-md-5 nopadding"><?php
                        	echo CHtml::label(Yii::t('app','From'),"attribute_range_" .$attribute->id."_1");
							echo CHtml::numberField("attribute[range][" .$attribute->id . "][0]", (!empty($_GET["attribute"][$attribute->id]))?$_GET["attribute"][$attribute->id]:"", array('id'=>"attribute_direct_" .$attribute->id."_1", 'class'=>'form-control'));
                        	?></div><div class="col-md-2 nopadding"></div><div class="col-md-5 nopadding"><?php
							echo CHtml::label(Yii::t('app','To'),"attribute_range_" .$attribute->id."_0");
							echo CHtml::numberField("attribute[range][" .$attribute->id . "][1]", (!empty($_GET["attribute"][$attribute->id]))?$_GET["attribute"][$attribute->id]:"", array('id'=>"attribute_direct_" .$attribute->id."_0",'class'=>'form-control'));
							?></div><?php

                        break;
                        case 6: // boolean
                        	?><div class="col-md-5 nopadding"><?php

							echo CHtml::radioButton("attribute[direct][" .$attribute->id . "]", (!empty($_GET["attribute"][$attribute->id]))?$_GET["attribute"][$attribute->id]:"", array('id'=>"attribute_direct_" .$attribute->id."_1", 'class'=>'','value'=>1));
                        	echo "&nbsp;" . CHtml::label(Yii::t('app','Yes'),"attribute_direct_" .$attribute->id."_1");
                        	?></div><div class="col-md-2 nopadding"></div><div class="col-md-5 nopadding"><?php
							echo CHtml::radioButton("attribute[direct][" .$attribute->id . "]", (!empty($_GET["attribute"][$attribute->id]))?$_GET["attribute"][$attribute->id]:"", array('id'=>"attribute_direct_" .$attribute->id."_0",'class'=>'','value'=>0));

							echo "&nbsp;" . CHtml::label(Yii::t('app','No'),"attribute_direct_" .$attribute->id."_0");
							?></div><?php

                        break;
                        case 2: // string
                        case 3: // textarea
                        case 4: // html
                        case 5: // url
                        case 7: // address
                        default:
							echo CHtml::textField("attribute[direct][" .$attribute->id . "]", (!empty($_GET["attribute"][$attribute->id]))?$_GET["attribute"][$attribute->id]:"", array('class'=>'form-control'));

                        break;
                    }
                ?>
			</div>
		<?php endforeach;?>
</div>
<?php endif; ?>