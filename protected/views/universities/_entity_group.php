                <div class="row"> 
                    <div id="blog-grid" class="blog-grid">
                        <?php $this->widget('zii.widgets.CListView', array(
                            'dataProvider'=>$entities,
                            'template'=>"{items}",
                            'itemView'=>'_entity',
                            'itemsCssClass'=>'items clearfix',
                            'pagerCssClass'=>'linkPager',
                            'pager'=>array(
                                'class'=>'CLinkPager',
                                'htmlOptions'=>array(
                                    'class'=>"pagination"
                                    ),
                                'firstPageLabel' => "<<",
                                'lastPageLabel' => ">>",
                                'nextPageLabel' => ">",
                                'prevPageLabel' => "<",
                                'selectedPageCssClass' => 'active',
                                'header'=>'',
                                'footer'=>'',

                            ),
                        )); 
                        ?>           


                    </div>                             
                </div>
