<?php

$this->breadcrumbs = array(
);

if ($model->region)
    $this->breadcrumbs[(string)$model->region->city] = array('city','id'=>$model->region->city_id);

$parentsArray = array();
$currentParent = $model->parent;
while($currentParent){
    $url = array();
    if ($currentParent->has_contents){
        if ($currentParent->slug){
            $url = array('page','p'=>$currentParent->slug);
        }else{
            $url = array('view','id'=>$currentParent->id);
        }
    }
    if ($url)
        $parentsArray[GxHtml::valueEx($currentParent)] = $url;
    else
        $parentsArray[] = $currentParent;

    if (!$model->region && !$currentParent->parent && $currentParent->region)
        $this->breadcrumbs[(string)$currentParent->region->city] = array('city','id'=>$currentParent->region->city_id);

    $currentParent = $currentParent->parent;
}
$this->breadcrumbs += array_reverse($parentsArray);
$this->breadcrumbs[] = GxHtml::valueEx($model);

$this->pageTitle = Yii::app()->name . ' - ' . GxHtml::valueEx($model);
$this->layout='//layouts/column1';

$baseUrl = Yii::app()->baseUrl; 
$baseAbsUrl = rtrim(Yii::app()->createAbsoluteUrl('/'),'/');

$place_image = "";
if ($model->mainImage) $place_image = $this->uploadsUrl . $model->mainImage->path;

$summary = (mb_strlen(strip_tags($model->contents_ar)) > 300) ? mb_substr(strip_tags($model->contents_ar),0,301).'...': strip_tags($model->contents_ar);

$url = "";
if ($model->slug){
    $url = $this->createAbsoluteUrl('page',array('p'=>$model->slug));
}else{
    $url = $this->createAbsoluteUrl('view',array('id'=>$model->id));
}
//<!-- for Facebook -->
Yii::app()->clientScript->registerMetaTag($model,null,null,array('property'=>'og:title'));

if ($place_image)
    Yii::app()->clientScript->registerMetaTag($place_image,null,null,array('property'=>'og:image'));
Yii::app()->clientScript->registerMetaTag($url,null,null,array('property'=>'og:url'));
Yii::app()->clientScript->registerMetaTag($summary,null,null,array('property'=>'og:description'));
Yii::app()->clientScript->registerMetaTag('place',null,null,array('property'=>'og:type'));
if ($model->lat){
    Yii::app()->clientScript->registerMetaTag($model->lat,null,null,array('property'=>'place:location:latitude'));
    Yii::app()->clientScript->registerMetaTag($model->lng,null,null,array('property'=>'place:location:longitude'));
}
$is_map_shown = false;
?>
<style>.gmap_canvas img{max-width:none!important;background:none!important}</style>

<!-- content begin -->
<div id="content" class="no-padding content_type_<?php echo strtolower(preg_replace('#\W| #','_', $model->type0->name_en));?>">

    <?php if ($model->type != 10): ?>

    <!-- section begin -->
    <section id="section-project" class="_no-padding-top content">
        <div class="container">
           
            <div class="row" style="direction:ltr">
                
            </div>         

            <div class="row">
                
                <div class="col-md-9">

                    <h2 class="pull-left visible-lg-block hidden-lg"><?php echo $model;?></h2>
                     <?php if ($model->libFiles): ?>
                            <div class="project-slider owl-carousel owl-theme" dir="ltr">
        
                                <?php
                                foreach ($model->libFiles as $image) {
                                    if ($image->id == $model->main_image_id) continue;
                                    ?>
                                        <div class="item"><img class="" src="<?php echo $this->uploadsUrl . $image->path;?>" alt=""></div>          
                                    <?php
                                }
                                ?>                      
                            </div>
                        <?php endif; ?>
                    <div class="project-info" id="project-info">
                        <?php echo $model->{"contents_" . Yii::app()->language};?>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589c28379a2f9dc1"></script> 

                        <?php if ($model->entityAttributes || $model->region): ?>
                        <ul class="project-list">
                        <?php if ($model->region):?>
                            <li class="entity_attribute entity_location"><span class="attribute_label "><strong><?php echo Yii::t('app','City'); ?>:</strong></span> <div class="attribute_value"><?php echo $model->region->city;?></div></li>
                            <li class="entity_attribute entity_location"><span class="attribute_label"><strong><?php echo Yii::t('app','Region'); ?>:</strong></span> <div class="attribute_value"><?php echo $model->region;?></div></li>

                        <?php endif; ?>
                        <?php foreach ($model->entityAttributes as $attribute): 
                            if ($attribute->entityTypeAttribute->attribute_type_id == 3){
                                $value = trim($attribute);
                                if (mb_strlen($value,'utf8')<50 && strpos($value,"\n")===false){
                                    $attribute->entityTypeAttribute->attribute_type_id = 2;
                                }
                            } 
                            ?>
                            <li class="entity_attribute attribute_type_<?php echo $attribute->entityTypeAttribute->attributeType;?>"><span class="attribute_label"><strong><?php echo $attribute->entityTypeAttribute; ?>:</strong></span> <div class="attribute_value"><?php
                            $value = "";
                            switch($attribute->entityTypeAttribute->attribute_type_id){
                                case 1: // integer
                                    $value = $attribute;
                                break;
                                case 7: // address
                                case 3: // textarea
                                    $value = nl2br($attribute);
                                break;
                                case 4: // html
                                    $value = $attribute;
                                break;
                                case 5: // url
                                    $value = '<a target="_blank" href="' . $attribute . '">' . $attribute . '</a>';

                                break;
                                case 6: // boolean
                                    $value = ($attribute)?Yii::t('app', 'Yes'):Yii::t('app', 'No');
                                break;
                                case 2: // string
                                default:
                                    $value = $attribute;

                                break;
                            }

                            echo (trim($value))?$value:Yii::t('app', 'N/A');?></div></li>

                        <?php if (!$is_map_shown && $model->lat && $model->lng && $attribute->entityTypeAttribute->attribute_type_id==7): ?>
                        <li class="entity_attribute attribute_type_map"> 
                        <div class="location map">
                            <div style='overflow:hidden;height:440px;width:100%;'>
                                <div id='gmap_canvas' class="gmap_canvas" style='height:440px;width:100%;'></div>
                                <div><small><a href="http://embedgooglemaps.com">                                   embed google maps                           </a></small></div>
                                <div><small><a href="https://termsofusegenerator.net">terms of use example</a></small></div>
                            </div>
                            <script type='text/javascript'>function init_map(){var myOptions = {zoom:15,center:new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>)});infowindow = new google.maps.InfoWindow({content:'<div style="margin-right: 15px;"><strong><?php echo $model;?></strong></div>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
                            <br />
                        </div>
                        </li>
                        <?php $is_map_shown = true;endif; ?>

                        <?php endforeach; ?>
                        </ul>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-3">
                        <div class="main-sidebar">

                           <?php if ($subEntities): ?>
                                    <aside class="widget widget_text widget_side_subentity">
                                        <h3 class="widget-title">
                                        	<?php echo Yii::t("app","Table of contents"); ?>
                                        </h3>
                                       <div class="tiny-border"></div>
                                                <a class="depart_type" href="#project-info">
                                                - <?php echo Yii::t("app","Basic information"); ?>
                                                </a>

				                                <!-- Related Project begin -->
				                                <?php foreach($subEntities as $subEntityType): ?>
				                                            <a class="depart_type" href="#subEntityType_<?php echo $subEntityType->id;?>">
				                                            - <?php echo $subEntityType; ?>
				                                            </a>
				                                            <?php endforeach; ?>
				                            <!-- Related Project close -->
				                            
                                      
                                    </aside>
                                
							<?php endif; ?>
                            <aside class="widget widget_text">
                                <h3 class="widget-title"><?php echo Yii::t("app","Advertisement"); ?></h3>
                                <div class="tiny-border"></div>                                         
                                <div class="textwidget advertsiment">
                                   <?php $this->generateAds(11); ?>                             
                                </div>
                            </aside>  

                            <?php if (!empty($this->related)): ?>
                            <aside class="widget widget_related_articles">
                                <h3 class="widget-title"><?php echo Yii::t('app','Related Articles');?></h3> 
                                <div class="tiny-border"></div>   
                                    <?php foreach($this->related as $url=>$title):?>
                                        - <a href="<?php echo $url;?>"><?php echo $title;?></a><br />
                                    <?php endforeach; ?>
                            </aside>
                            <?php endif; ?>

                            <?php if($sameUniversityEntities): ?>
                            <aside class="widget widget_text">
                                <h3 class="widget-title">
                                       <?php echo ($model->type0 && $model->parent)?Yii::t('app', 'More of {parent}',array('{parent}'=>$model->parent)):'';?>
                                </h3>
                                <div class="tiny-border"></div>
                                <div class="main_univ">
                                    <div class="main_sec">
                                    <?php foreach ($sameUniversityEntities as $entity) {
                                        $url = "";
                                        if ($entity->slug){
                                            $url = $this->createAbsoluteUrl('page',array('p'=>$entity->slug));
                                        }else{
                                            $url = $this->createAbsoluteUrl('view',array('id'=>$entity->id));
                                        }

                                        ?>
                                        <a class="depart_type" href="<?php echo $url;?>">
                                             <?php echo $entity; ?>      
                                        </a>
                                        <?php
                                    }
                                    ?>
                                    </div>
                                </div>
                                <a class="more_see_univer more" href="#"><?php echo Yii::t('app','المزيد');?></a>
                                <a class="more_see_univer less" href="#"><?php echo Yii::t('app','اقل');?></a>
                            </aside>
                            <?php endif; ?>

                            <?php if($sameCategoryEntities): ?>
                             <aside class="widget widget_text">
                                <h3 class="widget-title">
                                       <?php echo ($model->category)?Yii::t('app', '{category_name}',array('{category_name}'=>$model->category)):'';?>
                                </h3>
                                <div class="tiny-border"></div>
                                <div class="main_univ">
                                    <div class="main_sec">
                                    <?php foreach ($sameCategoryEntities as $entity) {
                                        $url = "";
                                        if ($entity->slug){
                                            $url = $this->createAbsoluteUrl('page',array('p'=>$entity->slug));
                                        }else{
                                            $url = $this->createAbsoluteUrl('view',array('id'=>$entity->id));
                                        }

                                        ?>
                                        <a class="depart_type" href="<?php echo $url;?>">
                                             <?php echo $entity->parent . ' - ' . $entity; ?>      
                                        </a>
                                        <?php
                                    }
                                    ?>
                                     </div>
                                </div>
                                <a class="more_see_univer more" href="#"><?php echo Yii::t('app','المزيد');?></a>
                                <a class="more_see_univer less" href="#"><?php echo Yii::t('app','اقل');?></a>
                            </aside>
                            <?php endif; ?>  


                            <?php if ($nearbyPlaces && $nearbyPlaces->itemCount) :?>
                            <div class="tabs tabs-2" data-active="1">
                                <header>
                                    <h4 class="active"><?php echo Yii::t('app','Nearby places');?></h4>
                                </header>

                                <div class="tabs-main">
                                    <div style="display: block;" class="tabs-item">
                                        <?php $this->widget('zii.widgets.CListView', array(
                                            'dataProvider'=>$nearbyPlaces,
                                            'itemView'=>'/places/_view_mini',
                                            'itemsCssClass'=>'items clearfix',
                                            'template'=>'{items}',
                                            'pager'=>false,
                                        )); 
                                        ?>                 
                                    </div>
                                </div>

                            </div>
                            <?php endif; ?>

                        </div>                        
                    </div>
            </div>

            <div class="col-md-9">
            <?php if(!$is_map_shown && $model->lat && $model->lng): ?>
            <hr style="padding: 5px;">
             <div class="text-center">
                <h1 class="box-title"><?php echo Yii::t('app','Location');?></h1>                                
                <div class="tiny-border"></div>
            </div>
            
            <div class="location map">
                <div style='overflow:hidden;height:440px;width:100%;'>
                    <div id='gmap_canvas' class="gmap_canvas" style='height:440px;width:100%;'></div>
                    <div><small><a href="http://embedgooglemaps.com">                                   embed google maps                           </a></small></div>
                    <div><small><a href="https://termsofusegenerator.net">terms of use example</a></small></div>
                </div>
                <script type='text/javascript'>function init_map(){var myOptions = {zoom:15,center:new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>)});infowindow = new google.maps.InfoWindow({content:'<div style="margin-right: 15px;"><strong><?php echo $model;?></strong></div>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
                <br />
            </div>
            <?php $is_map_shown = true;endif; ?>
            
            </div>
        </div>
    </section>
    <?php endif; ?>

    <?php if ($campuses): ?>
    <!-- Related Project begin -->
    
    <section id="section-campuses" class="no-padding-top">
        <div class="container">
            <div class="col-md-9">
                <div class="text-center">
                <h2 class="box-title"><?php echo Yii::t('app', 'Campuses'); ?></h2>                                
                <div class="tiny-border"></div>
            </div>
            <?php foreach($campuses as $campus): ?>
            <div class="campus">
                <h3 class="feature-title"> <?php echo $campus;?></h3>
                <div class="campus map">
                    <div style='overflow:hidden;height:440px;width:100%;'>
                        <div id='campus_<?php echo $campus->id;?>' class='gmap_canvas' style='height:440px;width:100%;'></div>
                        <div><small><a href="http://embedgooglemaps.com">                                   embed google maps                           </a></small></div>
                        <div><small><a href="https://termsofusegenerator.net">terms of use example</a></small></div>
                    </div>
                    <script type='text/javascript'>function init_map_campus_<?php echo $campus->id;?>(){var myOptions = {zoom:15,center:new google.maps.LatLng(<?php echo $campus->lat;?>,<?php echo $campus->lng;?>),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('campus_<?php echo $campus->id;?>'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(<?php echo $campus->lat;?>,<?php echo $campus->lng;?>)});infowindow = new google.maps.InfoWindow({content:'<div style="margin-right: 15px;"><strong><?php echo $campus;?></strong></div>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map_campus_<?php echo $campus->id;?>);</script>
                    <br />
                </div>
            </div>
            <?php endforeach; ?>
            </div>
        </div>     
    </section>
  
    <!-- Related Project close -->
    <?php endif; ?>
    
     <section id="section-colleges" class="no-padding-top">
        <div class="container">
            <div class="col-md-9">
        <?php if ($subEntities): ?>
            <!-- Related Project begin -->
            <?php foreach($subEntities as $subEntityType): ?>
                <div id="subEntityType_<?php echo $subEntityType->id;?>" class="widget widget_text widget_subentity">
                <h3 class="widget-title"><?php echo $subEntityType; ?></h3>
                   <div class="tiny-border"></div>
                    <div class="row sub-entity sub-entity-<?php echo $subEntityType->name_en; ?>">
                        <div class="col-md-12">
                            <div class="latest-projects-2 clearfix">                                
                                <div class="latest-projects-wrapper">
                                    <div class="row"> 
                                        <div class="blog-grid">
                                            <?php
                                            foreach ($subEntityType->entities as $entity) {
                                                $this->renderPartial('/universities/_entity_mini',array('data'=>$entity));
                                            }
                                            ?>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <a class="more_see" href="#"><?php echo Yii::t('app','See More');?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
                   
            <!-- Related Project close -->
            <?php endif; ?>
            <?php /* ?>
            <div class="tabs tabs-2" data-active="1">
                <header>
                    <h4><?php echo Yii::t('app','Faculties');?></h4>

                </header>

                <div class="tabs-main">
                    <div class="tabs-item">
                        <p></p>
                        <div class="main_univ">
                            <div class="blog_grid">                         
                                <a class="btn btn-primary btn-radius width grey mleft" href="#">Engeneering</a>
                                <a class="btn btn-primary btn-radius width grey mleft" href="#">Commerce</a>
                                <a class="btn btn-primary btn-radius width grey mleft" href="#">Computer Sceince</a>
                                <a class="btn btn-primary btn-radius width grey mleft" href="#">Sceinces</a>
                                <a class="btn btn-primary btn-radius width grey mleft" href="#">Law</a>
                            </div>
                        </div>
                        <a class="more_see_univer more" href="#"><?php echo Yii::t('app','See More');?></a>
                        <a class="more_see_univer less" href="#"><?php echo Yii::t('app','See Less');?></a>
                    </div>
                </div>
            </div>
            <div class="tabs tabs-2" data-active="1">
                <header>
                    <h4><?php echo Yii::t('app','Faculties');?></h4>

                </header>

                <div class="tabs-main">
                    <div class="tabs-item">
                        <p></p>
                        <div class="main_univ">
                            <div class="blog_grid">                         
                                <a class="btn btn-primary btn-radius width grey mleft" href="#">Cairo Engeneering</a>
                                <a class="btn btn-primary btn-radius width grey mleft" href="#">Alex Engeneering</a>
                                <a class="btn btn-primary btn-radius width grey mleft" href="#">Aswan Engeneering</a>
                                <a class="btn btn-primary btn-radius width grey mleft" href="#">Port Said Engeneering</a>
                                <a class="btn btn-primary btn-radius width grey mleft" href="#">Ismailia Engeneering</a>
                            </div>
                        </div>
                        <a class="more_see_univer more" href="#"><?php echo Yii::t('app','See More');?></a>
                        <a class="more_see_univer less" href="#"><?php echo Yii::t('app','See Less');?></a>
                    </div>
                </div>

            </div>
            <?php */ ?>
        </div>
        </div>
    </section>
    <?php if (Yii::app()->language!="ar"): ?>
    <div class="container">
        <div class="col-md-9">
            <span class="note pull-left">* The content is auto translated by third party engine.</span>
        </div>
    </div>
    <?php endif; ?>
    <section class="adv_bottom">
	<div class="container">
        <div class="col-md-12">
		<?php $this->generateAds(14); ?>
	   </div>
	</div>
    </section>
    <section class="no-padding-top">
        <div class="container">
            <div class="col-md-9">
            <div class="fb-comments" data-href="<?php echo $url; ?>" width="100%" data-numposts="5"></div>
        </div>     
        </div>
    </section>
    
</div>
<!-- content close -->
<div class="clearfix"></div>

<style>
    .container{
        max-width: 100%;
    }
    aside .main_univ:not(.remove_height){
        max-height: 190px;
    }
</style>
<script type="text/javascript">
    /* Portfolio Sorting */
    jQuery(document).ready(function($){
        $(".project-slider").owlCarousel({
            singleItem: true,
            lazyLoad: true,
            navigation: true,
            autoPlay : true,
            navigationText: [
              "<i class='fa fa-chevron-left'></i>",
              "<i class='fa fa-chevron-right'></i>"
            ],
            slideSpeed : 400,
        });
        $('#section-colleges aside .latest-projects-wrapper').each(function(){
          var this_height = $(this).find('.blog-grid').height();
          if(this_height > 200){
            $(this).next('a.more_see').css('display','block');
          }
        });
         $('.main_sec').each(function(){
          var this_height = $(this).height();
          if(this_height > 195){
            $(this).parent().next('a.more_see_univer.more').css('display','block');
          }
        });
        $('a.more_see_univer.more').click(function(e){
            e.preventDefault();
            $(this).parent().find('a.more_see_univer.less').css('display','block');
            $(this).hide();
            $(this).prev().toggleClass('remove_height');
        });
        $('a.more_see_univer.less').click(function(e){
            e.preventDefault();
            $(this).parent().find('a.more_see_univer.more').css('display','block');
            $(this).hide();
            $(this).prev().prev().toggleClass('remove_height');
        });
        $('a.more_see').click(function(e){
            e.preventDefault();
            $(this).prev().toggleClass('remove_height');
        });
        $('.widget_side_subentity a').click(function(e){
            e.preventDefault();
            var me = $(this);
            $("html, body").animate({ scrollTop: $(me.attr('href')).offset().top - 100 }, 1000);
        });
    });
      $(window).load(function(){
    	//serTimeout(function(){
    		$('.entity_attribute.attribute_type_textarea').each(function(){
			  var this_height = $(this).find('.attribute_value').height();
			  if(this_height > 210){
				$(this).append('<span class="see_more text_more"><?php echo Yii::t('app','See More');?></span>');
                $(this).append('<span class="see_more text_less"><?php echo Yii::t('app','See Less');?></span>');
		      }
			});
			$(document).on('click','.see_more',function(){
				$(this).parent('.entity_attribute.attribute_type_textarea').toggleClass('remove_height_max');
			});
    	//},10000);
    });
</script>
