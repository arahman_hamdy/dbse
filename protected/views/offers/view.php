<?php
$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);
$this->pageTitle = Yii::app()->name . ' - ' . GxHtml::valueEx($model);
?>
<div class="container">
    <div class="row"> 
        <div class="clearfix" style="margin-top: 20px;"></div>
        <div class="col-md-9">
            <div class="blog-single">
                <!-- post begin -->
                <article>
                    <div class="post-metadata visible-lg-block hidden-lg">
                        <h3><?php echo $model; ?></h3>
                    </div>

                    <div class="post-media">
                        <img src="<?php echo $model->imageUrl; ?>" class="img-responsive">                                    
                    </div>
                    
                    <br />

                    <?php if($model->lat && $model->lng): ?>
                    <a href="#map">
                    <?php endif; ?>
                    <i class="fa fa-fw fa-map-marker"></i><span><?php echo $model->{"address_" . Yii::app()->language};?></span>
                    <?php if($model->lat && $model->lng): ?>
                    </a>
                    <?php endif; ?>
                    <hr />
                    <div class="post-content">
                        <div class="post-entry">
                            <?php echo $model->{"description_" . Yii::app()->language}; ?>
                            <div class="hr"></div>
                           
                            <h3><?php echo Yii::t("app", "Offer Information")?></h3>
                            <ul class="list-style-1">
                                <li><?php echo Yii::t("app", "Expiration Date")?>  <?php echo date("Y-m-d", strtotime($model->expiration_date)); ?></li>
                                <li><?php echo Yii::t("app", "Cost")?>: <?php echo $model->price . ' ' . $model->place->country->payment_currency?></li>
                            </ul>                                                                  
                        </div>
                        <div class="clearfix"></div>

                        <div id="map">
                        </div>
                        <?php if($model->lat && $model->lng): ?>
                        <hr style="padding: 5px;">
                        <div>
                            <div style='overflow:hidden;height:440px;width:100%;'>
                                <div id='gmap_canvas' style='height:440px;width:100%;'></div>
                                <div><small><a href="http://embedgooglemaps.com">                                   embed google maps                           </a></small></div>
                                <div><small><a href="https://termsofusegenerator.net">terms of use example</a></small></div>
                                <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
                            </div>
                            <script type='text/javascript'>function init_map(){var myOptions = {zoom:15,center:new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>)});infowindow = new google.maps.InfoWindow({content:'<div style="margin-right: 15px;"><strong><?php echo $model;?></strong><br><?php echo $model->{"address_" . Yii::app()->language};?><br></div>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
                            <br />
                        </div>
                        <?php endif; ?>

                        <p style="display:none">
                            <a href="#" class="btn btn-primary btn-small">Attending</a>
                            <a href="#" class="btn btn-primary btn-small">remind me </a>
                            <a href="#" class="btn btn-primary btn-small">book </a>
                        </p>

                    <div id="booking-form">
                       <div class="white-popup" style="padding: 0; margin: 0;">
                        <h3><?php echo Yii::t("app", "Booking details"); ?></h3>

                <?php if (Yii::app()->user->isGuest):?>
                        <?php echo GxHtml::link(Yii::t('app', 'Login to book'), $this->createUrl("//user/login"),array('class'=>'btn btn-primary btn-big')); ?>
                        <div class="clearfix"></div>
                        <br />
                <?php elseif (($model->expiration_date && strtotime($model->expiration_date)<time())):?>
                    <p><?php echo Yii::t("app","Booking to this offer already closed!");?></p>
                <?php else: ?>



                    <?php $form = $this->beginWidget('GxActiveForm', array(
                        'id' => 'event-registration-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                            'afterValidate' => 'js:function(form, data, hasError) { 
                                if(hasError) {
                                    for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
                                    return false;
                                }
                                else {
                                    form.children().removeClass("validate-has-error");
                                    return true;
                                }
                            }',
                            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                                if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
                                else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
                            }'
                        ),
                        'htmlOptions'=>array(
                            'class'=>'form-horizontal form-groups-bordered',
                            'enctype' => 'multipart/form-data',
                        )
                    ));
                    ?>
                    <?php if ($formModel->errors): ?> 
                        <div class="alert alert-danger"> 
                            <?php echo $form->errorSummary($formModel); ?> 
                        </div> 
                    <?php endif; ?> 

                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'room_id'); ?>
                            <div class="col-sm-9 selectpicker"> 
                                <?php
                                $items = array();
                                $itemsOptions = array();
                                foreach($rooms as $room){
                                    $items[$room->id] = $room->{"name_" . Yii::app()->language} . "(" . $room->price . $model->place->country->payment_currency . ")";
                                    //$itemsOptions[$room->id] = array("disabled"=>"disabled");
                                }

                                 echo $form->dropDownList($formModel, 'room_id', $items, array("class"=>"nodefault selectpicker",'options'=>$itemsOptions)); ?>
                                <?php echo $form->error($formModel,'room_id'); ?>
                            </div> 
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'name'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'name', array('maxlength' => 100)); ?>
                                <?php echo $form->error($formModel,'name'); ?>
                            </div> 
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'phone'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'phone', array('maxlength' => 50)); ?>
                                <?php echo $form->error($formModel,'phone'); ?>
                            </div> 
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'email'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'email', array('maxlength' => 50)); ?>
                                <?php echo $form->error($formModel,'email'); ?>
                            </div> 
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'date'); ?>
                            <div class="col-sm-9"> 
                                <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array( 
                            'model' => $formModel, 
                            'attribute' => 'date', 
                            'value' => $formModel->date, 
                            'htmlOptions' => array('class' => 'form-control'), 
                            'options' => array( 
                                'showButtonPanel' => true, 
                                'changeYear' => true, 
                                'dateFormat' => 'yy-mm-dd', 
                                'format' => 'yyyy-mm-dd', 
                                ), 
                            ));
                ; ?>
                                <?php echo $form->error($formModel,'date'); ?>
                            </div> 
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'time_from'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'time_from', array('class' => 'datetimePicker', 'placeholder'=>Yii::t("app","HH:MM"))); ?>
                                <?php echo $form->error($formModel,'time_from'); ?>
                            </div>  
                        </div> 
                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'time_to'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'time_to', array('class' => 'datetimePicker', 'placeholder'=>Yii::t("app","HH:MM"))); ?>
                                <?php echo $form->error($formModel,'time_to'); ?>
                            </div> 
                        </div> 

                        <div class="form-group"> 
                            <?php echo $form->labelEx($formModel,'persons'); ?>
                            <div class="col-sm-9"> 
                                <?php echo $form->textField($formModel, 'persons'); ?>
                                <?php echo $form->error($formModel,'persons'); ?>
                            </div> 
                        </div>


                    <?php if ($services): ?>
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo Yii::t("app","Addon services")?></label>
                        <div class="col-sm-9"> 
                        <?php foreach($services as $service): ?>
                            <div class="clearfix"></div>
                            <label>
                            <input type="checkbox" name="services[]" value="<?php echo $service->id;?>" class="pull-left addon-service" style="margin-left: 10px;"> <?php echo $service;?> (<?php echo $service->price . $model->place->country->payment_currency;?>)
                            </label>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>

                    <div class="form-group"> 
                        <?php echo $form->labelEx($formModel,'payment_method'); ?>
                        <div class="col-sm-9"> 
                            <?php
                            $payment_options = array();
                            if ($model->place->accepts_online_payment){
                                $payment_options[1] = Yii::t("app", "Pay online now");
                            }
                            if ($model->place->accepts_offline_payment){
                                $payment_options[2] = Yii::t("app", "Book now, pay later");
                            }

                            ?>
                            <?php echo $form->dropDownList($formModel, 'payment_method', $payment_options); ?>
                            <?php echo $form->error($formModel,'payment_method'); ?>
                        </div> 
                    </div> 

                    <div class="form-group"> 
                        <?php echo $form->labelEx($formModel,'notes'); ?>
                        <div class="col-sm-9"> 
                            <?php echo $form->textField($formModel, 'notes', array('maxlength' => 200)); ?>
                            <?php echo $form->error($formModel,'notes'); ?>
                        </div> 
                    </div> 

                    <div class="form-group"> 
                        <div class="col-sm-3"> 
                        </div>
                        <div class="col-sm-4"> 
                            <?php echo GxHtml::submitButton(Yii::t('app', 'Book now'), array('class'=>'btn btn-primary btn-big')); ?>
                        </div> 
                        <div class="col-sm-5"> 
                            <span class="total_cost_label pull-right">
                                Total cost
                            </span>
                            <span class="total_cost_value pull-right">
                                0
                            </span>
                            <span class="total_cost_currency pull-right">
                                <?php echo $model->place->country->payment_currency;?>
                            </span>
                        </div>                         
                    </div> 
                    <?php $this->endWidget(); ?><!-- form -->
                    <?php endif; ?>
                     </div>
                     </div> 

                </article>
                <!-- post close -->
            </div>                       
        </div>
        <d        <div class="col-md-3">
           <div class="like_block col-md-12"> 
               <div class="clearfix"></div>
               <ul> 
                   
                    <li class="pull-right" id="like_tab">
                        <a class="offer_like_button" href="#">Like</a>
                        <span class="counter"><span class="likes_counter"><?php echo $model->likes;?></span> <?php echo Yii::t("app", "likes");?></span>
                    </li>
                    <li class="pull-right" id="report_tab">
                        <a class="offer_report_button" href="#">Report</a>
                        <span class="counter"><span class="reports_counter"><?php echo $model->reports;?></span> <?php echo Yii::t("app", "reports");?></span>
                    </li>
                </ul> 

                <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-primary center full-width offer_book_button" href="#booking-form"><?php echo Yii::t("app", "Book");?></a>
                </div>   
                </div>
           </div> 
            
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $offer_like_button_pressed = false;
        $(document).on('click','.offer_like_button ',function(e){
            e.preventDefault();
            if ($offer_like_button_pressed) return;
            $.post("<?php echo $this->createUrl("like",array("id"=>$model->id));?>",
                function(data) {
                    $(".likes_counter").html(data)
                }
             );

            $offer_like_button_pressed = true;
        });

        $offer_report_button_pressed = false;
        $(document).on('click','.offer_report_button ',function(e){
            e.preventDefault();
            if ($offer_report_button_pressed) return;
            $.post("<?php echo $this->createUrl("report",array("id"=>$model->id));?>",
                function(data) {
                    $(".reports_counter").html(data)
                }
             );

            $offer_report_button_pressed = true;
        });

        $offer_attending_button_pressed = false;
        $(document).on('click','.offer_attending_button ',function(e){
            e.preventDefault();
            if ($offer_attending_button_pressed) return;
            $.post("<?php echo $this->createUrl("attending",array("id"=>$model->id));?>",
                function(data) {
                    $(".attendings_counter").html(data)
                }
             );

            $offer_attending_button_pressed = true;
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function($){
        $('.selectpicker select').selectpicker();
        $('.datetimePicker').timepicker({ 'scrollDefault': 'now', 'timeFormat': 'H:i', 'forceRoundTime': true });

        $('.datetimePicker').on('changeTime', function() {
            updateCost();
        });

        $('.addon-service').on('change', function() {
            updateCost();
        });

        updateCost();
    });

    var roomsCosts = <?php echo CJSON::encode(CHtml::listData($rooms, 'id', 'price')); ?>;
    var servicesCosts = <?php echo CJSON::encode(CHtml::listData($services, 'id', 'price')); ?>;

    var costs = 0;

    function updateCost(){
        costs = 0;

        time_from = $("#Booking_time_from").val().split(":");
        time_from_converted = 0;
        if (typeof time_from[1] != "undefined"){
            time_from_converted = parseInt(time_from[0]) + parseFloat(time_from[1]/60);
        }

        time_to = $("#Booking_time_to").val().split(":");
        time_to_converted = 0;
        if (typeof time_to[1] != "undefined"){
            time_to_converted = parseInt(time_to[0]) + parseFloat(time_to[1]/60);
        }

        hours = 0;
        if (time_to_converted <= time_from_converted){
            time_to_converted += 24;
        }

        if (typeof time_from[1] != "undefined" && typeof time_to[1] != "undefined"){
            hours = time_to_converted - time_from_converted;
        }

        room_id = $("#Booking_room_id").val();
        if (typeof roomsCosts[room_id] != "undefined")
            costs += parseFloat(roomsCosts[room_id]);

        services = $(".addon-service:checked").map(function() {
            return this.value;
        }).get();
        for(i=0; i<services.length; i++){
            service_id = services[i];
            if (typeof servicesCosts[service_id] != "undefined")
                costs += parseFloat(servicesCosts[service_id]);
        }

        costs *= hours;

        $(".total_cost_value").html(costs);
    }
  
</script>