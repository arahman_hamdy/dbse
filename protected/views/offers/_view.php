<!-- post begin -->
<article class="item col-md-4 col-sm-6">
    <div class="post-media">
        <a href="<?php echo $this->createUrl("view",array("id"=>$data->id)); ?>"><img  alt="" src="<?php echo $data->imageUrl; ?>" class="img-responsive">
        <div class="post-date">
            <span class="date-day"><?php echo $data->price; ?></span>
            <span class="date-month"><?php echo $data->place->country->{"currency_" . Yii::app()->language}; ?></span>
        </div>
        </a>
    </div>
    <div class="post-content">
       <a href="<?php echo $this->createUrl("view",array("id"=>$data->id)); ?>">
        <div class="post-text">
            <span class="byline pull-left">
                <?php echo $data; ?>
            </span>
        </div>
        </a>
    </div>
</article>
<!-- post close -->
