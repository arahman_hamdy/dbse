
<div class="container">
    <div class="row"> 
    	<div class="clearfix" style="margin-top: 20px;"></div>
            <div class="col-md-9">
                <h3>عروضي</h3>
                <table cellspacing="0" class="shop_table shop_table_responsive cart">
                                <thead>
                                    <tr>
                                        <th class="product-remove">&nbsp;</th>
                                        <th class="product-thumbnail">&nbsp;</th>
                                        <th class="product-name">اسم العرض</th>
                                        <th class="product-price">السعر</th>
                                        <th class="product-quantity">تاريخ الانتهاء</th>
                                        <th class="product-subtotal">جهة الانشاء</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="cart_item">
                                        <td class="product-remove">
                                            <a class="remove" href="#">×</a>                   
                                        </td>
                                        <td class="product-thumbnail">
                                            <a href="#">
                                                <img src="images/shop/thumb/product-thumb-1.jpg">
                                            </a>                 
                                        </td>
                                        <td data-title="Product" class="offer-name">
                                            <a href="#">عرض رقم واحد</a>
                                        </td>
                                        <td data-title="Price" class="offer-price">
                                            <span class="amount">$2,189</span>                   
                                        </td>
                                        <td data-title="Quantity" class="offer-date">
                                           30 فبراير 2019
                                        </td>
                                        <td data-title="place" class="offer-place">
                                            <span class="amount">الدقي</span>                
                                        </td>
                                    </tr>
                                    <tr class="cart_item">
                                        <td class="product-remove">
                                            <a class="remove" href="#">×</a>                   
                                        </td>
                                        <td class="product-thumbnail">
                                            <a href="#">
                                                <img src="images/shop/thumb/product-thumb-1.jpg">
                                            </a>                 
                                        </td>
                                        <td data-title="Product" class="offer-name">
                                            <a href="#">عرض رقم واحد</a>
                                        </td>
                                        <td data-title="Price" class="offer-price">
                                            <span class="amount">$2,189</span>                   
                                        </td>
                                        <td data-title="Quantity" class="offer-date">
                                           30 فبراير 2019
                                        </td>
                                        <td data-title="place" class="offer-place">
                                            <span class="amount">الدقي</span>                
                                        </td>
                                    </tr>
                                    <tr class="cart_item">
                                        <td class="product-remove">
                                            <a class="remove" href="#">×</a>                   
                                        </td>
                                        <td class="product-thumbnail">
                                            <a href="#">
                                                <img src="images/shop/thumb/product-thumb-1.jpg">
                                            </a>                 
                                        </td>
                                        <td data-title="Product" class="offer-name">
                                            <a href="#">عرض رقم واحد</a>
                                        </td>
                                        <td data-title="Price" class="offer-price">
                                            <span class="amount">$2,189</span>                   
                                        </td>
                                        <td data-title="Quantity" class="offer-date">
                                           30 فبراير 2019
                                        </td>
                                        <td data-title="place" class="offer-place">
                                            <span class="amount">الدقي</span>                
                                        </td>
                                    </tr>
                                    <tr class="cart_item">
                                        <td class="product-remove">
                                            <a class="remove" href="#">×</a>                   
                                        </td>
                                        <td class="product-thumbnail">
                                            <a href="#">
                                                <img src="images/shop/thumb/product-thumb-1.jpg">
                                            </a>                 
                                        </td>
                                        <td data-title="Product" class="offer-name">
                                            <a href="#">عرض رقم واحد</a>
                                        </td>
                                        <td data-title="Price" class="offer-price">
                                            <span class="amount">$2,189</span>                   
                                        </td>
                                        <td data-title="Quantity" class="offer-date">
                                           30 فبراير 2019
                                        </td>
                                        <td data-title="place" class="offer-place">
                                            <span class="amount">الدقي</span>                
                                        </td>
                                    </tr>
                                    <tr class="cart_item">
                                        <td class="product-remove">
                                            <a class="remove" href="#">×</a>                   
                                        </td>
                                        <td class="product-thumbnail">
                                            <a href="#">
                                                <img src="images/shop/thumb/product-thumb-1.jpg">
                                            </a>                 
                                        </td>
                                        <td data-title="Product" class="offer-name">
                                            <a href="#">عرض رقم واحد</a>
                                        </td>
                                        <td data-title="Price" class="offer-price">
                                            <span class="amount">$2,189</span>                   
                                        </td>
                                        <td data-title="Quantity" class="offer-date">
                                           30 فبراير 2019
                                        </td>
                                        <td data-title="place" class="offer-place">
                                            <span class="amount">الدقي</span>                
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                                         
            </div>
<div class="col-md-3">
    <div class="main-sidebar">
        <?php if (!empty($this->blocks)): ?>
        <?php foreach($this->blocks as $block):?>
        <aside class="widget widget_block">
            <h3 class="widget-title"><?php echo $block;?></h3> 
            <div class="tiny-border"></div>
            <?php switch ($block->template_id) {
                case 1:
                    $category = BlogCategory::model()->cache(60)->findByPK($block->article_cat_id);
                    if (!$category || $category->is_deleted) continue;
                    ?>
                        <?php 

                        $criteria = new CDbCriteria;
                        $criteria->compare("category_id",$block->article_cat_id);
                        $criteria->compare("t.is_deleted",0);
                        $criteria->compare("t.status",1);
                        $criteria->order = "t.id desc";
                        $criteria->limit = "10";

                        $articles = BlogArticle::model()->cache(0)->findAll($criteria);
                        foreach($articles as $article):?>
                            <?php if ($article->image_file): ?>
                            <img class="related_img pull-left" src="<?php echo $article->image_file;?>"  alt="">
                            <?php endif; ?>
                            <a class="pull-left" href="<?php echo $article->url;?>"><?php echo $article;?></a>
                            <div class="clearfix"></div>
                        <?php endforeach; ?>
                    <?php
                    break;

                case 11:
                    echo $block->html;
                    break;
                default:
                    continue;
            } ?>
        </aside>
        <?php endforeach; ?>
        <?php endif; ?>

    </div>         
</div>

	</div>
</div>
