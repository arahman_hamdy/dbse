<?php
$this->breadcrumbs = array(
    Yii::t('app','Offers') => array('index'),
);
?>

<div class="container">
    <div class="row"> 
        <div class="clearfix" style="margin-top: 20px;"></div>
<div class="col-md-12">

<div id="offers-grid" class="blog-grid">


    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'_view',
        'itemsCssClass'=>'',
        'pagerCssClass'=>'pagination-ourter text-center',
        'pager'=>array(
            'class'=>'CLinkPager',
            'htmlOptions'=>array(
                'class'=>"pagination"
                ),
            'firstPageLabel' => "<<",
            'lastPageLabel' => ">>",
            'nextPageLabel' => ">",
            'prevPageLabel' => "<",
            'selectedPageCssClass' => 'active',
            'header'=>'',
            'footer'=>'',

        ),
    )); 
    ?>

    </div>
</div>
</div>
</div>
