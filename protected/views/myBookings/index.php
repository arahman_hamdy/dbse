<?php
$this->breadcrumbs = array(
    Yii::t('app','Membership') => array('index'),
);
?>

<div class="container">
    <div class="row"> 
    	<p></p><p></p>
<div class="col-md-12">
	 <div class="blog-single">
                <!-- post begin -->
                <article>
                    <div class="post-content">
                            
                         <div class="hr"></div>
                         <h3><?php echo Yii::t("app", "Co-working spaces bookings"); ?></h3>

                        <div class="dataTables_wrapper form-inline">
                        <?php $this->widget('CGridViewCustom', array(
                            'id' => 'bookings-grid',
                            'dataProvider' => $bookings,
                            'filter' => null,
                            'show_create_button' => false,
                            'itemsCssClass' => "shop_table shop_table_responsive cart",
                            'columns' => array(
                                'dateline',
                                array( 
                                        'name'=>'place_id', 
                                        'value'=>'GxHtml::valueEx($data->place)', 
                                        ),
                                array( 
                                        'name'=>'room_id', 
                                        'value'=>'GxHtml::valueEx($data->room)', 
                                        ),
                                'name',
                                'total',
                                'start_date',
                                'end_date',
                                'persons',
                                array( 
                                            'name' => 'is_paid', 
                                            'value' => '($data->is_paid == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')', 
                                            'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), 
                                            ),

                            ),
                        )); ?>
                        </div>
                       
                        <div class="clearfix"></div>

                         <div class="hr"></div>
                         <h3><?php echo Yii::t("app", "Events bookings"); ?></h3>

                        <div class="dataTables_wrapper form-inline">
                        <?php $this->widget('CGridViewCustom', array(
                            'id' => 'events-grid',
                            'dataProvider' => $events,
                            'filter' => null,
                            'show_create_button' => false,
                            'itemsCssClass' => "shop_table shop_table_responsive cart",
                            'columns' => array(
                                'dateline',
                                array( 
                                        'name'=>'event_id', 
                                        'value'=>'GxHtml::valueEx($data->event)', 
                                        ),
                                'name',
                                'persons',
                                'total',
                                array( 
                                            'name' => 'is_paid', 
                                            'value' => '($data->is_paid == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')', 
                                            'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), 
                                            ),
                            ),
                        )); ?>
                        </div>
                       
                        <div class="clearfix"></div>                        
                    </div>
                </article>
                <!-- post close -->
            </div>
</div>

	</div>
</div>
