<?php

/**
 * GxActiveForm class file.
 *
 * @author Rodrigo Coelho <rodrigo@giix.org>
 * @link http://giix.org/
 * @copyright Copyright &copy; 2010-2011 Rodrigo Coelho
 * @license http://giix.org/license/ New BSD License
 */

/**
 * GxActiveForm provides forms with additional features.
 *
 * @author Rodrigo Coelho <rodrigo@giix.org>
 */
class GxActiveForm extends CActiveForm {

	/**
	 * Renders a checkbox list for a model attribute.
	 * This method is a wrapper of {@link GxHtml::activeCheckBoxList}.
	 * #MethodTracker
	 * This method is based on {@link CActiveForm::checkBoxList}, from version 1.1.7 (r3135). Changes:
	 * <ul>
	 * <li>Uses GxHtml.</li>
	 * </ul>
	 * @see CActiveForm::checkBoxList
	 * @param CModel $model The data model.
	 * @param string $attribute The attribute.
	 * @param array $data Value-label pairs used to generate the check box list.
	 * @param array $htmlOptions Addtional HTML options.
	 * @return string The generated check box list.
	 */
	public function checkBoxList($model, $attribute, $data, $htmlOptions = array()) {
		return GxHtml::activeCheckBoxList($model, $attribute, $data, $htmlOptions);
	}
	
	public function labelEx($model,$attribute,$htmlOptions=array()){
		if(isset($htmlOptions['class'])){
			$htmlOptions['class'] = $htmlOptions['class'] . ((stripos($htmlOptions['class'],'nodefault')!==false)?'':" col-sm-3 control-label");
		}else{
			$htmlOptions['class'] = ('col-sm-3 control-label');
		}
		
		return CHtml::activeLabelEx($model,$attribute,$htmlOptions);
	}
	
	public function textField($model,$attribute,$htmlOptions=array()){
		if(isset($htmlOptions['class'])){
			$htmlOptions['class'] = $htmlOptions['class'] . ((stripos($htmlOptions['class'],'nodefault')!==false)?'':" form-control");
		}else{
			$htmlOptions['class'] = ('form-control');
		}
		return CHtml::activeTextField($model,$attribute,$htmlOptions);
	}
	public function passwordField($model,$attribute,$htmlOptions=array()){
		if(isset($htmlOptions['class'])){
			$htmlOptions['class'] = $htmlOptions['class'] . ((stripos($htmlOptions['class'],'nodefault')!==false)?'':" form-control");
		}else{
			$htmlOptions['class'] = ('form-control');
		}
		return CHtml::activeTextField($model,$attribute,$htmlOptions);
	}
	public function dropDownList($model,$attribute,$data,$htmlOptions=array()){
		if(isset($htmlOptions['class'])){
			$htmlOptions['class'] = $htmlOptions['class'] . ((stripos($htmlOptions['class'],'nodefault')!==false)?'':" form-control selectboxit");
		}else{
			$htmlOptions['class'] = ('form-control selectboxit');
		}
		return CHtml::activeDropDownList($model,$attribute,$data,$htmlOptions);
	}
	public function dateField($model,$attribute,$htmlOptions=array()) {
		if(isset($htmlOptions['class'])){
			$htmlOptions['class'] = $htmlOptions['class'] . ((stripos($htmlOptions['class'],'nodefault')!==false)?'':" form-control datepicker");
		}else{
			$htmlOptions['class'] = ('form-control datepicker');
		}
		return CHtml::activeDateField($model,$attribute,$htmlOptions);
	}
	public function textArea($model,$attribute,$htmlOptions=array()) {
		if(isset($htmlOptions['class'])){
			$htmlOptions['class'] = $htmlOptions['class'] . ((stripos($htmlOptions['class'],'nodefault')!==false)?'':" form-control");
		}else{
			$htmlOptions['class'] = ('form-control autogrow');
		}
		return CHtml::activeTextArea($model,$attribute,$htmlOptions);
	}
	public function checkBox($model,$attribute,$htmlOptions=array()) {
		if(isset($htmlOptions['class'])){
			$htmlOptions['class'] = $htmlOptions['class'] . ((stripos($htmlOptions['class'],'nodefault')!==false)?'':" icheck");
		}else{
			$htmlOptions['class'] = ('icheck');
		}
		return CHtml::activeCheckBox($model,$attribute,$htmlOptions);
	}

	public function enumDropDownList($model, $attribute, $htmlOptions = array()){
		if(isset($htmlOptions['class'])){
			$htmlOptions['class'] = $htmlOptions['class'] . ((stripos($htmlOptions['class'],'nodefault')!==false)?'':" form-control selectboxit");
		}else{
			$htmlOptions['class'] = ('form-control selectboxit');
		}
		return GxHtml::enumDropDownList($model, $attribute, $htmlOptions);
	}
	
	public $errorMessageCssClass = "validate-has-error";

}