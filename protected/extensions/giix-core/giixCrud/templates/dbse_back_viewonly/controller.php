<?php
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/**
 * This is a Yii controller to manage <?php echo $this->modelClass; ?> in the backend.
 * It supports adding, editing, and deleting <?php echo $this->modelClass; ?>.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class <?php echo $this->controllerClass; ?> extends BackMainController {

<?php 
	$authpath = 'ext.giix-core.giixCrud.templates.default.auth.';
	Yii::app()->controller->renderPartial($authpath . $this->authtype);
?>

	/**
	 * Default action to list current records.
	 */
	public function actionIndex() {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new <?php echo $this->modelClass; ?>('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['<?php echo $this->modelClass; ?>']))
			$model->setAttributes($_GET['<?php echo $this->modelClass; ?>']);

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * View <?php echo $this->modelClass; ?> details.
	 *
	 * @param integer ID of the <?php echo $this->modelClass; ?> to be viewed.
	 */
	public function actionView($id) {
		
		// View place is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}
}