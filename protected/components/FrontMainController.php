<?php

class FrontMainController extends Controller
{
	public $menus = array();

	public function getMainController(){
		$mainController = "site";
		switch(strtolower($this->id)){
			case 'events':
			case 'offers':
			case 'coworking':
			case 'places':
				$mainController = 'coworking';
				$mainController = 'coworking';
			break;
			case 'schools':
				$mainController = 'schools';
			break;
			case 'universities':
				$mainController = 'universities';
			break;
			case 'saudiuniversities':
				$mainController = 'saudiUniversities';
			break;
		}
		Yii::app()->homeUrl = array('/' . $mainController);
		return $mainController;
	}
}
