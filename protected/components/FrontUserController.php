<?php

class FrontUserController extends FrontMainController
{
	protected $user;

	public function beforeAction($action){
		if (Yii::app()->user->isGuest){
			Yii::app()->user->loginRequired();
			return false;
		}

		$user = User::model()->findByPK(Yii::app()->user->id);
		if (!$user){
			Yii::app()->user->loginRequired();
			return false;
		}

		$this->user = $user;

		return parent::beforeAction($action);
	}
}
