<?php 

class Payment extends CComponent
{
	public $endpoint_url = "https://demo.globalgatewaye4.firstdata.com/payment";
	public $merchant_id = "";
	public $key = "";
	public $validation_key = "";
	public $return_action = "";

    public function init() {

    }

	public function makeTransaction($options){
		$transaction = new Transaction;
		$transaction->user_id = Yii::app()->user->id;
		$transaction->type = $options['type'];
		$transaction->amount = number_format($options['amount'], 2, ".", "");
		$transaction->currency = $options['currency'];
		$transaction->reference = $options['reference'];
		if ($options['form_reference'])
			$transaction->form_reference = $options['form_reference'];
		$transaction->status = ($transaction->amount)?0:1;
		$transaction->dateline = date("Y-m-d H:i:s");
		$transaction->save();

		return $transaction;
	}

	public function makePayment($options){
		$transaction = $this->makeTransaction($options);
		
		if (isset($options['payment_method']) && $options['payment_method'] == 2){
			$this->onSucceeded($transaction, array('x_response_reason_text'=>Yii::t('app', 'Transaction succeeded')),$options);
			Yii::app()->end();
		}

		if ($transaction->status == 1){
			$this->onSucceeded($transaction, array('x_response_reason_text'=>'Transaction succeeded'),$options);
			Yii::app()->end();
		}


		$paymentForm = array();
		$paymentForm['x_login'] = $this->merchant_id;
		$paymentForm['x_amount'] = $transaction->amount;
		$paymentForm['x_currency_code'] = $transaction->currency;
		$paymentForm['x_fp_sequence'] = uniqid(time());
		$paymentForm['x_invoice_num'] =	$transaction->id;
		$paymentForm['x_fp_timestamp'] = time();
		$paymentForm['x_receipt_link_url'] = Yii::app()->controller->createAbsoluteUrl($this->return_action);
		$paymentForm['x_show_form'] = 'PAYMENT_FORM';

		$hashString = $paymentForm['x_login'] . "^" . $paymentForm['x_fp_sequence'] . "^" . $paymentForm['x_fp_timestamp'] . "^" . $paymentForm['x_amount'] . "^" . $paymentForm['x_currency_code'];

		$paymentForm['x_fp_hash'] = hash_hmac('MD5', $hashString, $this->key);

		$this->doPay($paymentForm);
	}

	public function onFailure($transaction, array $return = array()){
		$transaction->status = 2;
		$transaction->info = CJSON::encode($return);
		$transaction->save();

		Yii::app()->user->setState("message_type", "danger");
		Yii::app()->user->setState("message_text", $return['x_response_reason_text']);
		Yii::app()->controller->redirect(array("/site/message"));
	}

	public function onSucceeded($transaction, array $return = array(), array $options = array()){
		$type = strtolower($transaction->type);
		$model = null;
		$formModel = null;
		
		switch($type){
			case 'event':
				$model = Event::model()->findByPK($transaction->reference);
				$formModel = EventRegistration::model()->findByPK($transaction->form_reference);
				break;
			case 'booking':
				$model = Place::model()->findByPK($transaction->reference);
				$formModel = Booking::model()->findByPK($transaction->form_reference);
				break;
			case 'offer':
				$model = Offer::model()->findByPK($transaction->reference);
				$formModel = Booking::model()->findByPK($transaction->form_reference);
				break;
			case 'premium':
				$model = Place::model()->findByPK($transaction->reference);
				$formModel = Premium::model()->findByPK($transaction->form_reference);
				break;
			default:
				throw new CHttpException(403);
				
		}

		$transaction->status = 1;
		$transaction->info = CJSON::encode($return);
		$transaction->save();

		$message = $return['x_response_reason_text'];

		switch($type){
			case 'event':
				$model->saveCounters(array('registered_persons'=>$formModel->persons));
				$formModel->transaction_id = $transaction->id;
				if (!isset($options['payment_method']) || $options['payment_method'] == 1){
					$formModel->is_paid = 1;
				}
				$formModel->status = 1;
				$formModel->save();
				break;
			case 'booking':
				$formModel->transaction_id = $transaction->id;
				if (!isset($options['payment_method']) || $options['payment_method'] == 1){
					$formModel->is_paid = 1;
				}
				$formModel->status = 1;
				$formModel->save();
				break;
			case 'offer':
				$formModel->transaction_id = $transaction->id;
				if (!isset($options['payment_method']) || $options['payment_method'] == 1){
					$formModel->is_paid = 1;
				}
				$formModel->status = 1;
				$formModel->save();
				break;
			case 'premium':

				if (!isset($options['payment_method']) || $options['payment_method'] == 1){
					$model->is_premium = 1;
					$model->save();

					User::model()->updateByPk($model->user_id, array("role_id"=>2));
					
					$formModel->is_active = 1;
					$formModel->expiration_date = date("Y-m-d H:i:s", strtotime("+1 month"));
					$formModel->save();
				}
				break;
		}

		Yii::app()->user->setState("message_type", "success");
		Yii::app()->user->setState("message_text", $message);
		Yii::app()->controller->redirect(array("/site/message"));
	}

	protected function doPay($paymentForm){
		?>
		<form id="paymentForm" action="<?php echo $this->endpoint_url;?>" method="post">
			<?php foreach($paymentForm as $name => $value): ?>
				<input type="hidden" name="<?php echo $name;?>" value="<?php echo $value;?>" />
			<?php endforeach; ?>
			<button id="paymentButton" type="submit">Continue</button>
		</form>
		<script type="text/javascript">
			document.getElementById('paymentButton').style.display = 'none';
			document.getElementById("paymentForm").submit();
		</script>
		<?php

		Yii::app()->end();
	}

	public function onResponse(array $data){
		$id = @$data['x_invoice_num'];
		$hash = @$data['x_MD5_Hash'];
		$tr_id = @$data['x_trans_id'];
		$amount = @$data['x_amount'];
		$code = @$data['x_response_code'];
		$caclulatedHash = md5($this->validation_key . $this->merchant_id . $tr_id . $amount);
		if ($caclulatedHash != $hash){
			throw new CHttpException(403);
		}
		
		$transaction = Transaction::model()->findByAttributes(array('id'=>$id));
		if ($transaction->status == 1){
			throw new CHttpException(403);
		}

		if ($code == 1){
			$this->onSucceeded($transaction, $data);
		}else{
			$this->onFailure($transaction, $data);
		}		
	}	
}