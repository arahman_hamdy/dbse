<?php

Yii::import("zii.widgets.jui.CJuiAutoComplete");

class CustomAutoComplete extends CJuiAutoComplete
{
    public $labelModelName = "";
    public $labelModelAttribute = "";

    /**
     * Run this widget.
     * This method registers necessary javascript and renders the needed HTML code.
     */
    public function run()
    {
        list($name,$id)=$this->resolveNameID();
 
        // Get ID Attribute of actual hidden field containing selected value
        $attr_id = get_class($this->model).'_'.$this->attribute;
     
        if(isset($this->htmlOptions['id']))
            $id=$this->htmlOptions['id'];
        else
            $this->htmlOptions['id']=$id;

        if(isset($this->htmlOptions['name']))
            $name=$this->htmlOptions['name'];

        $autocomplete_id = $id . '_autocomplete';

        if($this->hasModel()) {
            $this->value = $this->model->{$this->attribute};
            $label = $this->value;
            if ($this->labelModelName && $this->labelModelAttribute){
                $modelName = $this->labelModelName;
                $model = $modelName::model()->findByPK($this->value);
                if ($model){
                    $label = $model->{$this->labelModelAttribute};
                }
            }
            echo CHtml::textField($name,$label, array('id'=>$autocomplete_id) + $this->htmlOptions);
            echo CHtml::activeHiddenField($this->model, $this->attribute);
        }else {
            $label = $this->value;
            echo CHtml::textField($name,$this->value,$this->htmlOptions);       
            CHtml::hiddenField($name,$this->value,$this->htmlOptions);          
        }

        if($this->sourceUrl!==null)
            $this->options['source']=CHtml::normalizeUrl($this->sourceUrl); 
        else
            $this->options['source']=$this->source;
     
        // Modify Focus Event to show label in text field instead of value
        if (!isset($this->options['focus'])) {
          $this->options['focus'] = 'js:function(event, ui) {
              $("#'.$autocomplete_id.'").val(ui.item.label);
              return false;
            }';    
        }
     
        if (!isset($this->options['select'])) {
          $this->options['select'] = 'js:function(event, ui) {
                $("#'.$autocomplete_id.'").val(ui.item.label);
                $("#'.$attr_id.'").val(ui.item.value);
                $("#'.$attr_id.'").trigger("change");
                    return false;
            }';
        }
     
        $options=CJavaScript::encode($this->options);
        //$options = $this->options;
 
        $js = "jQuery('#{$autocomplete_id}').autocomplete($options);
        jQuery('#{$autocomplete_id}').on('blur', function(){
            $('#{$attr_id}').trigger('blur');
        });
        jQuery('#{$autocomplete_id}').on('change', function(){
            if ($(this).val()) return;
            $('#{$attr_id}').val('');
        });
        ";
 
        $cs = Yii::app()->getClientScript();
        $cs->registerScript(__CLASS__.'#'.$autocomplete_id, $js);
    }
}
