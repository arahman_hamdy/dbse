<?php

Yii::import('zii.widgets.grid.CGridView');

class CGridViewCustom extends CGridView
{
	public $show_create_button = true;
	public $additionalButtons = array();

	public function init()
	{
		$this->itemsCssClass = "table table-bordered datatable dataTable";
		$this->summaryCssClass .= "dataTables_info";
		$this->summaryText .= Yii::t('app', "Showing {start} to {end} of {count} entries");
		$this->pagerCssClass .= "dataTables_paginate paging_bootstrap";
		$this->pager = array(
			'htmlOptions'=>array(
				'class'=>'pagination pagination-sm',
			),
			'header'=>'',
			'nextPageLabel'=>'<i class="entypo-right-open"></i>',
			'prevPageLabel'=>'<i class="entypo-left-open"></i>',
			'firstPageLabel'=>'←',
			'lastPageLabel'=>'→',
			'hiddenPageCssClass'=>'disabled',
			'selectedPageCssClass'=>'active',
		);
				
		if ($this->show_create_button){
			$createButton = array(Yii::app()->controller->createAbsoluteUrl('create')=>Yii::t("app","Add New"));
			$this->additionalButtons = $createButton + $this->additionalButtons;
		}

		$this->template = '
			<div class="row">';
		
		if (!empty($this->additionalButtons)){
			$this->template .= '		<div class="col-xs-6 col-left">
					<div class="dataTables_length">
					</div>
					</div>';
		}else{
			/*
			 $this->template .= '		<div class="col-xs-6 col-left">
						<div class="dataTables_length">
							<label>
								'.CHtml::dropDownList('pageSize',10,array(5=>5,10=>10,25=>25,50=>50,100=>100),array('class'=>'form-control')).' records per page
							</label>
						</div>
					</div>';
			*/
		}

		if (!empty($this->additionalButtons)){
			$this->template .= '
					<div class="col-xs-6 col-right text-right">
						<div class="dataTables_filter">';
			foreach ($this->additionalButtons as $url => $text) {
				$this->template .= '<a href="'.$url.'" class="btn btn-success" title="' . $text. '">' . $text. '</a>';
			}
			$this->template .= '
						</div>
					</div>';
		}
		$this->template .= '
			</div>
			{items}
			<div class="row">
				<div class="col-xs-6 col-left">
					{summary}
				</div>
				<div class="col-xs-6 col-right">
					{pager}
				</div>
			</div>';
		
		parent::init();
		
	}

	/**
	 * Renders the filter.
	 * @since 1.1.1
	 */
	public function renderFilter()
	{
		if($this->filter!==null)
		{
			echo "<tr class=\"{$this->filterCssClass}\">\n";
			foreach($this->columns as $column)
				$this->renderFilterCell($column);
			echo "</tr>\n";
		}
	}

	/**
	 * Renders the filter cell.
	 * @since 1.1.1
	 */
	public function renderFilterCell($column)
	{
		echo CHtml::openTag('td',$column->filterHtmlOptions);
		$this->renderFilterCellContent($column);
		echo "</td>";
	}

	/**
	 * Renders the filter cell content.
	 * This method will render the {@link filter} as is if it is a string.
	 * If {@link filter} is an array, it is assumed to be a list of options, and a dropdown selector will be rendered.
	 * Otherwise if {@link filter} is not false, a text field is rendered.
	 * @since 1.1.1
	 */
	protected function renderFilterCellContent($column)
	{
		if (!property_exists($column,"filter")) return;

		if(is_string($column->filter))
			echo $column->filter;
		elseif($column->filter!==false && $column->grid->filter!==null && $column->name!==null && strpos($column->name,'.')===false)
		{
			$model = clone $column->grid->filter;
			$model->{$column->name} = null;
			if(is_array($column->filter))
				echo CHtml::activeDropDownList($model, $column->name, $column->filter, array('id'=>false,'prompt'=>''));
			elseif($column->filter===null)
				echo CHtml::activeTextField($model, $column->name, array('id'=>false));
		}
		else
			parent::renderFilterCellContent();
	}

}
