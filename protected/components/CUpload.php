<?php 

class CUpload extends CComponent{
	public $uploadsUrl = "";
	public $uploadsPath = "";
	public $imagesUrl = "";
	
    public function init() {
		$this->uploadsPath = Yii::getPathOfAlias("webroot.uploads") . '/';
		$this->uploadsUrl = Yii::app()->createAbsoluteUrl('/') . '/uploads/';
		$this->imagesUrl = Yii::app()->createAbsoluteUrl('/') . '/images/';
    }    
}
