<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends GxController
{
	public $country_id = 1;
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	public $positions = array(
			5=>'HEADER',
			10=>'TOP_RIGHT',
			20=>'TOP_LEFT',
			30=>'RIGHT_SIDE_1',
			40=>'LEFT_SIDE_1',
			50=>'FOOTER_1',
			60=>'FOOTER_2',
			70=>'ARTICLE',
	);
	
	public $uploadsUrl = "";
	public $uploadsPath = "";
	public $imagesUrl = "";
	
	public $themeURL = null;

	public function init(){
		parent::init();
		$this->uploadsPath = Yii::app()->upload->uploadsPath;
		$this->uploadsUrl = Yii::app()->upload->uploadsUrl;
		$this->imagesUrl = Yii::app()->upload->imagesUrl;
		$this->themeURL = Yii::app()->theme->baseUrl;

		/* */
		if (!empty($_GET['lang']) && in_array($_GET['lang'],array('ar','en'))){
			Yii::app()->user->setState('lang',$_GET['lang']);
		}

		
		Yii::app()->language = Yii::app()->user->getState('lang','ar');
		/* */
		//Yii::app()->language = "ar";

	}
	
	public function getQueryArgs($excludeArray = array()){
		
		$get_array = array_merge(array(),$_GET);
		foreach($excludeArray as $excludeKey){
			unset($get_array[$excludeKey]);
		}
		
		return $get_array;
		
	}
	
	public function rederView($block) {
		if($block->template_id == 1)
			$this->renderPartial('//panels/news_list', array("block"=>$block));
		else if($block->template_id == 2)
			$this->renderPartial('//panels/main_and_list', array("block"=>$block));
		else if($block->template_id == 3)
			$this->renderPartial('//panels/two_categories', array("block"=>$block));
		else if($block->template_id == 4)
			$this->renderPartial('//panels/popular', array("block"=>$block));
		else if($block->template_id == 5)
			$this->renderPartial('//panels/main_slider', array("block"=>$block));
		else if($block->template_id == 6)
			$this->renderPartial('//panels/slider_category', array("block"=>$block));
		else if($block->template_id == 7)
			$this->renderPartial('//panels/free', array("block"=>$block));
		else if($block->template_id == 8)
			$this->renderPartial('//panels/images_gallery', array("block"=>$block));
		else if($block->template_id == 9)
			$this->renderPartial('//panels/videos', array("block"=>$block));
		else if($block->template_id == 10)
			$this->renderPartial('//panels/thumbnails', array("block"=>$block));
		else if($block->template_id == 11)
			$this->renderPartial('//panels/html', array("block"=>$block));
	}

	public function youtube($url) {
	  # get video id from url
	  $urlQ = parse_url( $url, PHP_URL_QUERY );
	  parse_str( $urlQ, $query );
	
	  # YouTube api v2 url
	  $apiURL = 'http://gdata.youtube.com/feeds/api/videos/'. $query['v'] .'?v=2&alt=jsonc';
	
	  # curl options
	  $options = array(
	    CURLOPT_URL  => $apiURL,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_BINARYTRANSFER => true,
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_TIMEOUT => 5 );
	
	  # connect api server through cURL
	  $ch = curl_init();
	  curl_setopt_array($ch, $options);
	  # execute cURL
	  $json = curl_exec($ch) or die( curl_error($ch) );
	  # close cURL connect
	  curl_close($ch);
	
	  # decode json encoded data
	  if ($data = json_decode($json))
	    return (object) $data->data;
	}

	public function generateAds($position_id){
		$criteria = new CDbCriteria;
		$criteria->with = 'advertisementPositions';
		$criteria->compare("pos_id",$position_id);
		$criteria->compare("is_active",1);
		$criteria->addCondition("t.country_id is null or t.country_id = " . $this->country_id);

		$criteria->order='sortid';
		$ads = Advertisement::model()->cache(60)->findAll($criteria);
		foreach($ads as $i=>$ad){
			if ($i){
				echo "&nbsp;<br />";
			}

			if ($ad->is_html){
				echo $ad->html;
			}else{
				$width = '100%';
				$height = '100%';
				$sizeConstraint = 'style="max-width:' . $width . ';max-height:' . $height . ';"';
				echo '<div class="adv_slide">';
				if ($ad->url) echo '<a target="_blank" href="' . $ad->url . '">';
				echo '<img src="' . $ad->image_file . '">';
				if ($ad->url) echo '</a>';
				echo '</div>';
			}
		}
	}

	public function local_month_name($locale, $monthnum)
	{
	    /**
	     *  Return the localized name of the given month
	     *  
	     *  @author Lucas Malor
	     *  @param string $locale The locale identifier (for example 'en_US')
	     *  @param int $monthnum The month as number
	     *  @return string The localized string
	     */

	    $fmt = new IntlDateFormatter($locale, IntlDateFormatter::LONG, 
	                                 IntlDateFormatter::NONE);

	    $fmt->setPattern('MMMM');
	    return $fmt->format(mktime(0, 0, 0, $monthnum, 1, 1970));
	}

}