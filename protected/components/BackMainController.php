<?php

class BackMainController extends Controller {
	
	public function init() {
		Yii::app()->theme = 'back-end';
		parent::init();
		Yii::app()->language='en';
		
	}

	public function actionUpload()
	{
		$tempFolder=$this->uploadsPath;

		//  mkdir($tempFolder, 0777, TRUE);
		//  mkdir($tempFolder.'chunks', 0777, TRUE);

		Yii::import("ext.EFineUploader.qqFileUploader");

		$uploader = new qqFileUploader();
		$uploader->allowedExtensions = array('jpg','jpeg','png','gif');
		$uploader->sizeLimit = 2 * 1024 * 1024;//maximum file size in bytes
		$uploader->chunksFolder = $tempFolder;

		$result = $uploader->handleUpload($tempFolder);
		$result['id'] = 0;
		$result['filename'] = $uploader->getUploadName();
		$result['folder'] = $this->uploadsUrl;

		$uploadedFile=$tempFolder.$result['filename'];

		$url = "";
		if ($result['filename']){
			// Check the $_FILES array and save the file. Assign the correct path to a variable ($url).
			$url = $this->uploadsUrl . $result['filename'];
				
			$fileModel =  new LibFile;
			$fileModel->type = "i";
			$fileModel->lib_cat_id = 2;
			$fileModel->thumb_path = 'file_image.png';
				
			$fileModel->name = $uploader->getName();
			$fileModel->path = $result['filename'];
			$fileModel->dateline = date("Y-m-d H:i:s");
			$fileModel->user_id = Yii::app()->user->id;
			$fileModel->is_archived = 0;
			
			if ($fileModel->save()){
				$message = "";
			}else{
				$message = "An error occured while saving the file!";
				$url = "";
			}

			$result['id'] = $fileModel->id;
		}

		header("Content-Type: text/plain");
		$result=htmlspecialchars(json_encode($result), ENT_NOQUOTES);
		echo $result;
		Yii::app()->end();
		
	}

	/**
	 * List regions to populate the dropdown.
	 *
	 * @param integer ID of the city.
	 */
	public function actionGetCityRegions($id){

		// Display name depending on the current language
		$displayField = "name_" . Yii::app()->language;

		// Set query criteria
		$criteria = new CDbCriteria;
		$criteria->select = array("id",$displayField);
		$criteria->compare("city_id",$id);
		$criteria->order = $displayField;
		
		// Get regions matches the criteria
		$regions = Region::model()->findAll($criteria);
		
		if (!$regions) die("{}");
		echo CJSON::encode(CHtml::listData($regions,"id",$displayField));
	}
	
}
