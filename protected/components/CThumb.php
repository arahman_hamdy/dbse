<?php 

class CThumb extends CComponent{
	public $thumbsDir = "images/thumbs";
	public $thumbWidth = "250";
	public $thumbHeight = "250";
	public $thumbsUrl = "";
	
    public function init() {
        $this->thumbsUrl = Yii::app()->upload->imagesUrl . 'thumbs';
    }
    public function generateThumb($imPath,$width = null,$height = null){
    	if (!$width && !$height){
    		$height = $this->thumbHeight;
    		$width = $this->thumbWidth;
    	}
    	return $this->doGenerateThumb($this->thumbsDir,$this->thumbsUrl,$imPath,$height,$width);
    }
    
    public function doGenerateThumb($thumbsDir,$thumbsUrl,$imPath,$thumbHeight,$thumbWidth){
    	 
    	$thumbPath = $thumbsDir.'/'.basename($imPath);
    	$thumbUrl = $thumbsUrl.'/'.basename($imPath);
    	//$thumbPath = dirname($imPath).'/thumbs/'.basename($imPath);
    
    	// Create thumbnail if not exists
    	if (!file_exists($thumbPath))
    	{
    		@mkdir($thumbsDir,0755,true);
    		$imgObj = Yii::app()->simpleImage->load($imPath);
    		 
    		if (!isset($thumbHeight))
    		{
    			$imgObj->resizeToWidth($thumbWidth);
    		}
    		else
    		{
    			$imgObj->resize($thumbWidth, $thumbHeight);
    		}
    		 
    		$imgObj->save($thumbPath);
    	}
    
    	//$imNode->wrap('<a href="'.$imPath.'" rel="gallery"></a>');
    	return $thumbUrl;
    
    }
    
}
