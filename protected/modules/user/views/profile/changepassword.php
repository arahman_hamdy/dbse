<?php $this->pageTitle=Yii::app()->name . ' - '.Yii::t('UserModule', "Change password");
$this->breadcrumbs=array(
	Yii::t('UserModule', "Profile") => array('/user/profile'),
	Yii::t('UserModule', "Change password"),
);
$this->menu=array(
	((UserModule::isAdmin())
		?array('label'=>Yii::t('UserModule', 'Manage Users'), 'url'=>array('/user/admin'))
		:array()),
    array('label'=>Yii::t('UserModule', 'List User'), 'url'=>array('/user')),
    array('label'=>Yii::t('UserModule', 'Profile'), 'url'=>array('/user/profile')),
    array('label'=>Yii::t('UserModule', 'Edit'), 'url'=>array('edit')),
    array('label'=>Yii::t('UserModule', 'Logout'), 'url'=>array('/user/logout')),
);
?>

<h1><?php echo Yii::t('UserModule', "Change password"); ?></h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'changepassword-form',
	'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note"><?php echo Yii::t('UserModule', 'Fields with <span class="required">*</span> are required.'); ?></p>
	<?php echo $form->errorSummary($model); ?>
	
	<div class="row">
	<?php echo $form->labelEx($model,'oldPassword'); ?>
	<?php echo $form->passwordField($model,'oldPassword'); ?>
	<?php echo $form->error($model,'oldPassword'); ?>
	</div>
	
	<div class="row">
	<?php echo $form->labelEx($model,'password'); ?>
	<?php echo $form->passwordField($model,'password'); ?>
	<?php echo $form->error($model,'password'); ?>
	<p class="hint">
	<?php echo Yii::t('UserModule', "Minimal password length 4 symbols."); ?>
	</p>
	</div>
	
	<div class="row">
	<?php echo $form->labelEx($model,'verifyPassword'); ?>
	<?php echo $form->passwordField($model,'verifyPassword'); ?>
	<?php echo $form->error($model,'verifyPassword'); ?>
	</div>
	
	
	<div class="row submit">
	<?php echo CHtml::submitButton(Yii::t('UserModule', "Save")); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->