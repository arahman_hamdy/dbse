<?php
$this->breadcrumbs=array(
	Yii::t('UserModule', "Profile") => array('/user/profile'),
	Yii::t('UserModule', "Edit"),
);

 $themeUrl = Yii::app()->theme->baseUrl;
$baseUrl = Yii::app()->baseUrl; ?>

<div id="content">
<div class="container">
	<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
	<div class="alert alert-warning">
		<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
	</div>
	<?php endif; ?>
	<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'profile-form',
		'enableAjaxValidation'=>true,
		'htmlOptions' => array('enctype'=>'multipart/form-data', 'class'=>'form-horizontal form-groups-bordered'),
	)); ?>
	
	<?php if($profile->getErrors() || $model->getErrors()) { ?>
		<div class="alert alert-danger">
		<?php echo $form->errorSummary(array($model,$profile)); ?>
		</div>
	<?php } ?>
	<?php 
			$profileFields=Profile::getFields();
			if ($profileFields) {
				foreach($profileFields as $field) {
				?>
		<div class="form-group">
			<label class="col-sm-3 control-label"><?php echo $form->labelEx($profile,$field->varname); ?></label>
			<div class="col-sm-7">
			<?php 
			if ($widgetEdit = $field->widgetEdit($profile)) {
				echo $widgetEdit;
			} elseif ($field->range) {
				echo $form->dropDownList($profile,$field->varname,Profile::range($field->range),array('class'=>'form-control'));
			} elseif ($field->field_type=="TEXT") {
				echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50, 'class'=>'form-control'));
			} else {
				echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255), 'class'=>'form-control'));
			}
			echo $form->error($profile,$field->varname); ?>
			</div>
		</div>	
				<?php
				}
			}
	?>
	
		<div class="form-group">
			<label class="col-sm-3 control-label"><?php Yii::t('UserModule', "Username")?></label>
			<div class="col-sm-7">
				<?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
				<?php echo $form->error($model,'username'); ?>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label"><?php Yii::t('UserModule', "Email")?></label>
			<div class="col-sm-7">
				<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128, 'class'=>'form-control')); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label"><?php Yii::t('UserModule', "Avatar")?></label>
			<div class="col-sm-7">
			<?php if($model->user_image) { ?><img width="50" src="<?php echo $baseUrl.'/images/'.$model->user_image; ?>" /><?php } ?>

				<?php echo $form->FileField($model, 'user_image', array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'user_image'); ?>

			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-3">
			</div>

			<div class="col-sm-7">
				<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('UserModule', 'Create') : Yii::t('UserModule', 'Save'), array('class'=>'btn btn-success')); ?>
			</div>
		</div>
	
	<?php $this->endWidget(); ?>
	
	</div><!-- form -->
</div>
</div>