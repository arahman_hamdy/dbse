<?php $this->pageTitle=Yii::app()->name . ' - '.Yii::t('UserModule', "Registration");
$this->breadcrumbs=array(
	Yii::t('UserModule', "Registration"),
);
$baseUrl = Yii::app()->baseUrl;

?>
<!-- bage header End -->
<div id="content">
<div class="container">
	<div class="white-popup col-md-6 col-md-offset-3">
		<h3 class="text-center">تسجيل عضو جديد</h3>
		<?php if(Yii::app()->user->hasFlash('registration')): ?>
		<h5 class="title"><?php echo Yii::app()->user->getFlash('registration'); ?></h5>
		<?php else: ?>
			<?php $form=$this->beginWidget('UActiveForm', array(
				'id'=>'registration-form',
				'enableAjaxValidation'=>true,
				'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
				'htmlOptions' => array('enctype'=>'multipart/form-data', 'class'=>'checkout shop-checkout'),
			));
			 ?>
					<?php 
						$profileFields=Profile::getFields();
						if ($profileFields) {
							foreach($profileFields as $field) {
							?>
								<div class="form-row form-row-wide">
										<?php 
										if ($widgetEdit = $field->widgetEdit($profile)) {
											echo $widgetEdit;
										} elseif ($field->range) {
											echo $form->dropDownList($profile,$field->varname,Profile::range($field->range), array("class"=>"form-control","placeholder"=>""));
										} elseif ($field->field_type=="TEXT") {
											echo$form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50), array("class"=>"form-control","placeholder"=>Yii::t('UserModule', $field->title)));
										} else {
											echo $form->textField($profile,$field->varname,array("class"=>"form-control","placeholder"=>Yii::t('UserModule', $field->title)),array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
										}
										 ?>
										<?php echo $form->error($profile,$field->varname); ?>
								</div>
							<?php
							}
						}
					?>
				<?php /*
				<div class="row">
					<div class="col-sm-8">
						<div class="form-group">
							<input type="text" name="first_name" id="first_name" class="form-control" placeholder="الاسم الاول" tabindex="1">
						</div>
					</div>
					<div class="col-sm-8">
						<div class="form-group">
							<input type="text" name="last_name" id="last_name" class="form-control" placeholder="الاسم الاخير" tabindex="2">
						</div>
					</div>
				</div> */ ?>
				<div class="form-row form-row-wide">
					<label>اسم المستخدم: </label>
					<?php echo $form->textField($model,'username', array("class"=>"form-control","placeholder"=>Yii::t('UserModule', 'Username'))); ?>
					<?php echo $form->error($model,'username'); ?>
				</div>
				<div class="form-row form-row-wide">
					<label>البريد الالكتروني: </label>
					<?php echo $form->textField($model,'email', array("class"=>"form-control","placeholder"=>Yii::t('UserModule', 'Email'))); ?>
					<?php echo $form->error($model,'email'); ?>
				</div>
				<div class="form-row form-row-first">
					<label>كلمة المرور: </label>
					<?php echo $form->passwordField($model,'password', array("class"=>"form-control","placeholder"=>Yii::t('UserModule', 'Password'))); ?>
					<?php echo $form->error($model,'password'); ?>
				</div>
				<div class="form-row form-row-last">
					<label>اعادة كلمة المرور: </label>
					<?php echo $form->passwordField($model,'verifyPassword', array("class"=>"form-control","placeholder"=>Yii::t('UserModule', 'Re-enter password'))); ?>
					<?php echo $form->error($model,'verifyPassword'); ?>
				</div>

				<?php if (UserModule::doCaptcha('registration')): ?>
				<div class="form-row form-row-first">
						<?php echo $form->textField($model,'verifyCode', array("class"=>"form-control","placeholder"=>Yii::t('UserModule', 'Human verification'))); ?>
						<?php echo $form->error($model,'verifyCode'); ?>
						<?php /*
						<p class="hint"><?php echo Yii::t('UserModule', "Please enter the letters as they are shown in the image above."); ?>
						<br/><?php echo Yii::t('UserModule', "Letters are not case-sensitive."); ?></p> */ ?>
				</div>
				<div class="form-row form-row-last">
						<?php //echo $form->labelEx($model,'verifyCode'); ?>
						
						<?php $this->widget('CCaptcha'); ?>
				</div>
				<?php endif; ?>
				<div class="clearfix"></div>
				<hr>
				<div class="form-row form-row-wide">
					<?php echo CHtml::submitButton(Yii::t('UserModule', 'Register'), array("class"=>"btn btn-primary btn-big")); ?>
				</div>
			<?php $this->endWidget(); ?>
		<?php endif; ?>
	</div>
</div>
</div>
<?php /*
<h1><?php echo Yii::t('UserModule', "Registration"); ?></h1>

<?php if(Yii::app()->user->hasFlash('registration')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('registration'); ?>
</div>
<?php else: ?>

<div class="form">
<?php $form=$this->beginWidget('UActiveForm', array(
	'id'=>'registration-form',
	'enableAjaxValidation'=>true,
	'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note"><?php echo Yii::t('UserModule', 'Fields with <span class="required">*</span> are required.'); ?></p>
	
	<?php echo $form->errorSummary(array($model,$profile)); ?>
	
	<div class="row">
	<?php echo $form->labelEx($model,'username'); ?>
	<?php echo $form->textField($model,'username'); ?>
	<?php echo $form->error($model,'username'); ?>
	</div>
	
	<div class="row">
	<?php echo $form->labelEx($model,'password'); ?>
	<?php echo $form->passwordField($model,'password'); ?>
	<?php echo $form->error($model,'password'); ?>
	<p class="hint">
	<?php echo Yii::t('UserModule', "Minimal password length 4 symbols."); ?>
	</p>
	</div>
	
	<div class="row">
	<?php echo $form->labelEx($model,'verifyPassword'); ?>
	<?php echo $form->passwordField($model,'verifyPassword'); ?>
	<?php echo $form->error($model,'verifyPassword'); ?>
	</div>
	
	<div class="row">
	<?php echo $form->labelEx($model,'email'); ?>
	<?php echo $form->textField($model,'email'); ?>
	<?php echo $form->error($model,'email'); ?>
	</div>
	
<?php 
		$profileFields=Profile::getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>
	<div class="row">
		<?php echo $form->labelEx($profile,$field->varname); ?>
		<?php 
		if ($widgetEdit = $field->widgetEdit($profile)) {
			echo $widgetEdit;
		} elseif ($field->range) {
			echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
		} elseif ($field->field_type=="TEXT") {
			echo$form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
		} else {
			echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
		}
		 ?>
		<?php echo $form->error($profile,$field->varname); ?>
	</div>	
			<?php
			}
		}
?>
	<?php if (UserModule::doCaptcha('registration')): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form->textField($model,'verifyCode'); ?>
		<?php echo $form->error($model,'verifyCode'); ?>
		
		<p class="hint"><?php echo Yii::t('UserModule', "Please enter the letters as they are shown in the image above."); ?>
		<br/><?php echo Yii::t('UserModule', "Letters are not case-sensitive."); ?></p>
	</div>
	<?php endif; ?>
	
	<div class="row submit">
		<?php echo CHtml::submitButton(Yii::t('UserModule', "Register")); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
<?php endif; ?> */ ?>