<?php
$this->pageTitle=Yii::app()->name . ' - '.Yii::t('UserModule', "Login");
$this->breadcrumbs=array(
	Yii::t('UserModule', "Login"),
);
$baseUrl = Yii::app()->baseUrl;
?>
<!-- bage header End -->
<div id="content">
<div class="container">
	<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>
	
	<div class="success">
		<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
	</div>
	
	<?php endif; ?>
	<div class="white-popup col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
		<h3 class="text-center">تسجيل الدخول</h3>
		<?php echo CHtml::beginForm(); ?>
			<?php echo CHtml::errorSummary($model); ?>
			<div class="form-group">
				<label>اسم المستخدم: </label>
				<?php echo CHtml::activeTextField($model,'username', array("class"=>"form-control","placeholder"=>Yii::t('UserModule', "Username"))) ?>
			</div>
			<div class="form-group">
				<label>كلمة المرور: </label>
				<?php echo CHtml::activePasswordField($model,'password', array("class"=>"form-control","placeholder"=>Yii::t('UserModule', "Password"))) ?>
			</div>
			<?php /* ?>
			<div class="form-group">
				<?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
				<?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
			</div>
			<?PHP */ ?>
			
			<hr>
			<div class="row">
				<div class="col-sm-16">
					<?php echo CHtml::submitButton(Yii::t('UserModule', "Login"), array("class"=>"btn btn-big btn-primary")); ?>
				</div>
			</div>
		<?php echo CHtml::endForm(); ?>
	</div>
<?php /*
<p><?php echo Yii::t('UserModule', "Please fill out the following form with your login credentials:"); ?></p>
<div class="form">
<?php echo CHtml::beginForm(); ?>

	<p class="note"><?php echo Yii::t('UserModule', 'Fields with <span class="required">*</span> are required.'); ?></p>
	
	<?php echo CHtml::errorSummary($model); ?>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'username'); ?>
		<?php echo CHtml::activeTextField($model,'username') ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'password'); ?>
		<?php echo CHtml::activePasswordField($model,'password') ?>
	</div>
	
	<div class="row">
		<p class="hint">
		<?php echo CHtml::link(Yii::t('UserModule', "Register"),Yii::app()->getModule('user')->registrationUrl); ?> | <?php echo CHtml::link(Yii::t('UserModule', "Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
		</p>
	</div>
	
	<div class="row rememberMe">
		<?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
		<?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton(Yii::t('UserModule', "Login")); ?>
	</div>
	
<?php echo CHtml::endForm(); ?>
</div><!-- form --> */ ?>
</div>
</div>
<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>