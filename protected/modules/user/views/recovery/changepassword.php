<?php $this->pageTitle=Yii::app()->name . ' - '.Yii::t('UserModule', "Change password");
$this->breadcrumbs=array(
	Yii::t('UserModule', "Login") => array('/user/login'),
	Yii::t('UserModule', "Change password"),
);
?>

<h1><?php echo Yii::t('UserModule', "Change password"); ?></h1>


<div class="form">
<?php echo CHtml::beginForm(); ?>

	<p class="note"><?php echo Yii::t('UserModule', 'Fields with <span class="required">*</span> are required.'); ?></p>
	<?php echo CHtml::errorSummary($form); ?>
	
	<div class="row">
	<?php echo CHtml::activeLabelEx($form,'password'); ?>
	<?php echo CHtml::activePasswordField($form,'password'); ?>
	<p class="hint">
	<?php echo Yii::t('UserModule', "Minimal password length 4 symbols."); ?>
	</p>
	</div>
	
	<div class="row">
	<?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
	<?php echo CHtml::activePasswordField($form,'verifyPassword'); ?>
	</div>
	
	
	<div class="row submit">
	<?php echo CHtml::submitButton(Yii::t('UserModule', "Save")); ?>
	</div>

<?php echo CHtml::endForm(); ?>
</div><!-- form -->