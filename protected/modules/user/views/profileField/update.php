<?php
$this->breadcrumbs=array(
	Yii::t('UserModule', 'Profile Fields')=>array('admin'),
	$model->title=>array('view','id'=>$model->id),
	Yii::t('UserModule', 'Update'),
);
$this->menu=array(
    array('label'=>Yii::t('UserModule', 'Create Profile Field'), 'url'=>array('create')),
    array('label'=>Yii::t('UserModule', 'View Profile Field'), 'url'=>array('view','id'=>$model->id)),
    array('label'=>Yii::t('UserModule', 'Manage Profile Field'), 'url'=>array('admin')),
    array('label'=>Yii::t('UserModule', 'Manage Users'), 'url'=>array('/user/admin')),
);
?>

<h2><?php echo Yii::t('UserModule', 'Update Profile Field ').$model->id; ?></h2>
<br />

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-body">
				<?php
				$this->renderPartial('_form', array(
						'model' => $model));
				?>			</div>
		</div>
	</div>
</div>