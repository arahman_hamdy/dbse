<?php
$this->layout = "//layouts/column1";

$this->breadcrumbs=array(
	Yii::t('UserModule', 'Profile Fields')=>array('admin'),
	Yii::t('UserModule', 'Create'),
);
$this->menu=array(
    array('label'=>Yii::t('UserModule', 'Manage Profile Field'), 'url'=>array('admin')),
    array('label'=>Yii::t('UserModule', 'Manage Users'), 'url'=>array('/user/admin')),
);
?>
<h2><?php echo Yii::t('UserModule', 'Create Profile Field'); ?></h2>
<br />
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-body">
				<?php
				$this->renderPartial('_form', array(
						'model' => $model,
						'buttons' => 'create'));
				?>			</div>
		</div>
	</div>
</div>