<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'profileField-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>
	<?php if ($model->errors): ?>
		<div class="alert alert-danger">
			<?php echo $form->errorSummary($model); ?>
		</div>
	<?php endif; ?>

	<p class="note"><?php echo Yii::t('UserModule', 'Fields with <span class="required">*</span> are required.'); ?></p>
	
	<div class="form-group varname">
		<?php echo $form->LabelEx($model,'varname'); ?>
		<div class="col-sm-5">
		<?php echo (($model->id)?$form->TextField($model,'varname',array('size'=>60,'maxlength'=>50,'readonly'=>true)):$form->TextField($model,'varname',array('size'=>60,'maxlength'=>50))); ?>
		<?php echo CHtml::error($model,'varname'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', "Allowed lowercase letters and digits."); ?></p>
		</div>
	</div>

	<div class="form-group title">
		<?php echo $form->LabelEx($model,'title'); ?>
		<div class="col-sm-5">
		<?php echo $form->TextField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo CHtml::error($model,'title'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'Field name on the language of "sourceLanguage".'); ?></p>
		</div>
	</div>

	<div class="form-group field_type">
		<?php echo $form->LabelEx($model,'field_type'); ?>
		<div class="col-sm-5">
		<?php echo (($model->id)?$form->TextField($model,'field_type',array('size'=>60,'maxlength'=>50,'readonly'=>true,'id'=>'field_type')):$form->DropDownList($model,'field_type',ProfileField::itemAlias('field_type'),array('id'=>'field_type'))); ?>
		<?php echo CHtml::error($model,'field_type'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'Field type column in the database.'); ?></p>
		</div>
	</div>

	<div class="form-group field_size">
		<?php echo $form->LabelEx($model,'field_size'); ?>
		<div class="col-sm-5">
		<?php echo (($model->id)?$form->TextField($model,'field_size',array('readonly'=>true)):$form->TextField($model,'field_size')); ?>
		<?php echo CHtml::error($model,'field_size'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'Field size column in the database.'); ?></p>
		</div>
	</div>

	<div class="form-group field_size_min">
		<?php echo $form->LabelEx($model,'field_size_min'); ?>
		<div class="col-sm-5">
		<?php echo $form->TextField($model,'field_size_min'); ?>
		<?php echo CHtml::error($model,'field_size_min'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'The minimum value of the field (form validator).'); ?></p>
		</div>
	</div>

	<div class="form-group required">
		<?php echo $form->LabelEx($model,'required'); ?>
		<div class="col-sm-5">
		<?php echo $form->DropDownList($model,'required',ProfileField::itemAlias('required')); ?>
		<?php echo CHtml::error($model,'required'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'Required field (form validator).'); ?></p>
		</div>
	</div>

	<div class="form-group match">
		<?php echo $form->LabelEx($model,'match'); ?>
		<div class="col-sm-5">
		<?php echo $form->TextField($model,'match',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo CHtml::error($model,'match'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', "Regular expression (example: '/^[A-Za-z0-9\s,]+$/u')."); ?></p>
		</div>
	</div>

	<div class="form-group range">
		<?php echo $form->LabelEx($model,'range'); ?>
		<div class="col-sm-5">
		<?php echo $form->TextField($model,'range',array('size'=>60,'maxlength'=>5000)); ?>
		<?php echo CHtml::error($model,'range'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'Predefined values (example: 1;2;3;4;5 or 1==One;2==Two;3==Three;4==Four;5==Five).'); ?></p>
		</div>
	</div>

	<div class="form-group error_message">
		<?php echo $form->LabelEx($model,'error_message'); ?>
		<div class="col-sm-5">
		<?php echo $form->TextField($model,'error_message',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo CHtml::error($model,'error_message'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'Error message when you validate the form.'); ?></p>
		</div>
	</div>

	<div class="form-group other_validator">
		<?php echo $form->LabelEx($model,'other_validator'); ?>
		<div class="col-sm-5">
		<?php echo $form->TextField($model,'other_validator',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo CHtml::error($model,'other_validator'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'JSON string (example: {example}).',array('{example}'=>CJavaScript::jsonEncode(array('file'=>array('types'=>'jpg, gif, png'))))); ?></p>
		</div>
	</div>

	<div class="form-group default">
		<?php echo $form->LabelEx($model,'default'); ?>
		<div class="col-sm-5">
		<?php echo (($model->id)?$form->TextField($model,'default',array('size'=>60,'maxlength'=>255,'readonly'=>true)):$form->TextField($model,'default',array('size'=>60,'maxlength'=>255))); ?>
		<?php echo CHtml::error($model,'default'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'The value of the default field (database).'); ?></p>
		</div>
	</div>

	<div class="form-group widget">
		<?php echo $form->LabelEx($model,'widget'); ?>
		<div class="col-sm-5">
		<?php 
		list($widgetsList) = ProfileFieldController::getWidgets($model->field_type);
		echo $form->DropDownList($model,'widget',$widgetsList,array('id'=>'widgetlist'));
		//echo $form->TextField($model,'widget',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo CHtml::error($model,'widget'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'Widget name.'); ?></p>
		</div>
	</div>

	<div class="form-group widgetparams">
		<?php echo $form->LabelEx($model,'widgetparams'); ?>
		<div class="col-sm-5">
		<?php echo $form->TextField($model,'widgetparams',array('size'=>60,'maxlength'=>5000,'id'=>'widgetparams')); ?>
		<?php echo CHtml::error($model,'widgetparams'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'JSON string (example: {example}).',array('{example}'=>CJavaScript::jsonEncode(array('param1'=>array('val1','val2'),'param2'=>array('k1'=>'v1','k2'=>'v2'))))); ?></p>
		</div>
	</div>

	<div class="form-group position">
		<?php echo $form->LabelEx($model,'position'); ?>
		<div class="col-sm-5">
		<?php echo $form->TextField($model,'position'); ?>
		<?php echo CHtml::error($model,'position'); ?>
		<p class="hint"><?php echo Yii::t('UserModule', 'Display order of fields.'); ?></p>
		</div>
	</div>

	<div class="form-group visible">
		<?php echo $form->LabelEx($model,'visible'); ?>
		<div class="col-sm-5">
		<?php echo $form->DropDownList($model,'visible',ProfileField::itemAlias('visible')); ?>
		<?php echo CHtml::error($model,'visible'); ?>
		</div>
	</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->

<div id="dialog-form" title="<?php echo Yii::t('UserModule', 'Widget parametrs'); ?>">
	<form>
	<fieldset>
		<label for="name">Name</label>
		<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />
		<label for="value">Value</label>
		<input type="text" name="value" id="value" value="" class="text ui-widget-content ui-corner-all" />
	</fieldset>
	</form>
</div>
