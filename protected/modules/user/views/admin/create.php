<?php
$this->breadcrumbs=array(
	Yii::t('UserModule', 'Users')=>array('admin'),
	Yii::t('UserModule', 'Create'),
);

$this->menu=array(
    array('label'=>Yii::t('UserModule', 'Manage Users'), 'url'=>array('admin')),
    array('label'=>Yii::t('UserModule', 'Manage Profile Field'), 'url'=>array('profileField/admin')),
    array('label'=>Yii::t('UserModule', 'List User'), 'url'=>array('/user')),
);
?>
<h1><?php echo Yii::t('UserModule', "Create User"); ?></h1>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>