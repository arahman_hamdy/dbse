<?php
$this->breadcrumbs=array(
	(Yii::t('UserModule', 'Users'))=>array('admin'),
	$model->username=>array('view','id'=>$model->id),
	(Yii::t('UserModule', 'Update')),
);
$this->menu=array(
    array('label'=>Yii::t('UserModule', 'Create User'), 'url'=>array('create')),
    array('label'=>Yii::t('UserModule', 'View User'), 'url'=>array('view','id'=>$model->id)),
    array('label'=>Yii::t('UserModule', 'Manage Users'), 'url'=>array('admin')),
    array('label'=>Yii::t('UserModule', 'Manage Profile Field'), 'url'=>array('profileField/admin')),
    array('label'=>Yii::t('UserModule', 'List User'), 'url'=>array('/user')),
);
?>

<h1><?php echo  Yii::t('UserModule', 'Update User')." ".$model->id; ?></h1>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>