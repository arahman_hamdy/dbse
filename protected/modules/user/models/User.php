<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $activkey
 * @property integer $superuser
 * @property integer $status
 * @property string $create_at
 * @property string $lastvisit_at
 *
 * The followings are the available model relations:
 * @property Profile $profiles
 */
class User extends GxActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANNED=-1;
	
	//TODO: Delete for next version (backward compatibility)
	const STATUS_BANED=-1;
	
	/**
	 * The followings are the available columns in table 'users':
	 * @var integer $id
	 * @var string $username
	 * @var string $password
	 * @var string $email
	 * @var string $activkey
	 * @var integer $createtime
	 * @var integer $lastvisit
	 * @var integer $superuser
	 * @var integer $status
	 * @var timestamp $create_at
	 * @var timestamp $lastvisit_at
	 */

	public function primaryKey(){
		return 'id';
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function label($n = 1) {
		return Yii::t('app', 'User|Users', $n);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->getModule('user')->tableUsers;
	}

	public static function representingColumn() {
		return 'username';
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.CConsoleApplication
		return ((get_class(Yii::app())=='CConsoleApplication' || (get_class(Yii::app())!='CConsoleApplication' && Yii::app()->getModule('user')->isAdmin()))?array(
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => Yii::t('UserModule', "Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => Yii::t('UserModule', "Incorrect password (minimal length 4 symbols).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => Yii::t('UserModule', "This user's name already exists.")),
			array('email', 'unique', 'message' => Yii::t('UserModule', "This user's email address already exists.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => Yii::t('UserModule', "Incorrect symbols (A-z0-9).")),
			array('status', 'in', 'range'=>array(self::STATUS_NOACTIVE,self::STATUS_ACTIVE,self::STATUS_BANNED)),
			array('superuser', 'in', 'range'=>array(0,1)),
			array('create_at', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => true, 'on' => 'insert'),
			array('lastvisit_at', 'default', 'value' => '0000-00-00 00:00:00', 'setOnEmpty' => true, 'on' => 'insert'),
			array('username, email, superuser, status', 'required'),
			array('superuser, status,role_id', 'numerical', 'integerOnly'=>true),
			array('ip', 'length', 'max'=>50),
			array('ip', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, username, password, email, activkey, create_at, lastvisit_at, superuser, status, ip, role_id', 'safe', 'on'=>'search'),
			
		):(1 || (Yii::app()->user->id==$this->id)?array(
			array('username, email', 'required'),
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => Yii::t('UserModule', "Incorrect username (length between 3 and 20 characters).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => Yii::t('UserModule', "This user's name already exists.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => Yii::t('UserModule', "Incorrect symbols (A-z0-9).")),
			array('email', 'unique', 'message' => Yii::t('UserModule', "This user's email address already exists.")),
			array('user_image', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'),
			
			array('create_at', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => true, 'on' => 'insert'),
			array('lastvisit_at', 'default', 'value' => '0000-00-00 00:00:00', 'setOnEmpty' => true, 'on' => 'insert'),
			array('superuser, status,role_id', 'numerical', 'integerOnly'=>true),
			array('ip', 'length', 'max'=>50),
			array('ip', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, username, password, email, activkey, create_at, lastvisit_at, superuser, status, ip, role_id', 'safe', 'on'=>'search'),
						
		):array()));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
        $relations = Yii::app()->getModule('user')->relations;
        if (!isset($relations['profile']))
            $relations['profile'] = array(self::HAS_ONE, 'Profile', 'user_id');
        
        $relations += array(
        			'articles' => array(self::HAS_MANY, 'Article', 'author_id'),
        			'articleHistories' => array(self::HAS_MANY, 'ArticleHistory', 'user_id'),
        			'happeningNowArticles' => array(self::HAS_MANY, 'HappeningNowArticle', 'user_id'),
        			'hotArticles' => array(self::HAS_MANY, 'HotArticle', 'user_id'),
        			'libFiles' => array(self::HAS_MANY, 'LibFile', 'user_id'),
        			'role' => array(self::BELONGS_TO, 'Role', 'role_id'),
        	);
        return $relations;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('UserModule', "Id"),
			'username'=>Yii::t('UserModule', "username"),
			'password'=>Yii::t('UserModule', "password"),
			'verifyPassword'=>Yii::t('UserModule', "Retype Password"),
			'email'=>Yii::t('UserModule', "E-mail"),
			'verifyCode'=>Yii::t('UserModule', "Verification Code"),
			'activkey' => Yii::t('UserModule', "activation key"),
			'createtime' => Yii::t('UserModule', "Registration date"),
			'create_at' => Yii::t('UserModule', "Registration date"),
			
			'lastvisit_at' => Yii::t('UserModule', "Last visit"),
			'superuser' => Yii::t('UserModule', "Superuser"),
			'status' => Yii::t('UserModule', "Status"),
			'ip' => Yii::t('UserModule', 'Ip'),
			'role_id' => null,
				
			'main_image_lib_file_id' =>  Yii::t('UserModule', 'Main image'),
			'main_image_lib_cat_id' =>  Yii::t('UserModule', 'Album'),
			
			'articles' => null,
			'articleHistories' => null,
			'happeningNowArticles' => null,
			'hotArticles' => null,
			'libFiles' => Yii::t('UserModule', 'Main image'),
			'LibCat' => Yii::t('UserModule', 'Album'),
			'profiles' => null,
			'role' => null,				
		);
	}
	
	public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'status='.self::STATUS_NOACTIVE,
            ),
            'banned'=>array(
                'condition'=>'status='.self::STATUS_BANNED,
            ),
            'superuser'=>array(
                'condition'=>'superuser=1',
            ),
            'notsafe'=>array(
            	'select' => 'id, username, password, email, activkey, create_at, lastvisit_at, superuser, status,ip,role_id',
            ),
        );
    }
	
	public function defaultScope()
    {
        return CMap::mergeArray(Yii::app()->getModule('user')->defaultScope,array(
            'alias'=>'user',
            //'select' => 'user.id, user.username, user.email, user.create_at, user.lastvisit_at, user.superuser, user.status,user.ip,user.role_id',
        ));
    }
	
	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => Yii::t('UserModule', 'Not active'),
				self::STATUS_ACTIVE => Yii::t('UserModule', 'Active'),
				self::STATUS_BANNED => Yii::t('UserModule', 'Banned'),
			),
			'AdminStatus' => array(
				'0' => Yii::t('UserModule', 'No'),
				'1' => Yii::t('UserModule', 'Yes'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
	
/**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        
        $criteria->compare('id',$this->id);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('activkey',$this->activkey);
        $criteria->compare('create_at',$this->create_at);
        $criteria->compare('lastvisit_at',$this->lastvisit_at);
        $criteria->compare('superuser',$this->superuser);
        $criteria->compare('status',$this->status);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('role_id', $this->role_id);
        
        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
        	'pagination'=>array(
				'pageSize'=>Yii::app()->getModule('user')->user_page_size,
			),
        ));
    }

    public function getCreatetime() {
        return strtotime($this->create_at);
    }

    public function setCreatetime($value) {
        $this->create_at=date('Y-m-d H:i:s',$value);
    }

    public function getLastvisit() {
        return strtotime($this->lastvisit_at);
    }

    public function setLastvisit($value) {
        $this->lastvisit_at=date('Y-m-d H:i:s',$value);
    }

    public function afterSave() {
        if (get_class(Yii::app())=='CWebApplication'&&Profile::$regMode==false) {
            Yii::app()->user->updateSession();
        }
        return parent::afterSave();
    }
}
