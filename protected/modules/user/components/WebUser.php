<?php

class WebUser extends CWebUser
{

    /**
     * @var boolean whether to enable cookie-based login. Defaults to false.
     */
    public $allowAutoLogin=true;
    /**
     * @var string|array the URL for login. If using array, the first element should be
     * the route to the login action, and the rest name-value pairs are GET parameters
     * to construct the login URL (e.g. array('/site/login')). If this property is null,
     * a 403 HTTP exception will be raised instead.
     * @see CController::createUrl
     */
    public $loginUrl=array('/user/login');

    public function getRole()
    {
        return $this->getState('__role');
    }

    public function getRoleId()
    {
    	return $this->getState('__role_id');
    }
    
    public function getId()
    {
        return $this->getState('__id') ? $this->getState('__id') : 0;
    }
    
    public function can($permission_const){
    	if ($this->isGuest) return 0;
    	$role_id = $this->getRoleId();
    	if ($role_id==1) return 1;
    	
    	$rolePermission = RolePermission::model()->findByAttributes(array("role_id"=>$role_id,"permission_id"=>$permission_const));
    	return (int)!!$rolePermission;
    }

//    protected function beforeLogin($id, $states, $fromCookie)
//    {
//        parent::beforeLogin($id, $states, $fromCookie);
//
//        $model = new UserLoginStats();
//        $model->attributes = array(
//            'user_id' => $id,
//            'ip' => ip2long(Yii::app()->request->getUserHostAddress())
//        );
//        $model->save();
//
//        return true;
//    }

    protected function afterLogin($fromCookie)
	{
        parent::afterLogin($fromCookie);
        $this->updateSession();
	}

    public function updateSession() {
        $user = Yii::app()->getModule('user')->user($this->id);
        if (!$user) return;
        $this->name = $user->username;
        $userAttributes = CMap::mergeArray(array(
                                                'email'=>$user->email,
                                                'username'=>$user->username,
                                                'create_at'=>$user->create_at,
                                                'lastvisit_at'=>$user->lastvisit_at,
        										'__role'=>$user->role->name,
        										'__role_id'=>$user->role_id,
        										'__id'=>$user->id,
                                           ),$user->profile->getAttributes());
        foreach ($userAttributes as $attrName=>$attrValue) {
            $this->setState($attrName,$attrValue);
        }
    }

    /**
     * Returns user model by user id.
     * @param integer $id user id. Default - current user id.
     * @return User
     */
    public function model($id=0) {
        return Yii::app()->getModule('user')->user($id);
    }

    /**
     * Returns user model by user id.
     * @param integer $id user id. Default - current user id.
     * @return User
     */
    public function user($id=0) {
        return $this->model($id);
    }

    /**
     * Returns user model by user name.
     * @param string username
     * @return User
     */
    public function getUserByName($username) {
        return Yii::app()->getModule('user')->getUserByName($username);
    }

    public function getAdmins() {
        return Yii::app()->getModule('user')->getAdmins();
    }


    /**
     * @return boolean
     */
    public function isAdmin() {
        return Yii::app()->getModule('user')->isAdmin();
    }

    /**
     * @return boolean
     */
    public function isManager() {
        return Yii::app()->getModule('user')->isManager();
    }

}

abstract class UserPermissions
{
    const Is_staff = 1;
    const Translation_All = 2;
    const Place_Create = 3;
    const Place_Publish = 4;
    const Place_Edit = 5;
    const Place_Delete = 6;
    const Entity_Create_Egypt = 7;
    const Entity_Publish_Egypt = 8;
    const Entity_Edit_Egypt = 9;
    const Entity_Delete_Egypt = 10;
    const School_Create = 11;
    const School_Publish = 12;
    const School_Edit = 13;
    const School_Delete = 14;
    const Entity_Create_Saudi = 15;
    const Entity_Publish_Saudi = 16;
    const Entity_Edit_Saudi = 17;
    const Entity_Delete_Saudi = 18;
    const BlogArticle_Create = 19;
    const BlogArticle_Publish = 20;
    const BlogArticle_Edit = 21;
    const BlogArticle_Delete = 22;
    const Event_Create = 23;
    const Event_Publish = 24;
    const Event_Edit = 25;
    const Event_Delete = 26;
    const Offer_Create = 27;
    const Offer_Publish = 28;
    const Offer_Edit = 29;
    const Offer_Delete = 30;
    const Advertisement_Create = 31;
    const Advertisement_Publish = 32;
    const Advertisement_Edit = 33;
    const Advertisement_Delete = 34;
    const Booking_View = 35;
    const Booking_Delete = 36;
    const Transaction_View = 37;
    const User_Create = 38;
    const User_Edit = 39;
    const User_Delete = 40;
}
