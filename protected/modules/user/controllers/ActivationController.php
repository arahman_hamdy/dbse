<?php

class ActivationController extends FrontMainController
{
	public $defaultAction = 'activation';

	
	/**
	 * Activation user account
	 */
	public function actionActivation () {
		if (!isset($_GET['email'])) $_GET['email'] = "";
		if (!isset($_GET['activkey'])) $_GET['activkey'] = "";
		
		$email = $_GET['email'];
		$activkey = $_GET['activkey'];
		if ($email&&$activkey) {
			$find = User::model()->notsafe()->findByAttributes(array('email'=>$email));
			if (isset($find)&&$find->status) {
			    $this->render('/user/message',array('title'=>Yii::t('UserModule', "User activation"),'content'=>Yii::t('UserModule', "You account is active.")));
			} elseif(isset($find->activkey) && ($find->activkey==$activkey)) {
				$find->activkey = UserModule::encrypting(microtime());
				$find->status = 1;
				$find->save();
			    $this->render('/user/message',array('title'=>Yii::t('UserModule', "User activation"),'content'=>Yii::t('UserModule', "You account is activated.")));
			} else {
			    $this->render('/user/message',array('title'=>Yii::t('UserModule', "User activation"),'content'=>Yii::t('UserModule', "Incorrect activation URL.")));
			}
		} else {
			$this->render('/user/message',array('title'=>Yii::t('UserModule', "User activation"),'content'=>Yii::t('UserModule', "Incorrect activation URL.")));
		}
	}

}