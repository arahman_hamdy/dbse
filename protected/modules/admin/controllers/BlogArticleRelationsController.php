<?php
/**
 * This is a Yii controller to manage BlogArticleRelations in the backend.
 * It supports adding, editing, and deleting BlogArticleRelations.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class BlogArticleRelationsController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex($articleId) {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new BlogArticleRelations('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['BlogArticleRelations']))
			$model->setAttributes($_GET['BlogArticleRelations']);

		$model->article_id = $articleId;

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Add new record
	 */
	public function actionCreate($articleId) {
		
		// Instantiate a Place model to add new record.
		$model = new BlogArticleRelations;

		$model->article_id = $articleId;

		// On post, fill model with requested data
		if (isset($_POST['BlogArticleRelations'])) {
			$model->setAttributes($_POST['BlogArticleRelations']);

			if ($model->save()) {
				// Save succeeded
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					// If the request is ajax, just return the http status 200.
					Yii::app()->end();
				else
					// If the request is not ajax, send redirect flag to add new record.
					$this->redirect(array('create','articleId'=>$articleId));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render create view.
		$this->render('create', array( 'model' => $model));
	}

	/**
	 * Edit record
	 *
	 * @param integer ID of the BlogArticleRelations to be edited.
	 */
	public function actionUpdate($id) {
		
		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'BlogArticleRelations');


		// On post, fill model with requested data
		if (isset($_POST['BlogArticleRelations'])) {
			$model->setAttributes($_POST['BlogArticleRelations']);

			if ($model->save()) {
				// Save succeeded, redirect to edit the same record
				$this->redirect(array('update', 'id' => $model->id));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render update view.
		$this->render('update', array(
				'model' => $model,
				));
	}

	/**
	 * View BlogArticleRelations details.
	 *
	 * @param integer ID of the BlogArticleRelations to be viewed.
	 */
	public function actionView($id) {
		
		// View place is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}

	/**
	 * Delete BlogArticleRelations.
	 *
	 * @param integer ID of the BlogArticleRelations to be deleted.
	 */
	public function actionDelete($id) {
		
		// Make sure that method is POST
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			
			// Delete the requested record
			$this->loadModel($id, 'BlogArticleRelations')->delete();

		} else
			// Raise an error if the method is not POST
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionFind($term, $type)
	{
		if (strlen($term)<3) {echo '[]';die;}
		if (!in_array($type, array('Entity','Place'))) {echo '[]';die;}

		$criteria = new CDbCriteria;
		$criteria->select = "id, name_en";
		$criteria->compare("is_deleted",0);

		$criteria2 = new CDbCriteria;
		$criteria2->compare('name_ar', $term, true);
		$criteria2->compare('name_en', $term, true, 'OR');

		$criteria->mergeWith($criteria2);

		if ($type == "Entity"){
			$results = Entity::model()->findAll($criteria);
		}else if ($type == "Place"){
			$results = Place::model()->findAll($criteria);
		}

		$outputAll = array();
		foreach($results as $result){
			$output['value'] = $result['id'];
			$output['label'] = $result['name_en'];

			$outputAll[] = $output;
		}
		echo CJSON::encode($outputAll);


	}	

}