<?php

class SettingsController extends BackMainController {

	public function beforeAction($action){
		if (Yii::app()->user->RoleId != 1) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		return parent::beforeAction($action);
	}
	
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Settings'),
		));
	}

	public function actionCreate() {
		$model = new Settings;


		if (isset($_POST['Settings'])) {
			$model->setAttributes($_POST['Settings']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Settings');


		if (isset($_POST['Settings'])) {
			$model->setAttributes($_POST['Settings']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Settings')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$model = new Settings('search');
		$model->unsetAttributes();

		if (isset($_GET['Settings']))
			$model->setAttributes($_GET['Settings']);

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		$dataProvider = new CActiveDataProvider('Settings');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

}