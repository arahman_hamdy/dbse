<?php

class LibCatController extends BackMainController {
	public $is_modal = false;
	
	public function beforeAction($action){
		if (
			!Yii::app()->user->can(UserPermissions::BlogArticle_Create) &&
			!Yii::app()->user->can(UserPermissions::BlogArticle_Edit) &&
			!Yii::app()->user->can(UserPermissions::Place_Create) &&
			!Yii::app()->user->can(UserPermissions::Place_Edit) &&
			!Yii::app()->user->can(UserPermissions::Entity_Create_Egypt) &&
			!Yii::app()->user->can(UserPermissions::Entity_Edit_Egypt) &&
			!Yii::app()->user->can(UserPermissions::School_Create) &&
			!Yii::app()->user->can(UserPermissions::School_Edit) &&
			!Yii::app()->user->can(UserPermissions::Entity_Create_Saudi) &&
			!Yii::app()->user->can(UserPermissions::Entity_Edit_Saudi)
		) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		
		return parent::beforeAction($action);
	}
	
	public function actionView($id) {
		$this->redirect(array_merge(array('update'), $_GET),true);
		
		$this->render('view', array(
			'model' => $this->loadModel($id, 'LibCat'),
		));
	}

	public function actionCreate() {
		//if (!Yii::app()->user->can(UserPermissions::LibCat_Create)) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		
		$model = new LibCat;


		if (isset($_POST['LibCat'])) {
			$model->setAttributes($_POST['LibCat']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		//if ($id<=5 || !Yii::app()->user->can(UserPermissions::LibCat_Update)) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		
		$model = $this->loadModel($id, 'LibCat');
					

		if (isset($_POST['LibCat'])) {
			$model->setAttributes($_POST['LibCat']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		//if ($id<=5 || !Yii::app()->user->can(UserPermissions::LibCat_Delete)) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$model = $this->loadModel($id, 'LibCat');
			
			if (!$model || $model->id<10)
				throw new CHttpException(400, Yii::t('app', 'Unable to delete system album.'));
			
			$model->is_deleted = 1;
			$model->save(false);
			
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionDeleteFile($id) {
		//if (!Yii::app()->user->can(UserPermissions::LibFile_DeleteVideo)) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
	
		$model = $this->loadModel($id, 'LibFile');
		$cat = $model->lib_cat_id;
		
		try{
			if ($model->delete()){
				@unlink($this->uploadsPath . $model->path);
			}
							
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('browse','cat'=>$cat));
		}catch(Exception $ex){
			throw new CHttpException(400, $ex->getMessage());
		}

	}
	
	public function actionIndex() {
		/*if (
			!Yii::app()->user->can(UserPermissions::LibCat_Create) &&
			!Yii::app()->user->can(UserPermissions::LibCat_Delete) &&
			!Yii::app()->user->can(UserPermissions::LibCat_Update)
		) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		*/
		$model = new LibCat('search');
		$model->unsetAttributes();

		if (isset($_GET['LibCat']))
			$model->setAttributes($_GET['LibCat']);

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		$dataProvider = new CActiveDataProvider('LibCat');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}
	
	public function actionBrowse(){
		/*if (
			!Yii::app()->user->can(UserPermissions::LibFile_AddVideoTitle) &&
			!Yii::app()->user->can(UserPermissions::LibFile_EditVideoTitle) &&
			!Yii::app()->user->can(UserPermissions::LibFile_CreateVideo) &&
			!Yii::app()->user->can(UserPermissions::LibFile_DeleteVideo) &&
			!Yii::app()->user->can(UserPermissions::Article_DeleteImage) &&
			!Yii::app()->user->can(UserPermissions::Article_UploadImage)
		) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		*/
		
		$this->_browse(false);
	}
	
		
	public function actionEditorBrowse(){
		/*if (
			!Yii::app()->user->can(UserPermissions::Article_DeleteImage) &&
			!Yii::app()->user->can(UserPermissions::Article_UploadImage)
		) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		*/
		$this->layout = "//layouts/content_only";
		
		$this->_browse(true);
	}
	
	protected function _browse($is_modal = false,$messages = array()){
		$messages = array();
		
		if (isset($_FILES['files']['tmp_name']) && count($_FILES['files']['tmp_name'])){
			if (!isset($_GET['cat'])) $_GET['cat'] = 1;
			$_GET['cat'] = (int) $_GET['cat'];
				
			$videos = array('3gp','mp4','ogg','flv');
			$images = array('jpeg','jpg','png','gif');
			$music = array('mp3','wav');
			$files = array('zip','rar');
				
			$allowedExtensions = array();
			
			/*	
			if (Yii::app()->user->can(UserPermissions::LibFile_CreateVideo) || Yii::app()->user->can(UserPermissions::Article_UploadImage))
			*/
				$allowedExtensions = array_merge($allowedExtensions,$images);
				
			/*
			if (Yii::app()->user->can(UserPermissions::LibFile_CreateVideo))
				$allowedExtensions = array_merge($allowedExtensions,$videos);
			*/	
			if (Yii::app()->user->RoleId == 1)
				$allowedExtensions = array_merge($allowedExtensions,$music,$files);
				
			$upload_errors = array(
					0=>"There is no error, the file uploaded with success",
					1=>"The uploaded file exceeds the upload_max_filesize directive in php.ini",
					2=>"The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
					3=>"The uploaded file was only partially uploaded",
					4=>"", // No file was uploaded
					6=>"Missing a temporary folder"
			);
				
			for($i=0; $i<count($_FILES['files']['tmp_name']);$i++){
				$tmpFile = @$_FILES['files']['tmp_name'][$i];
				$orgFileName = @$_FILES['files']['name'][$i];
				$extension = strtolower(pathinfo($orgFileName, PATHINFO_EXTENSION));
		
				$message = "";
		
				$url = "";
				if ($_FILES['files']['error'][$i]){
					$message = $upload_errors[$_FILES['files']['error'][$i]];
				}else if (!$extension || !in_array($extension,$allowedExtensions)){
					$message = "Unknown file extensions. Allowed extensions are " . implode(", ",$allowedExtensions) . ".";
				}else{
						
					$newFileName = uniqid() . time() . "." . $extension;
					$filePath = $newFileName;
					move_uploaded_file($tmpFile, $this->uploadsPath . $newFileName);
					// Check the $_FILES array and save the file. Assign the correct path to a variable ($url).
					$url = $this->uploadsUrl . $newFileName;
					// Usually you will only assign something here if the file could not be uploaded.
		
					$fileModel =  new LibFile;
					if (in_array($extension,$images)){
						$fileModel->type = "i";
						$fileModel->thumb_path = 'file_image.png';
					}else if (in_array($extension,$videos)){
						$fileModel->type = "v";
						$fileModel->thumb_path = 'file_video.png';
					}else if (in_array($extension,$music)){
						$fileModel->type = "m";
						$fileModel->lib_cat_id = 5;
						$fileModel->thumb_path = 'file_music.png';
					}else{
						$fileModel->type = "f";
						$fileModel->thumb_path = 'file_other.png';
					}
						
					$fileModel->lib_cat_id = $_GET['cat'];
		
					$fileModel->name = $orgFileName;
					$fileModel->path = $filePath;
					$fileModel->dateline = date("Y-m-d H:i:s");
					$fileModel->user_id = Yii::app()->user->id;
					$fileModel->is_archived = 1;
						
					if (isset($_POST['tags'][$i]))
						$fileModel->tags = $_POST['tags'][$i];
		
					if ($fileModel->save()){
						$message = "";
					}else{
						$message = "An error occured while saving the file!";
						$url = "";
					}
				}
		
				if ($message)
					$messages[] = $message;
			}
				
			if (!isset($_POST['youtube'])) $_POST['youtube'] = array();
				
			for($i=0; $i<count($_POST['youtube']);$i++){
				$tmpFile = $_POST['youtube'][$i];
				if (!$tmpFile) continue;
		
				$matchesCount = preg_match("#watch.*\?.*v=([^\&]+)#", $tmpFile,$matches);
					
				$message = "";
					
				$url = "";
				if (!$matchesCount){
					$message = "Invalid youtube link!";
				}else{
		
					$newFileName = $matches[1];
					// Check the $_FILES array and save the file. Assign the correct path to a variable ($url).
					$url = $tmpFile;
					// Usually you will only assign something here if the file could not be uploaded.
						
					$fileModel =  new LibFile;
					$fileModel->type = "y";
					$fileModel->thumb_path = 'file_youtube.png';
					$fileModel->lib_cat_id = $_GET['cat'];
						
					$fileModel->name = "YT_" . $newFileName;
					$fileModel->path = $newFileName;
					$fileModel->dateline = date("Y-m-d H:i:s");
					$fileModel->user_id = Yii::app()->user->id;
					$fileModel->is_archived = 1;
		
					if (isset($_POST['tags'][$i]))
						$fileModel->tags = $_POST['tags'][$i];
						
					if ($fileModel->save()){
						$message = "";
					}else{
						$message = "An error occured while saving the file!";
						$url = "";
					}
				}
					
				if ($message)
					$messages[] = $message;
			}
		}
				
		$model = new LibCat('search');
		$model->unsetAttributes();
		
		$currentCat = null;
		$currentTitle = "";
		
		if (isset($_GET['cat'])){
			$model->parent_cat_id = $_GET['cat'];
			$currentCat = LibCat::model()->findByPK($_GET['cat']);
			if ($currentCat)
				$currentTitle = $currentCat->title;
		}else{
			$model->parent_cat_id = "NULL";
		}
		
		
		$this->breadcrumbs = array(
				Yii::t('app', 'Library')=>($is_modal)?$this->createUrl('editorBrowse',$this->getQueryArgs(array("cat"))):$this->createUrl('browse',$this->getQueryArgs(array("cat"))),
		);
		
		$catPath = array();
		if ($currentCat){
		
			do{
				$catPath = array($currentCat->title=>($is_modal)?$this->createUrl('editorBrowse',array('cat'=>$currentCat->id)+$this->getQueryArgs(array("cat"))):$this->createUrl('browse',array('cat'=>$currentCat->id)+$this->getQueryArgs(array("cat")))) + $catPath;
				$currentCat = $currentCat->parentCat;
			}while($currentCat);
		}
		
		$this->breadcrumbs = array_merge($this->breadcrumbs,$catPath);
		
		$fileModel = null;
		
		if (isset($_GET['cat'])){
		
			$libFile = new LibFile;
			$criteria = new CDbCriteria;
			
				$criteria->compare('lib_cat_id', $_GET['cat']);
			
			if (isset($_GET['types'])){
				$criteria->addInCondition('type', $_GET['types']);
			}
			$criteria->order = "dateline desc, id desc";
					
			$fileModel = new CActiveDataProvider($libFile, array(
					'criteria' => $criteria,
					'pagination'=>array(
							'pageSize'=>120,
					),
			));
		}
		
						
		$this->is_modal = $is_modal;
		
		$this->render('editorBrowse', array(
				'model' => $model,
				'currentTitle'=>$currentTitle,
				'fileModel' => $fileModel,
				'is_modal' => $is_modal,
				'messages' => $messages,
		));
		
	}
	
	public function actionEditorUpload(){
		/*if (
			!Yii::app()->user->can(UserPermissions::Article_UploadImage)
		) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		*/
		// Required: anonymous function reference number as explained above.
		$funcNum = @$_GET['CKEditorFuncNum'] ;
		// Optional: instance name (might be used to load a specific configuration file or anything else).
		$CKEditor = @$_GET['CKEditor'] ;
		// Optional: might be used to provide localized messages.
		$langCode = @$_GET['langCode'] ;
	
		$videos = array('3gp','mp4','ogg','flv');
		$images = array('jpeg','jpg','png','gif');
		$music = array('mp3','wav');
		$files = array('zip','rar');
	
		$allowedExtensions = array();
		
		//if (Yii::app()->user->can(UserPermissions::LibFile_CreateVideo) || Yii::app()->user->can(UserPermissions::Article_UploadImage))
			$allowedExtensions = array_merge($allowedExtensions,$images);
		
		//if (Yii::app()->user->can(UserPermissions::LibFile_CreateVideo))
		//	$allowedExtensions = array_merge($allowedExtensions,$videos);
		
		if (Yii::app()->user->RoleId == 1)
			$allowedExtensions = array_merge($allowedExtensions,$music,$files);
		
		$tmpFile = @$_FILES['upload']['tmp_name'];
		$orgFileName = @$_FILES['upload']['name'];
		$extension = strtolower(pathinfo($orgFileName, PATHINFO_EXTENSION));
	
		$upload_errors = array(
				0=>"There is no error, the file uploaded with success",
				1=>"The uploaded file exceeds the upload_max_filesize directive in php.ini",
				2=>"The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
				3=>"The uploaded file was only partially uploaded",
				4=>"No file was uploaded",
				6=>"Missing a temporary folder"
		);
	
		$url = "";
		if ($_FILES['upload']['error']){
			$message = $upload_errors[$_FILES['upload']['error']];
		}else if (!$extension || !in_array($extension,$allowedExtensions)){
			$message = "Unknown file extensions. Allowed extensions are " . implode(", ",$allowedExtensions) . ".";
		}else{
	
			$newFileName = uniqid() . time() . "." . $extension;
			$filePath = $newFileName;
			move_uploaded_file($tmpFile, $this->uploadsPath . $newFileName);
			// Check the $_FILES array and save the file. Assign the correct path to a variable ($url).
			$url = $this->uploadsUrl . $newFileName;
			// Usually you will only assign something here if the file could not be uploaded.
				
			$fileModel =  new LibFile;
			if (in_array($extension,$images)){
				$fileModel->type = "i";
				$fileModel->lib_cat_id = 2;
				$fileModel->thumb_path = 'file_image.png';
			}else if (in_array($extension,$videos)){
				$fileModel->type = "v";
				$fileModel->lib_cat_id = 3;
				$fileModel->thumb_path = 'file_video.png';
			}else if (in_array($extension,$music)){
				$fileModel->type = "m";
				$fileModel->lib_cat_id = 5;
				$fileModel->thumb_path = 'file_music.png';
			}else{
				$fileModel->type = "f";
				$fileModel->lib_cat_id = 1;
				$fileModel->thumb_path = 'file_other.png';
			}
				
			$fileModel->name = $orgFileName;
			$fileModel->path = $filePath;
			$fileModel->dateline = date("Y-m-d H:i:s");
			$fileModel->user_id = Yii::app()->user->id;
			$fileModel->is_archived = 1;
				
			if ($fileModel->save()){
				$message = "";
			}else{
				$message = "An error occured while saving the file!";
				$url = "";
			}
		}
	
		echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
	
	}
		

}