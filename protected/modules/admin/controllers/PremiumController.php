<?php
/**
 * This is a Yii controller to manage Premium in the backend.
 * It supports adding, editing, and deleting Premium.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class PremiumController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex() {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new Premium('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['Premium']))
			$model->setAttributes($_GET['Premium']);

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * View Premium details.
	 *
	 * @param integer ID of the Premium to be viewed.
	 */
	public function actionView($id) {
		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'Premium');
		
		// Render index view.
		$this->render('view', array(
			'model' => $model,
		));
	}
}