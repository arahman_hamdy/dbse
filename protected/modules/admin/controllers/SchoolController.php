<?php
/**
 * This is a Yii controller to manage School in the backend.
 * It supports adding, editing, and deleting School.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class SchoolController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex() {
		// Instantiate a School model to generate a DataProvider for gridview.
		$model = new School('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['School']))
			$model->setAttributes($_GET['School']);

        $model->is_deleted = 0;

        if (!Yii::app()->user->isManager())
            $model->user_id = Yii::app()->user->id;

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Add new record
	 */
	public function actionCreate() {
		
		// Instantiate a School model to add new record.
		$model = new School;


        // On post, fill model with requested data
        if (isset($_POST['School'])) {
 
            $transaction = Yii::app()->db->beginTransaction();
            try{
                $model->setAttributes($_POST['School']);
                
                $model->user_id = Yii::app()->user->id;
                $model->visits = 0;
                $model->is_deleted = 0;
                $model->dateline = date("Y-m-d H:i:s");

                $relatedData = array(
                    'libFiles' => empty($_POST['School']['libFiles']) ? null : $_POST['School']['libFiles'],
                    );
 
                if ($model->saveWithRelated($relatedData)) {
                    // Save succeeded
  
                    $transaction->commit();
 
                    if (Yii::app()->getRequest()->getIsAjaxRequest())
                        // If the request is ajax, just return the http status 200.
                        Yii::app()->end();
                    else
                        // If the request is not ajax, send redirect flag to add new record.
                        $this->redirect(array('create'));
                }
            }catch(Exception $ex){
                $transaction->rollback();
            }
            // If save failed, the model is already populated with the errors in errors propery.
 
        }

		// Render create view.
		$this->render('create', array( 'model' => $model));
	}

	/**
	 * Edit record
	 *
	 * @param integer ID of the School to be edited.
	 */
	public function actionUpdate($id) {
		
		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'School');

        if ($model->region){
            $model->city = $model->region->city_id;
        }

        // On post, fill model with requested data
        if (isset($_POST['School'])) {
 
            $transaction = Yii::app()->db->beginTransaction();
            try{
                $model->main_image_id = null;
                $model->setAttributes($_POST['School']);

                $relatedData = array(
                    'libFiles' => empty($_POST['School']['libFiles']) ? null : $_POST['School']['libFiles'],
                    );
 
                if ($model->saveWithRelated($relatedData)) {
 
                    //$_POST['School']
                    $transaction->commit();
 
                    $this->redirect(array('update', 'id' => $model->id));
                }
            }catch(Exception $ex){
                $transaction->rollback();
            }
            // If save failed, the model is already populated with the errors in errors propery.
        }else{
 
            /*
            // If the main image is sent with the request
            if ($model->mainImage && $model->mainImage->path){
 
                // If it's type is image
                if ($model->mainImage->type == "i")
                    // Generate a thumbnail of the image
                    $model->main_image_lib_file_thumb_url = Yii::app()->upload->uploadsUrl . $model->mainImage->path;
                else
                    // Set thumbnail to the default image
                    $model->main_image_lib_file_thumb_url = Yii::app()->upload->uploadsUrl . $model->mainImage->thumb_path;
            }
            */          
        }
 
		// Render update view.
		$this->render('update', array(
				'model' => $model,
				));
	}

	/**
	 * View School details.
	 *
	 * @param integer ID of the School to be viewed.
	 */
	public function actionView($id) {
		
		// View is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}

	/**
	 * Delete School.
	 *
	 * @param integer ID of the School to be deleted.
	 */
	public function actionDelete($id) {
		
		// Make sure that method is POST
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			
            // Delete the requested record
            $model = $this->loadModel($id, 'School');
            $model->is_deleted = 1;
            $model->save();

		} else
			// Raise an error if the method is not POST
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

    public function actionDeleteImage()
    {
 
        // Make sure that method is POST
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $image = LibFile::model()->findByPK($id);
            if ($image){
                SchoolImage::model()->deleteAll('image_id=:id',array(':id'=>$id));
 
                @unlink($this->uploadsPath . $image->path);
                $image->delete();
                echo 1;
                exit;
            }
            echo 0;
 
        } else
            // Raise an error if the method is not POST
            echo 0; 
    }
 
    public function actionFind($q)
    {
        if (strlen($q)<3) {echo '[]';die;}
 
        $criteria = new CDbCriteria;
        $criteria->select = "id, name_en";
        $criteria->compare("is_deleted",0);
        $criteria->compare("status",1);
 
        $criteria2 = new CDbCriteria;
        $criteria2->compare('name_ar', $q, true);
        $criteria2->compare('name_en', $q, true, 'OR');
 
        $criteria->mergeWith($criteria2);
 
        $results = School::model()->findAll($criteria);
 
        $outputAll = array();
        foreach($results as $result){
            $output['value'] = $result['id'];
            $output['desc'] = $result['name_en'];
 
            $outputAll[] = $output;
        }
        echo CJSON::encode($outputAll);
 
 
    }

}