<?php
/**
 * This is a Yii controller to manage Transaction in the backend.
 * It supports adding, editing, and deleting Transaction.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class TransactionController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex() {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new Transaction('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['Transaction']))
			$model->setAttributes($_GET['Transaction']);

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * View Transaction details.
	 *
	 * @param integer ID of the Transaction to be viewed.
	 */
	public function actionView($id) {
		// Load the record of the requested ID.
		$model = Transaction::model()->findByPK($id);

		if (!$model){
			throw new CHttpException(404, Yii::t("app","Page not found!"));
		}
		
		// Render view.
		$this->render('view', array(
			'model' => $model,
		));
	}
}