<?php
/**
 * This is a Yii controller to manage Entity in the backend.
 * It supports adding, editing, and deleting Entity.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class EntityController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex($entityId = null) {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new Entity('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['Entity']))
			$model->setAttributes($_GET['Entity']);

		//if (!Yii::app()->user->isManager())
		//	$model->user_id = Yii::app()->user->id;

		$model->is_deleted = 0;
		if ($entityId) $model->parent_id = $entityId;

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Add new record
	 */
	public function actionCreate($entityId = null) {
		
		// Instantiate a Place model to add new record.
		$model = new Entity;

		if ($entityId) $model->parent_id = $entityId;


		// On post, fill model with requested data
		if (isset($_POST['Entity'])) {
			$transaction = Yii::app()->db->beginTransaction();
			try{

				$model->setAttributes($_POST['Entity']);

				$relatedData = array(
					'libFiles' => empty($_POST['Entity']['libFiles']) ? null : $_POST['Entity']['libFiles'],
					); 

				if ($model->saveWithRelated($relatedData)) {
					if (!empty($_POST['EntityAttribute'])) {
						foreach($_POST['EntityAttribute'] as $key=>$entityAttributeData){
							$entityAttribute = new EntityAttribute;
							$entityAttribute->setAttributes($entityAttributeData);
							$entityAttribute->entity_id = $model->id;
							$entityAttribute->entity_type_attribute_id = $key;
							if (!$entityAttribute->save()){
								throw new Exception("Unable to add attributes");
							}
						}

					}

					$transaction->commit();
					
					// Save succeeded
					if (Yii::app()->getRequest()->getIsAjaxRequest())
						// If the request is ajax, just return the http status 200.
						Yii::app()->end();
					else
						// If the request is not ajax, send redirect flag to add new record.
						$this->redirect(array('create','entityId'=>$entityId));

				}else{
					throw new Exception("An error occurred!");
				}

			}catch(Exception $ex){
				// If save failed, the model is already populated with the errors in errors propery.
				$model->addError(null, $ex->getMessage());
				$transaction->rollback();
			}
		}

		// Render create view.
		$this->render('create', array( 'model' => $model));
	}

	/**
	 * Edit record
	 *
	 * @param integer ID of the Entity to be edited.
	 */
	public function actionUpdate($id) {
		
		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'Entity');

		if ($model->region){
			$model->city = $model->region->city_id;
		}

		// On post, fill model with requested data
		if (isset($_POST['Entity'])) {
			$transaction = Yii::app()->db->beginTransaction();
			try{

				$model->setAttributes($_POST['Entity']);

				$relatedData = array(
					'libFiles' => empty($_POST['Entity']['libFiles']) ? null : $_POST['Entity']['libFiles'],
					);

				if ($model->saveWithRelated($relatedData)) {

					EntityAttribute::model()->deleteAll("entity_id=". $model->id);
					if (!empty($_POST['EntityAttribute'])) {
						foreach($_POST['EntityAttribute'] as $key=>$entityAttributeData){
							$entityAttribute = new EntityAttribute;
							$entityAttribute->setAttributes($entityAttributeData);
							$entityAttribute->entity_id = $model->id;
							$entityAttribute->entity_type_attribute_id = $key;
							if (!$entityAttribute->save()){
								throw new Exception("Unable to add attributes");
							}

						}

					}

					$transaction->commit();

					// Save succeeded, redirect to edit the same record
					$this->redirect(array('update', 'id' => $model->id));
				}else{
					throw new Exception("An error occurred!");
				}

			}catch(Exception $ex){
				// If save failed, the model is already populated with the errors in errors propery.
				$model->addError(null, $ex->getMessage());
				$transaction->rollback();
			}
		}

		// Render update view.
		$this->render('update', array(
				'model' => $model,
				));
	}

	/**
	 * View Entity details.
	 *
	 * @param integer ID of the Entity to be viewed.
	 */
	public function actionView($id) {
		
		// View place is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}

	/**
	 * Delete Entity.
	 *
	 * @param integer ID of the Entity to be deleted.
	 */
	public function actionDelete($id) {
		
		// Make sure that method is POST
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			
			// Delete the requested record
			$model = $this->loadModel($id, 'Entity');
			$model->is_deleted = 1;
			$model->save();

		} else
			// Raise an error if the method is not POST
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionGetAttributes($type_id, $entity_id = null){
		if (!$type_id) die;

		$cs=Yii::app()->clientScript;
		$cs->scriptMap=array(
		        'jquery.js'=>false,
		); 		
		$form = new GxActiveForm();

		$criteria = new CDbCriteria;
		$criteria->compare('entity_type_id', $type_id);
		$criteria->with = array('entityAttribute'=>array('on'=>'entityAttribute.entity_id=:entity_id'));
		$criteria->together=true;
		$criteria->order = 'sortid';
		$criteria->params[':entity_id'] = $entity_id;
		$models = EntityTypeAttribute::model()->findAll($criteria);

		if ($models){
			$this->renderPartial("_entity_attributes",array('form'=>$form,'models'=>$models),false,true);
		}
	}

	public function actionDeleteImage()
	{

		// Make sure that method is POST
		if (!empty($_POST['id'])) {
			$id = $_POST['id'];
			$image = LibFile::model()->findByPK($id);
			if ($image){
				EntityImages::model()->deleteAll('image_id=:id',array(':id'=>$id));

		  		@unlink($this->uploadsPath . $image->path);
		  		$image->delete();
		  		echo 1;
		  		exit;
			}
			echo 0;

		} else
			// Raise an error if the method is not POST
			echo 0;	
	}
}