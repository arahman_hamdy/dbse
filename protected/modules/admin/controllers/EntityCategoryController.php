<?php
/**
 * This is a Yii controller to manage EntityCategory in the backend.
 * It supports adding, editing, and deleting EntityCategory.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class EntityCategoryController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex() {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new EntityCategory('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['EntityCategory']))
			$model->setAttributes($_GET['EntityCategory']);

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Add new record
	 */
	public function actionCreate() {
		
		// Instantiate a Place model to add new record.
		$model = new EntityCategory;


		// On post, fill model with requested data
		if (isset($_POST['EntityCategory'])) {
			$model->setAttributes($_POST['EntityCategory']);

			if ($model->save()) {
				// Save succeeded
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					// If the request is ajax, just return the http status 200.
					Yii::app()->end();
				else
					// If the request is not ajax, send redirect flag to add new record.
					$this->redirect(array('create'));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render create view.
		$this->render('create', array( 'model' => $model));
	}

	/**
	 * Edit record
	 *
	 * @param integer ID of the EntityCategory to be edited.
	 */
	public function actionUpdate($id) {
		
		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'EntityCategory');


		// On post, fill model with requested data
		if (isset($_POST['EntityCategory'])) {
			$model->setAttributes($_POST['EntityCategory']);

			if ($model->save()) {
				// Save succeeded, redirect to edit the same record
				$this->redirect(array('update', 'id' => $model->id));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render update view.
		$this->render('update', array(
				'model' => $model,
				));
	}

	/**
	 * View EntityCategory details.
	 *
	 * @param integer ID of the EntityCategory to be viewed.
	 */
	public function actionView($id) {
		
		// View place is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}

	/**
	 * Delete EntityCategory.
	 *
	 * @param integer ID of the EntityCategory to be deleted.
	 */
	public function actionDelete($id) {
		
		// Make sure that method is POST
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			
			// Delete the requested record
			$this->loadModel($id, 'EntityCategory')->delete();

		} else
			// Raise an error if the method is not POST
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

}