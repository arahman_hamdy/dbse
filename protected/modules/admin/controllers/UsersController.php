<?php

class UsersController extends BackMainController {

	public function beforeAction($action){
		if (
			!Yii::app()->user->can(UserPermissions::User_Create) &&
			!Yii::app()->user->can(UserPermissions::User_Delete) &&
			!Yii::app()->user->can(UserPermissions::User_Edit)
		) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		return parent::beforeAction($action);
	}
	
	public function actionView($id) {
		$this->redirect(array_merge(array('update'), $_GET),true);
		
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Users'),
		));
	}

	public function actionCreate() {
		if (!Yii::app()->user->can(UserPermissions::User_Create)) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
				
		$model = new CreateUser;
		$profile = new Profile;

		if (isset($_POST['CreateUser'])) {
			$model->setAttributes($_POST['CreateUser']);
			if (isset($_POST['Profile']))
				$profile->setAttributes($_POST['Profile']);

			if (Yii::app()->user->RoleId != 1 && $model->role_id = 1) $model->role_id = null;
				
			if ($model->password){
				$model->password = md5($model->password);
				$model->verifyPassword = $model->password;
			}
			
			$model->status = 1;
			
			if ($model->save()) {
				$profile->user_id = $model->id;
				$profile->save(false);
				
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model,'profile'=>$profile));
	}

	public function actionUpdate($id) {
		if (!Yii::app()->user->can(UserPermissions::User_Edit)) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		
		$model = $this->loadModel($id, 'ManageUser');
		
		if (!$model /*|| $model->id == 1*/)
			throw new CHttpException(400, Yii::t('app', 'Unable to edit the requested user.'));
		

		if (isset($_POST['ManageUser'])) {
			$oldPassword = $model->password;
			
			$model->setAttributes($_POST['ManageUser']);
			if (Yii::app()->user->RoleId != 1 && $model->role_id = 1) $model->role_id = null;

			//array_push($rules,array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => Yii::t('UserModule', "Retype Password is incorrect.")));
				
			if ($model->password){
				$model->password = md5($model->password);
				$model->verifyPassword = $model->password;
			}
			
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array(
				'model' => $model, 'profile'=>$model->profile
				));
	}

	public function actionDelete($id) {
		if (!Yii::app()->user->can(UserPermissions::User_Delete)) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$model = $this->loadModel($id, 'Users');
			if (!$model || $model->id == 1)
				throw new CHttpException(400, Yii::t('app', 'Unable to delete system admin.'));
			
			$model->status = 0;
			$model->save(false);
			
			$model->delete();
			
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$model = new Users('search');
		$model->unsetAttributes();

		if (isset($_GET['Users']))
			$model->setAttributes($_GET['Users']);
		
		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		$dataProvider = new CActiveDataProvider('Users');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

}