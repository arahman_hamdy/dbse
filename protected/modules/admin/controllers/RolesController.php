<?php

class RolesController extends BackMainController {


	public function beforeAction($action){
		if (Yii::app()->user->RoleId != 1) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		return parent::beforeAction($action);
	}
	
	public function actionView($id) {
		$this->redirect(array_merge(array('update'), $_GET),true);
		
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Role'),
		));
	}

	public function actionCreate() {
		$model = new Role;


		if (isset($_POST['Role'])) {
			$model->setAttributes($_POST['Role']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Role');
		if (!$model || $model->id == 1)
			throw new CHttpException(400, Yii::t('app', 'Unable to edit system role.'));
		

		if (isset($_POST['Role'])) {
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$model->setAttributes($_POST['Role']);
	
				if (!$model->save()) {
					throw new Exception("Unable to save the role!");
				}
				
				RolePermission::model()->deleteAllByAttributes(array('role_id'=>$id));
				
				foreach($model->permissions as $perm_id){
					$rolePermission = new RolePermission;
					$rolePermission->role_id = $model->id;
					$rolePermission->permission_id = $perm_id;
					$rolePermission->dateline = date("Y-m-d H:i:s");
					
					if (!$rolePermission->save()) {
						throw new Exception("Unable to save the permission!");
					}
				
				}
				
				$transaction->commit();
					
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('index'));
					
					
			}catch (Exception $ex){
					
				try{$transaction->rollback();}catch(Exception $e){}
				$model->addError(null,$ex->getMessage());
				//throw $ex;
			}
				
		}else{
			$model->permissions = array_values(CHtml::listData($model->rolePermissions,'permission_id','permission_id'));
		}
				
		$this->render('update', array(
				'model' => $model,
		));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Role')->delete();
			if (!$model || $model->id == 1)
				throw new CHttpException(400, Yii::t('app', 'Unable to delete system role.'));
				
				

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$model = new Role('search');
		$model->unsetAttributes();

		if (isset($_GET['Role']))
			$model->setAttributes($_GET['Role']);

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		$dataProvider = new CActiveDataProvider('Role');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

}