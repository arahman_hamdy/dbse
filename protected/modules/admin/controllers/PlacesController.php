<?php

/**
 * This is a Yii controller to manage places in the backend.
 * It supports adding, editing, and deleting places.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */
class PlacesController extends BackMainController {

	/**
	 * Default action to list current records.
	 */
	public function actionIndex() {

		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new Place('search');
		$model->unsetAttributes();
		$model->is_deleted = 0;

		// Set attributes of gridview filters.
		if (isset($_GET['Place']))
			$model->setAttributes($_GET['Place']);

		if (!Yii::app()->user->isManager())
			$model->user_id = Yii::app()->user->id;

		if ($this->action->id != 'pending'){
			$model->is_draft = 0;
		}

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionPublish($id) {
		if (!Yii::app()->user->can(UserPermissions::Place_Publish)) throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'Place');
		
		if ($model->draft_for){
			$mainModel = $this->loadModel($model->draft_for, 'Place');
			$mainModel->setAttributes($model->attributes);
			$mainModel->is_draft = 0;
			$mainModel->save();

			$model->is_deleted = 1;
			$model->save();
		}else{
			$model->is_draft = 0;
			$model->save();
		}
	}

	public function actionPending() {
		$_GET['Place']['is_draft'] = 1;
		$this->actionIndex();
	}

	/**
	 * Add new record
	 */
	public function actionCreate() {
		if (!Yii::app()->user->can(UserPermissions::Place_Create)) throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		// Instantiate a Place model to add new record.
		$model = new Place;
		$model->user_id = Yii::app()->user->id;

		// On post, fill model with requested data
		if (isset($_POST['Place'])) {

			$transaction = Yii::app()->db->beginTransaction();
			try{
				$model->status = 1; // Active
				$model->setAttributes($_POST['Place']);
				
				$model->is_premium = 0;

				if (!Yii::app()->user->can(UserPermissions::Place_Publish)){
					$model->is_draft = 1; // Inactive
				}

				if (!Yii::app()->user->isAdmin())
					$model->user_id = Yii::app()->user->id;
				$model->ratings_total = 0;
				$model->ratings_count = 0;
				$model->ratings_avg = 0;
				$model->visits = 0;
				$model->is_deleted = 0;
				$model->dateline = date("Y-m-d H:i:s");

				$relatedData = array(
					'libFiles' => empty($_POST['Place']['libFiles']) ? null : $_POST['Place']['libFiles'],
					'properties' => empty($_POST['Place']['properties']) ? null : $_POST['Place']['properties'],
					'targets' => empty($_POST['Place']['targets']) ? null : $_POST['Place']['targets'],
					); 
				if ($model->saveWithRelated($relatedData)) {
					// Save succeeded

					// Cache properties for better performance
					$propertiesArray = CHtml::listData($model->properties,'id','id');
					sort($propertiesArray);
					$model->properties_cache = implode($propertiesArray);
					$model->save();

					$transaction->commit();

					if (Yii::app()->getRequest()->getIsAjaxRequest())
						// If the request is ajax, just return the http status 200.
						Yii::app()->end();
					else
						// If the request is not ajax, send redirect flag to add new record.
						$this->redirect(array('create'));
				}
			}catch(Exception $ex){
				$transaction->rollback();
			}
			// If save failed, the model is already populated with the errors in errors propery.

		}

		// Render create view.
		$this->render('create', array( 'model' => $model));
	}

	public function actionImport(){
		$importPath = Yii::getPathOfAlias("application") . '/../../places_import';

		Yii::import("ext.phpexcel.Classes.PHPExcel",true);
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objReader->setReadDataOnly(true);

		$objPHPExcel = $objReader->load("$importPath/places_import.xlsx");
		$objWorksheet = $objPHPExcel->getActiveSheet();

		$highestRow = $objWorksheet->getHighestRow(); 
		$highestColumn = $objWorksheet->getHighestColumn(); 

		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 

		$fields = array();
		for ($row = 1; $row <= $highestRow; ++$row) {
			$placeArray = array();
			for ($col = 0; $col < $highestColumnIndex; ++$col) {
				if ($row==1){
					$fields[] = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
				}else{
					$placeArray[$fields[$col]] = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
				}
			}
			if ($row == 1) continue;

			$model = new Place();
			$model->setAttributes($placeArray);
			$model->id = null;
			$model->is_premium = 0;
			$model->user_id = Yii::app()->user->id;
			$model->ratings_total = 0;
			$model->ratings_count = 0;
			$model->ratings_avg = 0;
			$model->visits = 0;
			$model->is_deleted = 0;

			$model->save();
			print_r($model->errors);die;
		  //mysql_query("INSERT INTO your_table (col1,col2) VALUES ($rows[1],$rows[2])");
		}

	}

	/**
	 * Edit record
	 *
	 * @param integer ID of the Place to be edited.
	 */
	public function actionUpdate($id) {
		if (!Yii::app()->user->can(UserPermissions::Place_Edit)) throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		// Load the record of the requested ID.
		$mainModel = $model = $this->loadModel($id, 'Place');
		
		if (!Yii::app()->user->isManager() && $model->user_id != Yii::app()->user->id)
			throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		$draftModel = null;

		if ($model->is_draft && $model->draft_for){
			$mainModel = Place::model()->findByPK($model->draft_for);
		}else{
			$criteria = new CDbCriteria;
			$criteria->compare('is_draft', 1);
			$criteria->compare('is_deleted', 0);
			$criteria->compare('user_id', Yii::app()->user->id);
			$criteria->compare('draft_for', $mainModel->id);
			$draftModel = Place::model()->find($criteria);
			if ($draftModel){
				$model->setAttributes($draftModel->attributes);
				$model->is_draft = 0;
				$model->draft_for = null;
			}
		}

		if ($model->region){
			$model->city = $model->region->city_id;
		}
		
		// On post, fill model with requested data
		if (isset($_POST['Place'])) {
			if (!$model->is_draft && !Yii::app()->user->can(UserPermissions::Place_Publish)){
				if ($draftModel)
					$model = $draftModel;
				else
					$model = new Place;

				$model->setAttributes($mainModel->attributes);
				$model->is_draft = 1; // Inactive
				$model->draft_for = $mainModel->id;
			}

			$transaction = Yii::app()->db->beginTransaction();
			try{
				$model->main_image_id = null;
				$model->setAttributes($_POST['Place']);

				$relatedData = array(
					'libFiles' => empty($_POST['Place']['libFiles']) ? null : $_POST['Place']['libFiles'],
					'properties' => empty($_POST['Place']['properties']) ? null : $_POST['Place']['properties'],
					'targets' => empty($_POST['Place']['targets']) ? null : $_POST['Place']['targets'],
					);

				if ($model->saveWithRelated($relatedData)) {
					// Save succeeded, redirect to edit the same record

					// Cache properties for better performance
					$propertiesArray = CHtml::listData($model->properties,'id','id');
					sort($propertiesArray);
					$model->properties_cache = ",".implode($propertiesArray).",";
					$model->save();


					//$_POST['Place']
					$transaction->commit();

					$this->redirect(array('update', 'id' => $model->id));
				}else{
					//print_r($model->errors);die;
				}
			}catch(Exception $ex){
				$transaction->rollback();
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}else{

			/*
			// If the main image is sent with the request
			if ($model->mainImage && $model->mainImage->path){

				// If it's type is image
				if ($model->mainImage->type == "i")
					// Generate a thumbnail of the image
					$model->main_image_lib_file_thumb_url = Yii::app()->upload->uploadsUrl . $model->mainImage->path;
				else
					// Set thumbnail to the default image
					$model->main_image_lib_file_thumb_url = Yii::app()->upload->uploadsUrl . $model->mainImage->thumb_path;
			}
			*/			
		}

		// Render update view.
		$this->render('update', array(
				'model' => $model,
				));
	}

	/**
	 * View place details.
	 *
	 * @param integer ID of the place to be viewed.
	 */
	public function actionView($id) {

		// View place is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}

	/**
	 * Delete place.
	 *
	 * @param integer ID of the place to be deleted.
	 */
	public function actionDelete($id) {
		if (!Yii::app()->user->can(UserPermissions::Place_Delete)) throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		// Make sure that method is POST
		if (Yii::app()->getRequest()->getIsPostRequest()) {

			// Delete the requested record
			$model = $this->loadModel($id, 'Place');
			if (!Yii::app()->user->isManager() && $model->user_id != Yii::app()->user->id)
				throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));
			
			$model->is_deleted = 1;
			$model->save();

		} else
			// Raise an error if the method is not POST
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionDeleteImage()
	{

		// Make sure that method is POST
		if (!empty($_POST['id'])) {
			$id = $_POST['id'];
			$image = LibFile::model()->findByPK($id);
			if ($image){
				PlaceImage::model()->deleteAll('image_id=:id',array(':id'=>$id));

		  		@unlink($this->uploadsPath . $image->path);
		  		$image->delete();
		  		echo 1;
		  		exit;
			}
			echo 0;

		} else
			// Raise an error if the method is not POST
			echo 0;	
	}

	public function actionFind($q)
	{
		if (strlen($q)<3) {echo '[]';die;}

		$criteria = new CDbCriteria;
		$criteria->select = "id, name_en";
		$criteria->compare("is_deleted",0);
		$criteria->compare("status",1);
		if (!Yii::app()->user->isManager())
			$criteria->compare('user_id', Yii::app()->user->id);


		$criteria2 = new CDbCriteria;
		$criteria2->compare('name_ar', $q, true);
		$criteria2->compare('name_en', $q, true, 'OR');

		$criteria->mergeWith($criteria2);
		$criteria->order = "name_en";
		$criteria->limit = 40;

		$results = Place::model()->findAll($criteria);

		$outputAll = array();
		foreach($results as $result){
			$output['value'] = $result['id'];
			$output['desc'] = $result['name_en'];

			$outputAll[] = $output;
		}
		echo CJSON::encode($outputAll);
	}

	public function actionSearch() {
		$term = "";
		if (!empty($_GET['term'])) $term = $_GET['term'];
		//if (strlen($term)<3) {echo '[]';die;}

		$res =array();

		$criteria = new CDbCriteria;
		$criteria->select = "id, name_en";
		$criteria->compare("is_deleted",0);
		$criteria->compare("status",1);

		if (!Yii::app()->user->isManager())
			$criteria->compare('user_id', Yii::app()->user->id);

		$criteria2 = new CDbCriteria;
		$criteria2->compare('name_ar', $term, true);
		$criteria2->compare('name_en', $term, true, 'OR');

		$criteria->mergeWith($criteria2);
		$criteria->order = "name_en";
		$criteria->limit = 40;

		$results = Place::model()->findAll($criteria);

		$outputAll = array();
		foreach($results as $result){
			$output['value'] = $result['id'];
			$output['label'] = $result['name_en'];

			$outputAll[] = $output;
		}
		echo CJSON::encode($outputAll);
	}

}