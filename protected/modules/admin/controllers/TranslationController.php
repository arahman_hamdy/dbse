<?php

class TranslationController extends BackMainController {

	public function beforeAction($action){
		if (Yii::app()->user->RoleId != 1) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		return parent::beforeAction($action);
	}
	
	public function actionIndex() {
		$this->render('index', array(
		));
	}
	
}