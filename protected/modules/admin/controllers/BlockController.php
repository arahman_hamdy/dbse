<?php

class BlockController extends BackMainController {

	public function beforeAction($action){
		if (Yii::app()->user->RoleId != 1) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		return parent::beforeAction($action);
	}
	
	public function actionView($id) {
		$this->redirect(array_merge(array('update'), $_GET),true);
		
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Block'),
		));
	}

	public function actionCreate() {
		$model = new Block;


		if (isset($_POST['Block'])) {
			$model->setAttributes($_POST['Block']);
			$model->position = 1;
			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('index'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Block');


		if (isset($_POST['Block'])) {
			$model->setAttributes($_POST['Block']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Block')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$model = new Block('search');
		$model->unsetAttributes();

		if (isset($_GET['Block']))
			$model->setAttributes($_GET['Block']);

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		$dataProvider = new CActiveDataProvider('Block');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

}