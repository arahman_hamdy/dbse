<?php
/**
 * This is a Yii controller to manage EventRegistration in the backend.
 * It supports adding, editing, and deleting EventRegistration.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class EventRegistrationController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex() {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new EventRegistration('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['EventRegistration']))
			$model->setAttributes($_GET['EventRegistration']);

		if (!Yii::app()->user->isManager())
			$model->owner_id = Yii::app()->user->id;

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * View EventRegistration details.
	 *
	 * @param integer ID of the EventRegistration to be viewed.
	 */
	public function actionView($id) {
		// Load the record of the requested ID.
		$model = EventRegistration::model()->findByPK($id);

		if (!$model){
			throw new CHttpException(404, Yii::t("app","Page not found!"));
		}

		if (!Yii::app()->user->isManager() && $model->event->user_id != Yii::app()->user->id)
			throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		
		// Render view.
		$this->render('view', array(
			'model' => $model,
		));
	}
}