<?php

class LoginController extends BackMainController
{
	public $layout = "//layouts/base";
	public $defaultAction = 'login';
	public $body_class = "login-page login-form-fall";
	
	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$response = array();
				$response['login_status'] = "invalid";
				$response['redirect_url'] = "";
				$model->attributes=$_POST['UserLogin'];
				$model->rememberMe = 1;
				
				if (strpos(Yii::app()->user->returnUrl,Yii::app()->controller->module->returnUrl[0])===false)
					Yii::app()->user->returnUrl = $this->createUrl(Yii::app()->controller->module->returnUrl[0]);
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$response['login_status'] = "success";
						
					$this->lastViset();
					if (Yii::app()->getBaseUrl()."/index.php" === Yii::app()->user->returnUrl)
						$response['redirect_url'] = $this->createUrl(Yii::app()->controller->module->returnUrl[0]);
					else
						$response['redirect_url'] = Yii::app()->user->returnUrl;
				}
				
				echo CJSON::encode($response);
				exit;
			}
			// display the login form
			$this->render('login',array('model'=>$model));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}
	
	private function lastViset() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit_at = date('Y-m-d H:i:s');
		$lastVisit->save();
	}

}