<?php
/**
 * This is a Yii controller to manage EntityTypeAttribute in the backend.
 * It supports adding, editing, and deleting EntityTypeAttribute.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class EntityTypeAttributeController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex($entityId = null) {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new EntityTypeAttribute('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['EntityTypeAttribute']))
			$model->setAttributes($_GET['EntityTypeAttribute']);

		if ($entityId) $model->entity_type_id = $entityId;

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Add new record
	 */
	public function actionCreate($entityId = null) {
		
		// Instantiate a Place model to add new record.
		$model = new EntityTypeAttribute;

		if ($entityId) $model->entity_type_id = $entityId;

		// On post, fill model with requested data
		if (isset($_POST['EntityTypeAttribute'])) {
			$model->setAttributes($_POST['EntityTypeAttribute']);

			if ($model->save()) {
				// Save succeeded
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					// If the request is ajax, just return the http status 200.
					Yii::app()->end();
				else
					// If the request is not ajax, send redirect flag to add new record.
					$this->redirect(array('create','entityId'=>$entityId));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render create view.
		$this->render('create', array( 'model' => $model));
	}

	/**
	 * Edit record
	 *
	 * @param integer ID of the EntityTypeAttribute to be edited.
	 */
	public function actionUpdate($id) {
		
		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'EntityTypeAttribute');


		// On post, fill model with requested data
		if (isset($_POST['EntityTypeAttribute'])) {
			$model->setAttributes($_POST['EntityTypeAttribute']);

			if ($model->save()) {
				// Save succeeded, redirect to edit the same record
				$this->redirect(array('update', 'id' => $model->id));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render update view.
		$this->render('update', array(
				'model' => $model,
				));
	}

	/**
	 * View EntityTypeAttribute details.
	 *
	 * @param integer ID of the EntityTypeAttribute to be viewed.
	 */
	public function actionView($id) {
		
		// View place is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}

	/**
	 * Delete EntityTypeAttribute.
	 *
	 * @param integer ID of the EntityTypeAttribute to be deleted.
	 */
	public function actionDelete($id) {
		
		// Make sure that method is POST
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			
			// Delete the requested record
			$this->loadModel($id, 'EntityTypeAttribute')->delete();

		} else
			// Raise an error if the method is not POST
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

}