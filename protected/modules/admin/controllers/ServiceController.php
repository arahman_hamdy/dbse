<?php
/**
 * This is a Yii controller to manage Service in the backend.
 * It supports adding, editing, and deleting Service.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class ServiceController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex($id) {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new Service('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['Service']))
			$model->setAttributes($_GET['Service']);

		$model->place_id = $id;

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Add new record
	 */
	public function actionCreate($id) {
		
		// Instantiate a Place model to add new record.
		$model = new Service;
		$model->place_id = $id;


		// On post, fill model with requested data
		if (isset($_POST['Service'])) {
			$model->setAttributes($_POST['Service']);

			if ($model->save()) {
				// Save succeeded
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					// If the request is ajax, just return the http status 200.
					Yii::app()->end();
				else
					// If the request is not ajax, send redirect flag to add new record.
					$this->redirect(array('create', 'id'=>$id));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render create view.
		$this->render('create', array( 'model' => $model));
	}

	/**
	 * Edit record
	 *
	 * @param integer ID of the Service to be edited.
	 */
	public function actionUpdate($id) {
		
		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'Service');


		// On post, fill model with requested data
		if (isset($_POST['Service'])) {
			$model->setAttributes($_POST['Service']);

			if ($model->save()) {
				// Save succeeded, redirect to edit the same record
				$this->redirect(array('update', 'id' => $model->id));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render update view.
		$this->render('update', array(
				'model' => $model,
				));
	}

	/**
	 * View Service details.
	 *
	 * @param integer ID of the Service to be viewed.
	 */
	public function actionView($id) {
		
		// View place is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}

	/**
	 * Delete Service.
	 *
	 * @param integer ID of the Service to be deleted.
	 */
	public function actionDelete($id) {
		
		// Make sure that method is POST
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			
			// Delete the requested record
			$model = $this->loadModel($id, 'Service');
			$model->is_deleted = 1;
			$model->save();

		} else
			// Raise an error if the method is not POST
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

}