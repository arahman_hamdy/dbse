<?php

class DefaultController extends BackMainController
{	
	public function actionIndex () {
		$this->render('index');
	}
	
	public function actionForm() {
		$this->render("form");
	}
	
	public function actionGrid() {
		$this->render("grid");
	}	
}