<?php
/**
 * This is a Yii controller to manage BlogArticle in the backend.
 * It supports adding, editing, and deleting BlogArticle.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class BlogArticleController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex() {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new BlogArticle('search');
		$model->unsetAttributes();
		$model->is_deleted=0;

		// Set attributes of gridview filters.
		if (isset($_GET['BlogArticle']))
			$model->setAttributes($_GET['BlogArticle']);

		if (!Yii::app()->user->isManager())
			$model->user_id = Yii::app()->user->id;

		if ($this->action->id != 'pending'){
			$model->is_draft = 0;
		}
		
		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionPublish($id) {
		if (!Yii::app()->user->can(UserPermissions::BlogArticle_Publish)) throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'BlogArticle');
		
		if ($model->draft_for){
			$mainModel = $this->loadModel($model->draft_for, 'BlogArticle');
			$mainModel->setAttributes($model->attributes);
			$mainModel->is_draft = 0;
			$mainModel->save();

			$model->is_deleted = 1;
			$model->save();
		}else{
			$model->is_draft = 0;
			$model->save();
		}
	}

	public function actionPending() {
		$_GET['BlogArticle']['is_draft'] = 1;
		$this->actionIndex();
	}


	/**
	 * Add new record
	 */
	public function actionCreate() {
		
		// Instantiate a Place model to add new record.
		$model = new BlogArticle;


		// On post, fill model with requested data
		if (isset($_POST['BlogArticle'])) {
			$model->status = 1; // Active
			$model->setAttributes($_POST['BlogArticle']);

			if (!Yii::app()->user->can(UserPermissions::BlogArticle_Publish)){
				$model->is_draft = 1; // Inactive
			}
			$model->is_draft = 1;

			$model->user_id = Yii::app()->user->id;
			$model->is_deleted = 0;
			$model->dateline = date("Y-m-d H:i:s");

			$uploader = CUploadedFile::getInstance($model,'image_file');

			if ($uploader){
		        $pathinfo = pathinfo($uploader->name);
		        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
		        $ext = $ext == '' ? $ext : '.' . $ext;

	            $uploadName = uniqid("slider_".time(),true) . $ext;
	            $uploadPath = $this->uploadsPath . $uploadName;
	            $uploader->saveAs($uploadPath);

				$fileModel =  new LibFile;
				$fileModel->type = "i";
				$fileModel->lib_cat_id = 2;
				$fileModel->thumb_path = 'file_image.png';
					
				$fileModel->name = $uploader->name;
				$fileModel->path = $uploadName;
				$fileModel->dateline = date("Y-m-d H:i:s");
				$fileModel->user_id = Yii::app()->user->id;
				$fileModel->is_archived = 0;
				
				$fileModel->save(false);

	       		$model->image_id=$fileModel->id;
	       		$model->image_file=$model->image_id;
	       	}

	       	if (!$model->image_id){
	       		$model->addError("image_id", "Image is required!");
		    }else if ($model->save()) {
				// Save succeeded
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					// If the request is ajax, just return the http status 200.
					Yii::app()->end();
				else
					// If the request is not ajax, send redirect flag to add new record.
					$this->redirect(array('create'));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render create view.
		$this->render('create', array( 'model' => $model));
	}

	/**
	 * Edit record
	 *
	 * @param integer ID of the BlogArticle to be edited.
	 */
	public function actionUpdate($id) {
		
		// Load the record of the requested ID.
		$mainModel = $model = $this->loadModel($id, 'BlogArticle');

		$draftModel = null;

		if ($model->is_draft && $model->draft_for){
			$mainModel = BlogArticle::model()->findByPK($model->draft_for);
		}else{
			$criteria = new CDbCriteria;
			$criteria->compare('is_draft', 1);
			$criteria->compare('is_deleted', 0);
			$criteria->compare('user_id', Yii::app()->user->id);
			$criteria->compare('draft_for', $mainModel->id);
			$draftModel = BlogArticle::model()->find($criteria);
			if ($draftModel){
				$model->setAttributes($draftModel->attributes);
				$model->is_draft = 0;
				$model->draft_for = null;
			}
		}

		// On post, fill model with requested data
		if (isset($_POST['BlogArticle'])) {
			if (!$model->is_draft && !Yii::app()->user->can(UserPermissions::Place_Publish)){
				if ($draftModel)
					$model = $draftModel;
				else
					$model = new BlogArticle;

				$model->setAttributes($mainModel->attributes);
				$model->is_draft = 1; // Inactive
				$model->draft_for = $mainModel->id;
			}

			$model->setAttributes($_POST['BlogArticle']);

			$uploader = CUploadedFile::getInstance($model,'image_file');

			if ($uploader){
		        $pathinfo = pathinfo($uploader->name);
		        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
		        $ext = $ext == '' ? $ext : '.' . $ext;

	            $uploadName = uniqid("blog_".time(),true) . $ext;
	            $uploadPath = $this->uploadsPath . $uploadName;
	            $uploader->saveAs($uploadPath);

				$fileModel =  new LibFile;
				$fileModel->type = "i";
				$fileModel->lib_cat_id = 2;
				$fileModel->thumb_path = 'file_image.png';
					
				$fileModel->name = $uploader->name;
				$fileModel->path = $uploadName;
				$fileModel->dateline = date("Y-m-d H:i:s");
				$fileModel->user_id = Yii::app()->user->id;
				$fileModel->is_archived = 0;
				
				$fileModel->save(false);

	       		$model->image_id=$fileModel->id;
	       		$model->image_file=$model->image_id;
	       	}

			if (!$model->image_id){
	       		$model->addError("image_id", "Image is required!");
		    }else if ($model->save()) {
				// Save succeeded, redirect to edit the same record
				$this->redirect(array('update', 'id' => $model->id));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render update view.
		$this->render('update', array(
				'model' => $model,
				));
	}

	/**
	 * View BlogArticle details.
	 *
	 * @param integer ID of the BlogArticle to be viewed.
	 */
	public function actionView($id) {
		
		// View place is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}

	/**
	 * Delete BlogArticle.
	 *
	 * @param integer ID of the BlogArticle to be deleted.
	 */
	public function actionDelete($id) {
		
		// Make sure that method is POST
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			
			// Delete the requested record
			$model = $this->loadModel($id, 'BlogArticle');
			$model->is_deleted = 1;
			$model->save();

		} else
			// Raise an error if the method is not POST
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

}