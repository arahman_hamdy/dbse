<?php

class CategoryController extends BackMainController {

	public function beforeAction($action){
		if (
			!Yii::app()->user->can(UserPermissions::Category_Create) &&
			!Yii::app()->user->can(UserPermissions::Category_Edit)
		) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		return parent::beforeAction($action);
	}
		
	public function actionView($id) {
		$this->redirect(array_merge(array('update'), $_GET),true);
		
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Category'),
		));
	}

	public function actionCreate() {
		if (!Yii::app()->user->can(UserPermissions::Category_Create)) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		
		$model = new Category;


		if (isset($_POST['Category'])) {
			$model->setAttributes($_POST['Category']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		if (!Yii::app()->user->can(UserPermissions::Category_Edit)) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		
		$model = $this->loadModel($id, 'Category');


		if (isset($_POST['Category'])) {
			$model->setAttributes($_POST['Category']);
			if ($model->parent_id == $model->id) $model->parent_id = null;
			
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (!Yii::app()->user->can(UserPermissions::Category_Edit)) throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
		
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$model = $this->loadModel($id, 'Category');
				
			$model->is_deleted = 1;
			$model->save(false);
				
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$model = new Category('search');
		$model->unsetAttributes();

		if (isset($_GET['Category']))
			$model->setAttributes($_GET['Category']);

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		$dataProvider = new CActiveDataProvider('Category');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

}