<?php
/**
 * This is a Yii controller to manage Advertisement in the backend.
 * It supports adding, editing, and deleting Advertisement.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class AdvertisementController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex() {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new Advertisement('search');
		$model->unsetAttributes();

		// Set attributes of gridview filters.
		if (isset($_GET['Advertisement']))
			$model->setAttributes($_GET['Advertisement']);

		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Add new record
	 */
	public function actionCreate() {
		
		// Instantiate a Place model to add new record.
		$model = new Advertisement;


		// On post, fill model with requested data
		if (isset($_POST['Advertisement'])) {
			$model->setAttributes($_POST['Advertisement']);
			$model->dateline = date("Y-m-d H:i:s");

			$uploader = CUploadedFile::getInstance($model,'image_file');

			if ($uploader){
		        $pathinfo = pathinfo($uploader->name);
		        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
		        $ext = $ext == '' ? $ext : '.' . $ext;

	            $uploadName = uniqid("slider_".time(),true) . $ext;
	            $uploadPath = $this->uploadsPath . $uploadName;
	            $uploader->saveAs($uploadPath);

				$fileModel =  new LibFile;
				$fileModel->type = "i";
				$fileModel->lib_cat_id = 2;
				$fileModel->thumb_path = 'file_image.png';
					
				$fileModel->name = $uploader->name;
				$fileModel->path = $uploadName;
				$fileModel->dateline = date("Y-m-d H:i:s");
				$fileModel->user_id = Yii::app()->user->id;
				$fileModel->is_archived = 0;
				
				$fileModel->save(false);

	       		$model->image_id=$fileModel->id;
	       		$model->image_file=$model->image_id;
	       	}

	       	if (!$model->image_id && !$model->is_html){
	       		$model->addError("image_id", "Image is required!");
		    }else{


				$relatedData = array(
					'advertisementPositions' => $_POST['Advertisement']['advertisementPositions'] === '' ? null : $_POST['Advertisement']['advertisementPositions'],
					);

				if ($model->saveWithRelated($relatedData)) {
					if (Yii::app()->getRequest()->getIsAjaxRequest())
						// If the request is ajax, just return the http status 200.
						Yii::app()->end();
					else
						// If the request is not ajax, send redirect flag to add new record.
						$this->redirect(array('create'));
				}
				// If save failed, the model is already populated with the errors in errors propery.
			}
		}

		// Render create view.
		$this->render('create', array( 'model' => $model));
	}

	/**
	 * Edit record
	 *
	 * @param integer ID of the Advertisement to be edited.
	 */
	public function actionUpdate($id) {
		
		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'Advertisement');

		// On post, fill model with requested data
		if (isset($_POST['Advertisement'])) {
			$model->setAttributes($_POST['Advertisement']);

			$uploader = CUploadedFile::getInstance($model,'image_file');

			if ($uploader){
		        $pathinfo = pathinfo($uploader->name);
		        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
		        $ext = $ext == '' ? $ext : '.' . $ext;

	            $uploadName = uniqid("slider_".time(),true) . $ext;
	            $uploadPath = $this->uploadsPath . $uploadName;
	            $uploader->saveAs($uploadPath);

				$fileModel =  new LibFile;
				$fileModel->type = "i";
				$fileModel->lib_cat_id = 2;
				$fileModel->thumb_path = 'file_image.png';
					
				$fileModel->name = $uploader->name;
				$fileModel->path = $uploadName;
				$fileModel->dateline = date("Y-m-d H:i:s");
				$fileModel->user_id = Yii::app()->user->id;
				$fileModel->is_archived = 0;
				
				$fileModel->save(false);

	       		$model->image_id=$fileModel->id;
	       		$model->image_file=$model->image_id;
	       	}

			if (!$model->image_id && !$model->is_html){
	       		$model->addError("image_id", "Image is required!");
		    }else{

				$relatedData = array(
					'advertisementPositions' => $_POST['Advertisement']['advertisementPositions'] === '' ? null : $_POST['Advertisement']['advertisementPositions'],
					);

				if ($model->saveWithRelated($relatedData)) {
					$this->redirect(array('update', 'id' => $model->id));
				}
				// If save failed, the model is already populated with the errors in errors propery.
			}
		}

		// Render update view.
		$this->render('update', array(
				'model' => $model,
				));
	}

	/**
	 * View Advertisement details.
	 *
	 * @param integer ID of the Advertisement to be viewed.
	 */
	public function actionView($id) {
		
		// View place is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}

	/**
	 * Delete Advertisement.
	 *
	 * @param integer ID of the Advertisement to be deleted.
	 */
	public function actionDelete($id) {
		
		// Make sure that method is POST
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			
			// Delete the requested record
			$this->loadModel($id, 'Advertisement')->delete();

		} else
			// Raise an error if the method is not POST
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

}