<?php
/**
 * This is a Yii controller to manage Offer in the backend.
 * It supports adding, editing, and deleting Offer.
 * 
 * @author   Mostafa Ameen <admin@uptop1.com>
 * @package  Backend
 */

class OfferController extends BackMainController {


	/**
	 * Default action to list current records.
	 */
	public function actionIndex() {
		// Instantiate a Place model to generate a DataProvider for gridview.
		$model = new Offer('search');
		$model->unsetAttributes();
		$model->is_deleted=0;

		// Set attributes of gridview filters.
		if (isset($_GET['Offer']))
			$model->setAttributes($_GET['Offer']);

		if (!Yii::app()->user->isManager())
			$model->user_id = Yii::app()->user->id;

		if ($this->action->id != 'pending'){
			$model->is_draft = 0;
		}
		
		// Render index view.
		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionPublish($id) {
		if (!Yii::app()->user->can(UserPermissions::Offer_Publish)) throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		// Load the record of the requested ID.
		$model = $this->loadModel($id, 'Offer');
		
		if ($model->draft_for){
			$mainModel = $this->loadModel($model->draft_for, 'Offer');
			$mainModel->setAttributes($model->attributes);
			$mainModel->is_draft = 0;
			$mainModel->save();

			$model->is_deleted = 1;
			$model->save();
		}else{
			$model->is_draft = 0;
			$model->save();
		}
	}

	public function actionPending() {
		$_GET['Offer']['is_draft'] = 1;
		$this->actionIndex();
	}

	/**
	 * Add new record
	 */
	public function actionCreate() {
		if (!Yii::app()->user->can(UserPermissions::Offer_Create)) throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));
		// Instantiate a Place model to add new record.
		$model = new Offer;


		// On post, fill model with requested data
		if (isset($_POST['Offer'])) {
			$model->is_active = 1; // Active

			$model->setAttributes($_POST['Offer']);
			$model->user_id = Yii::app()->user->id;
			$model->dateline = date("Y-m-d H:i:s");

			if (!Yii::app()->user->can(UserPermissions::Offer_Publish)){
				$model->is_draft = 1; // Inactive
			}

			$uploader = CUploadedFile::getInstance($model,'image_file');

			if ($uploader){
		        $pathinfo = pathinfo($uploader->name);
		        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
		        $ext = $ext == '' ? $ext : '.' . $ext;

	            $uploadName = uniqid("slider_".time(),true) . $ext;
	            $uploadPath = $this->uploadsPath . $uploadName;
	            $uploader->saveAs($uploadPath);

				$fileModel =  new LibFile;
				$fileModel->type = "i";
				$fileModel->lib_cat_id = 2;
				$fileModel->thumb_path = 'file_image.png';
					
				$fileModel->name = $uploader->name;
				$fileModel->path = $uploadName;
				$fileModel->dateline = date("Y-m-d H:i:s");
				$fileModel->user_id = Yii::app()->user->id;
				$fileModel->is_archived = 0;
				
				$fileModel->save(false);

	       		$model->image_id=$fileModel->id;
	       		$model->image_file=$model->image_id;
	       	}

			if ($model->save()) {
				// Save succeeded
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					// If the request is ajax, just return the http status 200.
					Yii::app()->end();
				else
					// If the request is not ajax, send redirect flag to add new record.
					$this->redirect(array('create'));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render create view.
		$this->render('create', array( 'model' => $model));
	}

	/**
	 * Edit record
	 *
	 * @param integer ID of the Offer to be edited.
	 */
	public function actionUpdate($id) {
		if (!Yii::app()->user->can(UserPermissions::Offer_Edit)) throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		// Load the record of the requested ID.
		$mainModel = $model = $this->loadModel($id, 'Offer');

		if (!Yii::app()->user->isManager() && $model->user_id != Yii::app()->user->id)
			throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		$draftModel = null;

		if ($model->is_draft && $model->draft_for){
			$mainModel = Offer::model()->findByPK($model->draft_for);
		}else{
			$criteria = new CDbCriteria;
			$criteria->compare('is_draft', 1);
			$criteria->compare('is_deleted', 0);
			$criteria->compare('user_id', Yii::app()->user->id);
			$criteria->compare('draft_for', $mainModel->id);
			$draftModel = Offer::model()->find($criteria);
			if ($draftModel){
				$model->setAttributes($draftModel->attributes);
				$model->is_draft = 0;
				$model->draft_for = null;
			}
		}


		// On post, fill model with requested data
		if (isset($_POST['Offer'])) {
			if (!$model->is_draft && !Yii::app()->user->can(UserPermissions::Offer_Publish)){
				if ($draftModel)
					$model = $draftModel;
				else
					$model = new Offer;

				$model->setAttributes($mainModel->attributes);
				$model->is_draft = 1; // Inactive
				$model->draft_for = $mainModel->id;
			}

			$model->setAttributes($_POST['Offer']);

			$uploader = CUploadedFile::getInstance($model,'image_file');

			if ($uploader){
		        $pathinfo = pathinfo($uploader->name);
		        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
		        $ext = $ext == '' ? $ext : '.' . $ext;

	            $uploadName = uniqid("slider_".time(),true) . $ext;
	            $uploadPath = $this->uploadsPath . $uploadName;
	            $uploader->saveAs($uploadPath);

				$fileModel =  new LibFile;
				$fileModel->type = "i";
				$fileModel->lib_cat_id = 2;
				$fileModel->thumb_path = 'file_image.png';
					
				$fileModel->name = $uploader->name;
				$fileModel->path = $uploadName;
				$fileModel->dateline = date("Y-m-d H:i:s");
				$fileModel->user_id = Yii::app()->user->id;
				$fileModel->is_archived = 0;
				
				$fileModel->save(false);

	       		$model->image_id=$fileModel->id;
	       		$model->image_file=$model->image_id;
	       	}
			if ($model->save()) {
				// Save succeeded, redirect to edit the same record
				$this->redirect(array('update', 'id' => $model->id));
			}
			// If save failed, the model is already populated with the errors in errors propery.
		}

		// Render update view.
		$this->render('update', array(
				'model' => $model,
				));
	}

	/**
	 * View Offer details.
	 *
	 * @param integer ID of the Offer to be viewed.
	 */
	public function actionView($id) {
		
		// View place is not required here. It's better to use edit instead.
		$this->redirect(array_merge(array('update'), $_GET),true);
	}

	/**
	 * Delete Offer.
	 *
	 * @param integer ID of the Offer to be deleted.
	 */
	public function actionDelete($id) {
		if (!Yii::app()->user->can(UserPermissions::Offer_Delete)) throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));

		// Make sure that method is POST
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			
			// Delete the requested record
			$model = $this->loadModel($id, 'Offer');
			if (!Yii::app()->user->isManager() && $model->user_id != Yii::app()->user->id)
				throw new CHttpException(401, Yii::t('app', 'You are not authorized to perform this operation.'));
			
			$model->is_deleted = 1;
			$model->save();

		} else
			// Raise an error if the method is not POST
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

}