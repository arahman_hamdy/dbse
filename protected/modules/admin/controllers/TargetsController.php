<?php

class TargetsController extends BackMainController {


	public function actionView($id) {
		$this->redirect(array_merge(array('update'), $_GET),true);

		$this->render('view', array(
			'model' => $this->loadModel($id, 'Target'),
		));
	}

	public function actionCreate() {
		$model = new Target;


		if (isset($_POST['Target'])) {
			$model->setAttributes($_POST['Target']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('create'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Target');


		if (isset($_POST['Target'])) {
			$model->setAttributes($_POST['Target']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Target')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$model = new Target('search');
		$model->unsetAttributes();

		if (isset($_GET['Target']))
			$model->setAttributes($_GET['Target']);

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		$dataProvider = new CActiveDataProvider('Target');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

}