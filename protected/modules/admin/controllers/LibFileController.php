<?php

class LibFileController extends BackMainController {

	public function beforeAction($action){
		throw new CHttpException(401, Yii::t('app', 'Permission denied!'));
	
		return parent::beforeAction($action);
	}
	
	public function actionView($id) {
		$this->redirect(array_merge(array('update'), $_GET),true);
		
		$this->render('view', array(
			'model' => $this->loadModel($id, 'LibFile'),
		));
	}

	public function actionCreate() {
		$model = new LibFile;


		if (isset($_POST['LibFile'])) {
			$model->setAttributes($_POST['LibFile']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'LibFile');


		if (isset($_POST['LibFile'])) {
			$model->setAttributes($_POST['LibFile']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'LibFile')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$model = new LibFile('search');
		$model->unsetAttributes();

		if (isset($_GET['LibFile']))
			$model->setAttributes($_GET['LibFile']);

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		$dataProvider = new CActiveDataProvider('LibFile');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}
	
	
}