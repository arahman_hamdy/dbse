<?php

class AdminModule extends CWebModule
{
	
	public $registrationUrl = array("/admin/registration");
	public $recoveryUrl = array("/admin/recovery/recovery");
	public $loginUrl = array("/admin/login");
	public $logoutUrl = array("/admin/logout");
	public $profileUrl = array("/admin/profile");
	public $returnUrl = array("/admin/");
	public $returnLogoutUrl = array("/admin/login");
	
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));
		
		Yii::app()->language = 'en';
		/*if (isset($_GET["language"]) && in_array($_GET["language"],array('ar','en'))){
			Yii::app()->user->setState("language",$_GET["language"]);
			Yii::app()->language = $_GET["language"];
		}else{
			Yii::app()->language = Yii::app()->user->getState("language","en");;
		}*/
			
		Yii::app()->user->loginUrl = array("/admin/login");
		Yii::app()->homeUrl = Yii::app()->createUrl("/admin");
			}

	public function beforeControllerAction($controller, $action)
	{
		if ($controller->id != "login" && (Yii::app()->user->isGuest || (!in_array(Yii::app()->user->roleId,array(1)) && !Yii::app()->user->can(UserPermissions::Is_staff)))){
			Yii::app()->user->logout();
			Yii::app()->user->loginRequired();
			return false;
		}
		
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
	
	public function importYoutubeVideos($pageToken = null){
		$key = "AIzaSyCvqsOsOjPdcoYAp75IiO4OIYIKIK7hTpE";
		$channel = "UC0GpuZFOMX0T5ytLHwpZYhg";
		
		
		$setting_youtube_last_fetch = Settings::model()->find("`key`='youtube_last_fetch'");
		if (!$setting_youtube_last_fetch){
			$setting_youtube_last_fetch = new Settings();
			$setting_youtube_last_fetch->key = 'youtube_last_fetch';
			$setting_youtube_last_fetch->save();
		}
		$setting_youtube_last_video = Settings::model()->find("`key`='youtube_last_video'");
		if (!$setting_youtube_last_video){
			$setting_youtube_last_video = new Settings();
			$setting_youtube_last_video->key = 'youtube_last_video';
			$setting_youtube_last_video->save();
		}
		
		if (!$pageToken && time()-$setting_youtube_last_fetch->value < 60*30) return;
		
		$link = "https://www.googleapis.com/youtube/v3/search?key=$key&channelId=$channel&part=snippet,id&order=date&maxResults=50";
		if ($pageToken)
			$link .= "&pageToken=$pageToken";
		
		$contents = self::get_web_page($link);
		$data = json_decode($contents,true);
		
		if (!isset($data['items']) || !$data['items']){
			return false;
		}
		
		$nextPageToken = null;
		if (isset($data['nextPageToken'])) $data['nextPageToken'];
		
		
		foreach($data['items'] as $video){
			if (!isset($video['id']['videoId'])) continue;
			$video_id = $video['id']['videoId'];
			if ($setting_youtube_last_video->value == $video_id){
				$nextPageToken = null;
				break;
			}
			
			if (isset($video['snippet']['title']))
				$name = $video['snippet']['title'];
			else 
				$name = "YT_$video_id";
			
			if (isset($video['snippet']['title']))
				$dateline = $video['snippet']['publishedAt'];
			else
				$dateline = date("Y-m-d H:i:s");
			
			$fileModel =  new LibFile;
			$fileModel->type = "y";
			$fileModel->thumb_path = 'file_youtube.png';
			$fileModel->lib_cat_id = 3;
			
			$fileModel->name = $name;
			$fileModel->path = $video_id;
			$fileModel->dateline = $dateline;
			$fileModel->user_id = 1;
			$fileModel->is_archived = 1;
			try{
				if (!$fileModel->save()){
					//print_r($fileModel->errors);
				}else{
					$setting_youtube_last_video->value = $video_id;
				}
			}catch (Exception $ex){
				
			}
		}
		$setting_youtube_last_fetch->value = time();
		$setting_youtube_last_fetch->save();
		
		$setting_youtube_last_video->save();
		
		if ($nextPageToken) return $this->importYoutubeVideos($nextPageToken);
	}
		
	public static function get_web_page($url) {
	    $options = array(
	        CURLOPT_RETURNTRANSFER => true,   // return web page
	        CURLOPT_HEADER         => false,  // don't return headers
	        CURLOPT_FOLLOWLOCATION => true,   // follow redirects
	        CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
	        CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
	        CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
	        CURLOPT_TIMEOUT        => 120,    // time-out on response
    		CURLOPT_SSL_VERIFYPEER => false,
    		CURLOPT_SSL_VERIFYHOST => false,
	    ); 
	     
	    $ch = curl_init($url);
	    curl_setopt_array($ch, $options);
	
	    $content  = curl_exec($ch);
	    curl_close($ch);
	
	    return $content;
	}
	
}
