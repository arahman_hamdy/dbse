<?php

$this->breadcrumbs = array(
	LibFile::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . LibFile::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . LibFile::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode(LibFile::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'lib-file-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		'type',
		'name',
		'path',
		'thumb_path',
		array(
				'name'=>'lib_cat_id',
				'value'=>'GxHtml::valueEx($data->libCat)',
				'filter'=>GxHtml::listDataEx(LibCat::model()->findAllAttributes(null, true)),
				),
		/*
		'tags',
		'dateline',
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				'filter'=>GxHtml::listDataEx(Users::model()->findAllAttributes(null, true)),
				),
		array(
					'name' => 'is_archived',
					'value' => '($data->is_archived == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		*/
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{info}{edit}{remove}',
			'buttons'=>array(
				'info'=>array(
					'options'=>array(
						'title'=>Yii::t('app','Details'),
						'class'=>'btn btn-info btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-info"></i> ' . Yii::t('app','View'),
					'url'=>'Yii::app()->controller->createUrl("view", array("id"=>$data->id))',
				),
				'edit'=>array(
					'options'=>array(
						'title'=>Yii::t('app','Edit'),
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> ' . Yii::t('app','Edit'),
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'remove'=>array(
					'options'=>array(
						'title'=>Yii::t('app','Edit'),
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-cancel"></i> ' . Yii::t('app','Delete'),
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#lib-file-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#lib-file-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#lib-file-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
