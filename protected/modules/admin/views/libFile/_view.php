<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('type')); ?>:
	<?php echo GxHtml::encode($data->type); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name')); ?>:
	<?php echo GxHtml::encode($data->name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('path')); ?>:
	<?php echo GxHtml::encode($data->path); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('thumb_path')); ?>:
	<?php echo GxHtml::encode($data->thumb_path); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('lib_cat_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->libCat)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tags')); ?>:
	<?php echo GxHtml::encode($data->tags); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('dateline')); ?>:
	<?php echo GxHtml::encode($data->dateline); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->user)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_archived')); ?>:
	<?php echo GxHtml::encode($data->is_archived); ?>
	<br />
	*/ ?>

</div>