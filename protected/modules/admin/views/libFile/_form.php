<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'lib-file-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>		<div class="form-group">
			<?php echo $form->labelEx($model,'type'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'type', array('maxlength' => 1)); ?>
				<?php echo $form->error($model,'type'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'path'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'path', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'path'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'thumb_path'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'thumb_path', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'thumb_path'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'lib_cat_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'lib_cat_id', GxHtml::listDataEx(LibCat::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'lib_cat_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'tags'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'tags', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'tags'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'dateline'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'dateline'); ?>
				<?php echo $form->error($model,'dateline'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'user_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(Users::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'user_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'is_archived'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'is_archived'); ?>
				<?php echo $form->error($model,'is_archived'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->