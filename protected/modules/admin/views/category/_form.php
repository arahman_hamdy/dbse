<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'category-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>		<div class="form-group">
			<?php echo $form->labelEx($model,'parent_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'parent_id', GxHtml::listDataEx(Category::model()->findAllAttributes(null, true)),array("prompt"=>"-")); ?>
				<?php echo $form->error($model,'parent_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'title'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'title', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'title'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'icon'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'icon', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'icon'); ?>
			</div>
		</div>
		<?php /*?> 
		<div class="form-group">
			<?php echo $form->labelEx($model,'thumb_file_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'thumb_file_id', GxHtml::listDataEx(LibFile::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'thumb_file_id'); ?>
			</div>
		</div>
		<?php */ ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'sortid'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sortid'); ?>
				<?php echo $form->error($model,'sortid'); ?>
			</div>
		</div>
	<div class="panel panel-gray inline-panel" data-collapsed="0">
	
	<div class="panel-heading">
		<div class="panel-title">
			<?php echo Yii::t("admin","SEO info");?>
		</div>
		
		<div class="panel-options">
			<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
		</div>
	</div>
		
	<div class="panel-body">
						
		
		<div class="form-group">
			<?php echo $form->labelEx($model,'keywords'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'keywords', array('maxlength' => 300,'class'=>'tagsinput'));?>
				<div class="label label-primary clear"><?php echo Yii::t("admin","Info");?></div> <small><?php echo Yii::t("admin",'Press "Enter" after each word to commit');?></small><div class="clear"></div>
				<?php echo $form->error($model,'keywords'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'description'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'description', array('maxlength' => 300,'class'=>'autogrow')); ?>
				<?php echo $form->error($model,'description'); ?>
			</div>
		</div>
	</div>
	</div>
<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->