<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'blog-category-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>	<?php if ($model->errors): ?>
		<div class="alert alert-danger">
			<?php echo $form->errorSummary($model); ?>
		</div>
	<?php endif; ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
				<?php echo $form->error($model,'name_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_en', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'name_en'); ?>
			</div>
		</div>
		<!--
		<div class="form-group">
			<?php echo $form->labelEx($model,'parent_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'parent_id', array(''=>'') + GxHtml::listDataEx(BlogCategory::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'parent_id'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'is_deleted'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'is_deleted'); ?>
				<?php echo $form->error($model,'is_deleted'); ?>
			</div>
		</div>
		-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'sortid'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sortid'); ?>
				<?php echo $form->error($model,'sortid'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->