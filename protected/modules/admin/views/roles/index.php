<?php

$this->breadcrumbs = array(
	Role::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Role::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Role::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode(Role::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'role-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		'name',
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{remove}',
			'buttons'=>array(
				'info'=>array(
					'options'=>array(
						'title'=>Yii::t('app','Details'),
						'class'=>'btn btn-info btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-info"></i> ' . Yii::t('app','View'),
					'url'=>'Yii::app()->controller->createUrl("view", array("id"=>$data->id))',
				),
				'edit'=>array(
					'visible'=>'$data->id!=1',
					'options'=>array(
						'title'=>Yii::t('app','Edit'),
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> ' . Yii::t('app','Edit'),
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'remove'=>array(
					'visible'=>'$data->id>3',
					'options'=>array(
						'title'=>Yii::t('app','Edit'),
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-cancel"></i> ' . Yii::t('app','Delete'),
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#role-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#role-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#role-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
