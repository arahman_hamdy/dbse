<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'role-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>
		<?php echo $form->errorSummary($model); ?>
		
		<div class="form-group">
			<?php echo $form->labelEx($model,'name'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
		</div>

        <div class="form-group"> 
            <?php echo $form->labelEx($model,'general_administration'); ?>
            <div class="col-sm-5"> 
                <?php echo $form->checkBox($model, 'general_administration'); ?>
                <?php echo $form->error($model,'general_administration'); ?>
            </div> 
        </div> 

		<div class="form-group">
			<?php echo $form->labelEx($model,'permissions'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBoxList($model,'permissions',GxHtml::listDataEx(Permission::model()->findAllAttributes(null, true))) ?>
				<?php echo $form->error($model,'permissions'); ?>
			</div>
		</div>
		
		
				
		
		
<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->