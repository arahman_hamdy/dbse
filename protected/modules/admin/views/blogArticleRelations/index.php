<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index','articleId'=>$model->article_id),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . BlogArticleRelations::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . BlogArticleRelations::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode((($model->article)?$model->article . ' > ':null) . BlogArticleRelations::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'blog-article-relations-grid',
	'dataProvider' => $model->search(),
	'additionalButtons' => array($this->createUrl('create',!empty($model->article)?array('articleId'=>$model->article_id):array())=>"Add New"),	
	'show_create_button' => false,
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		array(
				'name'=>'article_id',
				'value'=>'GxHtml::valueEx($data->article)',
				'filter'=>GxHtml::listDataEx(BlogArticle::model()->findAllAttributes(null, true)),
				),
		'type',
		'referenced_id',
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{remove}',
			'buttons'=>array(
				'edit'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> Edit',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'remove'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-cancel"></i> Delete',
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#blog-article-relations-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#blog-article-relations-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#blog-article-relations-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
