<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'blog-article-relations-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>
		<?php if ($model->errors): ?>
			<div class="alert alert-danger">
				<?php echo $form->errorSummary($model); ?>
			</div>
		<?php endif; ?>
		<?php echo $form->hiddenField($model, 'article_id'); ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'type'); ?>
			<div class="col-sm-5">
				<?php echo $form->enumDropDownList($model, 'type'); ?>
				<?php echo $form->error($model,'type'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'referenced_id'); ?>
			<div class="col-sm-5">
				<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
					'name'=>'autocomplete_article',
					'value'=>$model->referenced_id,
					'source'=>"js:function(request, response) {

			$.ajax({
                url: '" . $this->createUrl('find') . "',
                dataType: 'json',
                data: {
                    term: request.term,
                    type: $('#BlogArticleRelations_type').val(), 
                },
                success: function(data) {
                    response(data);
                }
            });						
  }",
					'htmlOptions'=>array('class'=>'form-control'),
					// additional javascript options for the autocomplete plugin
					'options'=>array(
						'minLength'=>2,
						'change'=> 'js:function( event, ui ) {
								jQuery( "#'.CHtml::activeId($model,'article_id').'" ).val( ui.item? ui.item.id : 0 );
								jQuery("#autocomplete_article").val( ui.item? ui.item.value : "" );
							}',
				        'select'=>'js:function( event, ui ) {
				            jQuery("#'.CHtml::activeId($model,'article_id').'").val(ui.item.id);
				            return true;
				        }'
					),
				));?>

				<?php //echo $form->textField($model, 'referenced_id'); ?>
				<?php echo $form->error($model,'referenced_id'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->