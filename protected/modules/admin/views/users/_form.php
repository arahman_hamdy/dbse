<?php $this->renderPartial('//layouts/form_includes'); ?>
<?php
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/js/ckeditor/ckeditor.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/js/ckeditor/adapters/jquery.js', CClientScript::POS_END);
?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'users-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>
		<?php echo $form->errorSummary($model); ?>
		
		<?php if ($model->isNewRecord): ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'username'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'username', array('maxlength' => 20)); ?>
				<?php echo $form->error($model,'username'); ?>
			</div>
		</div>
		<?php endif; ?>
		
		<div class="form-group">
			<?php echo $form->labelEx($model,'password'); ?>
			<div class="col-sm-5">
				<?php echo $form->passwordField($model, 'password', array('maxlength' => 128,'value'=>'')); ?>
				<?php echo $form->error($model,'password'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'email'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'email', array('maxlength' => 128)); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'role_id'); ?>
			<div class="col-sm-5">
				<?php
					$roles = GxHtml::listDataEx(Role::model()->findAllAttributes(null, true));
					if (Yii::app()->user->RoleId != 1) unset($roles[1]);
				?>
				<?php echo $form->dropDownList($model, 'role_id', $roles); ?>
				<?php echo $form->error($model,'role_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'status'); ?>
			<div class="col-sm-5">
				<?php echo $form->radioButtonList($model, 'status', array(0=>Yii::t("UserModule","Inactive"),1=>Yii::t("UserModule","Active"))); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
		</div>
		
		
<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->