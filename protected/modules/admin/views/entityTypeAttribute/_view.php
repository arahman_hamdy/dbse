<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('entity_type_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->entityType)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('attribute_type_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->attributeType)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_ar')); ?>:
	<?php echo GxHtml::encode($data->name_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_en')); ?>:
	<?php echo GxHtml::encode($data->name_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_searchable')); ?>:
	<?php echo GxHtml::encode($data->is_searchable); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sortid')); ?>:
	<?php echo GxHtml::encode($data->sortid); ?>
	<br />

</div>