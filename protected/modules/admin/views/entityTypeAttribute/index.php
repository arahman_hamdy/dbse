<?php

$this->breadcrumbs = array(
	EntityTypeAttribute::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . EntityTypeAttribute::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . EntityTypeAttribute::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode( (($model->entityType)?$model->entityType . ' > ':null) . EntityTypeAttribute::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'entity-type-attribute-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'show_create_button' => false,
	'additionalButtons' => array($this->createUrl('create',!empty($model->entityType)?array('entityId'=>$model->entity_type_id):array())=>"Add new"),	
	'columns' => array(
		'id',
		array(
				'name'=>'entity_type_id',
				'value'=>'GxHtml::valueEx($data->entityType)',
				'filter'=>GxHtml::listDataEx(EntityType::model()->active()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'attribute_type_id',
				'value'=>'GxHtml::valueEx($data->attributeType)',
				'filter'=>GxHtml::listDataEx(AttributeType::model()->findAllAttributes(null, true)),
				),
		'name_en',
		array(
					'name' => 'is_searchable',
					'value' => '($data->is_searchable == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'sortid',
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{remove}',
			'buttons'=>array(
				'edit'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> Edit',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'remove'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-cancel"></i> Delete',
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#entity-type-attribute-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#entity-type-attribute-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#entity-type-attribute-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
