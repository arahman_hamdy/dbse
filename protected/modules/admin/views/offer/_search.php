<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'place_id'); ?>
		<?php echo $form->dropDownList($model, 'place_id', GxHtml::listDataEx(Place::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'name_ar'); ?>
		<?php echo $form->textField($model, 'name_ar', array('maxlength' => 50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'name_en'); ?>
		<?php echo $form->textField($model, 'name_en', array('maxlength' => 50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'description_ar'); ?>
		<?php echo $form->textArea($model, 'description_ar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'description_en'); ?>
		<?php echo $form->textArea($model, 'description_en'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'image_id'); ?>
		<?php echo $form->dropDownList($model, 'image_id', GxHtml::listDataEx(LibFile::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'expiration_date'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'expiration_date',
			'value' => $model->expiration_date,
			'htmlOptions' => array('class' => 'form-control'),
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(Users::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'likes'); ?>
		<?php echo $form->textField($model, 'likes'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'reports'); ?>
		<?php echo $form->textField($model, 'reports'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'dateline'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'dateline',
			'value' => $model->dateline,
			'htmlOptions' => array('class' => 'form-control'),
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'is_active'); ?>
		<?php echo $form->dropDownList($model, 'is_active', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'is_deleted'); ?>
		<?php echo $form->dropDownList($model, 'is_deleted', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
