<?php

$this->breadcrumbs = array(
	Offer::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Offer::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Offer::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo (($this->action->id=='pending')?"Pending ":"") . GxHtml::encode(Offer::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php 
	$dataProvider = $model->search();
	$dataProvider->criteria->order = "id desc";

	$this->widget('CGridViewCustom', array(
	'id' => 'offer-grid',
	'dataProvider' => $dataProvider,
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		'name_ar',
		'name_en',
		array(
				'name'=>'place_id',
				'value'=>'GxHtml::valueEx($data->place)',
				),
		'expiration_date',
		/*
		array(
				'name'=>'image_id',
				'value'=>'GxHtml::valueEx($data->image)',
				'filter'=>GxHtml::listDataEx(LibFile::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				'filter'=>GxHtml::listDataEx(Users::model()->findAllAttributes(null, true)),
				),
		'likes',
		'reports',
		'dateline',
		array(
					'name' => 'is_active',
					'value' => '($data->is_active == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		array(
					'name' => 'is_deleted',
					'value' => '($data->is_deleted == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		*/
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{remove}{publish}',
			'buttons'=>array(
				'edit'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'visible'=>(string)Yii::app()->user->can(UserPermissions::Offer_Edit),
					'label'=>'<i class="entypo-pencil"></i> Edit',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'publish'=>array(
					'options'=>array(
						'title'=>'Publish',
						'class'=>'btn btn-default btn-info btn-sm btn-icon icon-left btn-publish',
					),
					'visible'=>'$data->is_draft && ' . (string)Yii::app()->user->can(UserPermissions::BlogArticle_Publish),
					'label'=>'<i class="entypo-check"></i> Publish',
					'url'=>'Yii::app()->controller->createUrl("publish", array("id"=>$data->id))',
				),
				'remove'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'visible'=>(string)Yii::app()->user->can(UserPermissions::Offer_Delete),
					'label'=>'<i class="entypo-cancel"></i> Delete',
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#offer-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#offer-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#offer-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});


		jQuery(document).on('click','#offer-grid a.btn-publish',function() {
			if(!confirm('Are you sure you want to publish this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#offer-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#offer-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
