<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('place_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->place)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_ar')); ?>:
	<?php echo GxHtml::encode($data->name_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_en')); ?>:
	<?php echo GxHtml::encode($data->name_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('description_ar')); ?>:
	<?php echo GxHtml::encode($data->description_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('description_en')); ?>:
	<?php echo GxHtml::encode($data->description_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('image_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->image)); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('expiration_date')); ?>:
	<?php echo GxHtml::encode($data->expiration_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->user)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('likes')); ?>:
	<?php echo GxHtml::encode($data->likes); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('reports')); ?>:
	<?php echo GxHtml::encode($data->reports); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateline')); ?>:
	<?php echo GxHtml::encode($data->dateline); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_active')); ?>:
	<?php echo GxHtml::encode($data->is_active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_deleted')); ?>:
	<?php echo GxHtml::encode($data->is_deleted); ?>
	<br />
	*/ ?>

</div>