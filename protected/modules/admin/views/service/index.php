<?php

$this->breadcrumbs = array(
	Service::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Service::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Service::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode((($model->place)?$model->place . ' > ':null) . Service::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php 
	$dataProvider = $model->search();
	$dataProvider->criteria->order = "id desc";
	
	$this->widget('CGridViewCustom', array(
	'id' => 'service-grid',
	'dataProvider' => $dataProvider,
	'additionalButtons' => array($this->createUrl('create',!empty($model->place)?array('id'=>$model->place_id):array())=>"Add New"),	
	'show_create_button' => false,
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		array(
				'name'=>'place_id',
				'value'=>'GxHtml::valueEx($data->place)',
				),
		'name_ar',
		'name_en',
		'price',
		'sortid',
		/*
		array(
					'name' => 'is_active',
					'value' => '($data->is_active == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		array(
					'name' => 'is_deleted',
					'value' => '($data->is_deleted == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		*/
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{remove}',
			'buttons'=>array(
				'edit'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> Edit',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'remove'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-cancel"></i> Delete',
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#service-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#service-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#service-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
