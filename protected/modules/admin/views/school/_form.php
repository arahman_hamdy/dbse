<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'school-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>
    <?php if ($model->errors): ?>
        <div class="alert alert-danger">
 
            <?php echo $form->errorSummary($model); ?>
        </div>
    <?php endif; ?>

		<div class="form-group">
			<?php echo $form->labelEx($model,'name_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_ar', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'name_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_en', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'name_en'); ?>
			</div>
		</div>

        <div class="form-group form-head">
            <span><?php echo Yii::t("app", "Location"); ?></span>
        </div>
        
        <div class="form-group">
            <?php echo $form->labelEx($model,'country_id'); ?>
            <div class="col-sm-5">
                <?php echo $form->dropDownList($model, 'country_id', GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true))); ?>
                <?php echo $form->error($model,'country_id'); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'city'); ?>
            <div class="col-sm-5">
                <?php echo $form->dropDownList($model, 'city', GxHtml::listDataEx(City::model()->findAllAttributes(null, true)),array('' => '-')); ?>
                <?php echo $form->error($model,'city'); ?>
            </div>
        </div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'region_id'); ?>
			<div class="col-sm-5">
                <?php echo $form->dropDownList($model, 'region_id',array('' => '-')); ?>
				<?php echo $form->error($model,'region_id'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'address_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'address_ar', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'address_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'address_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'address_en', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'address_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'lat'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'lat'); ?>
				<?php echo $form->error($model,'lat'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'lng'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'lng'); ?>
				<?php echo $form->error($model,'lng'); ?>
			</div>
		</div>

        <div class="form-group form-head">
            <span><?php echo Yii::t("app", "Images"); ?></span>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'schoolImages'); ?>
            <div class="col-sm-9">
        <?php 
        $url=$this->createUrl('deleteImage');
        $this->widget('ext.EFineUploader.EFineUploader',
         array(
           'id'=>'FineUploader',
           'config'=>array(
           'autoUpload'=>true,
           'request'=>array(
              'endpoint'=>$this->createUrl('upload'),//'/files/upload',// OR ,
              'params'=>array('YII_CSRF_TOKEN'=>Yii::app()->request->csrfToken),
                           ),
           'retry'=>array('enableAuto'=>true,'preventRetryResponseProperty'=>true),
           'chunking'=>array('enable'=>true,'partSize'=>100),//bytes
            'template'=>'<div class="qq-drop-processing"></div><div class="qq-upload-drop-area"></div>
                    <div class="qq-uploader span4 left"><div class="qq-upload-button btn left">' . Yii::t('app','Upload image') . '</div>
                    <div class="clear"></div>
                    <ul class="qq-upload-list"></ul><br/></div>',
                           
           'callbacks'=>array(
                'onComplete'=>"js:function(id, name, response){
                   if(response.success){
                    $('#place_images').append(
                        $('<div />')
                            .addClass('image')
                            .append($('<span />').addClass('filename').append($('<label />')
                                .append($('<input />').attr('name','School[main_image_id]').attr('value',response.id).attr('type','radio'))
                                .append(' " . Yii::t('app','Default') . "')
                                ))
                            .append($('<span />').addClass('close').html('X').click(function(){
                             if (!confirm('Are you sure you want to delete this image?')) return false;                             
                             $.ajax({
                                url: '". $url . "',
                                type: 'POST',
                                data: { 'id': response.id },
                                success: function(data){}
                            });
                            \$(this).parent().remove()}))
                            .append($('<img />').attr('src',response.folder + response.filename))
                            .append($('<input />').attr('type','hidden').attr('value',response.id).attr('name','School[libFiles][]'))
                        );
                   }
               }",
                'onError'=>"js:function(id, name, errorReason){}",
                 ),
           'validation'=>array(
                     'allowedExtensions'=>array('jpg','jpeg','png','gif'),
                     'sizeLimit'=>2 * 1024 * 1024,//maximum file size in bytes
                     //'minSizeLimit'=>2*1024*1024,// minimum file size in bytes
                              ),
          )
          ));
     
            ?>
            <?php echo $form->error($model,'main_image_id'); ?>
        
            <div id="place_images">
                <?php
                $imagesUrlPath = $this->uploadsUrl;
                $imagesArray = array();

                if ($model->mainImage){
                	$imagesArray[$model->main_image_id] = $model->mainImage;
                }
 
 
                if (!empty($_POST['School']['libFiles'])){
                    $AdditionalFiles = $_POST['School']['libFiles'];
 
                    $criteria = new CDbCriteria;
                    $criteria->addInCondition('id',$AdditionalFiles);
                    $AdditionalImages = LibFile::model()->findAll($criteria);
 
                    foreach ($AdditionalImages as $image) {
                        $imagesArray[$image->id] = $image;
                    }
                }else{
                    foreach ($model->schoolImages as $schoolImage) {
                        $imagesArray[$schoolImage->image_id] = $schoolImage->image;
                    }                   
                }
 
                foreach($imagesArray as $image){
                    ?>
                    <div class='image'>
                    <span class='filename'><label><input type='radio' name='School[main_image_id]' value='<?php echo $image->id;?>' <?php if ($image->id==$model->main_image_id)echo " checked='checked' "; ?> /> <?php echo Yii::t('app','Default');?></label></span>
                    <span class='close'>X</span>
                    <img src='<?php echo "$imagesUrlPath{$image->path}"; ?>' />
                    <input type='hidden' value='<?php echo $image->id;?>' name='School[libFiles][]' />
                    </div>
                    <?php 
                }
                
            
                
                if (count($imagesArray))
                {   //Yii::app()->clientScript->registerScript('images_close','$("#place_images span.close").click(function(){
                    //$(this).parent().remove()});');
                    $url = CJavaScript::encode($this->createUrl('deleteImage'));
                    Yii::app()->clientScript->registerScript('images_close','$("#place_images span.close").click(function(){
                        if (!confirm("Are you sure you want to delete this image?")) return false;
                     $.ajax({
                        url: '. $url . ',
                        type: "POST",
                        data: { "id": $(this).parent().children("input:hidden").val()},
                        success: function(data){}
                    });
                    $(this).parent().remove();
                    
                    
                    });');
                }
                    ?>
            </div>
            <div class="clear"></div>
 
 
 
 
        </div>
        </div>

        <div class="form-group form-head">
            <span><?php echo Yii::t("app", "Contact details"); ?></span>
        </div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'phone'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'phone', array('maxlength' => 500)); ?>
				<?php echo $form->error($model,'phone'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'mobile'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'mobile', array('maxlength' => 500)); ?>
				<?php echo $form->error($model,'mobile'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'fax'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'fax', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'fax'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'website'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'website', array('maxlength' => 400)); ?>
				<?php echo $form->error($model,'website'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'facebook'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'facebook', array('maxlength' => 500)); ?>
				<?php echo $form->error($model,'facebook'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'email'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'email', array('maxlength' => 500)); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'founded'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'founded'); ?>
				<?php echo $form->error($model,'founded'); ?>
			</div>
		</div>

        <div class="form-group form-head">
            <span><?php echo Yii::t("app", "Classification"); ?></span>
        </div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'arabic'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'arabic'); ?>
				<?php echo $form->error($model,'arabic'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'languages'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'languages'); ?>
				<?php echo $form->error($model,'languages'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'azhar'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'azhar'); ?>
				<?php echo $form->error($model,'azhar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'islamic'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'islamic'); ?>
				<?php echo $form->error($model,'islamic'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'sisters'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'sisters'); ?>
				<?php echo $form->error($model,'sisters'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'hotel'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'hotel'); ?>
				<?php echo $form->error($model,'hotel'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'international'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'international'); ?>
				<?php echo $form->error($model,'international'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'british'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'british'); ?>
				<?php echo $form->error($model,'british'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'american'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'american'); ?>
				<?php echo $form->error($model,'american'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'french'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'french'); ?>
				<?php echo $form->error($model,'french'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'germany'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'germany'); ?>
				<?php echo $form->error($model,'germany'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'ib'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'ib'); ?>
				<?php echo $form->error($model,'ib'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'turkish'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'turkish'); ?>
				<?php echo $form->error($model,'turkish'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'sudanese'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'sudanese'); ?>
				<?php echo $form->error($model,'sudanese'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'libyan'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'libyan'); ?>
				<?php echo $form->error($model,'libyan'); ?>
			</div>
		</div>

        <div class="form-group form-head">
            <span><?php echo Yii::t("app", "Application"); ?></span>
        </div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'application_start'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'application_start', array('maxlength' => 6)); ?>
				<?php echo $form->error($model,'application_start'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'application_end'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'application_end', array('maxlength' => 17)); ?>
				<?php echo $form->error($model,'application_end'); ?>
			</div>
		</div>

        <div class="form-group form-head">
            <span><?php echo Yii::t("app", "Expenses"); ?></span>
        </div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'kg_1'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'kg_1', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'kg_1'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'kg_2'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'kg_2', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'kg_2'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'pri_1'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'pri_1', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'pri_1'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'pri_2'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'pri_2', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'pri_2'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'pri_3'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'pri_3', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'pri_3'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'pri_4'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'pri_4', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'pri_4'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'pri_5'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'pri_5', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'pri_5'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'pri_6'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'pri_6', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'pri_6'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'prep_1'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'prep_1', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'prep_1'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'prep_2'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'prep_2', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'prep_2'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'prep_3'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'prep_3', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'prep_3'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'sec_1'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sec_1', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'sec_1'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'sec_2'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sec_2', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'sec_2'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'sec_3'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sec_3', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'sec_3'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'transportation'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'transportation', array('maxlength' => 157)); ?>
				<?php echo $form->error($model,'transportation'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'notes_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'notes_ar'); ?>
				<?php echo $form->error($model,'notes_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'notes_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'notes_en'); ?>
				<?php echo $form->error($model,'notes_en'); ?>
			</div>
		</div>

        <div class="form-group form-head">
            <span><?php echo Yii::t("app", "Certificates"); ?></span>
        </div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'ncacs'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'ncacs'); ?>
				<?php echo $form->error($model,'ncacs'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'cita'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'cita'); ?>
				<?php echo $form->error($model,'cita'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'msa'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'msa'); ?>
				<?php echo $form->error($model,'msa'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'sis'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'sis'); ?>
				<?php echo $form->error($model,'sis'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'naas'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'naas'); ?>
				<?php echo $form->error($model,'naas'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'madonna'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'madonna'); ?>
				<?php echo $form->error($model,'madonna'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'manhattan'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'manhattan'); ?>
				<?php echo $form->error($model,'manhattan'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'advanced'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'advanced'); ?>
				<?php echo $form->error($model,'advanced'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'cambridge'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'cambridge'); ?>
				<?php echo $form->error($model,'cambridge'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'iso'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'iso'); ?>
				<?php echo $form->error($model,'iso'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'naqaae'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'naqaae'); ?>
				<?php echo $form->error($model,'naqaae'); ?>
			</div>
		</div>
<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->
<script>
var defaultRegion = <?php echo (int)$model->region_id; ?>;
var isRequestingRegions = false;
jQuery("#School_city").change(function(){
    if (isRequestingRegions) return;
    jQuery("#School_city").attr("disabled","disabled");
    jQuery("#School_region_id").attr("disabled","disabled");
 
    jQuery.ajax({
            url: "<?php echo $this->createUrl("getCityRegions") ?>/id/" + jQuery("#School_city").val(),
            type: "get",
            dataType: "json",
            success: function (response) {
                jQuery("#School_region_id option.generated").remove();
                for(i in response){
                    var option = jQuery("<option>").addClass("generated").val(i).html(response[i]);
                    if (option.val()==defaultRegion) option.attr("selected","selected");
                    jQuery("#School_region_id").append(option);
                }
            },
            complete: function(jqXHR, textStatus, errorThrown) {
                jQuery("#School_city").removeAttr("disabled");
                jQuery("#School_city").selectBoxIt("refresh");
 
                jQuery("#School_region_id").removeAttr("disabled");
                jQuery("#School_region_id").selectBoxIt("refresh");
 
            }
 
 
        });
});
jQuery("#School_city").trigger("change");
</script>
