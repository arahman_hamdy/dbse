<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('name_ar')); ?>:
	<?php echo GxHtml::encode($data->name_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_en')); ?>:
	<?php echo GxHtml::encode($data->name_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('region_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->region)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('address_ar')); ?>:
	<?php echo GxHtml::encode($data->address_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('address_en')); ?>:
	<?php echo GxHtml::encode($data->address_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('lat')); ?>:
	<?php echo GxHtml::encode($data->lat); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('lng')); ?>:
	<?php echo GxHtml::encode($data->lng); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('phone')); ?>:
	<?php echo GxHtml::encode($data->phone); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('mobile')); ?>:
	<?php echo GxHtml::encode($data->mobile); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fax')); ?>:
	<?php echo GxHtml::encode($data->fax); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('website')); ?>:
	<?php echo GxHtml::encode($data->website); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('facebook')); ?>:
	<?php echo GxHtml::encode($data->facebook); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('email')); ?>:
	<?php echo GxHtml::encode($data->email); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('founded')); ?>:
	<?php echo GxHtml::encode($data->founded); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('arabic')); ?>:
	<?php echo GxHtml::encode($data->arabic); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('languages')); ?>:
	<?php echo GxHtml::encode($data->languages); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('azhar')); ?>:
	<?php echo GxHtml::encode($data->azhar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('islamic')); ?>:
	<?php echo GxHtml::encode($data->islamic); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sisters')); ?>:
	<?php echo GxHtml::encode($data->sisters); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('hotel')); ?>:
	<?php echo GxHtml::encode($data->hotel); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('international')); ?>:
	<?php echo GxHtml::encode($data->international); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('british')); ?>:
	<?php echo GxHtml::encode($data->british); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('american')); ?>:
	<?php echo GxHtml::encode($data->american); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('french')); ?>:
	<?php echo GxHtml::encode($data->french); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('germany')); ?>:
	<?php echo GxHtml::encode($data->germany); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ib')); ?>:
	<?php echo GxHtml::encode($data->ib); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('turkish')); ?>:
	<?php echo GxHtml::encode($data->turkish); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sudanese')); ?>:
	<?php echo GxHtml::encode($data->sudanese); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('libyan')); ?>:
	<?php echo GxHtml::encode($data->libyan); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('application_start')); ?>:
	<?php echo GxHtml::encode($data->application_start); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('application_end')); ?>:
	<?php echo GxHtml::encode($data->application_end); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('kg_1')); ?>:
	<?php echo GxHtml::encode($data->kg_1); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('kg_2')); ?>:
	<?php echo GxHtml::encode($data->kg_2); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('pri_1')); ?>:
	<?php echo GxHtml::encode($data->pri_1); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('pri_2')); ?>:
	<?php echo GxHtml::encode($data->pri_2); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('pri_3')); ?>:
	<?php echo GxHtml::encode($data->pri_3); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('pri_4')); ?>:
	<?php echo GxHtml::encode($data->pri_4); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('pri_5')); ?>:
	<?php echo GxHtml::encode($data->pri_5); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('pri_6')); ?>:
	<?php echo GxHtml::encode($data->pri_6); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('prep_1')); ?>:
	<?php echo GxHtml::encode($data->prep_1); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('prep_2')); ?>:
	<?php echo GxHtml::encode($data->prep_2); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('prep_3')); ?>:
	<?php echo GxHtml::encode($data->prep_3); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sec_1')); ?>:
	<?php echo GxHtml::encode($data->sec_1); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sec_2')); ?>:
	<?php echo GxHtml::encode($data->sec_2); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sec_3')); ?>:
	<?php echo GxHtml::encode($data->sec_3); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('transportation')); ?>:
	<?php echo GxHtml::encode($data->transportation); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('notes_ar')); ?>:
	<?php echo GxHtml::encode($data->notes_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('notes_en')); ?>:
	<?php echo GxHtml::encode($data->notes_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ncacs')); ?>:
	<?php echo GxHtml::encode($data->ncacs); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cita')); ?>:
	<?php echo GxHtml::encode($data->cita); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('msa')); ?>:
	<?php echo GxHtml::encode($data->msa); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sis')); ?>:
	<?php echo GxHtml::encode($data->sis); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('naas')); ?>:
	<?php echo GxHtml::encode($data->naas); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('madonna')); ?>:
	<?php echo GxHtml::encode($data->madonna); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('manhattan')); ?>:
	<?php echo GxHtml::encode($data->manhattan); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('advanced')); ?>:
	<?php echo GxHtml::encode($data->advanced); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cambridge')); ?>:
	<?php echo GxHtml::encode($data->cambridge); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('iso')); ?>:
	<?php echo GxHtml::encode($data->iso); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('naqaae')); ?>:
	<?php echo GxHtml::encode($data->naqaae); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateline')); ?>:
	<?php echo GxHtml::encode($data->dateline); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_deleted')); ?>:
	<?php echo GxHtml::encode($data->is_deleted); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('visits')); ?>:
	<?php echo GxHtml::encode($data->visits); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('country_id')); ?>:
	<?php echo GxHtml::encode($data->country_id); ?>
	<br />
	*/ ?>

</div>