<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'transaction-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>
	<?php if ($model->errors): ?>
		<div class="alert alert-danger">
			<?php echo $form->errorSummary($model); ?>
		</div>
	<?php endif; ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'user_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'user_id', array(''=>'') + GxHtml::listDataEx(Users::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'user_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'type'); ?>
			<div class="col-sm-5">
				<?php echo $form->enumDropDownList($model, 'type'); ?>
				<?php echo $form->error($model,'type'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'reference'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'reference'); ?>
				<?php echo $form->error($model,'reference'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'amount'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'amount'); ?>
				<?php echo $form->error($model,'amount'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'status'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'status'); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'info'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'info'); ?>
				<?php echo $form->error($model,'info'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'dateline'); ?>
			<div class="col-sm-5">
				<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'dateline',
			'value' => $model->dateline,
			'htmlOptions' => array('class' => 'form-control'),
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				'format' => 'yyyy-mm-dd',
				),
			));
; ?>
				<?php echo $form->error($model,'dateline'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->