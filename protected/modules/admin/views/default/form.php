<?php
$baseUrl = Yii::app()->baseUrl;
$themeUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($themeUrl.'/assets/js/selectboxit/jquery.selectBoxIt.css');
$cs->registerCssFile($themeUrl.'/assets/js/select2/select2.css');
$cs->registerCssFile($themeUrl.'/assets/js/selectboxit/jquery.selectBoxIt.css');
$cs->registerCssFile($themeUrl.'/assets/js/icheck/skins/minimal/_all.css');

$cs->registerScriptFile($themeUrl.'/assets/js/select2/select2.min.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/bootstrap-datepicker.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/icheck/icheck.min.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/selectboxit/jquery.selectBoxIt.min.js', CClientScript::POS_END);
?>
<ol class="breadcrumb bc-3" >
	<li>
		<a href="index.html"><i class="fa-home"></i>Home</a>
	</li>
	<li>
		<a href="forms-main.html">Forms</a>
	</li>
	<li class="active">

		<strong>Basic Elements</strong>
	</li>
</ol>

<h2>Basic Form Elements</h2>
<br />
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-body">
				<form class="form-horizontal form-groups-bordered">
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Field 1</label>
						<div class="col-sm-5">
							<input type="text" class="form-control datepicker" id="field-1" placeholder="Placeholder">
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Password</label>
						<div class="col-sm-5">
							<input type="password" class="form-control" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Textarea</label>
						<div class="col-sm-5">
							<textarea class="form-control autogrow" placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Select List</label>
						<div class="col-sm-5">
							<select class="selectboxit" data-first-option="false">
								<option></option>
								<option>Option 1</option>
								<option>Option 2</option>
								<option>Option 3</option>
								<option>Option 4</option>
								<option>Option 5</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Checkbox List</label>
						<div class="col-sm-5">
							<ul class="icheck-list">
								<li>
									<input type="checkbox" class="icheck" id="minimal-checkbox-1">
									<label for="minimal-checkbox-1">Checkbox 1</label>
								</li>
								<li>
									<input type="checkbox" class="icheck" id="minimal-checkbox-2">
									<label for="minimal-checkbox-2">Checkbox 2</label>
								</li>
							</ul>
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Radio List</label>
						<div class="col-sm-5">
							<ul class="icheck-list">
								<li>
									<input type="radio" name="radio" class="icheck" id="minimal-radio-1">
									<label for="minimal-radio-1">Radio 1</label>
								</li>
								<li>
									<input type="radio" name="radio" class="icheck" id="minimal-radio-2">
									<label for="minimal-radio-2">Radio 2</label>
								</li>
							</ul>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>