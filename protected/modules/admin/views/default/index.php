<?php /* @var $this Controller */
$baseUrl = Yii::app()->baseUrl;
$themeUrl = Yii::app()->theme->baseUrl;

$criteria = new CDbCriteria;
$usersCount = Users::model()->cache(60)->count($criteria);

$placesCount = 0;

$criteria = new CDbCriteria;
$criteria->compare("type",'i');
$criteria->compare("is_archived",1);
$imagesCount = LibFile::model()->cache(60)->count($criteria);

$criteria = new CDbCriteria;
$criteria->compare("status",1);
$activeUsersCount = Users::model()->cache(60)->count($criteria);

$criteria = new CDbCriteria;
$criteria->addInCondition("status",array(1,2));
$criteria->compare("is_premium",1);
$premiumCount = Place::model()->cache(60)->count($criteria);

$criteria = new CDbCriteria;
$criteria->compare("status",2);
$pendingPlaces = Place::model()->cache(60)->count($criteria);

$criteria = new CDbCriteria;
$criteria->compare("status",1);
$activePlaces = Place::model()->cache(60)->count($criteria);

/*
$criteria = new CDbCriteria;
$criteria->compare("is_copy",0);
$criteria->compare("is_deleted",0);
$criteria->compare("status_id",2);
$publishedArticlesCount = Article::model()->cache(60)->count($criteria);
*/


?>

<div class="row">
	<div class="col-sm-3 col-xs-6">

		<div class="tile-stats tile-red">
			<div class="icon"><i class="entypo-users"></i></div>
			<div class="num" data-start="0" data-end="<?php echo $usersCount;?>" data-postfix="" data-duration="500" data-delay="0">0</div>

			<h3><?php echo Yii::t("admin","Registered users");?></h3>
			<p><?php echo Yii::t("admin","so far in our website.");?></p>
		</div>

	</div>

	<div class="col-sm-3 col-xs-6">

		<div class="tile-stats tile-brown">
			<div class="icon"><i class="entypo-newspaper"></i></div>
			<div class="num" data-start="0" data-end="<?php echo $placesCount;?>" data-postfix="" data-duration="500" data-delay="0">0</div>

			<h3><?php echo Yii::t("admin","Places count");?></h3>
			<p><?php echo Yii::t("admin","All places in database.");?></p>
		</div>

	</div>

	<div class="col-sm-3 col-xs-6">

		<div class="tile-stats tile-blue">
			<div class="icon"><i class="entypo-flash"></i></div>
			<div class="num" data-start="0" data-end="<?php echo $premiumCount;?>" data-postfix="" data-duration="500" data-delay="0">0</div>

			<h3><?php echo Yii::t("admin","Premium");?></h3>
			<p><?php echo Yii::t("admin","Premium accounts.");?></p>
		</div>

	</div>

	<div class="col-sm-3 col-xs-6">

		<div class="tile-stats tile-orange">
			<div class="icon"><i class="entypo-picture"></i></div>
			<div class="num" data-start="0" data-end="<?php echo $imagesCount;?>" data-postfix="" data-duration="500" data-delay="0">0</div>

			<h3><?php echo Yii::t("admin","Images");?></h3>
			<p><?php echo Yii::t("admin","in our library.");?></p>
		</div>

	</div>

</div>

<br />
<br />

<div class="row">
	<div class="col-sm-6 col-xs-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">Users</div>
			</div>
			
			<div class="panel-body">
				<div id="chart-users" style="height: 250px"></div>
			</div>
		</div>
	</div>

	<div class="col-sm-6 col-xs-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">Places</div>
			</div>
			
			<div class="panel-body">
				<div id="chart-places" style="height: 250px"></div>
			</div>
		</div>
	</div>

</div>

<script src="<?php echo $themeUrl; ?>/assets/js/raphael-min.js"></script>
<script src="<?php echo $themeUrl; ?>/assets/js/morris.min.js"></script>

<script type="text/javascript">

var articles_chart = Morris.Donut({
	element: 'chart-places',
	data: [
		{label: "Pending", value: <?php echo $pendingPlaces?>},
		{label: "Active", value: <?php echo $activePlaces?>},
	],
	colors: ['#21a9e1', '#00a651']
});

var users_chart = Morris.Donut({
	element: 'chart-users',
	data: [
		{label: "Active", value: <?php echo $activeUsersCount?>},
		{label: "Inactive", value: <?php echo $usersCount-$activeUsersCount?>},
	],
	colors: ['#883cd2', '#465045']
});



</script> 