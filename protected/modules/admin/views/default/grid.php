<?php
$baseUrl = Yii::app()->baseUrl;
$themeUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($themeUrl.'/assets/js/selectboxit/jquery.selectBoxIt.css');
$cs->registerCssFile($themeUrl.'/assets/js/select2/select2.css');
$cs->registerCssFile($themeUrl.'/assets/js/selectboxit/jquery.selectBoxIt.css');
$cs->registerCssFile($themeUrl.'/assets/js/datatables/responsive/css/datatables.responsive.css');
?>
<ol class="breadcrumb bc-3" >
	<li>
		<a href="index.html"><i class="fa-home"></i>Home</a>
	</li>
	<li>

		<a href="forms-main.html">Tables</a>
	</li>
	<li class="active">

		<strong>Grid</strong>
	</li>
</ol>

<h2>Basic Form Elements</h2>
<br />
<div class="dataTables_wrapper form-inline">
	<div class="row">
		<div class="col-xs-6 col-left">
			<div class="dataTables_length">
				<label>
					<select class="form-control">
						<option value="10">10</option>
						<option value="25">25</option>
						<option value="50">50</option>
						<option value="-1">All</option>
					</select> records per page
				</label>
			</div>
		</div>
		<div class="col-xs-6 col-right">
			<div class="dataTables_filter">
			</div>
		</div>
	</div>
	<table class="table table-bordered datatable dataTable">
		<thead>
			<tr>
				<th class="sorting_asc">Rendering engine</th>
				<th>actions</th>
			</tr>
		</thead>
		<tr>
			<td>Ahmed</td>
			<td>
				<a href="#" class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					Edit
				</a>
				
				<a href="#" class="btn btn-danger btn-sm btn-icon icon-left">
					<i class="entypo-cancel"></i>
					Delete
				</a>
				
				<a href="#" class="btn btn-info btn-sm btn-icon icon-left">
					<i class="entypo-info"></i>
					Profile
				</a>
			</td>
		</tr>
	</table>
	<div class="row">
		<div class="col-xs-6 col-left">
			<div class="dataTables_info">Showing 1 to 8 of 60 entries</div>
		</div>
		<div class="col-xs-6 col-right">
			<div class="dataTables_paginate paging_bootstrap">
				<ul class="pagination pagination-sm">
					<li class="prev disabled">
						<a href="#"><i class="entypo-left-open"></i></a>
					</li>
					<li class="active">
						<a href="#">1</a>
					</li>
					<li>
						<a href="#">2</a>
					</li>
					<li>
						<a href="#">3</a>
					</li>
					<li>
						<a href="#">4</a>
					</li>
					<li>
						<a href="#">5</a>
					</li>
					<li class="next">
						<a href="#"><i class="entypo-right-open"></i></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>