<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('place-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'place-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				'filter'=>GxHtml::listDataEx(Users::model()->findAllAttributes(null, true)),
				),
		'name_ar',
		'name_en',
		'description_ar',
		'description_en',
		/*
		'notes_ar',
		'notes_en',
		array(
				'name'=>'region_id',
				'value'=>'GxHtml::valueEx($data->region)',
				'filter'=>GxHtml::listDataEx(Region::model()->findAllAttributes(null, true)),
				),
		'address_ar',
		'address_en',
		'prices_plans_ar',
		'prices_plans_en',
		'lowest_price',
		'heigest_price',
		'catering_ar',
		'catering_en',
		'featured_ar',
		'featured_en',
		'rooms',
		'capacity',
		'lat',
		'lng',
		'founded',
		'email',
		'mobile',
		'phone',
		'open_hour',
		'close_hour',
		'website',
		'facebook_page',
		'twitter_page',
		'is_premium',
		'status',
		array(
				'name'=>'draft_for',
				'value'=>'GxHtml::valueEx($data->draftFor)',
				'filter'=>GxHtml::listDataEx(Place::model()->findAllAttributes(null, true)),
				),
		'ratings_total',
		'ratings_count',
		'ratings_avg',
		'visits',
		'properties_cache',
		array(
				'name'=>'main_image_id',
				'value'=>'GxHtml::valueEx($data->mainImage)',
				'filter'=>GxHtml::listDataEx(LibFile::model()->findAllAttributes(null, true)),
				),
		'gallery_cat_id',
		array(
					'name' => 'is_deleted',
					'value' => '($data->is_deleted == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'dateline',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>