<?php

$this->breadcrumbs = array(
	Place::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Place::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Place::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo (($this->action->id=='pending')?"Pending ":"") . GxHtml::encode(Place::label(2)); ?></h2>



<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'place-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				'filter'=>GxHtml::listDataEx(Users::model()->findAllAttributes(null, true)),
				),
		'name_ar',
		'name_en',
		array(
				'name'=>'status',
				'value'=>function($data){
					$values=array(0=>"Inactive", 1=>"Active", 2=>"Pending Review");
					if (!isset($values[$data->status])) return "";
					return $values[$data->status];
				},
				'filter'=>array(0=>"Inactive", 1=>"Active", 2=>"Pending Review"),
				),
		/*
		'notes_ar',
		'notes_en',
		array(
				'name'=>'region_id',
				'value'=>'GxHtml::valueEx($data->region)',
				'filter'=>GxHtml::listDataEx(Region::model()->findAllAttributes(null, true)),
				),
		'address_ar',
		'address_en',
		'prices_plans_ar',
		'prices_plans_en',
		'lowest_price',
		'heigest_price',
		'catering_ar',
		'catering_en',
		'featured_ar',
		'featured_en',
		'rooms',
		'capacity',
		'lat',
		'lng',
		'founded',
		'email',
		'mobile',
		'phone',
		'open_hour',
		'close_hour',
		'website',
		'facebook_page',
		'twitter_page',
		'is_premium',
		array(
				'name'=>'draft_for',
				'value'=>'GxHtml::valueEx($data->draftFor)',
				'filter'=>GxHtml::listDataEx(Place::model()->findAllAttributes(null, true)),
				),
		'ratings_total',
		'ratings_count',
		'ratings_avg',
		'visits',
		'properties_cache',
		array(
				'name'=>'main_image_id',
				'value'=>'GxHtml::valueEx($data->mainImage)',
				'filter'=>GxHtml::listDataEx(LibFile::model()->findAllAttributes(null, true)),
				),
		'gallery_cat_id',
		array(
					'name' => 'is_deleted',
					'value' => '($data->is_deleted == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'dateline',
		*/
		array(
				'name'=>'country_id',
				'value'=>'GxHtml::valueEx($data->country)',
				'filter'=>GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true)),
				),

		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{rooms}{services}{remove}{publish}',
			'buttons'=>array(
				'edit'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'visible'=>(string)Yii::app()->user->can(UserPermissions::Place_Edit),
					'label'=>'<i class="entypo-pencil"></i> Edit',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'publish'=>array(
					'options'=>array(
						'title'=>'Publish',
						'class'=>'btn btn-default btn-info btn-sm btn-icon icon-left btn-publish',
					),
					'visible'=>'$data->is_draft && ' . (string)Yii::app()->user->can(UserPermissions::BlogArticle_Publish),
					'label'=>'<i class="entypo-check"></i> Publish',
					'url'=>'Yii::app()->controller->createUrl("publish", array("id"=>$data->id))',
				),
				'rooms'=>array(
					'options'=>array(
						'title'=>'Rooms',
						'class'=>'btn btn-green btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-list"></i> Rooms',
					'url'=>'Yii::app()->controller->createUrl("room/index", array("id"=>$data->id))',
				),				
				'services'=>array(
					'options'=>array(
						'title'=>'Services',
						'class'=>'btn btn-blue btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-list"></i> Services',
					'url'=>'Yii::app()->controller->createUrl("service/index", array("id"=>$data->id))',
				),				
				'remove'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'visible'=>(string)Yii::app()->user->can(UserPermissions::Place_Delete),
					'label'=>'<i class="entypo-cancel"></i> Delete',
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#place-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#place-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#place-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});

		jQuery(document).on('click','#place-grid a.btn-publish',function() {
			if(!confirm('Are you sure you want to publish this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#place-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#place-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});

	});
</script>


<?php /*
<h2><?php echo Yii::t("app","Import from excel") ?></h2>
<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-body">
				

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'import-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>

<form class="form-horizontal form-groups-bordered" id="target-form" action="/dbse/admin/targets/create" method="post">
		<div class="form-group">
			<label class="col-sm-3 control-label required" for="compressed_file">Compressed excel file <span class="required">*</span></label>
			<div class="col-sm-5">
				<input class="form-control" name="file" id="compressed_file" type="file">				
			</div>
		</div>
<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::button(Yii::t('app', 'Import'), array('class'=>'btn btn-success')); ?>		<a href="<?php echo Yii::app()->createUrl("/places_import.zip"); ?>" class="btn btn-default">Download sample</a>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->
			</div>
		</div>
	</div>
<?php */ ?>	
