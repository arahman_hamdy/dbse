<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(Users::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'name_ar'); ?>
		<?php echo $form->textField($model, 'name_ar', array('class'=>'rtl', 'maxlength' => 100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'name_en'); ?>
		<?php echo $form->textField($model, 'name_en', array('maxlength' => 100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'description_ar'); ?>
		<?php echo $form->textArea($model, 'description_ar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'description_en'); ?>
		<?php echo $form->textArea($model, 'description_en'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'notes_ar'); ?>
		<?php echo $form->textField($model, 'notes_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'notes_en'); ?>
		<?php echo $form->textField($model, 'notes_en', array('maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'region_id'); ?>
		<?php echo $form->dropDownList($model, 'region_id', GxHtml::listDataEx(Region::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'address_ar'); ?>
		<?php echo $form->textField($model, 'address_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'address_en'); ?>
		<?php echo $form->textField($model, 'address_en', array('maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'prices_plans_ar'); ?>
		<?php echo $form->textArea($model, 'prices_plans_ar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'prices_plans_en'); ?>
		<?php echo $form->textArea($model, 'prices_plans_en'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'lowest_price'); ?>
		<?php echo $form->textField($model, 'lowest_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'heigest_price'); ?>
		<?php echo $form->textField($model, 'heigest_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'catering_ar'); ?>
		<?php echo $form->textArea($model, 'catering_ar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'catering_en'); ?>
		<?php echo $form->textArea($model, 'catering_en'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'featured_ar'); ?>
		<?php echo $form->textField($model, 'featured_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'featured_en'); ?>
		<?php echo $form->textField($model, 'featured_en', array('maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'rooms'); ?>
		<?php echo $form->textField($model, 'rooms'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'capacity'); ?>
		<?php echo $form->textField($model, 'capacity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'lat'); ?>
		<?php echo $form->textField($model, 'lat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'lng'); ?>
		<?php echo $form->textField($model, 'lng'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'founded'); ?>
		<?php echo $form->textField($model, 'founded', array('maxlength' => 4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'email'); ?>
		<?php echo $form->textField($model, 'email', array('maxlength' => 50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'mobile'); ?>
		<?php echo $form->textField($model, 'mobile', array('maxlength' => 50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'phone'); ?>
		<?php echo $form->textField($model, 'phone', array('maxlength' => 50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'open_hour'); ?>
		<?php echo $form->textField($model, 'open_hour'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'close_hour'); ?>
		<?php echo $form->textField($model, 'close_hour'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'website'); ?>
		<?php echo $form->textField($model, 'website', array('maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'facebook_page'); ?>
		<?php echo $form->textField($model, 'facebook_page', array('maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'twitter_page'); ?>
		<?php echo $form->textField($model, 'twitter_page', array('maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'is_premium'); ?>
		<?php echo $form->textField($model, 'is_premium'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'status'); ?>
		<?php echo $form->textField($model, 'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'draft_for'); ?>
		<?php echo $form->dropDownList($model, 'draft_for', GxHtml::listDataEx(Place::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ratings_total'); ?>
		<?php echo $form->textField($model, 'ratings_total'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ratings_count'); ?>
		<?php echo $form->textField($model, 'ratings_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ratings_avg'); ?>
		<?php echo $form->textField($model, 'ratings_avg'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'visits'); ?>
		<?php echo $form->textField($model, 'visits'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'properties_cache'); ?>
		<?php echo $form->textField($model, 'properties_cache', array('maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'main_image_id'); ?>
		<?php echo $form->dropDownList($model, 'main_image_id', GxHtml::listDataEx(LibFile::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'gallery_cat_id'); ?>
		<?php echo $form->textField($model, 'gallery_cat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'is_deleted'); ?>
		<?php echo $form->dropDownList($model, 'is_deleted', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'dateline'); ?>
		<?php echo $form->textField($model, 'dateline'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
