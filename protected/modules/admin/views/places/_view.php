<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->user)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_ar')); ?>:
	<?php echo GxHtml::encode($data->name_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_en')); ?>:
	<?php echo GxHtml::encode($data->name_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('description_ar')); ?>:
	<?php echo GxHtml::encode($data->description_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('description_en')); ?>:
	<?php echo GxHtml::encode($data->description_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('notes_ar')); ?>:
	<?php echo GxHtml::encode($data->notes_ar); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('notes_en')); ?>:
	<?php echo GxHtml::encode($data->notes_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('region_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->region)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('address_ar')); ?>:
	<?php echo GxHtml::encode($data->address_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('address_en')); ?>:
	<?php echo GxHtml::encode($data->address_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('prices_plans_ar')); ?>:
	<?php echo GxHtml::encode($data->prices_plans_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('prices_plans_en')); ?>:
	<?php echo GxHtml::encode($data->prices_plans_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('lowest_price')); ?>:
	<?php echo GxHtml::encode($data->lowest_price); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('heigest_price')); ?>:
	<?php echo GxHtml::encode($data->heigest_price); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('catering_ar')); ?>:
	<?php echo GxHtml::encode($data->catering_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('catering_en')); ?>:
	<?php echo GxHtml::encode($data->catering_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('featured_ar')); ?>:
	<?php echo GxHtml::encode($data->featured_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('featured_en')); ?>:
	<?php echo GxHtml::encode($data->featured_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('rooms')); ?>:
	<?php echo GxHtml::encode($data->rooms); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('capacity')); ?>:
	<?php echo GxHtml::encode($data->capacity); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('lat')); ?>:
	<?php echo GxHtml::encode($data->lat); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('lng')); ?>:
	<?php echo GxHtml::encode($data->lng); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('founded')); ?>:
	<?php echo GxHtml::encode($data->founded); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('email')); ?>:
	<?php echo GxHtml::encode($data->email); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('mobile')); ?>:
	<?php echo GxHtml::encode($data->mobile); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('phone')); ?>:
	<?php echo GxHtml::encode($data->phone); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('open_hour')); ?>:
	<?php echo GxHtml::encode($data->open_hour); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('close_hour')); ?>:
	<?php echo GxHtml::encode($data->close_hour); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('website')); ?>:
	<?php echo GxHtml::encode($data->website); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('facebook_page')); ?>:
	<?php echo GxHtml::encode($data->facebook_page); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('twitter_page')); ?>:
	<?php echo GxHtml::encode($data->twitter_page); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_premium')); ?>:
	<?php echo GxHtml::encode($data->is_premium); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('draft_for')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->draftFor)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ratings_total')); ?>:
	<?php echo GxHtml::encode($data->ratings_total); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ratings_count')); ?>:
	<?php echo GxHtml::encode($data->ratings_count); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ratings_avg')); ?>:
	<?php echo GxHtml::encode($data->ratings_avg); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('visits')); ?>:
	<?php echo GxHtml::encode($data->visits); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('properties_cache')); ?>:
	<?php echo GxHtml::encode($data->properties_cache); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('main_image_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->mainImage)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('gallery_cat_id')); ?>:
	<?php echo GxHtml::encode($data->gallery_cat_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_deleted')); ?>:
	<?php echo GxHtml::encode($data->is_deleted); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateline')); ?>:
	<?php echo GxHtml::encode($data->dateline); ?>
	<br />
	*/ ?>

</div>