<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'user',
			'type' => 'raw',
			'value' => $model->user !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->user)), array('users/view', 'id' => GxActiveRecord::extractPkValue($model->user, true))) : null,
			),
'name_ar',
'name_en',
'description_ar',
'description_en',
'notes_ar',
'notes_en',
array(
			'name' => 'region',
			'type' => 'raw',
			'value' => $model->region !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->region)), array('region/view', 'id' => GxActiveRecord::extractPkValue($model->region, true))) : null,
			),
'address_ar',
'address_en',
'prices_plans_ar',
'prices_plans_en',
'lowest_price',
'heigest_price',
'catering_ar',
'catering_en',
'featured_ar',
'featured_en',
'rooms',
'capacity',
'lat',
'lng',
'founded',
'email',
'mobile',
'phone',
'open_hour',
'close_hour',
'website',
'facebook_page',
'twitter_page',
'is_premium',
'status',
array(
			'name' => 'draftFor',
			'type' => 'raw',
			'value' => $model->draftFor !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->draftFor)), array('place/view', 'id' => GxActiveRecord::extractPkValue($model->draftFor, true))) : null,
			),
'ratings_total',
'ratings_count',
'ratings_avg',
'visits',
'properties_cache',
array(
			'name' => 'mainImage',
			'type' => 'raw',
			'value' => $model->mainImage !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->mainImage)), array('libFile/view', 'id' => GxActiveRecord::extractPkValue($model->mainImage, true))) : null,
			),
'gallery_cat_id',
'is_deleted:boolean',
'dateline',
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('events')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->events as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('event/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('places')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->places as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('place/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('placeImages')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->placeImages as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('placeImage/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('placeProperties')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->placeProperties as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('placeProperty/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('placeTargets')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->placeTargets as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('placeTarget/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>