<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'place-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>
	<?php if ($model->is_draft): ?>
		<div class="alert alert-warning">
			<?php echo Yii::t('app', 'This is a draft version pending approval.'); ?>
		</div>
	<?php endif; ?>

	<?php if ($model->errors): ?>
		<div class="alert alert-danger">
			<?php echo $form->errorSummary($model); ?>
		</div>
	<?php endif; ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'country_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'country_id', GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'country_id'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'name_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_ar', array('class'=>'rtl', 'maxlength' => 100)); ?>
				<?php echo $form->error($model,'name_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_en', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'name_en'); ?>
			</div>
		</div>

		<?php if (Yii::app()->user->isAdmin()): ?>
        <div class="form-group"> 
            <?php echo $form->labelEx($model,'user_id'); ?>
            <div class="col-sm-5"> 
                <?php echo $form->dropDownList($model, 'user_id', array(''=>'') + GxHtml::listDataEx(Users::model()->findAllAttributes(null, true))); ?>
                <?php echo $form->error($model,'user_id'); ?>
            </div> 
        </div> 
    	<?php endif; ?>

		<div class="form-group">
			<?php echo $form->labelEx($model,'city'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'city', GxHtml::listDataEx(City::model()->findAllAttributes(null, true)),array('' => '-')); ?>
				<?php echo $form->error($model,'city'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'region_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'region_id',array('' => '-')); ?>
				<?php echo $form->error($model,'region_id'); ?>
			</div>
		</div>

		<?php /* ?>
		<div class="form-group lib_image">
			<?php echo $form->labelEx($model,'main_image_id'); ?>
			<div class="col-sm-5">
				<?php $readOnly=false; if (!$model->isNewRecord && (($model->main_image_id && !Yii::app()->user->can(UserPermissions::Article_DeleteImage)) || (!$model->main_image_id && !Yii::app()->user->can(UserPermissions::Article_UploadImage)))) $readOnly=true; ?>
				<div class="col-sm-5 gallery-env">
					<article class="image-thumb" style="<?php if (!$model->main_image_lib_file_thumb_url)echo "display:none";?>">
							
						<?php echo $form->hiddenField($model, 'main_image_lib_file_thumb_url',array('class'=>'_lib_image_thumb')); ?>
						<a target="_blank" href="<?php echo $model->main_image_lib_file_thumb_url; ?>" class="image">
							<img src="<?php echo Yii::app()->thumb->generateThumb($model->main_image_lib_file_thumb_url,250,250);?>" />
						</a>
						
						<?php if (!$readOnly): ?>
						<div class="image-options">
							<a href="#" class="delete"><i class="entypo-cancel"></i></a>
						</div>				
						<?php endif ?>
						
					</article>
					<?php if (!$readOnly): ?>
					<button type="button" class="btn btn-default"><?php echo Yii::t('app','Set image');?></button>
					<?php endif ?>
				</div>
				<?php echo $form->hiddenField($model, 'main_image_id',array('class'=>'_lib_image_id')); ?>
				<?php echo $form->error($model,'main_image_id'); ?>
			</div>
		</div>
		<?php */ ?>

		<div class="form-group">
			<?php echo $form->labelEx($model,'placeImages'); ?>
			<div class="col-sm-9">
		<?php 
		$url=$this->createUrl('deleteImage');
		$this->widget('ext.EFineUploader.EFineUploader',
		 array(
		   'id'=>'FineUploader',
		   'config'=>array(
		   'autoUpload'=>true,
		   'request'=>array(
			  'endpoint'=>$this->createUrl('upload'),//'/files/upload',// OR ,
			  'params'=>array('YII_CSRF_TOKEN'=>Yii::app()->request->csrfToken),
						   ),
		   'retry'=>array('enableAuto'=>true,'preventRetryResponseProperty'=>true),
		   'chunking'=>array('enable'=>true,'partSize'=>100),//bytes
			'template'=>'<div class="qq-drop-processing"></div><div class="qq-upload-drop-area"></div>
					<div class="qq-uploader span4 left"><div class="qq-upload-button btn left">' . Yii::t('app','Upload image') . '</div>
					<div class="clear"></div>
					<ul class="qq-upload-list"></ul><br/></div>',
						   
		   'callbacks'=>array(
				'onComplete'=>"js:function(id, name, response){
				   if(response.success){
					$('#place_images').append(
						$('<div />')
							.addClass('image')
							.append($('<span />').addClass('filename').append($('<label />')
								.append($('<input />').attr('name','Place[main_image_id]').attr('value',response.id).attr('type','radio'))
								.append(' " . Yii::t('app','Default') . "')
								))
							.append($('<span />').addClass('close').html('X').click(function(){
							 if (!confirm('Are you sure you want to delete this image?')) return false;								
							 $.ajax({
								url: '". $url . "',
								type: 'POST',
								data: { 'id': response.id },
								success: function(data){}
							});
							\$(this).parent().remove()}))
							.append($('<img />').attr('src',response.folder + response.filename))
							.append($('<input />').attr('type','hidden').attr('value',response.id).attr('name','Place[libFiles][]'))
						);
				   }
			   }",
				'onError'=>"js:function(id, name, errorReason){}",
				 ),
		   'validation'=>array(
					 'allowedExtensions'=>array('jpg','jpeg','png','gif'),
					 'sizeLimit'=>2 * 1024 * 1024,//maximum file size in bytes
					 //'minSizeLimit'=>2*1024*1024,// minimum file size in bytes
							  ),
		  )
		  ));
	 
			?>
			<?php echo $form->error($model,'main_image_id'); ?>
		
			<div id="place_images">
				<?php
				$imagesUrlPath = $this->uploadsUrl;
				$imagesArray = array();

                if ($model->mainImage){
                	$imagesArray[$model->main_image_id] = $model->mainImage;
                }

				if (!empty($_POST['Place']['libFiles'])){
					$AdditionalFiles = $_POST['Place']['libFiles'];

					$criteria = new CDbCriteria;
					$criteria->addInCondition('id',$AdditionalFiles);
					$AdditionalImages = LibFile::model()->findAll($criteria);

					foreach ($AdditionalImages as $image) {
						$imagesArray[$image->id] = $image;
					}
				}else{
					foreach ($model->placeImages as $placeImage) {
						$imagesArray[$placeImage->image_id] = $placeImage->image;
					}					
				}

				foreach($imagesArray as $image){
					?>
					<div class='image'>
					<span class='filename'><label><input type='radio' name='Place[main_image_id]' value='<?php echo $image->id;?>' <?php if ($image->id==$model->main_image_id)echo " checked='checked' "; ?> /> <?php echo Yii::t('app','Default');?></label></span>
					<span class='close'>X</span>
					<img src='<?php echo "$imagesUrlPath{$image->path}"; ?>' />
					<input type='hidden' value='<?php echo $image->id;?>' name='Place[libFiles][]' />
					</div>
					<?php 
				}
				
			
				
				if (count($imagesArray))
				{	//Yii::app()->clientScript->registerScript('images_close','$("#place_images span.close").click(function(){
					//$(this).parent().remove()});');
					$url = CJavaScript::encode($this->createUrl('deleteImage'));
					Yii::app()->clientScript->registerScript('images_close','$("#place_images span.close").click(function(){
						if (!confirm("Are you sure you want to delete this image?")) return false;
					 $.ajax({
						url: '. $url . ',
						type: "POST",
						data: { "id": $(this).parent().children("input:hidden").val()},
						success: function(data){}
					});
					$(this).parent().remove();
					
					
					});');
				}
					?>
			</div>
			<div class="clear"></div>
		</div>
		</div>

		<?php /*
		<div class="form-group">
			<?php echo $form->labelEx($model,'gallery_cat_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'gallery_cat_id', LibCat::getListTreeView(),array('prompt'=>''));?>
				<?php echo $form->error($model,'gallery_cat_id'); ?>
			</div>
		</div>
		<?php */ ?>

		<div class="form-group">
			<?php echo $form->labelEx($model,'description_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'description_ar'); ?>
				<?php echo $form->error($model,'description_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'description_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'description_en'); ?>
				<?php echo $form->error($model,'description_en'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'accepts_online_payment'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'accepts_online_payment'); ?>
				<?php echo $form->error($model,'accepts_online_payment'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'accepts_offline_payment'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'accepts_offline_payment'); ?>
				<?php echo $form->error($model,'accepts_offline_payment'); ?>
			</div>
		</div>

		<div class="form-group form-head">
			<span><?php echo Yii::t("app", "Location"); ?></span>
		</div>
		
		<div class="form-group">
			<?php echo $form->labelEx($model,'address_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'address_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
				<?php echo $form->error($model,'address_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'address_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'address_en', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'address_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'lat'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'lat'); ?>
				<?php echo $form->error($model,'lat'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'lng'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'lng'); ?>
				<?php echo $form->error($model,'lng'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'main_branch'); ?>
			<div class="col-sm-5">
				<?php
				$this->widget('CustomAutoComplete', array(
					'model'=>$model,
					'attribute'=>'main_branch',
					'labelModelName'=>'Place',
					'labelModelAttribute'=>'name_en',
					'source'=>$this->createUrl('places/search'),
					// additional javascript options for the autocomplete plugin
					'options'=>array(
						'showAnim'=>'fold',
					),
					'htmlOptions'=>array(
						'class'=>'form-control',
					)
				));
				?>			
				<!--
				<input type="text" id="Place_main_branch" name="Place[main_branch]" class="form-control typeahead" data-remote="<?php echo $this->createUrl("find");?>?q=%QUERY" data-template="<div class='thumb-entry'><span class='text'><strong>{{value}}</strong><em>{{desc}}</em></span></div>" value="<?php echo $model->main_branch; ?>" /> -->
				<?php echo $form->error($model,'main_branch'); ?>
			</div>
		</div>


		<div class="form-group form-head">
			<span><?php echo Yii::t("app", "Properties"); ?></span>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<?php echo $form->checkBoxList($model, 'properties', GxHtml::encodeEx(GxHtml::listDataEx(Property::model()->findAllAttributes(null, true)), false, true)); ?>
			</div>
		</div>

		<div class="form-group form-head">
			<span><?php echo Yii::t("app", "Targets"); ?></span>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<?php echo $form->checkBoxList($model, 'targets', GxHtml::encodeEx(GxHtml::listDataEx(Target::model()->findAllAttributes(null, true)), false, true)); ?>
			</div>
		</div>

		<!-- <div class="form-group">
			<?php echo $form->labelEx($model,'targets_text_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'targets_text_ar'); ?>
				<?php echo $form->error($model,'targets_text_ar'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'targets_text_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'targets_text_en'); ?>
				<?php echo $form->error($model,'targets_text_en'); ?>
			</div>
		</div> -->

		<div class="form-group">
			<?php echo $form->labelEx($model,'works_from_here_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'works_from_here_ar'); ?>
				<?php echo $form->error($model,'works_from_here_ar'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'works_from_here_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'works_from_here_en'); ?>
				<?php echo $form->error($model,'works_from_here_en'); ?>
			</div>
		</div>

		<?php /* ?>
		<div class="form-group form-head">
			<span><?php echo Yii::t("app", "Targets"); ?></span>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<?php echo $form->checkBoxList($model, 'targets', GxHtml::encodeEx(GxHtml::listDataEx(Target::model()->findAllAttributes(null, true)), false, true)); ?>
			</div>
		</div>
		<?php */ ?>

		<div class="form-group form-head">
			<span><?php echo Yii::t("app", "Contact details"); ?></span>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'email'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'email', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'mobile'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'mobile', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'mobile'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'phone'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'phone', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'phone'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'website'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'website', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'website'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'facebook_page'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'facebook_page', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'facebook_page'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'twitter_page'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'twitter_page', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'twitter_page'); ?>
			</div>
		</div>

		<div class="form-group form-head">
			<span><?php echo Yii::t("app", "Additional info"); ?></span>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'prices_plans_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'prices_plans_ar'); ?>
				<?php echo $form->error($model,'prices_plans_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'prices_plans_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'prices_plans_en'); ?>
				<?php echo $form->error($model,'prices_plans_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'lowest_price'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'lowest_price'); ?>
				<?php echo $form->error($model,'lowest_price'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'heigest_price'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'heigest_price'); ?>
				<?php echo $form->error($model,'heigest_price'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'catering_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'catering_ar'); ?>
				<?php echo $form->error($model,'catering_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'catering_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'catering_en'); ?>
				<?php echo $form->error($model,'catering_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'featured_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'featured_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
				<?php echo $form->error($model,'featured_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'featured_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'featured_en', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'featured_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'rooms'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'rooms'); ?>
				<?php echo $form->error($model,'rooms'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'capacity'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'capacity'); ?>
				<?php echo $form->error($model,'capacity'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'founded'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'founded', array('maxlength' => 4)); ?>
				<?php echo $form->error($model,'founded'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'open_hour'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'open_hour'); ?>
				<?php echo $form->error($model,'open_hour'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'close_hour'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'close_hour'); ?>
				<?php echo $form->error($model,'close_hour'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'notes_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'notes_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
				<?php echo $form->error($model,'notes_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'notes_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'notes_en', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'notes_en'); ?>
			</div>
		</div>

		<?php if (Yii::app()->user->can(UserPermissions::Place_Publish)): ?>
        <div class="form-group"> 
            <?php echo $form->labelEx($model,'status'); ?>
            <div class="col-sm-5"> 
                <?php echo $form->checkBox($model, 'status'); ?>
                <?php echo $form->error($model,'status'); ?>
            </div> 
        </div> 
    	<?php endif; ?>



<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->
<script>
var defaultRegion = <?php echo (int)$model->region_id; ?>;
var isRequestingRegions = false;
jQuery("#Place_city").change(function(){
	if (isRequestingRegions) return;
	jQuery("#Place_city").attr("disabled","disabled");
	jQuery("#Place_region_id").attr("disabled","disabled");

	jQuery.ajax({
			url: "<?php echo $this->createUrl("getCityRegions") ?>/id/" + jQuery("#Place_city").val(),
			type: "get",
			dataType: "json",
			success: function (response) {
				jQuery("#Place_region_id option.generated").remove();
				for(i in response){
					var option = jQuery("<option>").addClass("generated").val(i).html(response[i]);
					if (option.val()==defaultRegion) option.attr("selected","selected");
					jQuery("#Place_region_id").append(option);
				}
			},
			complete: function(jqXHR, textStatus, errorThrown) {
				jQuery("#Place_city").removeAttr("disabled");
				jQuery("#Place_city").selectBoxIt("refresh");

				jQuery("#Place_region_id").removeAttr("disabled");
				jQuery("#Place_region_id").selectBoxIt("refresh");

			}


		});
});
jQuery("#Place_city").trigger("change");
</script>