<?php

$this->breadcrumbs = array(
	Premium::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Premium::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Premium::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode(Premium::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'premium-grid',
	'dataProvider' => $model->search(),
	'show_create_button' => false,
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				),
		array(
				'name'=>'place_id',
				'value'=>'GxHtml::valueEx($data->place)',
				),
		array(
				'name'=>'entity_id',
				'value'=>'GxHtml::valueEx($data->entity)',
				),
		'expiration_date',
		'dateline',
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{details}',
			'buttons'=>array(
				'details'=>array(
					'options'=>array(
						'title'=>'Details',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-doc-text"></i> Details',
					'url'=>'Yii::app()->controller->createUrl("view", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>