<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->user)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('place_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->place)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('entity_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->entity)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('expiration_date')); ?>:
	<?php echo GxHtml::encode($data->expiration_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateline')); ?>:
	<?php echo GxHtml::encode($data->dateline); ?>
	<br />

</div>