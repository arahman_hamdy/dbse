<?php $baseUrl = Yii::app()->baseUrl;
$themeUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();

$cs->registerScript("loginurl","var \$loginUrl='" . $this->createUrl(Yii::app()->user->loginUrl[0]) . "', \$returnUrl='" . $this->createUrl(Yii::app()->controller->module->returnUrl[0]) . "';", CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/jquery.validate.min.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl.'/assets/js/neon-login.js', CClientScript::POS_END);
?>
<style type="text/css">
body {
  color: #949494;
}
</style>
<div class="login-container">
	
	<div class="login-header login-caret">
		
		<div class="login-content">
			
			<h1>DBSE</h1>
						
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>
	
	<div class="login-form">
		
		<div class="login-content">
			
			<div class="form-login-error">
				<h3>Invalid login</h3>
				<p>Please check <strong>username</strong> and <strong>password</strong>.</p>
			</div>
			
			<form method="post" role="form" id="form_login">
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="text" class="form-control" name="UserLogin[username]" id="username" placeholder="Username" autocomplete="off" />
					</div>
					
				</div>
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						
						<input type="password" class="form-control" name="UserLogin[password]" id="password" placeholder="Password" autocomplete="off" />
					</div>
				
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Login In
					</button>
				</div>
				
			</form>
			
			
		</div>
		
	</div>
	
</div>
