<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'blog-article-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
		'enctype' => 'multipart/form-data',
	)
));
?>
	<?php if ($model->is_draft): ?>
		<div class="alert alert-warning">
			<?php echo Yii::t('app', 'This is a draft version pending approval.'); ?>
		</div>
	<?php endif; ?>

		<?php if ($model->errors): ?>
			<div class="alert alert-danger">
				<?php echo $form->errorSummary($model); ?>
			</div>
		<?php endif; ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'category_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'category_id', array(''=>'') + BlogCategory::getListTreeView()); ?>
				<?php echo $form->error($model,'category_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'title_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'title_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
				<?php echo $form->error($model,'title_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'title_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'title_en', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'title_en'); ?>
			</div>
		</div>
		<div class="form-group ad_image_type">
			<?php echo $form->labelEx($model,'image_file'); ?>
			<div class="col-sm-5">
				<img style="max-width:100%; max-height:100px" src="<?php echo $model->image_file; ?>" />

				<?php echo $form->fileField($model, 'image_file'); ?>
				<?php echo $form->error($model,'image_file'); ?>
			</div>
		</div>
        <div class="form-group"> 
            <?php echo $form->labelEx($model,'short_description_ar'); ?>
            <div class="col-sm-5"> 
                <?php echo $form->textArea($model, 'short_description_ar', array('class'=>'rtl', 'maxlength' => 300)); ?>
                <?php echo $form->error($model,'short_description_ar'); ?>
            </div> 
        </div> 
        <div class="form-group"> 
            <?php echo $form->labelEx($model,'short_description_en'); ?>
            <div class="col-sm-5"> 
                <?php echo $form->textArea($model, 'short_description_en', array('maxlength' => 300)); ?>
                <?php echo $form->error($model,'short_description_en'); ?>
            </div> 
        </div> 
		<div class="form-group">
			<?php echo $form->labelEx($model,'content_ar'); ?>
			<div class="col-sm-9">
				<?php echo $form->textArea($model, 'content_ar', array('class'=>'ckeditor_rtl')); ?>
				<?php echo $form->error($model,'content_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'content_en'); ?>
			<div class="col-sm-9">
				<?php echo $form->textArea($model, 'content_en', array('class'=>'ckeditor_ltr')); ?>
				<?php echo $form->error($model,'content_en'); ?>
			</div>
		</div>

		<?php if (Yii::app()->user->can(UserPermissions::BlogArticle_Publish)): ?>
        <div class="form-group"> 
            <?php echo $form->labelEx($model,'status'); ?>
            <div class="col-sm-5"> 
                <?php echo $form->checkBox($model, 'status'); ?>
                <?php echo $form->error($model,'status'); ?>
            </div> 
        </div> 
    	<?php endif; ?>

		<!--
		<div class="form-group">
			<?php echo $form->labelEx($model,'status'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'status'); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'is_deleted'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'is_deleted'); ?>
				<?php echo $form->error($model,'is_deleted'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'dateline'); ?>
			<div class="col-sm-5">
				<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'dateline',
			'value' => $model->dateline,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
				<?php echo $form->error($model,'dateline'); ?>
			</div>
		</div>
		-->

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->