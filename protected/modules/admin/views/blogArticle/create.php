<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Add'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url' => array('index')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>

<h2><?php echo Yii::t('app', 'Add') . ' ' . GxHtml::encode($model->label()); ?></h2>
<br />

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-body">
				<?php
				$this->renderPartial('_form', array(
						'model' => $model,
						'buttons' => 'create'));
				?>			</div>
		</div>
	</div>
</div>