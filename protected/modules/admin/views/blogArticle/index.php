<?php

$this->breadcrumbs = array(
	BlogArticle::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . BlogArticle::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . BlogArticle::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo (($this->action->id=='pending')?"Pending ":"") . GxHtml::encode(BlogArticle::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'blog-article-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		array(
				'name'=>'category_id',
				'value'=>'GxHtml::valueEx($data->category)',
				'filter'=>BlogCategory::getListTreeView(),
				),
		'title_ar',
		'title_en',
		/*
		'status',
		array(
					'name' => 'is_deleted',
					'value' => '($data->is_deleted == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'dateline',
		*/
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{remove}{preview}{relations}{publish}',
			'buttons'=>array(
				'preview'=>array(
					'options'=>array(
						'title'=>'Preview',
						'class'=>'btn btn-default btn-info btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> Preview',
					'url'=>'Yii::app()->controller->createUrl("//blog/view", array("id"=>$data->id))',
				),
				'publish'=>array(
					'options'=>array(
						'title'=>'Publish',
						'class'=>'btn btn-default btn-info btn-sm btn-icon icon-left btn-publish',
					),
					'visible'=>'$data->is_draft && ' . (string)Yii::app()->user->can(UserPermissions::BlogArticle_Publish),
					'label'=>'<i class="entypo-check"></i> Publish',
					'url'=>'Yii::app()->controller->createUrl("publish", array("id"=>$data->id))',
				),
				'edit'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'visible'=>(string)Yii::app()->user->can(UserPermissions::BlogArticle_Edit),
					'label'=>'<i class="entypo-pencil"></i> Edit',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'relations'=>array(
					'options'=>array(
						'title'=>'Relations',
						'class'=>'btn btn-green btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-link"></i> Relations',
					'url'=>'Yii::app()->controller->createUrl("blogArticleRelations/index", array("articleId"=>$data->id))',
				),								
				'remove'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'visible'=>(string)Yii::app()->user->can(UserPermissions::BlogArticle_Delete),
					'label'=>'<i class="entypo-cancel"></i> Delete',
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#blog-article-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#blog-article-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#blog-article-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});

		jQuery(document).on('click','#blog-article-grid a.btn-publish',function() {
			if(!confirm('Are you sure you want to publish this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#blog-article-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#blog-article-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
