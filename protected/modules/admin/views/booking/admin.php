<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('booking-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'booking-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		array(
				'name'=>'place_id',
				'value'=>'GxHtml::valueEx($data->place)',
				'filter'=>GxHtml::listDataEx(Place::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'offer_id',
				'value'=>'GxHtml::valueEx($data->offer)',
				'filter'=>GxHtml::listDataEx(Offer::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'room_id',
				'value'=>'GxHtml::valueEx($data->room)',
				'filter'=>GxHtml::listDataEx(Room::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				'filter'=>GxHtml::listDataEx(Users::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'transaction_id',
				'value'=>'GxHtml::valueEx($data->transaction)',
				'filter'=>GxHtml::listDataEx(Transaction::model()->findAllAttributes(null, true)),
				),
		/*
		'name',
		'phone',
		'email',
		'total',
		'start_date',
		'end_date',
		'persons',
		'notes',
		array(
					'name' => 'is_paid',
					'value' => '($data->is_paid == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'status',
		'dateline',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>