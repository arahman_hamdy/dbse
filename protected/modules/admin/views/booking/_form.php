<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'booking-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'place',
			'type' => 'raw',
			'value' => $model->place !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->place)), array('places/view', 'id' => GxActiveRecord::extractPkValue($model->place, true))) : null,
			),
array(
			'name' => 'offer',
			'type' => 'raw',
			'value' => $model->offer !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->offer)), array('offer/view', 'id' => GxActiveRecord::extractPkValue($model->offer, true))) : null,
			),
array(
			'name' => 'room',
			'type' => 'raw',
			'value' => $model->room !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->room)), array('room/view', 'id' => GxActiveRecord::extractPkValue($model->room, true))) : null,
			),
array(
			'name' => 'user',
			'type' => 'raw',
			'value' => $model->user !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->user)), array('users/view', 'id' => GxActiveRecord::extractPkValue($model->user, true))) : null,
			),
array(
			'name' => 'transaction',
			'type' => 'raw',
			'value' => $model->transaction !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->transaction)), array('transaction/view', 'id' => GxActiveRecord::extractPkValue($model->transaction, true))) : null,
			),
array(
			'name' => 'payment_method',
			'type' => 'raw',
			'value' => $model->payment_method == 1 ? 'Online':'Offline',
			),
'price',
'total',
'start_date',
'end_date',
'status',
'dateline',
	),
)); ?>


	<?php if ($model->errors): ?>
		<div class="alert alert-danger">
			<?php echo $form->errorSummary($model); ?>
		</div>
	<?php endif; ?>

		<div class="form-group">
			<?php echo $form->labelEx($model,'name'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'phone'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'phone', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'phone'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'email'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'email', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'persons'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'persons'); ?>
				<?php echo $form->error($model,'persons'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'notes'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'notes', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'notes'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'is_paid'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'is_paid'); ?>
				<?php echo $form->error($model,'is_paid'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->