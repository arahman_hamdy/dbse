<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'place',
			'type' => 'raw',
			'value' => $model->place !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->place)), array('place/view', 'id' => GxActiveRecord::extractPkValue($model->place, true))) : null,
			),
array(
			'name' => 'offer',
			'type' => 'raw',
			'value' => $model->offer !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->offer)), array('offer/view', 'id' => GxActiveRecord::extractPkValue($model->offer, true))) : null,
			),
array(
			'name' => 'room',
			'type' => 'raw',
			'value' => $model->room !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->room)), array('room/view', 'id' => GxActiveRecord::extractPkValue($model->room, true))) : null,
			),
array(
			'name' => 'user',
			'type' => 'raw',
			'value' => $model->user !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->user)), array('users/view', 'id' => GxActiveRecord::extractPkValue($model->user, true))) : null,
			),
array(
			'name' => 'transaction',
			'type' => 'raw',
			'value' => $model->transaction !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->transaction)), array('transaction/view', 'id' => GxActiveRecord::extractPkValue($model->transaction, true))) : null,
			),
'payment_method',
'name',
'phone',
'email',
'price',
'total',
'start_date',
'end_date',
'persons',
'notes',
'is_paid:boolean',
'status',
'dateline',
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('bookingDetails')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->bookingDetails as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('bookingDetail/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>