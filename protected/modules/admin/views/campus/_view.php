<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('entity_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->entity)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_en')); ?>:
	<?php echo GxHtml::encode($data->name_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_ar')); ?>:
	<?php echo GxHtml::encode($data->name_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('lat')); ?>:
	<?php echo GxHtml::encode($data->lat); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('lng')); ?>:
	<?php echo GxHtml::encode($data->lng); ?>
	<br />

</div>