<?php

$this->breadcrumbs = array(
	Campus::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Campus::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Campus::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode((($model->entity)?$model->entity . ' > ':null) . Campus::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'campus-grid',
	'dataProvider' => $model->search(),
	'additionalButtons' => array($this->createUrl('create',!empty($model->entity)?array('entityId'=>$model->entity_id):array())=>"Add New"),	
	'show_create_button' => false,
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		array(
				'name'=>'entity_id',
				'value'=>'GxHtml::valueEx($data->entity)',
				'filter'=>GxHtml::listDataEx(Entity::model()->findAllAttributes(null, true)),
				),
		'name_en',
		'name_ar',
		'lat',
		'lng',
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{remove}',
			'buttons'=>array(
				'edit'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> Edit',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'remove'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-cancel"></i> Delete',
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#campus-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#campus-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#campus-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
