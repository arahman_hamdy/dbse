<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'slider-image-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
        'enctype' => 'multipart/form-data',
	)
));
?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'country_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'country_id', array(''=>'All') + GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'country_id'); ?>
			</div>
		</div>

		<?php echo $form->hiddenField($model, 'slider_id'); ?>
		<?php echo $form->hiddenField($model, 'image_id'); ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'title'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'title', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'title'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'image_file'); ?>
			<div class="col-sm-5">
				<?php echo $form->fileField($model, 'image_file'); ?>
				<?php echo $form->error($model,'image_file'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'url'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'url', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'url'); ?>
			</div>
		</div>
		<!--
		<div class="form-group">
			<?php echo $form->labelEx($model,'description'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'description', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'description'); ?>
			</div>
		</div>
		-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'sortid'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sortid'); ?>
				<?php echo $form->error($model,'sortid'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->