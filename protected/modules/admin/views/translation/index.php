<?php
$this->widget('tstranslation.widgets.TsTranslationWidget', array(
		'includeBootstrap' => false,
		'showTooltips' => false, // if you want disable bootstrap tooltips
		'scriptPosition' => CClientScript::POS_END,
		'minifyScripts'=>false,
));
