<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('place_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->place)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_ar')); ?>:
	<?php echo GxHtml::encode($data->name_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_en')); ?>:
	<?php echo GxHtml::encode($data->name_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('capacity')); ?>:
	<?php echo GxHtml::encode($data->capacity); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('price')); ?>:
	<?php echo GxHtml::encode($data->price); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sortid')); ?>:
	<?php echo GxHtml::encode($data->sortid); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('is_active')); ?>:
	<?php echo GxHtml::encode($data->is_active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_deleted')); ?>:
	<?php echo GxHtml::encode($data->is_deleted); ?>
	<br />
	*/ ?>

</div>