<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'search-history-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>		<div class="form-group">
			<?php echo $form->labelEx($model,'department'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'department', array('maxlength' => 20)); ?>
				<?php echo $form->error($model,'department'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'query'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'query', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'query'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'advanced'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'advanced'); ?>
				<?php echo $form->error($model,'advanced'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'dateline'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'dateline'); ?>
				<?php echo $form->error($model,'dateline'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->