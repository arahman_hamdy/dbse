<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('department')); ?>:
	<?php echo GxHtml::encode($data->department); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('query')); ?>:
	<?php echo GxHtml::encode($data->query); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('advanced')); ?>:
	<?php echo GxHtml::encode($data->advanced); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateline')); ?>:
	<?php echo GxHtml::encode($data->dateline); ?>
	<br />

</div>