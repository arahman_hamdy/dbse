<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('label_ar')); ?>:
	<?php echo GxHtml::encode($data->label_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('label_en')); ?>:
	<?php echo GxHtml::encode($data->label_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('type')); ?>:
	<?php echo GxHtml::encode($data->type); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_required')); ?>:
	<?php echo GxHtml::encode($data->is_required); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sortid')); ?>:
	<?php echo GxHtml::encode($data->sortid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_system')); ?>:
	<?php echo GxHtml::encode($data->is_system); ?>
	<br />

</div>