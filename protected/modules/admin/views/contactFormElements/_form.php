<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'contact-form-elements-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>		<div class="form-group">
			<?php echo $form->labelEx($model,'label_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'label_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
				<?php echo $form->error($model,'label_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'label_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'label_en', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'label_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'type'); ?>
			<div class="col-sm-5">
				<?php echo $form->enumDropDownList($model, 'type'); ?>
				<?php echo $form->error($model,'type'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'is_required'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'is_required'); ?>
				<?php echo $form->error($model,'is_required'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'sortid'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sortid'); ?>
				<?php echo $form->error($model,'sortid'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->