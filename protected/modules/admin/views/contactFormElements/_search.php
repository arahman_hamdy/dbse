<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'label_ar'); ?>
		<?php echo $form->textField($model, 'label_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'label_en'); ?>
		<?php echo $form->textField($model, 'label_en', array('maxlength' => 200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'type'); ?>
		<?php echo $form->enumDropDownList($model, 'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'is_required'); ?>
		<?php echo $form->dropDownList($model, 'is_required', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sortid'); ?>
		<?php echo $form->textField($model, 'sortid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'is_system'); ?>
		<?php echo $form->dropDownList($model, 'is_system', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
