<?php

$this->breadcrumbs = array(
	ContactFormElements::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . ContactFormElements::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . ContactFormElements::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode(ContactFormElements::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'contact-form-elements-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		'label_ar',
		'label_en',
		'type',
		array(
					'name' => 'is_required',
					'value' => '($data->is_required == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'sortid',
		/*
		array(
					'name' => 'is_system',
					'value' => '($data->is_system == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		*/
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{remove}',
			'buttons'=>array(
				'edit'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> Edit',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'remove'=>array(
					'visible'=>'!$data->is_system',
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-cancel"></i> Delete',
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#contact-form-elements-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#contact-form-elements-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#contact-form-elements-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
