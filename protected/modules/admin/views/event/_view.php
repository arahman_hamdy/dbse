<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('place_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->place)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_ar')); ?>:
	<?php echo GxHtml::encode($data->name_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_en')); ?>:
	<?php echo GxHtml::encode($data->name_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('start_date')); ?>:
	<?php echo GxHtml::encode($data->start_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('end_date')); ?>:
	<?php echo GxHtml::encode($data->end_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('description_ar')); ?>:
	<?php echo GxHtml::encode($data->description_ar); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('description_en')); ?>:
	<?php echo GxHtml::encode($data->description_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('image_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->image)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('address_ar')); ?>:
	<?php echo GxHtml::encode($data->address_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('address_en')); ?>:
	<?php echo GxHtml::encode($data->address_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('lat')); ?>:
	<?php echo GxHtml::encode($data->lat); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('lng')); ?>:
	<?php echo GxHtml::encode($data->lng); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('organization_ar')); ?>:
	<?php echo GxHtml::encode($data->organization_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('organization_en')); ?>:
	<?php echo GxHtml::encode($data->organization_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('notes_ar')); ?>:
	<?php echo GxHtml::encode($data->notes_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('notes_en')); ?>:
	<?php echo GxHtml::encode($data->notes_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('accepts_online_payment')); ?>:
	<?php echo GxHtml::encode($data->accepts_online_payment); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('accepts_offline_payment')); ?>:
	<?php echo GxHtml::encode($data->accepts_offline_payment); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('max_persons')); ?>:
	<?php echo GxHtml::encode($data->max_persons); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('registered_persons')); ?>:
	<?php echo GxHtml::encode($data->registered_persons); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('price')); ?>:
	<?php echo GxHtml::encode($data->price); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('likes')); ?>:
	<?php echo GxHtml::encode($data->likes); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('reports')); ?>:
	<?php echo GxHtml::encode($data->reports); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('attending')); ?>:
	<?php echo GxHtml::encode($data->attending); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->user)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_deleted')); ?>:
	<?php echo GxHtml::encode($data->is_deleted); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_active')); ?>:
	<?php echo GxHtml::encode($data->is_active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateline')); ?>:
	<?php echo GxHtml::encode($data->dateline); ?>
	<br />
	*/ ?>

</div>