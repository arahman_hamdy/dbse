<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('event-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'event-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		array(
				'name'=>'place_id',
				'value'=>'GxHtml::valueEx($data->place)',
				'filter'=>GxHtml::listDataEx(Place::model()->findAllAttributes(null, true)),
				),
		'name_ar',
		'name_en',
		'start_date',
		'end_date',
		/*
		'description_ar',
		'description_en',
		array(
				'name'=>'image_id',
				'value'=>'GxHtml::valueEx($data->image)',
				'filter'=>GxHtml::listDataEx(LibFile::model()->findAllAttributes(null, true)),
				),
		'address_ar',
		'address_en',
		'lat',
		'lng',
		'organization_ar',
		'organization_en',
		'notes_ar',
		'notes_en',
		array(
					'name' => 'accepts_online_payment',
					'value' => '($data->accepts_online_payment == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		array(
					'name' => 'accepts_offline_payment',
					'value' => '($data->accepts_offline_payment == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'max_persons',
		'registered_persons',
		'price',
		'likes',
		'reports',
		'attending',
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				'filter'=>GxHtml::listDataEx(Users::model()->findAllAttributes(null, true)),
				),
		'status',
		array(
					'name' => 'is_deleted',
					'value' => '($data->is_deleted == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		array(
					'name' => 'is_active',
					'value' => '($data->is_active == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'dateline',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>