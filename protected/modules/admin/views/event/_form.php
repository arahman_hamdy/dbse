<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'event-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
		'enctype' => 'multipart/form-data',
	)
));
?>
	<?php if ($model->is_draft): ?>
		<div class="alert alert-warning">
			<?php echo Yii::t('app', 'This is a draft version pending approval.'); ?>
		</div>
	<?php endif; ?>

	<?php if ($model->errors): ?>
		<div class="alert alert-danger">
			<?php echo $form->errorSummary($model); ?>
		</div>
	<?php endif; ?>

		<div class="form-group">
			<?php echo $form->labelEx($model,'country_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'country_id', array(''=>'') + GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'country_id'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'place_id'); ?>
			<div class="col-sm-5">
				<?php
				$this->widget('CustomAutoComplete', array(
					'model'=>$model,
					'attribute'=>'place_id',
					'labelModelName'=>'Place',
					'labelModelAttribute'=>'name_en',
					'source'=>$this->createUrl('places/search'),
					// additional javascript options for the autocomplete plugin
					'options'=>array(
						'showAnim'=>'fold',
					),
					'htmlOptions'=>array(
						'class'=>'form-control',
					)
				));
				?>	
				<?php echo $form->error($model,'place_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_ar', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'name_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_en', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'name_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'image_file'); ?>
			<div class="col-sm-5">
				<img style="max-width:100%; max-height:100px" src="<?php echo $model->image_file; ?>" />

				<?php echo $form->fileField($model, 'image_file'); ?>
				<?php echo $form->error($model,'image_file'); ?>
			</div>
		</div>		
		<div class="form-group">
			<?php echo $form->labelEx($model,'start_date'); ?>
			<div class="col-sm-5">
				<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'start_date',
			'value' => $model->start_date,
			'htmlOptions' => array('class' => 'form-control'),
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				'format' => 'yyyy-mm-dd',
				),
			));
; ?>
				<?php echo $form->error($model,'start_date'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'end_date'); ?>
			<div class="col-sm-5">
				<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'end_date',
			'value' => $model->end_date,
			'htmlOptions' => array('class' => 'form-control'),
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				'format' => 'yyyy-mm-dd',
				),
			));
; ?>
				<?php echo $form->error($model,'end_date'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'description_ar'); ?>
			<div class="col-sm-9">
				<?php echo $form->textArea($model, 'description_ar', array('class'=>'ckeditor_rtl')); ?>
				<?php echo $form->error($model,'description_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'description_en'); ?>
			<div class="col-sm-9">
				<?php echo $form->textArea($model, 'description_en', array('class'=>'ckeditor_ltr')); ?>
				<?php echo $form->error($model,'description_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'address_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'address_ar', array('maxlength' => 150)); ?>
				<?php echo $form->error($model,'address_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'address_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'address_en', array('maxlength' => 150)); ?>
				<?php echo $form->error($model,'address_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'lat'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'lat'); ?>
				<?php echo $form->error($model,'lat'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'lng'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'lng'); ?>
				<?php echo $form->error($model,'lng'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'organization_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'organization_ar', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'organization_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'organization_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'organization_en', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'organization_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'notes_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'notes_ar'); ?>
				<?php echo $form->error($model,'notes_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'notes_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'notes_en'); ?>
				<?php echo $form->error($model,'notes_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'accepts_online_payment'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'accepts_online_payment'); ?>
				<?php echo $form->error($model,'accepts_online_payment'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'accepts_offline_payment'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'accepts_offline_payment'); ?>
				<?php echo $form->error($model,'accepts_offline_payment'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'last_payment_date'); ?>
			<div class="col-sm-5">
				<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'last_payment_date',
			'value' => $model->last_payment_date,
			'htmlOptions' => array('class' => 'form-control'),
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				'format' => 'yyyy-mm-dd',
				),
			));
; ?>
				<?php echo $form->error($model,'last_payment_date'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'max_persons'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'max_persons'); ?>
				<?php echo $form->error($model,'max_persons'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'price'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'price'); ?>
				<?php echo $form->error($model,'price'); ?>
			</div>
		</div>

		<?php if (Yii::app()->user->can(UserPermissions::Event_Publish)): ?>
        <div class="form-group"> 
            <?php echo $form->labelEx($model,'is_active'); ?>
            <div class="col-sm-5"> 
                <?php echo $form->checkBox($model, 'is_active'); ?>
                <?php echo $form->error($model,'is_active'); ?>
            </div> 
        </div> 
    	<?php endif; ?>
		
<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->