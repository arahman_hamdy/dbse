<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'place_id'); ?>
		<?php echo $form->dropDownList($model, 'place_id', GxHtml::listDataEx(Place::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'name_ar'); ?>
		<?php echo $form->textField($model, 'name_ar', array('maxlength' => 100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'name_en'); ?>
		<?php echo $form->textField($model, 'name_en', array('maxlength' => 100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'start_date'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'start_date',
			'value' => $model->start_date,
			'htmlOptions' => array('class' => 'form-control'),
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				'format' => 'yyyy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'end_date'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'end_date',
			'value' => $model->end_date,
			'htmlOptions' => array('class' => 'form-control'),
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				'format' => 'yyyy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'description_ar'); ?>
		<?php echo $form->textArea($model, 'description_ar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'description_en'); ?>
		<?php echo $form->textArea($model, 'description_en'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'image_id'); ?>
		<?php echo $form->dropDownList($model, 'image_id', GxHtml::listDataEx(LibFile::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'address_ar'); ?>
		<?php echo $form->textField($model, 'address_ar', array('maxlength' => 150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'address_en'); ?>
		<?php echo $form->textField($model, 'address_en', array('maxlength' => 150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'lat'); ?>
		<?php echo $form->textField($model, 'lat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'lng'); ?>
		<?php echo $form->textField($model, 'lng'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'organization_ar'); ?>
		<?php echo $form->textField($model, 'organization_ar', array('maxlength' => 100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'organization_en'); ?>
		<?php echo $form->textField($model, 'organization_en', array('maxlength' => 100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'notes_ar'); ?>
		<?php echo $form->textArea($model, 'notes_ar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'notes_en'); ?>
		<?php echo $form->textArea($model, 'notes_en'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'accepts_online_payment'); ?>
		<?php echo $form->dropDownList($model, 'accepts_online_payment', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'accepts_offline_payment'); ?>
		<?php echo $form->dropDownList($model, 'accepts_offline_payment', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'max_persons'); ?>
		<?php echo $form->textField($model, 'max_persons'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'registered_persons'); ?>
		<?php echo $form->textField($model, 'registered_persons'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'price'); ?>
		<?php echo $form->textField($model, 'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'likes'); ?>
		<?php echo $form->textField($model, 'likes'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'reports'); ?>
		<?php echo $form->textField($model, 'reports'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'attending'); ?>
		<?php echo $form->textField($model, 'attending'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(Users::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'status'); ?>
		<?php echo $form->textField($model, 'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'is_deleted'); ?>
		<?php echo $form->dropDownList($model, 'is_deleted', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'is_active'); ?>
		<?php echo $form->dropDownList($model, 'is_active', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'dateline'); ?>
		<?php echo $form->textField($model, 'dateline'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
