<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'blog-menu-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>	<?php if ($model->errors): ?>
		<div class="alert alert-danger">
			<?php echo $form->errorSummary($model); ?>
		</div>
	<?php endif; ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_ar', array('class'=>'rtl', 'maxlength' => 200)); ?>
				<?php echo $form->error($model,'name_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_en', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'name_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'parent_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'parent_id', BlogMenu::getListTreeView(),array("prompt"=>"")); ?>
				<?php echo $form->error($model,'parent_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'type'); ?>
			<div class="col-sm-5">
				<?php echo $form->enumDropDownList($model, 'type',array("prompt"=>"-")); ?>
				<?php Yii::app()->clientScript->registerScript("block_type_change",'
					jQuery("#' . CHtml::activeId($model,'type') . '").on("change",function(e){
						var thisVal = jQuery(this).val();
		
						jQuery(".type_free").hide();
						jQuery(".type_category").hide();
						jQuery(".type_page").hide();
			
						if (thisVal == "URL"){
							jQuery(".type_free").show();
						}else if (thisVal == "Category"){
							jQuery(".type_category").show();
						}else if (thisVal == "Article"){
							jQuery(".type_page").show();
						}
					});
					jQuery("#' . CHtml::activeId($model,'type') . '").trigger("change");
					') ?>
				
				<?php echo $form->error($model,'type'); ?>
			</div>
		</div>
		<div class="form-group type_category">
			<?php echo $form->labelEx($model,'category_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'category_id',BlogCategory::getListTreeView(),array("prompt"=>"")); ?>
				<?php echo $form->error($model,'category_id'); ?>
			</div>
		</div>
		<div class="form-group type_page">
			<?php echo $form->labelEx($model,'article_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'article_id', array(''=>'') + GxHtml::listDataEx(BlogArticle::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'article_id'); ?>
			</div>
		</div>
		<div class="form-group type_free">
			<?php echo $form->labelEx($model,'url'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'url', array('maxlength' => 500)); ?>
				<?php echo $form->error($model,'url'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'icon'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'icon', array('maxlength' => 20)); ?>
				<?php echo $form->error($model,'icon'); ?>
			</div>
				<a href="http://fontawesome.io/icons/" target="_blank">Reference</a>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'sortid'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sortid'); ?>
				<?php echo $form->error($model,'sortid'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->