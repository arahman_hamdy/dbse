<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('name_ar')); ?>:
	<?php echo GxHtml::encode($data->name_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_en')); ?>:
	<?php echo GxHtml::encode($data->name_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('parent_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->parent)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('type')); ?>:
	<?php echo GxHtml::encode($data->type); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('category_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->category)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('article_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->article)); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('url')); ?>:
	<?php echo GxHtml::encode($data->url); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('icon')); ?>:
	<?php echo GxHtml::encode($data->icon); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_deleted')); ?>:
	<?php echo GxHtml::encode($data->is_deleted); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sortid')); ?>:
	<?php echo GxHtml::encode($data->sortid); ?>
	<br />
	*/ ?>

</div>