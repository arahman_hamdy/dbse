<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'lib-cat-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>		<div class="form-group">
			<?php echo $form->labelEx($model,'title'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'title', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'title'); ?>
			</div>
		</div>
		<!-- 
		<div class="form-group">
			<?php echo $form->labelEx($model,'thumb_file'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'thumb_file', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'thumb_file'); ?>
			</div>
		</div>
		-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'parent_cat_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'parent_cat_id', LibCat::getListTreeView(),array("prompt"=>""));?>
				<?php echo $form->error($model,'parent_cat_id'); ?>
			</div>
		</div>
		<!-- 
		<div class="form-group">
			<?php echo $form->labelEx($model,'notes'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'notes', array('maxlength' => 300)); ?>
				<?php echo $form->error($model,'notes'); ?>
			</div>
		</div>
		 -->
		<div class="form-group">
			<?php echo $form->labelEx($model,'sortid'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sortid'); ?>
				<?php echo $form->error($model,'sortid'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->