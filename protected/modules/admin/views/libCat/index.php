<?php

$this->breadcrumbs = array(
	LibCat::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . LibCat::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . LibCat::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode(LibCat::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'lib-cat-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'show_create_button' => Yii::app()->user->can(UserPermissions::LibCat_Create),
	'columns' => array(
		'id',
		'title',
		array(
				'name'=>'parent_cat_id',
				'value'=>'GxHtml::valueEx($data->parentCat)',
				'filter'=>GxHtml::listDataEx(LibCat::model()->findAllAttributes(null, true)),
				),
		//'notes',
		'sortid',
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{remove}',
			'buttons'=>array(
				'info'=>array(
					'options'=>array(
						'title'=>Yii::t('app','Details'),
						'class'=>'btn btn-info btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-info"></i> ' . Yii::t('app','View'),
					'url'=>'Yii::app()->controller->createUrl("view", array("id"=>$data->id))',
				),
				'edit'=>array(
					'visible'=>'$data->id>5 && '.Yii::app()->user->can(UserPermissions::LibCat_Update),
					'options'=>array(
						'title'=>Yii::t('app','Edit'),
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> ' . Yii::t('app','Edit'),
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'remove'=>array(
					'visible'=>'$data->id>5 && '.Yii::app()->user->can(UserPermissions::LibCat_Delete),
					'options'=>array(
						'title'=>Yii::t('app','Delete'),
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-cancel"></i> ' . Yii::t('app','Delete'),
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#lib-cat-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#lib-cat-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#lib-cat-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
