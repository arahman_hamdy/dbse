		<div class="col-sm-2 col-xs-4">
			
			<article class="album">
				
				<header>
					
					<a href="<?php echo $this->createUrl($this->action->id,array('cat'=>$data->id)+$this->getQueryArgs());?>">
						<img src="<?php echo $this->uploadsUrl . (($data->thumb_file)?$data->thumb_file:"folder_base.png"); ?>" />
					</a>
					
					<?php if (!$this->is_modal && Yii::app()->user->can(UserPermissions::LibCat_Update)): ?>
					<a href="<?php echo $this->createUrl('update',array('id'=>$data->id));?>" class="album-options">
						<i class="entypo-cog"></i>
						<?php echo Yii::t("app","Edit");?>
					</a>
					<?php endif; ?>
					
				</header>
				
				<section class="album-info">
					<h3><a href="<?php echo $this->createUrl($this->action->id,array('cat'=>$data->id)+$this->getQueryArgs());?>"><?php echo $data->title ;?></a></h3>
					<?php /* ?>
					<p style="overflow: hidden;height:35px"><?php echo $data->notes . '&nbsp;' ;?></p>
					<?php */ ?>
				</section>
				
			</article>
			
		</div>