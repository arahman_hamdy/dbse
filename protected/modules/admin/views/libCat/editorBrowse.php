<?php 

?>
	<?php if (1 || !$is_modal): ?>

	<div class="panel panel-default" data-collapsed="<?php echo (count($messages))?0:1?>">
	
		<div class="panel-heading">
			<div class="panel-title">
				<?php echo Yii::t("app","Upload file(s)");?>
			</div>
			
			<div class="panel-options">
				<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
			</div>			
		</div>
		
		<div class="panel-body">
		
			<div class="col-md-12">
				<?php foreach($messages as $message):?>
					<div class="alert alert-danger"><strong><?php echo Yii::t("app","Error!")?></strong> <?php echo $message;?></div>
				<?php endforeach;?>
			</div>
			
			<div class="row"></div>
			<?php $this->renderPartial('//layouts/form_includes'); ?>
			
			<?php $form=$this->beginWidget('CActiveForm', array(
			    'id'=>'upload-form',
				//'action'=>"upload",
			    'enableAjaxValidation'=>false,
			    'htmlOptions' => array('enctype' => 'multipart/form-data',"class"=>"form-horizontal form-groups-bordered"), // ADD THIS
			));?>
			
				<?php for($i=1;$i<=3;$i++):?>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo Yii::t("app","Select file") . " " . $i; ?> </label>
					
					<div class="col-sm-4">
						
						<input type="file" accept="image/*;capture=camera" name="files[]" class="form-control file2 inline btn btn-primary" data-label="<i class='glyphicon glyphicon-circle-arrow-up'></i> &nbsp;<?php echo Yii::t("app","Browse File");?>" />
						
					</div>
					
					<label class="col-sm-1 control-label"><?php echo Yii::t("app","Tags");?></label>
					<div class="col-sm-5">
						<div class="">
							<input type="text" name="tags[]" class="tagsinput" value="">
						</div>
					</div>
				</div>
				<?php endfor; ?>
				<?php /*
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo Yii::t("app","Youtube video"); ?> </label>
					
					<div class="col-sm-4">
						
						<input type="text" name="youtube[]" class="form-control file2 inline" />
						
					</div>
					
					<label class="col-sm-1 control-label"><?php echo Yii::t("app","Tags");?></label>
					<div class="col-sm-5">
						<div class="">
							<input type="text" name="tags[]" class="tagsinput" value="">
						</div>
					</div>
				</div>
				<?php */ ?>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-5">
						<button type="submit" class="btn btn-blue">
							<?php echo Yii::t("app","Upload")?>
						</button>
					</div>
					
					<div class="col-sm-5">
						<div class="tag_note">
							<div class="label label-default clear"><?php echo Yii::t("admin","Info");?></div> <small><?php echo Yii::t("admin",'Press "Enter" after each tag to commit');?></small><div class="clear"></div>
						</div>
					</div>
					
				</div>								
			
			<?php $this->endWidget(); ?><!-- form -->
		
		</div>
	
	</div>
	
<br />
		<?php endif;?>
	
<div class="gallery-env">

	<div class="row">
		
		<?php 
			$this->widget('zii.widgets.CListView', array(
					'dataProvider'=>$model->search(),
					'itemView'=>'_editorBrowseCat',
					'emptyText' => '',
					'template' => "{summary}\n{sorter}\n{items}\n<div class='clear'></div>{pager}",
			));
			
		?>
	
	</div>
	
	<?php  if ($fileModel):?>
	<div class="row">
		<?php 
		
			$this->widget('zii.widgets.CListView', array(
					'dataProvider'=>$fileModel,
					'itemView'=>'_editorBrowseFile',
					'emptyText' => 'No files found',
					'template' => "{summary}\n{sorter}\n{items}\n<div class='clear'></div>{pager}",
			));
			
		?>
	</div>
	<?php endif;?>
	
	
</div>

<?php if ($is_modal): ?>
<script>
	jQuery(document).on("click",".editor_file",function(e){
		e.preventDefault();

		<?php 
		$funcNum = "";
		
		if (isset($_GET['CKEditorFuncNum']))
			$funcNum = $_GET['CKEditorFuncNum'];
		else if (isset($_GET['libraryFilePopup']) && isset($_GET['func_num']))
			$funcNum = $_GET['func_num'];
		?>
		
		var funcNum = '<?php echo $funcNum?>';
		var fileUrl = this.href;
		

		
		<?php if (isset($_GET['CKEditorFuncNum'])): ?>
			window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
			window.close();
			
		<?php elseif (isset($_GET['libraryFilePopup']) && isset($_GET['func_num'])): ?>
			var editor_file_id = jQuery(this).closest("div.lib_editor_file").find("input.editor_file_id").val();
			thumb_url = jQuery(this).closest(".image-thumb").find("img").attr("src");
			window.top.libraryFilePopupFunction( funcNum, editor_file_id, thumb_url, fileUrl );
			window.top.document.getElementById('modalExternal_close').click()
			
		<?php endif; ?>
		
		
		
		
	});
</script>
<?php endif;?>