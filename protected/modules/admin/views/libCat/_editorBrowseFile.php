				<div class="col-sm-2 col-xs-4 lib_editor_file">
					<article class="image-thumb">
						
						<?php if ($data->type=='y'):?>
							<a href="https://www.youtube.com/watch?v=<?php echo $data->path;?>" class="image file_<?php echo $data->type;?> editor_file" target="_blank">
						<?php else:?>
							<a href="<?php echo $this->uploadsUrl . $data->path;?>" class="image file_<?php echo $data->type;?> editor_file" target="_blank">
						<?php endif;?>
						
						<?php if ($data->type=='i'):?>
							<?php $this->beginWidget('application.extensions.thumbnailer.Thumbnailer', array(
							 
							        'thumbsDir' => 'images/thumbs',
									'thumbsUrl' => $this->imagesUrl . 'thumbs',
							        'thumbWidth' => 250,
							        'thumbHeight' => 250, // Optional
							    )
							); ?>
							<img src="<?php echo $this->uploadsUrl . $data->path; ?>" />
							<?php $this->endWidget(); ?>
						<?php else:?>
							<img src="<?php echo $this->uploadsUrl . (($data->thumb_path)?$data->thumb_path:"file_other.png"); ?>" />
						<?php endif;?>
						
						</a>
						
						<?php if (!$this->is_modal && Yii::app()->user->can(UserPermissions::LibFile_DeleteVideo)):?>
						<div class="image-options">
							<a href="<?php echo $this->createUrl("deleteFile",array('id'=>$data->id));?>" class="delete" onclick="return confirm('<?php echo Yii::t("app","Are you sure you want to delete the selected file?");?>');"><i class="entypo-cancel"></i></a>
						</div>
						<?php endif; ?>
						
						<input type="hidden" class="editor_file_id" value="<?php echo $data->id; ?>" />
						
						<section class="album-info">
							<h4 style="text-overflow:ellipsis;white-space: nowrap;overflow: hidden;">
								<?php if ($data->type=='y'):?>
									<a href="https://www.youtube.com/watch?v=<?php echo $data->path;?>" class="image file_<?php echo $data->type;?> editor_file" target="_blank"><?php echo $data->name ;?></a>
								<?php else:?>
									<a class="editor_file" href="<?php echo $this->uploadsUrl . $data->path;?>"><?php echo $data->name ;?></a>
								<?php endif;?>
									
								
							</h4>
							
							<p style="text-overflow:ellipsis;white-space: nowrap;overflow: hidden;"><?php echo $data->dateline . '&nbsp;' ;?></p>
						</section>						
					</article>
				
				</div>
