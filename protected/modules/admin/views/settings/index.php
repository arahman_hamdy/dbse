<?php

$this->breadcrumbs = array(
	Settings::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Settings::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Settings::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode(Settings::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'settings-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		'key',
		'value',
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{view}{edit}{delete}',
			'buttons'=>array(
				'view'=>array(
					'options'=>array(
						'title'=>Yii::t('app','Details'),
						'class'=>'btn btn-info btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-info"></i>',
					'url'=>'Yii::app()->controller->createUrl("view", array("id"=>$data->id))',
				),
				'edit'=>array(
					'options'=>array(
						'title'=>Yii::t('app','Edit'),
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i>',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'details'=>array(
					'options'=>array(
						'title'=>Yii::t('app','Edit'),
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-cancel"></i>',
					'url'=>'Yii::app()->controller->createUrl("view", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#settings-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#settings-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#settings-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
