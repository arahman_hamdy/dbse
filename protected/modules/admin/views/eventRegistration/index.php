<?php

$this->breadcrumbs = array(
	EventRegistration::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . EventRegistration::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . EventRegistration::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode(EventRegistration::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php	$dataProvider = $model->search();
	$dataProvider->criteria->order = "t.id desc";
	
	$this->widget('CGridViewCustom', array(
	'id' => 'event-registration-grid',
	'dataProvider' => $dataProvider,
	'show_create_button' => false,
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		array(
				'name'=>'event_id',
				'value'=>'GxHtml::valueEx($data->event)',
				),
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				),
		'name',
		'persons',
		'total',
		'is_paid',
		/*
		'notes',
		array(
				'name'=>'transaction_id',
				'value'=>'GxHtml::valueEx($data->transaction)',
				'filter'=>GxHtml::listDataEx(Transaction::model()->findAllAttributes(null, true)),
				),
		'price',
		'status',
		'dateline',
		*/
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{details}',
			'buttons'=>array(
				'details'=>array(
					'options'=>array(
						'title'=>'Details',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-doc-text"></i> Details',
					'url'=>'Yii::app()->controller->createUrl("view", array("id"=>$data->id))',
				),
			)
		),
	),
)); ?>
</div>