<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'event-registration-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>
	<?php if ($model->errors): ?>
		<div class="alert alert-danger">
			<?php echo $form->errorSummary($model); ?>
		</div>
	<?php endif; ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'event_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'event_id', array(''=>'') + GxHtml::listDataEx(Event::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'event_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'user_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'user_id', array(''=>'') + GxHtml::listDataEx(Users::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'user_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'phone'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'phone', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'phone'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'email'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'email', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'persons'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'persons'); ?>
				<?php echo $form->error($model,'persons'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'total'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'total'); ?>
				<?php echo $form->error($model,'total'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'notes'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'notes', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'notes'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'transaction_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'transaction_id', array(''=>'') + GxHtml::listDataEx(Transaction::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'transaction_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'price'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'price'); ?>
				<?php echo $form->error($model,'price'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'is_paid'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'is_paid'); ?>
				<?php echo $form->error($model,'is_paid'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'status'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'status'); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'dateline'); ?>
			<div class="col-sm-5">
				<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'dateline',
			'value' => $model->dateline,
			'htmlOptions' => array('class' => 'form-control'),
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				'format' => 'yyyy-mm-dd',
				),
			));
; ?>
				<?php echo $form->error($model,'dateline'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->