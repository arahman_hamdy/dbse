<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'event',
			'type' => 'raw',
			'value' => $model->event !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->event)), array('event/view', 'id' => GxActiveRecord::extractPkValue($model->event, true))) : null,
			),
array(
			'name' => 'user',
			'type' => 'raw',
			'value' => $model->user !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->user)), array('users/view', 'id' => GxActiveRecord::extractPkValue($model->user, true))) : null,
			),
'name',
'phone',
'email',
'persons',
'total',
'notes',
array(
			'name' => 'transaction',
			'type' => 'raw',
			'value' => $model->transaction !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->transaction)), array('transaction/view', 'id' => GxActiveRecord::extractPkValue($model->transaction, true))) : null,
			),
'price',
array(
			'name' => 'payment_method',
			'type' => 'raw',
			'value' => $model->payment_method == 1 ? 'Online':'Offline',
			),
'is_paid',
'status',
'dateline',
	),
)); ?>

