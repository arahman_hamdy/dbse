<div class="panel panel-default panel-shadow" data-collapsed="0">
<div class="panel-heading">
<div class="panel-title">Attributes</div>
<div class="panel-options"><a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a></div> </div>
<!-- panel body -->
<div class="panel-body">

		<div class="form-groups-bordered">
		<div class="form-group hidden"></div>
		<?php foreach($models as $model):
		?>
		<div class="translate-group">
		<div class="form-group">
		<?php
		$htmlMethod = "inputField";
		$cssClass = "form-control";
		$htmlOptions = array();
		$TwoInRow = true;
		switch($model->attribute_type_id){
			case 1: // integer
				$htmlMethod = "numberField";
			break;
			case 7: // address
			case 3: // textarea
				$htmlMethod = "textArea";
			break;
			case 4: // html
				$htmlMethod = "textArea";
				$cssClass .= " ckeditor";
				$TwoInRow = false;
			break;
			case 5: // url
				$htmlMethod = "urlField";

			break;
			case 6: // boolean
				$htmlMethod = "checkBox";
				$cssClass = "icheck";
				Yii::app()->clientScript->registerScript('attribute_checkbox',"		if($('input.icheck').length > 0) {
						$('input.icheck').iCheck({
							checkboxClass: 'icheckbox_minimal-blue',
							radioClass: 'iradio_minimal-blue'
						});
					}");
			break;
			case 2: // string
			default:
				$htmlMethod = "textField";

			break;
		}
		?>

			<?php echo CHtml::label($model->name_en,"EntityAttribute_{$model->id}_value_en",array('class'=>(($TwoInRow)?'col-sm-2':'col-sm-2') . ' control-label')); ?>
			<div class="<?php echo (($TwoInRow)?'col-sm-4':'col-sm-9'); ?>">
				<?php echo CHtml::$htmlMethod("EntityAttribute[{$model->id}][value_en]", ($model->entityAttribute)?$model->entityAttribute->value_en:'',array('class'=>str_replace("ckeditor","ckeditor_ltr","entity_attribute lang_en " . $cssClass))); ?>
				<a class="entity_attribute_translate translate" data-lang-from="ar" data-lang-to="en" href="#">From Arabic</a>
			</div>
		<?php if(!$TwoInRow): ?>
		</div>
		<div class="form-group">
		<?php endif; ?>
			<?php echo CHtml::label($model->name_ar,"EntityAttribute_{$model->id}_value_ar",array('class'=>(($TwoInRow)?'col-sm-2':'col-sm-2') . ' control-label')); ?>
			<div class="<?php echo (($TwoInRow)?'col-sm-4':'col-sm-9'); ?>">
				<?php echo CHtml::$htmlMethod("EntityAttribute[{$model->id}][value_ar]", ($model->entityAttribute)?$model->entityAttribute->value_ar:'',array('class'=>str_replace("ckeditor","ckeditor_rtl","entity_attribute lang_ar " . $cssClass))); ?>
				<a class="entity_attribute_translate translate" data-lang-from="en" data-lang-to="ar"  href="#">From English</a>
			</div>

		</div>
		</div>
		<?php endforeach; ?>
		</div>

</div>
</div> 
