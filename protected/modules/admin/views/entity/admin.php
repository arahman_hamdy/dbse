<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('entity-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'entity-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		array(
				'name'=>'type',
				'value'=>'GxHtml::valueEx($data->type0)',
				'filter'=>GxHtml::listDataEx(EntityType::model()->active()->findAllAttributes(null, true)),
				),
		'name_ar',
		'name_en',
		array(
				'name'=>'parent_id',
				'value'=>'GxHtml::valueEx($data->parent)',
				'filter'=>GxHtml::listDataEx(Entity::model()->findAllAttributes(null, true)),
				),
		array(
					'name' => 'has_contents',
					'value' => '($data->has_contents == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		/*
		'slug',
		array(
				'name'=>'image_id',
				'value'=>'GxHtml::valueEx($data->image)',
				'filter'=>GxHtml::listDataEx(LibFile::model()->findAllAttributes(null, true)),
				),
		'contents_ar',
		'contents_en',
		'keywords',
		'description',
		'sortid',
		array(
					'name' => 'is_active',
					'value' => '($data->is_active == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		array(
					'name' => 'is_deleted',
					'value' => '($data->is_deleted == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'dateline',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>