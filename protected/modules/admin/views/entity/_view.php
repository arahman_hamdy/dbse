<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('type')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->type0)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_ar')); ?>:
	<?php echo GxHtml::encode($data->name_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name_en')); ?>:
	<?php echo GxHtml::encode($data->name_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('parent_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->parent)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('has_contents')); ?>:
	<?php echo GxHtml::encode($data->has_contents); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('slug')); ?>:
	<?php echo GxHtml::encode($data->slug); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('image_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->image)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('contents_ar')); ?>:
	<?php echo GxHtml::encode($data->contents_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('contents_en')); ?>:
	<?php echo GxHtml::encode($data->contents_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('keywords')); ?>:
	<?php echo GxHtml::encode($data->keywords); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('description')); ?>:
	<?php echo GxHtml::encode($data->description); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sortid')); ?>:
	<?php echo GxHtml::encode($data->sortid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_active')); ?>:
	<?php echo GxHtml::encode($data->is_active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_deleted')); ?>:
	<?php echo GxHtml::encode($data->is_deleted); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateline')); ?>:
	<?php echo GxHtml::encode($data->dateline); ?>
	<br />
	*/ ?>

</div>