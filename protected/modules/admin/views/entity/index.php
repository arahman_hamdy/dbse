<?php

$this->breadcrumbs = array(
	Entity::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Entity::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Entity::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode( (($model->parent)?$model->parent . ' > ':null) . Entity::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'entity-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'show_create_button' => false,
	'additionalButtons' => array($this->createUrl('create',!empty($model->parent)?array('entityId'=>$model->parent_id):array())=>"Add new"),	
	'columns' => array(
		'id',
		array(
				'name'=>'type',
				'value'=>'GxHtml::valueEx($data->type0)',
				'filter'=>GxHtml::listDataEx(EntityType::model()->active()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'category_id',
				'value'=>'GxHtml::valueEx($data->category)',
				'filter'=>GxHtml::listDataEx(EntityCategory::model()->findAllAttributes(null, true)),
				),		
		'name_ar',
		'name_en',
		/*
		array(
				'name'=>'parent_id',
				'value'=>'GxHtml::valueEx($data->parent)',
				'filter'=>GxHtml::listDataEx(Entity::model()->findAllAttributes(null, true)),
				),
		array(
					'name' => 'has_contents',
					'value' => '($data->has_contents == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'slug',
		array(
				'name'=>'image_id',
				'value'=>'GxHtml::valueEx($data->image)',
				'filter'=>GxHtml::listDataEx(LibFile::model()->findAllAttributes(null, true)),
				),
		'contents_ar',
		'contents_en',
		'keywords',
		'description',
		'sortid',
		array(
					'name' => 'is_active',
					'value' => '($data->is_active == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		array(
					'name' => 'is_deleted',
					'value' => '($data->is_deleted == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'dateline',
		*/
		array(
				'name'=>'country_id',
				'value'=>'GxHtml::valueEx($data->country)',
				'filter'=>GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true)),
				),		
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{subentities}{campuses}{remove}',
			'buttons'=>array(
				'edit'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> Edit',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'remove'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-cancel"></i> Delete',
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
				'subentities'=>array(
					'options'=>array(
						'title'=>'Sub entities',
						'class'=>'btn btn-green btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-list"></i> Sub entities',
					'url'=>'Yii::app()->controller->createUrl("entity/index", array("entityId"=>$data->id))',
				),				
				'campuses'=>array(
					'visible'=>'!$data->parent_id',
					'options'=>array(
						'title'=>'Campuses',
						'class'=>'btn btn-blue btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-location"></i> Campuses',
					'url'=>'Yii::app()->controller->createUrl("campus/index", array("entityId"=>$data->id))',
				),				
			)
		),
	),
)); ?>
</div>

<style type="text/css">
	.grid-view .button-column {
	    width: 290px;
	}
</style>
<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#entity-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#entity-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#entity-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
