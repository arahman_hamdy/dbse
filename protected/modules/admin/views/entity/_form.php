<?php $this->renderPartial('//layouts/form_includes'); ?>
<?php
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/js/ckeditor/ckeditor.js?t=2', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/js/ckeditor/adapters/jquery.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/js/bootstrap-tagsinput.min.js', CClientScript::POS_END);
?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'entity-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>
	<?php if ($model->errors): ?>
		<div class="alert alert-danger">
			<?php echo $form->errorSummary($model); ?>
		</div>
	<?php endif; ?>

		<div class="form-group">
			<?php echo $form->labelEx($model,'parent_id'); ?>
			<div class="col-sm-5">
				<?php echo CHtml::label(($model->parent)?$model->parent:'-','',array('class'=>'control-label')); ?>
				<?php echo $form->hiddenField($model, 'parent_id'); ?>
				<?php // echo $form->dropDownList($model, 'parent_id', array(''=>'') + GxHtml::listDataEx(Entity::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'parent_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'type'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'type', array(''=>'') + GxHtml::listDataEx(EntityType::model()->active()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'type'); ?>
			</div>
		</div>
        <div class="form-group"> 
            <?php echo $form->labelEx($model,'category_id'); ?>
            <div class="col-sm-5"> 
                <?php echo $form->dropDownList($model, 'category_id', array(''=>'') + GxHtml::listDataEx(EntityCategory::model()->findAllAttributes(null, true))); ?>
                <?php echo $form->error($model,'category_id'); ?>
            </div> 
        </div> 
        <div class="form-group"> 
            <?php echo $form->labelEx($model,'country_id'); ?>
            <div class="col-sm-5"> 
                <?php echo $form->dropDownList($model, 'country_id', GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true))); ?>
                <?php echo $form->error($model,'country_id'); ?>
            </div> 
        </div>         		
		<div class="form-group">
			<?php echo $form->labelEx($model,'name_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_ar', array('class'=>'rtl', 'maxlength' => 50)); ?>
				<?php echo $form->error($model,'name_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'name_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'name_en', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'name_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'has_contents'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'has_contents'); ?>
				<?php echo $form->error($model,'has_contents'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'entityImages'); ?>
			<div class="col-sm-9">
		<?php 
		$url=$this->createUrl('deleteImage');
		$this->widget('ext.EFineUploader.EFineUploader',
		 array(
		   'id'=>'FineUploader',
		   'config'=>array(
		   'autoUpload'=>true,
		   'request'=>array(
			  'endpoint'=>$this->createUrl('upload'),//'/files/upload',// OR ,
			  'params'=>array('YII_CSRF_TOKEN'=>Yii::app()->request->csrfToken),
						   ),
		   'retry'=>array('enableAuto'=>true,'preventRetryResponseProperty'=>true),
		   'chunking'=>array('enable'=>true,'partSize'=>100),//bytes
			'template'=>'<div class="qq-drop-processing"></div><div class="qq-upload-drop-area"></div>
					<div class="qq-uploader span4 left"><div class="qq-upload-button btn left">' . Yii::t('app','Upload image') . '</div>
					<div class="clear"></div>
					<ul class="qq-upload-list"></ul><br/></div>',
						   
		   'callbacks'=>array(
				'onComplete'=>"js:function(id, name, response){
				   if(response.success){
					$('#entity_images').append(
						$('<div />')
							.addClass('image')
							.append($('<span />').addClass('filename').append($('<label />')
								.append($('<input />').attr('name','Entity[main_image_id]').attr('value',response.id).attr('type','radio'))
								.append(' " . Yii::t('app','Thumbnail') . "')
								))
							.append($('<span />').addClass('close').html('X').click(function(){
							 if (!confirm('Are you sure you want to delete this image?')) return false;								
							 $.ajax({
								url: '". $url . "',
								type: 'POST',
								data: { 'id': response.id },
								success: function(data){}
							});
							\$(this).parent().remove()}))
							.append($('<img />').attr('src',response.folder + response.filename))
							.append($('<input />').attr('type','hidden').attr('value',response.id).attr('name','Entity[libFiles][]'))
						);
				   }
			   }",
				'onError'=>"js:function(id, name, errorReason){}",
				 ),
		   'validation'=>array(
					 'allowedExtensions'=>array('jpg','jpeg','png','gif'),
					 'sizeLimit'=>2 * 1024 * 1024,//maximum file size in bytes
					 //'minSizeLimit'=>2*1024*1024,// minimum file size in bytes
							  ),
		  )
		  ));
	 
			?>
			<?php echo $form->error($model,'main_image_id'); ?>
		
			<div id="entity_images">
				<?php
				$imagesUrlPath = $this->uploadsUrl;
				$imagesArray = array();


				if (!empty($_POST['Entity']['libFiles'])){
					$AdditionalFiles = $_POST['Entity']['libFiles'];

					$criteria = new CDbCriteria;
					$criteria->addInCondition('id',$AdditionalFiles);
					$AdditionalImages = LibFile::model()->findAll($criteria);

					foreach ($AdditionalImages as $image) {
						$imagesArray[$image->id] = $image;
					}
				}else{
					foreach ($model->entityImages as $entityImage) {
						$imagesArray[$entityImage->image_id] = $entityImage->image;
					}					
				}

				foreach($imagesArray as $image){
					?>
					<div class='image'>
					<span class='filename'><label><input type='radio' name='Entity[main_image_id]' value='<?php echo $image->id;?>' <?php if ($image->id==$model->main_image_id)echo " checked='checked' "; ?> /> <?php echo Yii::t('app','Thumbnail');?></label></span>
					<span class='close'>X</span>
					<img src='<?php echo "$imagesUrlPath{$image->path}"; ?>' />
					<input type='hidden' value='<?php echo $image->id;?>' name='Entity[libFiles][]' />
					</div>
					<?php 
				}
				
			
				
				if (count($imagesArray))
				{	//Yii::app()->clientScript->registerScript('images_close','$("#entity_images span.close").click(function(){
					//$(this).parent().remove()});');
					$url = CJavaScript::encode($this->createUrl('deleteImage'));
					Yii::app()->clientScript->registerScript('images_close','$("#entity_images span.close").click(function(){
						if (!confirm("Are you sure you want to delete this image?")) return false;
					 $.ajax({
						url: '. $url . ',
						type: "POST",
						data: { "id": $(this).parent().children("input:hidden").val()},
						success: function(data){}
					});
					$(this).parent().remove();
					
					
					});');
				}
					?>
			</div>
			<div class="clear"></div>
		</div>
		</div>

		<div class="form-group contents_field">
			<?php echo $form->labelEx($model,'slug'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'slug', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'slug'); ?>
			</div>
		</div>

		<div class="form-group contents_field">
			<?php echo $form->labelEx($model,'contents_ar'); ?>
			<div class="col-sm-9">
				<?php echo $form->textArea($model, 'contents_ar', array('class'=>'ckeditor_rtl')); ?>
				<?php echo $form->error($model,'contents_ar'); ?>
			</div>
		</div>
		<div class="form-group contents_field">
			<?php echo $form->labelEx($model,'contents_en'); ?>
			<div class="col-sm-9">
				<?php echo $form->textArea($model, 'contents_en', array('class'=>'ckeditor_ltr')	); ?>
				<?php echo $form->error($model,'contents_en'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'city'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'city', array('' => '-') + GxHtml::listDataEx(City::model()->findAllAttributes(null, true)),array('' => '-')); ?>
				<?php echo $form->error($model,'city'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'region_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'region_id',array('' => '-')); ?>
				<?php echo $form->error($model,'region_id'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'lat'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'lat'); ?>
				<?php echo $form->error($model,'lat'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'lng'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'lng'); ?>
				<?php echo $form->error($model,'lng'); ?>
			</div>
		</div>

		<div class="form-group contents_field">
			<?php echo $form->labelEx($model,'keywords'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'keywords', array('maxlength' => 1500, 'class'=>'tagsinput')); ?>
				<?php echo $form->error($model,'keywords'); ?>
			</div>
		</div>
		<div class="form-group contents_field">
			<?php echo $form->labelEx($model,'description'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'description', array('maxlength' => 1500)); ?>
				<?php echo $form->error($model,'description'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'sortid'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sortid'); ?>
				<?php echo $form->error($model,'sortid'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'is_active'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'is_active'); ?>
				<?php echo $form->error($model,'is_active'); ?>
			</div>
		</div>

<div id="attributes">
</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->

<script type="text/javascript">
	function loadAttributes(){
		jQuery("#attributes").load('<?php echo $this->createUrl("getAttributes",array('type_id'=>'TYPE_ID_PLACEHOLER','entity_id'=>$model->id));?>'.replace('TYPE_ID_PLACEHOLER',jQuery('#Entity_type').val()));
	}
	jQuery('#Entity_type').change(function(){
		loadAttributes();
	});
	loadAttributes();


	jQuery('#Entity_has_contents').on('ifChecked',function(){
		jQuery('.contents_field').show();
	});
	jQuery('#Entity_has_contents').on('ifUnchecked',function(){
		jQuery('.contents_field').hide();
	});

	if (jQuery('#Entity_has_contents').is(':checked')){
		jQuery('.contents_field').show();
	}else{
		jQuery('.contents_field').hide();
	}

	var defaultRegion = <?php echo (int)$model->region_id; ?>;
	var isRequestingRegions = false;
	jQuery("#Entity_city").change(function(){
		if (isRequestingRegions) return;
		jQuery("#Entity_city").attr("disabled","disabled");
		jQuery("#Entity_region_id").attr("disabled","disabled");

		jQuery.ajax({
				url: "<?php echo $this->createUrl("getCityRegions") ?>/id/" + jQuery("#Entity_city").val(),
				type: "get",
				dataType: "json",
				success: function (response) {
					jQuery("#Entity_region_id option.generated").remove();
					for(i in response){
						var option = jQuery("<option>").addClass("generated").val(i).html(response[i]);
						if (option.val()==defaultRegion) option.attr("selected","selected");
						jQuery("#Entity_region_id").append(option);
					}
				},
				complete: function(jqXHR, textStatus, errorThrown) {
					jQuery("#Entity_city").removeAttr("disabled");
					jQuery("#Entity_city").selectBoxIt("refresh");

					jQuery("#Entity_region_id").removeAttr("disabled");
					jQuery("#Entity_region_id").selectBoxIt("refresh");

				}


			});
	});
	jQuery("#Entity_city").trigger("change");

	jQuery(document).on("click",".entity_attribute_translate",function(e){
		e.preventDefault();
		var me = jQuery(this);
		fromLang = me.data('lang-from');
		toLang = me.data('lang-to');
		textToTranslate = me.closest("div.translate-group").find(".lang_"+fromLang).val();
		inputToUpdate = me.closest("div.translate-group").find(".lang_"+toLang);

        var url = '<?php echo $this->createUrl("/tstranslation/googleTranslate");?>';
        inputToUpdate.val("...");
        $.ajax({
            type: "POST",
            url: url,
            data: {"value": textToTranslate, "language": toLang, "from": fromLang},
            success: function (dataJson) {
                var data = JSON.parse(dataJson);
                if (data.ok == 1) {
                    inputToUpdate.val(data.message);
                } else {
                    alert(data.message);
                }
            }
        }, false);


		console.log(fromLang, toLang, textToTranslate, inputToUpdate);
	});
</script>