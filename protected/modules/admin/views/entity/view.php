<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'type0',
			'type' => 'raw',
			'value' => $model->type0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->type0)), array('entityType/view', 'id' => GxActiveRecord::extractPkValue($model->type0, true))) : null,
			),
'name_ar',
'name_en',
array(
			'name' => 'parent',
			'type' => 'raw',
			'value' => $model->parent !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->parent)), array('entity/view', 'id' => GxActiveRecord::extractPkValue($model->parent, true))) : null,
			),
'has_contents:boolean',
'slug',
array(
			'name' => 'image',
			'type' => 'raw',
			'value' => $model->image !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->image)), array('libFile/view', 'id' => GxActiveRecord::extractPkValue($model->image, true))) : null,
			),
'contents_ar',
'contents_en',
'keywords',
'description',
'sortid',
'is_active:boolean',
'is_deleted:boolean',
'dateline',
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('entities')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->entities as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('entity/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('entityAttributes')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->entityAttributes as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('entityAttribute/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('entityChildrens')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->entityChildrens as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('entityChildren/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('entityChildrens1')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->entityChildrens1 as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('entityChildren/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('entityImages')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->entityImages as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('entityImages/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('entityTypeAttributes')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->entityTypeAttributes as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('entityTypeAttribute/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>