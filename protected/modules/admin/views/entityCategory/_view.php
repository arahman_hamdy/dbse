<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('title_ar')); ?>:
	<?php echo GxHtml::encode($data->title_ar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('title_en')); ?>:
	<?php echo GxHtml::encode($data->title_en); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sortid')); ?>:
	<?php echo GxHtml::encode($data->sortid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('country_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->country)); ?>
	<br />

</div>