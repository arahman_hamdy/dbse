<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'block-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
	)
));
?>
		<?php if ($model->errors): ?>
			<div class="alert alert-danger">
				<?php echo $form->errorSummary($model); ?>
			</div>
		<?php endif; ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'title_ar'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'title_ar', array('class'=>'rtl', 'maxlength' => 100)); ?>
				<?php echo $form->error($model,'title_ar'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'title_en'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'title_en', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'title_en'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'type'); ?>
			<div class="col-sm-5">
				<?php echo $form->enumDropDownList($model, 'type',array("prompt"=>"-")); ?>
				<?php Yii::app()->clientScript->registerScript("block_type_change",'
					jQuery("#' . CHtml::activeId($model,'type') . '").on("change",function(e){
						var thisVal = jQuery(this).val();
		
						jQuery(".article_cat_panel").hide();
			
						if (thisVal == "Blog Category"){
							jQuery(".article_cat_panel").show();
						}
					});
					jQuery("#' . CHtml::activeId($model,'type') . '").trigger("change");
					') ?>
				<?php echo $form->error($model,'type'); ?>
			</div>
		</div>
		<div class="form-group article_cat_panel" style="display:none">
			<?php echo $form->labelEx($model,'article_cat_id'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'article_cat_id', BlogCategory::getListTreeView(),array("prompt"=>""));?>
				<?php echo $form->error($model,'article_cat_id'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'template_id'); ?>
			<div class="col-sm-5">
				<?php Yii::app()->clientScript->registerScript("block_template_change",'
					jQuery("#' . CHtml::activeId($model,'template_id') . '").on("change",function(e){
						var thisVal = jQuery(this).val();
		
						jQuery(".html_field").hide();
						
						if (thisVal == 11){
							jQuery(".html_field").show();
						}
						if (thisVal == 1){
						}
					});
					jQuery("#' . CHtml::activeId($model,'template_id') . '").trigger("change");
					') ?>			
				<?php echo $form->dropDownList($model, 'template_id', GxHtml::listDataEx(BlockTemplate::model()->findAllAttributes(null, true)),array("prompt"=>"-")); ?>
				<?php echo $form->error($model,'template_id'); ?>
			</div>
		</div>
		<div class="form-group html_field" style="display:none">
			<?php echo $form->labelEx($model,'html'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'html', array('class'=>'autogrow')); ?>
				<?php echo $form->error($model,'html'); ?>
			</div>
		</div>	
		<!--	
		<div class="form-group">
			<?php echo $form->labelEx($model,'position'); ?>
			<div class="col-sm-5">
				<?php echo $form->dropDownList($model, 'position',$this->positions); ?>
				<?php echo $form->error($model,'position'); ?>
			</div>
		</div>
		-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'sortid'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sortid'); ?>
				<?php echo $form->error($model,'sortid'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'status'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'status'); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
		</div>

<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->