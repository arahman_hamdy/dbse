<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('title')); ?>:
	<?php echo GxHtml::encode($data->title); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('image_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->image)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('url')); ?>:
	<?php echo GxHtml::encode($data->url); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('html')); ?>:
	<?php echo GxHtml::encode($data->html); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_html')); ?>:
	<?php echo GxHtml::encode($data->is_html); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateline')); ?>:
	<?php echo GxHtml::encode($data->dateline); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('is_active')); ?>:
	<?php echo GxHtml::encode($data->is_active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sortid')); ?>:
	<?php echo GxHtml::encode($data->sortid); ?>
	<br />
	*/ ?>

</div>