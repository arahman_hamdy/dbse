<?php $this->renderPartial('//layouts/form_includes'); ?>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'advertisement-form',
	'enableAjaxValidation' => false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate' => 'js:function(form, data, hasError) { 
			if(hasError) {
				for(var i in data) $("#"+i).parent().parent().addClass("validate-has-error");
				return false;
			}
			else {
				form.children().removeClass("validate-has-error");
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if(hasError) $("#"+attribute.id).parent().parent().addClass("validate-has-error");
			else $("#"+attribute.id).parent().parent().removeClass("validate-has-error"); 
		}'
	),
	'htmlOptions'=>array(
		'class'=>'form-horizontal form-groups-bordered',
		'enctype' => 'multipart/form-data',
	)
));
?>
	<?php if ($model->errors): ?>
		<div class="alert alert-danger">
			<?php echo $form->errorSummary($model); ?>
		</div>
	<?php endif; ?>

        <div class="form-group"> 
            <?php echo $form->labelEx($model,'country_id'); ?>
            <div class="col-sm-5"> 
                <?php echo $form->dropDownList($model, 'country_id', array(''=>'All') + GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true))); ?>
                <?php echo $form->error($model,'country_id'); ?>
            </div> 
        </div> 

		<div class="form-group">
			<?php echo $form->labelEx($model,'title'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'title', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'title'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'is_html'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'is_html'); ?>
				<?php echo $form->error($model,'is_html'); ?>
			</div>
		</div>

		<div class="form-group ad_image_type">
			<?php echo $form->labelEx($model,'image_file'); ?>
			<div class="col-sm-5">
				<img style="max-width:100%; max-height:100px" src="<?php echo $model->image_file; ?>" />

				<?php echo $form->fileField($model, 'image_file'); ?>
				<?php echo $form->error($model,'image_file'); ?>
			</div>
		</div>

		<div class="form-group ad_image_type">
			<?php echo $form->labelEx($model,'url'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'url', array('maxlength' => 200)); ?>
				<?php echo $form->error($model,'url'); ?>
			</div>
		</div>
		<div class="form-group ad_html_type" style="display:none">
			<?php echo $form->labelEx($model,'html'); ?>
			<div class="col-sm-5">
				<?php echo $form->textArea($model, 'html'); ?>
				<?php echo $form->error($model,'html'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'is_active'); ?>
			<div class="col-sm-5">
				<?php echo $form->checkBox($model, 'is_active'); ?>
				<?php echo $form->error($model,'is_active'); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'sortid'); ?>
			<div class="col-sm-5">
				<?php echo $form->textField($model, 'sortid',array("rows"=>7)); ?>
				<?php echo $form->error($model,'sortid'); ?>
			</div>
		</div>

		<div class="form-group form-head">
			<span><?php echo Yii::t("app", "Positions"); ?></span>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<?php echo $form->checkBoxList($model, 'advertisementPositions', GxHtml::encodeEx(GxHtml::listDataEx(AdvertisementPosition::model()->findAllAttributes(null, true)), false, true)); ?>
			</div>
		</div>


<div class="form-group">
	<div class="col-lg-12">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'btn btn-success')); ?>		<button type="reset" class="btn">Reset</button>
	</div>
</div>
<?php $this->endWidget(); ?><!-- form -->

<script>
jQuery("#Advertisement_is_html").on("ifChanged",function(){
	if ($(this).is(":checked")){
		$(".ad_html_type").show();
		$(".ad_image_type").hide();
	}else{
		$(".ad_image_type").show();
		$(".ad_html_type").hide();
	}

});
jQuery("#Advertisement_is_html").trigger("ifChanged");
</script>