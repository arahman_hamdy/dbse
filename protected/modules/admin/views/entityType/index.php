<?php

$this->breadcrumbs = array(
	EntityType::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . EntityType::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . EntityType::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode(EntityType::label(2)); ?></h2>

<br />
<div class="dataTables_wrapper form-inline">
<?php $this->widget('CGridViewCustom', array(
	'id' => 'entity-type-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'filterCssClass'=>'replace-inputs',
	'columns' => array(
		'id',
		'name_en',
		'name_ar',
		array(
					'name' => 'is_system',
					'value' => '($data->is_system == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		array(
			'name'=>'country_id',
			'value'=>'GxHtml::valueEx($data->country)',
			'filter'=>GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true)),
		),

		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'template'=>'{edit}{attributes}{remove}',
			'buttons'=>array(
				'edit'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-default btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-pencil"></i> Edit',
					'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
				),
				'remove'=>array(
					'options'=>array(
						'title'=>'Edit',
						'class'=>'btn btn-danger btn-sm btn-icon icon-left',
					),
					'visible'=>'!$data->is_system',
					'label'=>'<i class="entypo-cancel"></i> Delete',
					'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
				),
				'attributes'=>array(
					'options'=>array(
						'title'=>'Attributes',
						'class'=>'btn btn-green btn-sm btn-icon icon-left',
					),
					'label'=>'<i class="entypo-list"></i> Attributes',
					'url'=>'Yii::app()->controller->createUrl("entityTypeAttribute/index", array("entityId"=>$data->id))',
				),				
			)
		),
	),
)); ?>
</div>

<style type="text/css">
	.grid-view .button-column {
	    width: 290px;
	}
</style>


<script>
	jQuery(document).ready(function(){
		jQuery(document).on('click','#entity-type-grid a.btn-danger',function() {
			if(!confirm('Are you sure you want to delete this item?')) return false;
			var th = this,
				afterDelete = function(){};
			jQuery('#entity-type-grid').yiiGridView('update', {
				type: 'POST',
				url: jQuery(this).attr('href'),
				success: function(data) {
					jQuery('#entity-type-grid').yiiGridView('update');
					afterDelete(th, true, data);
				},
				error: function(XHR) {
					return afterDelete(th, false, XHR);
				}
			});
			return false;
		});
	});
</script>
